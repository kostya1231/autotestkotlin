import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.interactions.Actions
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

class A_TestElement : Setup() {

    @Test
    fun NoSettigsText() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Test_Element")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Test_Element\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Тест на проверку если в настройках информация не заполнена" + VersionBrowser(driver))





            AvtirizationOther(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Настройки")
            //
            DriverFindElement("//*[@id='profile-page.settings-block.name']", driver).clear()
            Log.WriteLine("Стереть имя в поле")
            //
            DriverFindElement("//*[@id='profile-page.settings-block.name']", driver).sendKeys("uzname")
            Log.WriteLine("Ввод имени")
            ////
            //DriverFindElement("//*[@id='profile-page.settings-block.alias']").SendKeys("azazazaza");
            //Log.WriteLine("Адрес страницы");
            ////
            DriverFindElement("(//*[@class='select-btn'])[1]", driver).click()
            Log.WriteLine("Пол")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])//li[1]", driver).click()
            Log.WriteLine("Мужской")
            //
            DriverFindElement("//*[@id='core.birth-date']", driver).sendKeys("29.04.1993")
            Log.WriteLine("Ввод даты рождения")
            //
            Thread.sleep(1000)
            DriverFindElement("//*[@id='core.birth-date']", driver).click()
            Log.WriteLine("Клик по полю")
            //
            DriverFindElement("//*[@id='profile-page.settings-block.phone']", driver).clear()
            //
            DriverFindElement("//*[@id='profile-page.settings-block.phone']", driver).sendKeys("89778665882")
            Log.WriteLine("Ввод телефона")
            //
            DriverFindElement("(//*[@class='select-btn'])[2]", driver).click()
            Log.WriteLine("Страна")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[2]//li[1]", driver).click()
            Log.WriteLine("Россия")
            //
            DriverFindElement("//*[@id='core.city']", driver).sendKeys("Москва")
            Log.WriteLine("Москва")
            Thread.sleep(500)
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[3]//li[1]", driver).click()
            //
            DriverFindElement("//*[@class='form-control pl-input-control_val pl-input-control_val-inner ng-untouched ng-pristine ng-valid ng-star-inserted']", driver).clear()
            Log.WriteLine("Очистить поле Раскажите о себе")
            //
            DriverFindElement("//*[@class='form-control pl-input-control_val pl-input-control_val-inner ng-untouched ng-pristine ng-valid ng-star-inserted']", driver).sendKeys("dasdasdasdasdasdas" +
                    "dasdasda" +
                    "sdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasda" +
                    "" +
                    "dasdasdasdasdasdasdasdasdasdasdasdas" +
                    "dasdasdas" +
                    "dasdasdasdasdasdasdasdasdasdasdasdas" +
                    "dasdasdasdasdasdasdasdasdasdasdasdas" +
                    "dasdasdasdasdasdassdasdasdasdasdasdasda" +
                    "sdas")
            Log.WriteLine("Ввод значения")
            //
            DriverFindElement("//*[@class='profile-settings_photo-link']", driver).click()
            Log.WriteLine("Изменить аватарку")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            DriverFindElement("//*[@class='button button__def button__xs']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary'])[2]", driver).click()
            Log.WriteLine("Сохранить изменения")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary']", driver).click()
            Log.WriteLine("Сохранить изменения")
            //
            DriverFindElement("(//*[@class='profile-settings_photo-link'])[2]", driver).click()
            Log.WriteLine("Удалить")
            //
            DriverFindElement("//*[@id='profile-page.settings-block.phone']", driver).click()
            Log.WriteLine("Стереть поле телефона")
            //
            for (i in 1..15) {
                val elActions = Actions(driver)
                elActions.keyDown(Keys.CONTROL).sendKeys(Keys.BACK_SPACE).keyUp(Keys.CONTROL).build().perform()
                Thread.sleep(50)
            }
            DriverFindElement("//*[@id='core.city']", driver).click()
            Log.WriteLine("Стереть поле город")
            //
            for (i in 1..15) {
                val elActions = Actions(driver)
                elActions.keyDown(Keys.CONTROL).sendKeys(Keys.BACK_SPACE).keyUp(Keys.CONTROL).build().perform()
                Thread.sleep(50)
            }
            //
            DriverFindElement("//*[@id='profile-page.settings-block.phone']", driver).click()
            //
            DriverFindElement("//*[@class='form-control pl-input-control_val pl-input-control_val-inner ng-valid ng-star-inserted ng-dirty ng-touched']", driver).click()
            Log.WriteLine("Стереть поле о себе")
            //
            for (i in 1..15) {
                val elActions = Actions(driver)
                elActions.keyDown(Keys.CONTROL).sendKeys(Keys.BACK_SPACE).keyUp(Keys.CONTROL).build().perform()
                Thread.sleep(50)
            }
            //
            DriverFindElement("//*[@class='btn btn-nd-primary']", driver).click()
            Log.WriteLine("Сохранить изменения")
            //
            Thread.sleep(1500)
            //
            driver.navigate().refresh()
            //
            fun findphone(): Boolean {
                return try {
                    driver.findElement(By.xpath("(//*[@ng-reflect-model='89778665882'])[2]"))
                    true
                } catch (e: Exception) {
                    false
                }
            }

            if (findphone()) {
                throw  Exception("Поле не пустое!!")
            }


            fun findcity(): Boolean {
                return try {
                    driver.findElement(By.xpath("//*[@class='h-user-menu_balance']//*[contains(text(), 'москва')]"))
                    true
                } catch (e: Exception) {
                    false
                }
            }

            if (findcity()) {
                throw  Exception("Поле не пустое!!")
            }


            fun findAboutMe(): Boolean {
                return try {
                    driver.findElement(By.xpath("(//*[@ng-reflect-model='dasdasdasdasdasdasdasdasdasdas'])[2]"))
                    true
                } catch (e: Exception) {
                    false
                }
            }

            if (findAboutMe()) {
                throw  Exception("Поле не пустое!!")
            }
            //

            Log.Close()
            Stop()
            driver.quit()
        } catch (e: Exception) {
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun B_DeleteValueContact() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Test_Element")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Test_Element\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()





            Log.WriteLine("Удаление значения в полях Настойки контакты" + VersionBrowser(driver))







            AvtirizationOther(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[6]", driver).click()
            Log.WriteLine("Настройки")
            //
            DriverFindElement("//*[@class='profile-nav']//li[2]", driver).click()
            Log.WriteLine("Контакты")
            //
            DriverFindElement("//*[@id='profile-page.settings-block.site']", driver).sendKeys("Qwerty")
            Log.WriteLine("Сайт")
            //
            DriverFindElement("(//*[@id='instagram'])", driver).sendKeys("Qwerty")
            Log.WriteLine("Instagram")
            //
            DriverFindElement("//*[@id='profile-page.settings-block.vk']", driver).sendKeys("Qwerty")
            Log.WriteLine("Vkontakte")
            //
            DriverFindElement("//*[@id='facebook']", driver).sendKeys("Qwerty")
            Log.WriteLine("Facebook")
            //
            DriverFindElement("//*[@id='twitter']", driver).sendKeys("Qwerty")
            Log.WriteLine("Twitter")
            //
            DriverFindElement("//*[@id='google+']", driver).sendKeys("Qwerty")
            Log.WriteLine("Google+")
            //
            DriverFindElement("//*[@id='Телеграм']", driver).sendKeys("Qwerty")
            Log.WriteLine("Телеграм")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary'])", driver).click()
            Log.WriteLine("Сохранить изменения")
            //
            DriverFindElement("//*[@id='profile-page.settings-block.site']", driver).click()
            //
            for (i in 1..15) {
                val elActions = Actions(driver)
                elActions.keyDown(Keys.CONTROL).sendKeys(Keys.BACK_SPACE).keyUp(Keys.CONTROL).build().perform()
                Thread.sleep(50)
            }
            //
            DriverFindElement("(//*[@id='instagram'])", driver).click()
            //
            for (i in 1..15) {
                val elActions = Actions(driver)
                elActions.keyDown(Keys.CONTROL).sendKeys(Keys.BACK_SPACE).keyUp(Keys.CONTROL).build().perform()
                Thread.sleep(50)
            }
            //
            DriverFindElement("//*[@id='profile-page.settings-block.vk']", driver).click()
            //
            for (i in 1..15) {
                val elActions = Actions(driver)
                elActions.keyDown(Keys.CONTROL).sendKeys(Keys.BACK_SPACE).keyUp(Keys.CONTROL).build().perform()
                Thread.sleep(50)
            }
            //
            DriverFindElement("//*[@id='facebook']", driver).click()
            //
            for (i in 1..15) {
                val elActions = Actions(driver)
                elActions.keyDown(Keys.CONTROL).sendKeys(Keys.BACK_SPACE).keyUp(Keys.CONTROL).build().perform()
                Thread.sleep(50)
            }
            //
            DriverFindElement("//*[@id='twitter']", driver).click()
            //
            for (i in 1..15) {
                val elActions = Actions(driver)
                elActions.keyDown(Keys.CONTROL).sendKeys(Keys.BACK_SPACE).keyUp(Keys.CONTROL).build().perform()
                Thread.sleep(50)
            }
            DriverFindElement("//*[@id='google+']", driver).click()
            //
            for (i in 1..15) {
                val elActions = Actions(driver)
                elActions.keyDown(Keys.CONTROL).sendKeys(Keys.BACK_SPACE).keyUp(Keys.CONTROL).build().perform()
                Thread.sleep(50)
            }
            //
            DriverFindElement("//*[@id='Телеграм']", driver).click()
            //
            for (i in 1..15) {
                val elActions = Actions(driver)
                elActions.keyDown(Keys.CONTROL).sendKeys(Keys.BACK_SPACE).keyUp(Keys.CONTROL).build().perform()
                Thread.sleep(50)
            }
            //

            DriverFindElement("(//*[@class='btn btn-nd-primary'])", driver).click()
            Log.WriteLine("Сохранить изменения")
            //
            driver.navigate().refresh()

            fun findSite(): Boolean {
                return try {
                    driver.findElement(By.xpath("(//*[@ng-reflect-model='Qwerty'])[2]"))
                    true
                } catch (e: Exception) {
                    false
                }
            }

            if (findSite()) {
                throw  Exception("Поле не пустое")
            }

            fun findInstagram(): Boolean {
                return try {
                    driver.findElement(By.xpath("(//*[@ng-reflect-model='Qwerty'])[4]"))
                    true
                } catch (e: Exception) {
                    false
                }
            }

            if (findInstagram()) {
                throw  Exception("Поле не пустое")
            }

            fun findVK(): Boolean {
                return try {

                    driver.findElement(By.xpath("(//*[@ng-reflect-model='Qwerty'])[6]"))
                    true
                } catch (e: Exception) {
                    false
                }
            }

            if (findVK()) {
                throw  Exception("Поле не пустое")
            }

            fun findFB(): Boolean {
                return try {

                    driver.findElement(By.xpath("(//*[@ng-reflect-model='Qwerty'])[8]"))
                    true
                } catch (e: Exception) {
                    false
                }
            }

            if (findFB()) {
                throw  Exception("Поле не пустое")
            }

            fun findTW(): Boolean {
                return try {

                    driver.findElement(By.xpath("(//*[@ng-reflect-model='Qwerty'])[10]"))
                    true
                } catch (e: Exception) {
                    false
                }
            }

            if (findTW()) {
                throw  Exception("Поле не пустое")
            }

            fun findGoogle(): Boolean {
                return try {

                    driver.findElement(By.xpath("(//*[@ng-reflect-model='Qwerty'])[12]"))
                    true
                } catch (e: Exception) {
                    false
                }
            }

            if (findGoogle()) {
                throw  Exception("Поле не пустое")
            }

            Log.Close()
            Stop()
            driver.quit()
        } catch (e: Exception) {
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }


    @Test
    fun C_ProjectMainDetalis() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Test_Element")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Test_Element\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            var world = 4
            val r = Random()
            val number = StringBuilder()
            for (i in 0 until world) {
                val tmp = r.nextInt(3)
                number.append(tmp)
            }


            Log.WriteLine("Заполнение проекта Основные детали-удаление данных с полей проверка на отсутствие данных в полях" + VersionBrowser(driver))




            Avtorization_Uz4name(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[1]", driver).click()
            Log.WriteLine("Созданные проекты")
            //
            fun find(): Boolean {
                try {
                    DriverFindElement("//*[@src='/images/icons/campaign-status-draft.svg']", driver)
                    return true
                } catch (e: Exception) {
                    return false
                }

            }
            if (find()) {
                try {
                    DriverFindElement("//*[@src='/images/icons/campaign-status-draft.svg']", driver).click()

                } catch (e: Exception) {

                }
            }

            while (true) {
                try {
                    Thread.sleep(500)
                    driver.findElement(By.xpath("//*[@class='profile-project-info_delete-link']")).click()
                    Log.WriteLine("Удалить черновик")
                    Thread.sleep(500)
                    driver.findElement(By.xpath("//*[@class='btn btn-primary']")).click()
                    Log.WriteLine("Да")
                    Thread.sleep(200)
                } catch (e: Exception) {
                    break
                }
            }
            //
            try {
                driver.findElement(By.xpath("//*[contains(text(), 'создать')]")).click()
                Log.WriteLine("Создать")
            } catch (e: Exception) {
                driver.findElement(By.xpath("//*[@class='header_create-link']")).click()
                Log.WriteLine("Создать Проект")
            }

            //
            DriverFindElement("//*[@class='checkbox']", driver).click()
            Log.WriteLine("Я Согласен на Условия")
            //
            DriverFindElement("//*[@class='btn btn-primary frc-i-accept-btn create-campaign']", driver).click()
            Log.WriteLine("Создать проект")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[1]", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='form-control control-xlg borderless'])", driver).sendKeys("web" + number.toString())
            Log.WriteLine("Красивый адрес проекта")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            try {
                DriverFindElement("//*[@class='btn btn-primary']", driver).click()
                Log.WriteLine("Сохранить")
            } catch (f: Exception) {

            }
            //
            Thread.sleep(2800)
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")
            //
            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Фото по ссылке")
            //
            DriverFindElement("//*[@name='img_url']", driver).sendKeys("https://cs8.pikabu.ru/post_img/big/2016/12/02/8/1480681679121964873.jpg")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Сохранить")

            Thread.sleep(1500)

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[2]", driver).sendKeys("cxcdsfdsfqq")
            Log.WriteLine("Коротко о проекте")
            //
            DriverFindElement("//*[@class='icon-select']", driver).click()
            Log.WriteLine("Страна реализции")
            //
            DriverFindElement("//*[@class='dropdown-menu']/li[2]", driver).click()
            Log.WriteLine("Выбор страны")
            //
            DriverFindElement("//*[@class='form-control control-xlg prj-crt-input ac_input']", driver).sendKeys("Москва")
            Log.WriteLine("Регион реализации")
            //
            DriverFindElement("//*[@class='ac_results']//li[1]", driver).click()
            Log.WriteLine("Москва")
            //
            DriverFindElement("//*[@name='cityNameRus']", driver).sendKeys("Алабино")
            Log.WriteLine("Город реализации")
            //
            DriverFindElement("//*[@class='ac_results']//*[@class='ac_even ac_over']", driver).click()
            //
            DriverFindElement("//*[@data-parse='number']", driver).clear()
            //
            DriverFindElement("//*[@data-parse='number']", driver).sendKeys("500000")
            Log.WriteLine("Финансовая цель")
            //
            DriverFindElement("//*[@class='project-create_col-val_col']", driver).click()
            Log.WriteLine("Срок окончания проекта")
            //
            DriverFindElement("(//*[@data-parse='number'])[2]", driver).click()
            //
            DriverFindElement("//*[@class='ui-datepicker-next ui-corner-all']", driver).click()
            Log.WriteLine("Следующий месяц")
            //
            DriverFindElement("//*[@class='ui-datepicker-calendar']//tr[4]//td[5]", driver).click()
            Log.WriteLine("Выбор даты")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            Thread.sleep(500)
            //
            //Удаление значений в полях

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[1]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control control-xlg borderless'])", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[2]", driver).clear()
            //
            DriverFindElement("//*[@class='form-control control-xlg prj-crt-input ac_input']", driver).clear()
            //
            //  DriverFindElement("//*[@name='cityNameRus']", driver).Clear();
            //
            DriverFindElement("//*[@data-parse='number']", driver).clear()
            //
            DriverFindElement("(//*[@data-parse='number'])[2]", driver).clear()
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            driver.navigate().refresh()
            //

            Thread.sleep(1500)
            //
            fun findelement(): Boolean {
                return try {
                    driver.findElement(By.xpath("(//*[contains(text(), 'cxcdsfdsfqq')])[1]"))
                    true
                } catch (e: Exception) {
                    false
                }
            }

            if (findelement()) {
                throw  Exception("Поле не пустое")
            }
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e: Exception) {
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun D_ProjectDetalis() {
        try {
            Thread.sleep(80000)
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Test_Element")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Test_Element\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Заполнение проекта  Детали-удаление данных + Сохранение" + VersionBrowser(driver))




            Avtorization_Uz4name(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[1]", driver).click()
            Log.WriteLine("Созданные проекты")
            //
            fun find(): Boolean {
                try {
                    DriverFindElement("//*[@src='/images/icons/campaign-status-draft.svg']", driver)
                    return true
                } catch (e: Exception) {
                    return false
                }

            }
            if (find()) {
                try {
                    DriverFindElement("//*[@src='/images/icons/campaign-status-draft.svg']", driver).click()

                } catch (e: Exception) {

                }
            }
            //
            DriverFindElement("//*[@class='profile-project-change_link']", driver).click()
            Log.WriteLine("Редактировать")
            //
            DriverFindElement("(//*[@class='project-create_tab_link'])[2]", driver).click()
            Log.WriteLine("Детали")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Сохранить")
            //
            Thread.sleep(2800)
            //
            DriverFindElement("(//*[@class='mceIcon mce_planetaphoto'])", driver).click()
            Log.WriteLine("Загрузить картинку в поле text-area")
            //
            DriverFindElement("//*[@class='link']", driver).click()
            Log.WriteLine("Загрузить картинку по ссылке")
            //
            DriverFindElement("//*[@name='img_url']", driver).sendKeys("https://coubsecure-s.akamaihd.net/get/b157/p/coub/simple/cw_timeline_pic/b2487f682ab/2000112a221659befa67c/big_1519931474_image.jpg")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            Thread.sleep(500)
            ////
            driver.navigate().refresh()
            //
            Thread.sleep(2500)
            //
            driver.switchTo().frame(0)
            //
            DriverFindElement("//*[@id='tinymce']", driver).click()
            Log.WriteLine("Выделить фото")
            //
            DriverFindElement("//*[@class='icon-close']", driver).click()
            Log.WriteLine("Удалить фото")
            //
            driver.switchTo().defaultContent()
            //
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            Thread.sleep(500)
            //
            driver.navigate().refresh()
            ////
            fun findelement(): Boolean {
                try {
                    driver.findElement(By.xpath("//*[@class='project-img-view']"))
                    Log.WriteLine("Загрузить Фото")
                    return true
                } catch (e: Exception) {
                    return false
                }
            }

            if (findelement()) {
                throw  Exception("Поле не пустое")
            }

            Log.Close()
            Stop()
            driver.quit()
        } catch (e: Exception) {
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun E_SharingPaymen() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Test_Element")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Test_Element\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Поддержанные проекты Оплата сбер-Шаринг на странице оплаты" + VersionBrowser(driver))






            Avtorization_Uz4name(driver, Log)
            //
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[2]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary action-card_btn js-navigate-to-purchase'])[1]", driver).click()
            Log.WriteLine("Оплатить")
            //
            fun FindEl(): Boolean {
                return try {
                    DriverFindElement("(//*[@class='form-ui-label'])[1]", driver)
                    //Согласен на условия
                    true
                } catch (e: Exception) {
                    false
                }
            }

            for (i in 0..60) {  //Клауд
                if (FindEl()) {

                    DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                    Log.WriteLine("Даю свое согласие")

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver).click()
                    Log.WriteLine("Карты других банков")
                    //
                    DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
                    Log.WriteLine("Оплатить покупку")
                    //
                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
                    Log.WriteLine("Оплата картой")
                    //
                    driver.switchTo().frame(0)
                    DriverFindElement("(//*[@class='row'])[1]//input", driver).click()
                    Log.WriteLine("клик по строке")
                    DriverFindElement("(//*[@class='row'])[1]//input", driver).sendKeys("4111 1111 1111 1111")
                    //
                    DriverFindElement("(//*[@id='inputMonth'])[1]", driver).sendKeys("12")
                    //
                    DriverFindElement("(//*[@id='inputYear'])[1]", driver).sendKeys("19")
                    //
                    DriverFindElement("(//*[@data-mask='000'])[1]", driver).sendKeys("123")
                    //
                    DriverFindElement("(//*[@id='cardHolder'])[1]", driver).sendKeys("QWERTY")
                    //
                    DriverFindElement("(//*[@class='button'])[1]", driver).click()
                    DriverFindElement("(//*[@class='button'])[2]", driver).click()
                    Log.WriteLine("OK")
                    driver.switchTo().defaultContent()
                    break
                }
            }
//            DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
//            Log.WriteLine("Даю свое согласие")
//            //
//            DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
//            Log.WriteLine("Оплатить покупку")
//            //
//            //DriverFindElement("(//*[@id='email'])[1]", driver).SendKeys("pain-net@yandex.ru");
//            //Log.WriteLine("Сбер ввод email");
//            ////
//            DriverFindElement("(//*[@id='cardnumber'])[1]", driver).sendKeys("4111 1111 1111 1111")
//            Log.WriteLine("Ваш банк ввод номера карты")
//            //
//            DriverFindElement("(//*[@name='expdate'])[1]", driver).sendKeys("1219")
//            Log.WriteLine("Ваш банк ввод месяц год")
//            //
//            DriverFindElement("(//*[@id='cvc'])[1]", driver).sendKeys("123")
//            Log.WriteLine("Ваш банк ввод CVC2")
//            //
//            DriverFindElement("(//*[@class='sbersafe-pay-button'])", driver).click()
//            Log.WriteLine("Оплатить")
//            //
//            DriverFindElement("(//*[@name='password'])", driver).sendKeys("12345678")
//            Log.WriteLine("password")
//            //
//            DriverFindElement("(//*[@value='Submit'])", driver).click()
//            Log.WriteLine("Submit")
//            //
            for (i in 1..8) {
                DriverFindElement("(//*[@class='sps-button']//*[@class='spsb-item'])[" + i.toString() + "]", driver).click()
            }
            Log.WriteLine("шарик")
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e: Exception) {
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun F_PaginatorVoznagrajdenia() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Test_Element")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Test_Element\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()





            Log.WriteLine("Вознаграждения пагинатор переключение" + VersionBrowser(driver))






            Avtorization_Uz4name(driver, Log)
            //
            DriverFindElement("(//*[@class='h-menu_link'])[2]", driver).click()
            Log.WriteLine("Вознаграждения")
            //
            for (i in 1..9) {
                Thread.sleep(50)
                DriverFindElement("(//*[@height='16'])[3]", driver).click()
            }
            Log.WriteLine("Переключение страниц")
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e: Exception) {
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun G_BannerStatisticsButton() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Test_Element")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Test_Element\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Новая админка раздел Релкама Баннеры + кнопка применить" + VersionBrowser(driver))






            Avtorization_Adminka(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[2]", driver).click()
            Log.WriteLine("Статистика баннеров")
            //
            Thread.sleep(8000)
            DriverFindElement("//*[contains(text(), ' Фильтры поиска ')]", driver).click()
            Log.WriteLine("Фильтр статистики")
            //
            try {
                DriverFindElement("//*[@class='mat-raised-button mat-primary']", driver).click()
            } catch (e: Exception) {
                println("Кнопка в режиме false")
            }
            //
            var text = DriverFindElement("//tbody//tr[1]//td[1]", driver).text
            //
            DriverFindElement("(//*[@class='mat-input-element mat-form-field-autofill-control cdk-text-field-autofill-monitored ng-untouched ng-pristine ng-valid'])[1]", driver).sendKeys(text.toString())
            Log.WriteLine("Ввести в строку поиска id")
            //
            DriverFindElement("//*[@class='mat-raised-button mat-primary']", driver).click()
            Log.WriteLine("Применить")
            //
            DriverFindElement("//tbody//tr[1]//td[1]", driver)
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e: Exception) {
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun ButtonUp() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Test_Element")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Test_Element\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Поиск проектов кнопка наверх" + VersionBrowser(driver))




            Avtorization_Uz4name(driver, Log)
            //
            DriverFindElement("(//*[@class='h-menu_link'])[1]", driver).click()
            Log.WriteLine("Проекты")
            //
            DriverFindElement("//*[@class='pl-pagination_list']//li[2]", driver)
            //
            DriverFindElement("//*[@class='flag-icon-gb']", driver).click()
            Log.WriteLine("Иконка локализации языка")
            //
            DriverFindElement("//*[@class='to-top_text']", driver).click()
            Log.WriteLine("Кнопка наверх")
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e: Exception) {
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun FaqFindTxt() {

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Faq")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Faq\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Faq поиск" + VersionBrowser(driver))



            DriverNavigate("$URL/faq", driver)
            //
            DriverFindElement("//*[@class='faq-search_input']", driver).sendKeys("спецпроекты")
            Log.WriteLine("Поиск")
            //
            try {
                DriverFindElement("//*[@class='faq-search-result_name']", driver)
                Log.WriteLine("результат")

            } catch (error: Exception) {
                println("Ошибка элемент не найден")
            }
            //
            Thread.sleep(1000)
            Log.Close()
            Stop()
            driver.quit()

        } catch (e: Exception) {
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }
}







