
import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import java.nio.file.Files
import java.nio.file.Paths



class AdminkaShop1 : SetupParallel() {

    @Test
    fun ShopPurchases(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Админка Магазин-Заказы магазина-Статус доставки" + VersionBrowser(driver))




            Avtorization_Adminka(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[3]", driver).click()
            Log.WriteLine("Магазин")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[1]", driver).click()
            Log.WriteLine("Заказы магазина")
            //

            for (i in 1..7) {
                DriverFindElement("(//*[@class='order-action-block order-status form-control'])[2]//option[" + i.toString() + "]", driver).click()

                DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            }
            Log.WriteLine("Перебор статусов")
            //
            DriverFindElement("(//*[@class='order-action-block order-status form-control'])[2]//option[1]", driver).click()
            //
            for (i in 1..3) {
                DriverFindElement("(//*[@class='order-action-block order-status form-control'])[1]//option[" + i.toString() + "]", driver).click()
            }
            Log.WriteLine("Статусы оплаты")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }


    @Test
    fun ToAnnul(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка Модерация-Аннулирование заказа" + VersionBrowser(driver))





            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(500)
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[1]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            DriverFindElement("//*[contains(text(), '                                                                                                               Доставка почтой                                                                          ,                                                                                                                Самовывоз                                                                                                                                        ')]", driver).click()
            //
            DriverFindElement("//*[contains(text(), 'Купить (1 шт.)')]", driver).click()
            Log.WriteLine("Купить 1 шт")
            //
            DriverFindElement("//*[@id='payment-page.question-to-buyer-label']", driver).sendKeys("123")
            Log.WriteLine("Данные")
            //
            //DriverFindElement("//*[@id='core.locality']", driver).SendKeys("Луна");
            //Log.WriteLine("Населённый пункт");
            ////
            //DriverFindElement("(//*[@id='core.zip-code'])[1]", driver).SendKeys("11111111");
            //Log.WriteLine("Индекс");
            ////
            //DriverFindElement("(//*[@id='core.address'])[1]", driver).SendKeys("Большой кратер");
            //Log.WriteLine("Адрес");
            ////
            //DriverFindElement("(//*[@id='core.recipient-name'])[1]", driver).SendKeys("Николас кейдж");
            //Log.WriteLine("ФИО получателя");
            ////
            //DriverFindElement("(//*[@id='core.contact-phone'])[1]", driver).SendKeys("84952222222");
            //Log.WriteLine("контактный телефон");
            ////
            fun FindEl(): Boolean {
                return try {
                    DriverFindElement("(//*[@class='form-ui-label'])[1]", driver)
                    //Согласен на условия
                    true
                } catch (e: Exception) {
                    false
                }
            }

            for (i in 0..60) {  //Клауд
                if (FindEl()) {

                    try{
                        DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
                        Log.WriteLine("Даю свое согласие")
                    }
                    catch (e:Exception){
                        DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                        Log.WriteLine("Даю свое согласие")
                    }

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver).click()
                    Log.WriteLine("Карты других банков")
                    //
                    DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
                    Log.WriteLine("Оплатить покупку")
                    //
                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
                    Log.WriteLine("Оплата картой")
                    //
                    driver.switchTo().frame(0)
                    DriverFindElement("(//*[@class='row'])[1]//input", driver).click()
                    Log.WriteLine("клик по строке")
                    DriverFindElement("(//*[@class='row'])[1]//input", driver).sendKeys("4111 1111 1111 1111")
                    //
                    DriverFindElement("(//*[@id='inputMonth'])[1]", driver).sendKeys("12")
                    //
                    DriverFindElement("(//*[@id='inputYear'])[1]", driver).sendKeys("19")
                    //
                    DriverFindElement("(//*[@data-mask='000'])[1]", driver).sendKeys("123")
                    //
                    DriverFindElement("(//*[@id='cardHolder'])[1]", driver).sendKeys("QWERTY")
                    //
                    DriverFindElement("(//*[@class='button'])[1]", driver).click()
                    DriverFindElement("(//*[@class='button'])[2]", driver).click()
                    Log.WriteLine("OK")
                    driver.switchTo().defaultContent()
                    break
                }
            }

            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[3]", driver).click()
            Log.WriteLine("Мои заказы")
            //
            val text = DriverFindElement("(//*[@class='private-office-purchases_num'])[1]", driver) // Вытащить текст из элемента
            var Text1 = text.text
            Text1 = Text1.replace("\\s".toRegex(), "")//Удаление пробелов
            var txt =  Text1.trim('№','№')
            //
            println(txt)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")
            //



            //DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).Click();
            //Log.WriteLine("Вход");
            ////
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            Thread.sleep(1500)
            //
            driver.navigate().to(Order + txt)
            //
            DriverFindElement("(//*[@class='order-action-block order-status form-control'])[2]//option[3]", driver).click()
            Log.WriteLine("Статус аннулирован")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-block']", driver).click()
            Log.WriteLine("Save")
            //
            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[8]", driver).click()
            Log.WriteLine("Выход")
            //
            Thread.sleep(500)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz4name@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            Thread.sleep(500)
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[3]", driver).click()
            Log.WriteLine("Мои заказы")
            //
            DriverFindElement("//*[contains(text(), 'Аннулирован')]", driver)
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun ShopSendStatus(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Админка Модерация- Статус заказа отправленный" + VersionBrowser(driver))




            Avtorization_Uz4name(driver, Log)
            //
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[1]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            DriverFindElement("//*[contains(text(), '                                                                                                               Доставка почтой                                                                          ,                                                                                                                Самовывоз                                                                                                                                        ')]", driver).click()
            //
            DriverFindElement("//*[contains(text(), 'Купить (1 шт.)')]", driver).click()
            Log.WriteLine("Купить 1 шт")
            //
            DriverFindElement("//*[@id='payment-page.question-to-buyer-label']", driver).sendKeys("123")
            Log.WriteLine("Данные")
            DriverFindElement("//*[@id='core.locality']", driver).sendKeys("Луна")
            Log.WriteLine("Населённый пункт")
            //
            DriverFindElement("(//*[@id='core.zip-code'])[1]", driver).sendKeys("11111111")
            Log.WriteLine("Индекс")
            //
            DriverFindElement("(//*[@id='core.address'])[1]", driver).sendKeys("Большой кратер")
            Log.WriteLine("Адрес")
            //
            DriverFindElement("(//*[@id='core.recipient-name'])[1]", driver).sendKeys("Николас кейдж")
            Log.WriteLine("ФИО получателя")
            //
            DriverFindElement("(//*[@id='core.contact-phone'])[1]", driver).sendKeys("84952222222")
            Log.WriteLine("контактный телефон")
            //
            fun FindEl(): Boolean {
                return try {
                    DriverFindElement("(//*[@class='form-ui-label'])[1]", driver)
                    //Согласен на условия
                    true
                } catch (e: Exception) {
                    false
                }
            }

            for (i in 0..60) {  //Клауд
                if (FindEl()) {

                    try{
                        DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
                        Log.WriteLine("Даю свое согласие")
                    }
                    catch (e:Exception){
                        DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                        Log.WriteLine("Даю свое согласие")
                    }

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver).click()
                    Log.WriteLine("Карты других банков")
                    //
                    DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
                    Log.WriteLine("Оплатить покупку")
                    //
                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
                    Log.WriteLine("Оплата картой")
                    //
                    driver.switchTo().frame(0)
                    DriverFindElement("(//*[@class='row'])[1]//input", driver).click()
                    Log.WriteLine("клик по строке")
                    DriverFindElement("(//*[@class='row'])[1]//input", driver).sendKeys("4111 1111 1111 1111")
                    //
                    DriverFindElement("(//*[@id='inputMonth'])[1]", driver).sendKeys("12")
                    //
                    DriverFindElement("(//*[@id='inputYear'])[1]", driver).sendKeys("19")
                    //
                    DriverFindElement("(//*[@data-mask='000'])[1]", driver).sendKeys("123")
                    //
                    DriverFindElement("(//*[@id='cardHolder'])[1]", driver).sendKeys("QWERTY")
                    //
                    DriverFindElement("(//*[@class='button'])[1]", driver).click()
                    DriverFindElement("(//*[@class='button'])[2]", driver).click()
                    Log.WriteLine("OK")
                    driver.switchTo().defaultContent()
                    break
                }
            }

            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")

            DriverFindElement("(//*[@class='h-user-menu_ico'])[3]", driver).click()
            Log.WriteLine("Мои заказы")

            val text = DriverFindElement("(//*[@class='private-office-purchases_num'])[1]", driver) // Вытащить текст из элемента
            var Text1 = text.text
            Text1 = Text1.replace("\\s".toRegex(), "")//Удаление пробелов
            var txt =  Text1.trim('№','№')

            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")

            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")

            //DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).Click();
            //Log.WriteLine("Вход");

            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")

            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина")

            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")

            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")

            Thread.sleep(1500)

            driver.navigate().to(Order + txt)
            //
            DriverFindElement("(//*[@class='order-action-block order-status form-control'])[2]//option[2]", driver).click()
            Log.WriteLine("Статус отправлен")

            DriverFindElement("//*[@class='btn btn-primary btn-block']", driver).click()
            Log.WriteLine("Save")

            DriverNavigate(URL, driver)

            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")

            DriverFindElement("(//*[@class='h-user-menu_ico'])[8]", driver).click()
            Log.WriteLine("Выход")

            Thread.sleep(600)

            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")

            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")

            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz4name@yandex.ru")
            Log.WriteLine("Ввод логина")

            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")

            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")

            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")

            DriverFindElement("(//*[@class='h-user-menu_ico'])[3]", driver).click()
            Log.WriteLine("Мои заказы")

            DriverFindElement("//*[contains(text(),'Отправлен')]", driver)
            Log.WriteLine("Отправлен")
            //
            DriverNavigate("https://yandex.ru", driver)
            Log.WriteLine("Вход на яндекс")
            //
            DriverFindElement("(//*[@class='home-link home-link_black_yes'])[1]", driver).click()
            Log.WriteLine("Вход на яндекс")
            //

            fun FindWindowYandex() : Boolean
            {
                return try {
                    Thread.sleep(1500)
                    driver.findElement(By.xpath("//*[@class='passp-page-overlay']"))
                    true
                } catch(e:Exception) {
                    false
                }
            }

            if (!FindWindowYandex())
            {
                DriverFindElement("//*[@name='login']", driver).sendKeys("uz4name")
                Log.WriteLine("Ввод логина")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passport-Button-Text']", driver).click()
                Log.WriteLine("Войти")
                //
            }

            if (FindWindowYandex())
            {
                DriverFindElement("//*[@name='login']", driver).sendKeys("uz4name")
                Log.WriteLine("Ввод Логина")
                //
                DriverFindElement("//*[@class='passp-login-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passp-password-form']//button[1]", driver).click()
                Log.WriteLine("Войти")

            }

            DriverFindElement("(//*[@class='mail-MessageSnippet-FromText'])[1]", driver).click()
            Log.WriteLine("Выбор письма")
            //
            Thread.sleep(4000)
            Log.WriteLine("Фото письма")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }


    @Test
    fun ShopIssuedBy(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Админка Модерация- Статус заказа Выдан" + VersionBrowser(driver))





            Avtorization_Uz2name(driver, Log)
            //
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[1]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            DriverFindElement("//*[contains(text(), '                                                                                                               Доставка почтой                                                                          ,                                                                                                                Самовывоз                                                                                                                                        ')]", driver).click()
            //
            DriverFindElement("//*[contains(text(), 'Купить (1 шт.)')]", driver).click()
            Log.WriteLine("Купить 1 шт")
            //
            DriverFindElement("//*[@id='payment-page.question-to-buyer-label']", driver).sendKeys("123")
            Log.WriteLine("Данные")
            //
            DriverFindElement("//*[@id='core.locality']", driver).sendKeys("Луна")
            Log.WriteLine("Населённый пункт")
            //
            DriverFindElement("(//*[@id='core.zip-code'])[1]", driver).sendKeys("11111111")
            Log.WriteLine("Индекс")
            //
            DriverFindElement("(//*[@id='core.address'])[1]", driver).sendKeys("Большой кратер")
            Log.WriteLine("Адрес")
            //
            DriverFindElement("(//*[@id='core.recipient-name'])[1]", driver).sendKeys("Николас кейдж")
            Log.WriteLine("ФИО получателя")
            //
            DriverFindElement("(//*[@id='core.contact-phone'])[1]", driver).sendKeys("84952222222")
            Log.WriteLine("контактный телефон")
            //
            fun FindEl(): Boolean {
                return try {
                    DriverFindElement("(//*[@class='form-ui-label'])[1]", driver)
                    //Согласен на условия
                    true
                } catch (e: Exception) {
                    false
                }
            }

            for (i in 0..60) {  //Клауд
                if (FindEl()) {

                    try{
                        DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
                        Log.WriteLine("Даю свое согласие")
                    }
                    catch (e:Exception){
                        DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                        Log.WriteLine("Даю свое согласие")
                    }

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver).click()
                    Log.WriteLine("Карты других банков")
                    //
                    DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
                    Log.WriteLine("Оплатить покупку")
                    //
                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
                    Log.WriteLine("Оплата картой")
                    //
                    driver.switchTo().frame(0)
                    DriverFindElement("(//*[@class='row'])[1]//input", driver).click()
                    Log.WriteLine("клик по строке")
                    DriverFindElement("(//*[@class='row'])[1]//input", driver).sendKeys("4111 1111 1111 1111")
                    //
                    DriverFindElement("(//*[@id='inputMonth'])[1]", driver).sendKeys("12")
                    //
                    DriverFindElement("(//*[@id='inputYear'])[1]", driver).sendKeys("19")
                    //
                    DriverFindElement("(//*[@data-mask='000'])[1]", driver).sendKeys("123")
                    //
                    DriverFindElement("(//*[@id='cardHolder'])[1]", driver).sendKeys("QWERTY")
                    //
                    DriverFindElement("(//*[@class='button'])[1]", driver).click()
                    DriverFindElement("(//*[@class='button'])[2]", driver).click()
                    Log.WriteLine("OK")
                    driver.switchTo().defaultContent()
                    break
                }
            }

            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")

            DriverFindElement("(//*[@class='h-user-menu_ico'])[3]", driver).click()
            Log.WriteLine("Мои заказы")

            val text = DriverFindElement("(//*[@class='private-office-purchases_num'])[1]", driver) // Вытащить текст из элемента
            var Text1 = text.text
            Text1 = Text1.replace("\\s".toRegex(), "")//Удаление пробелов
            var txt =  Text1.trim('№','№')

            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")

            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")

            //DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).Click();
            //Log.WriteLine("Вход");

            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")

            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина")

            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")

            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")

            Thread.sleep(2000)

            driver.navigate().to(Order + txt)

            DriverFindElement("(//*[@class='order-action-block order-status form-control'])[2]//option[4]", driver).click()
            Log.WriteLine("Статус выдан")

            DriverFindElement("//*[@class='btn btn-primary btn-block']", driver).click()
            Log.WriteLine("Save")

            DriverNavigate(URL, driver)

            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")

            DriverFindElement("(//*[@class='h-user-menu_ico'])[8]", driver).click()
            Log.WriteLine("Выход")

            Thread.sleep(500)

            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")

            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")

            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz2name@yandex.ru")
            Log.WriteLine("Ввод логина")

            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")

            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")

            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")

            DriverFindElement("(//*[@class='h-user-menu_ico'])[3]", driver).click()
            Log.WriteLine("Мои заказы")

            DriverFindElement("//*[contains(text(), 'Выдан')]", driver)
            Log.WriteLine("Выдан")

            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }
}