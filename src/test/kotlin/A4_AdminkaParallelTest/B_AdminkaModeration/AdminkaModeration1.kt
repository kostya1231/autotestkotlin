
import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.WebDriver
import java.nio.file.Files
import java.nio.file.Paths

class AdminkaModeration1 : SetupParallel() {

    @Test
    fun ModerationСhangePassword(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка Модерация-пользователи-Изменение пароля" + VersionBrowser(driver))





            Avtorization_Adminka(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Пользователи")
            //
            DriverFindElement("//*[@id='xlInput']", driver).sendKeys("uz4name@yandex.ru")
            Log.WriteLine("Ввод е-майла")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Поск юзера")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-outline']", driver).click()
            Log.WriteLine("Редактировать")
            //
            DriverFindElement("//*[@name='password']", driver).sendKeys("nekroman911")
            Log.WriteLine("Изменение пароля")
            //
            DriverFindElement("//*[@id='submitNewPassword']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverNavigate(URL, driver)

            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[8]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz4name@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            Thread.sleep(1500)
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun ModerationСhangeAdressPage(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка Модерация-пользователи-Изменение адреса страницы" + VersionBrowser(driver))






            Avtorization_Adminka(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Пользователи")
            //
            DriverFindElement("//*[@id='xlInput']", driver).sendKeys("uz2name@yandex.ru")
            Log.WriteLine("Ввод е-майла")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Поск юзера")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-outline']", driver).click()
            Log.WriteLine("Редактировать")
            //
            DriverFindElement("//*[@name='aliasName']", driver).sendKeys("Qwerty_Qwerty")
            Log.WriteLine("Ввод адреса страницы")
            //
            DriverFindElement("//*[@name='changeAliasButton']", driver).click()
            Log.WriteLine("Cохранить")
            //
            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[8]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz2name@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            Thread.sleep(500)
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[6]", driver).click()
            Log.WriteLine("Настройки")
            //
            Thread.sleep(1500)
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }


    @Test
    fun ModerationChangeEMail(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка Модерация-пользователи-Изменение почтового адреса" + VersionBrowser(driver))






            Avtorization_Adminka(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Пользователи")
            //
            DriverFindElement("//*[@id='xlInput']", driver).sendKeys("uz4name@yandex.ru")
            Log.WriteLine("Ввод е-майла")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Поск юзера")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-outline']", driver).click()
            Log.WriteLine("Редактировать")
            //
            DriverFindElement("//*[@name='email']", driver).sendKeys("uz4name13@yandex.ru")
            Log.WriteLine("Ввод нового адреса")
            //
            DriverFindElement("//*[@name='submitNewEmail']", driver).click()
            Log.WriteLine("Сохранить")
            //
            val mail = "uz4name13@yandex.ru"
            //
            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[8]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys(mail)
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).clear()
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            //    DriverFindElement("(//*[@name='password'])", driver).SendKeys("nekroman911");
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            Thread.sleep(400)
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Пользователи")
            //
            DriverFindElement("//*[@id='xlInput']", driver).sendKeys("uz4name@yandex.ru")
            Log.WriteLine("Ввод е-майла")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Поск юзера")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-outline']", driver).click()
            Log.WriteLine("Редактировать")
            //
            DriverFindElement("//*[@name='email']", driver).sendKeys("uz4name@yandex.ru")
            Log.WriteLine("Ввод нового адреса")
            //
            DriverFindElement("//*[@name='submitNewEmail']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[8]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz4name@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")

            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun ModerationChangeStatus(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка Модерация-пользователи-Изменение Статуса" + VersionBrowser(driver))





            Avtorization_Adminka(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Пользователи")
            //
            DriverFindElement("//*[@id='xlInput']", driver).sendKeys("uz4name@yandex.ru")
            Log.WriteLine("Ввод е-майла")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Поск юзера")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-outline']", driver).click()
            Log.WriteLine("Редактировать")
            //
            for (i in 1..4) {
                DriverFindElement("//*[@class='form-control']//option[" + i.toString() + "]", driver).click()
                Log.WriteLine("Не подтвердил регистрацию")

                DriverFindElement("(//*[@class='btn btn-primary'])[5]", driver).click()
                Log.WriteLine("Сохранить")

            }
            //
            DriverFindElement("//*[@class='form-control']//option[3]", driver).click()
            Log.WriteLine("Активный")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[5]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }


    @Test
    fun ModerationChangeRole(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()





            Log.WriteLine("Админка Модерация-пользователи-Изменение Статуса-Без комментариев" + VersionBrowser(driver))





            Avtorization_Adminka(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Пользователи")
            //
            DriverFindElement("//*[@id='xlInput']", driver).sendKeys("uz2name@yandex.ru")
            Log.WriteLine("Ввод е-майла")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Поск юзера")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-outline']", driver).click()
            Log.WriteLine("Редактировать")
            //
            DriverFindElement("(//*[@name='roleNames'])[4]", driver).click()
            Log.WriteLine("Без комментариев")
            //
            DriverFindElement("(//*[@name='submitRoles'])", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[8]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz2name@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            Thread.sleep(500)
            //
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[1]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            Thread.sleep(13000)
            //
            DriverFindElement("(//*[@class='project-tab_list'])[1]//li[3]", driver).click()
            Log.WriteLine("Новости Комментарии")
            //
            DriverFindElement("//*[contains(text(), '                       Вы не можете оставлять комментарии здесь.              ')]", driver)
            Log.WriteLine("Вы не можете оставлять комментарии здесь.")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@class='header_user-link js-signup-link']", driver).click()
            Log.WriteLine("Вход")
            //
            ////
            //DriverFindElement("//*[@class='ng-tns-c3-0 ng-star-inserted']", driver).Click();
            //Log.WriteLine("Сбросить евент");
            ////
            DriverFindElement("//*[@class='form-control login-control_input js-input-email']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn js-btn-submit-login']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            Thread.sleep(3000)
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Пользователи")
            //
            DriverFindElement("//*[@id='xlInput']", driver).sendKeys("uz2name@yandex.ru")
            Log.WriteLine("Ввод е-майла")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Поск юзера")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-outline']", driver).click()
            Log.WriteLine("Редактировать")
            //
            DriverFindElement("(//*[@name='roleNames'])[4]", driver).click()
            Log.WriteLine("Без комментариев")
            //
            DriverFindElement("(//*[@name='submitRoles'])", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[8]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz2name@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            Thread.sleep(500)
            //
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[1]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            Thread.sleep(3500)
            //
            DriverFindElement("(//*[@class='project-tab_list'])[1]//li[3]", driver).click()
            Log.WriteLine("Новости Комментарии")
            //
            Thread.sleep(1500)
            //
            DriverFindElement("//*[@class='js-comment-textarea']", driver).sendKeys("qwerty")
            Log.WriteLine("Ввод коментария")
            //
            DriverFindElement("//*[@class='h-user_img']", driver)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun BalanceReduce() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Admin- Баланс Уменьшение" + VersionBrowser(driver))





            Avtorization_Uz2name(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[4]", driver).click()
            Log.WriteLine("Баланс")
            //
            DriverFindElement("//*[@class='button button__def button__sm']", driver).click()
            Log.WriteLine("Пополнить баланс")
            //
            DriverFindElement("//*[@id='core.total-amount']", driver).clear()
            Log.WriteLine("Очистить поле")
            //
            DriverFindElement("//*[@id='core.total-amount']", driver).sendKeys("1000")
            Log.WriteLine("Ввод в поле значения 1000")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary project-payment_btn']", driver).click()
            Log.WriteLine("Перейти к оплате Сбер")
            //
            //DriverFindElement("(//*[@id='email'])[1]", driver).SendKeys("pain-net@yandex.ru");
            //Log.WriteLine("Сбер ввод email");
            ////
            DriverFindElement("(//*[@id='cardnumber'])[1]", driver).sendKeys("4111 1111 1111 1111")
            Log.WriteLine("Ваш банк ввод номера карты")
            //
            DriverFindElement("(//*[@name='expdate'])[1]", driver).sendKeys("1219")
            Log.WriteLine("Ваш банк ввод месяц год")
            //
            DriverFindElement("(//*[@id='cvc'])[1]", driver).sendKeys("123")
            Log.WriteLine("Ваш банк ввод CVC2")
            //
            DriverFindElement("(//*[@class='sbersafe-pay-button'])", driver).click()
            Log.WriteLine("Оплатить")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("12345678")
            Log.WriteLine("password")
            //
            DriverFindElement("(//*[@value='Submit'])", driver).click()
            Log.WriteLine("Submit")
            Thread.sleep(1500)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")

            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Пользователи")
            //
            DriverFindElement("//*[@id='xlInput']", driver).sendKeys("uz2name@yandex.ru")
            Log.WriteLine("Ввод е-майла")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Поск юзера")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-outline']", driver).click()
            Log.WriteLine("Редактировать")
            //
            DriverFindElement("(//*[@name='amount'])[1]", driver).sendKeys("1000")
            Log.WriteLine("На сколько уменьшить баланс")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[8]", driver).click()
            Log.WriteLine("Удалить")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
            Log.WriteLine("OK")
            //
            Thread.sleep(1500)
            driver.navigate().refresh()
            Thread.sleep(500)
            DriverFindElement("//*[contains(text(), 'Текущий баланс: 0.0000')]", driver)
            Log.WriteLine("Текущий баланс: 0.0000")
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e: Exception) {
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }


    @Test
    fun BalanceIceSumma(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Admin- Баланс Уменьшение" + VersionBrowser(driver))




            Avtorization_Uz4name(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[4]", driver).click()
            Log.WriteLine("Баланс")
            //
            DriverFindElement("//*[@class='button button__def button__sm']", driver).click()
            Log.WriteLine("Пополнить баланс")
            //
            DriverFindElement("//*[@id='core.total-amount']", driver).clear()
            Log.WriteLine("Очистить поле")
            //
            DriverFindElement("//*[@id='core.total-amount']", driver).sendKeys("1000")
            Log.WriteLine("Ввод в поле значения 1000")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary project-payment_btn']", driver).click()
            Log.WriteLine("Перейти к оплате Сбер")
            //
            //DriverFindElement("(//*[@id='email'])[1]", driver).SendKeys("pain-net@yandex.ru");
            //Log.WriteLine("Сбер ввод email");
            ////
            DriverFindElement("(//*[@id='cardnumber'])[1]", driver).sendKeys("4111 1111 1111 1111")
            Log.WriteLine("Ваш банк ввод номера карты")
            //
            DriverFindElement("(//*[@name='expdate'])[1]", driver).sendKeys("1219")
            Log.WriteLine("Ваш банк ввод месяц год")
            //
            DriverFindElement("(//*[@id='cvc'])[1]", driver).sendKeys("123")
            Log.WriteLine("Ваш банк ввод CVC2")
            //
            DriverFindElement("(//*[@class='sbersafe-pay-button'])", driver).click()
            Log.WriteLine("Оплатить")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("12345678")
            Log.WriteLine("password")
            //
            DriverFindElement("(//*[@value='Submit'])", driver).click()
            Log.WriteLine("Submit")
            //
            Thread.sleep(1500)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Пользователи")
            //
            DriverFindElement("//*[@id='xlInput']", driver).sendKeys("uz4name@yandex.ru")
            Log.WriteLine("Ввод е-майла")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Поск юзера")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-outline']", driver).click()
            Log.WriteLine("Редактировать")
            //
            DriverFindElement("(//*[@name='amount'])[2]", driver).sendKeys("1000")
            Log.WriteLine("Cумма")
            //
            DriverFindElement("(//*[@class='btn btn-primary js-freeze'])", driver).click()
            Log.WriteLine("Заморозить")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[contains(text(), 'Замороженная сумма: 1000.0000')]", driver)
            Log.WriteLine("Замороженная сумма: 1000.0000")
            //
            DriverFindElement("(//*[@class='btn btn-default js-all-frozen-amount'])[1]", driver).click()
            Log.WriteLine("Вся сумма")
            //
            DriverFindElement("(//*[@class='btn btn-default js-unfreeze'])[1]", driver).click()
            Log.WriteLine("Вернуть на счёт")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[contains(text(), 'Замороженная сумма: 0.0000')]", driver)
            Log.WriteLine("Замороженная сумма: 0.0000")
            //
            DriverFindElement("(//*[@name='amount'])[2]", driver).sendKeys("1000")
            Log.WriteLine("Cумма")
            //
            DriverFindElement("(//*[@class='btn btn-primary js-freeze'])", driver).click()
            Log.WriteLine("Заморозить")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='btn btn-default js-all-frozen-amount'])[1]", driver).click()
            Log.WriteLine("Вся сумма")
            //
            DriverFindElement("(//*[@class='btn btn-default js-decrease'])[1]", driver).click()
            Log.WriteLine("Списать 1000 р")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
            Log.WriteLine("OK")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun ZakazInvestingStatus() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка Модерация-Заказы Инвестинга-статусы" + VersionBrowser(driver))





            Avtorization_Adminka(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']//li[6]", driver).click()
            Log.WriteLine("Заказы инвестинга")
            //
            for (i in 1..4) {
                DriverFindElement("//*[@id='status']//option[" + i.toString() + "]", driver).click()
                //
                DriverFindElement("//*[@id='search']", driver).click()
            }
            Log.WriteLine("Перебор статусов")
            //
            for (i in 1..5) {

                DriverFindElement("(//*[@class='btn-group col-lg-8']//*[@class='btn btn-default btn-outline'])[" + i.toString() + "]", driver).click()

            }
            Log.WriteLine("Время проектов")
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e: Exception) {
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }


    @Test
    fun ZakazInvestingEdit(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Админка Модерация-Заказы Инвестинга-Редактировать + скачать чек" + VersionBrowser(driver))





            Avtorization_Adminka(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']//li[6]", driver).click()
            Log.WriteLine("Заказы инвестинга")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-outline'])[1]", driver).click()
            Log.WriteLine("Редактировать")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-circle btn-outline btn-lg'])[1]", driver).click()
            Log.WriteLine("Скачать счёт")
            //
//            val file = ("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\winium\\Winium.Desktop.Driver.exe")
//            Runtime.getRuntime().exec(file)
//
//            val dc =  DesiredCapabilities()
//            dc.setCapability("app", "C:/Windows/system32/cmd.exe")
//            val driver1 = RemoteWebDriver(java.net.URL("http://localhost:9999"), dc)
//            Thread.sleep(500)
//            Runtime.getRuntime().exec("taskkill /F /IM cmd.exe")
//
//            driver1.findElement(By.name("Действия")).click()
//            //
//            driver1.findElement(By.name("Открыть")).click()
//            //
//            Runtime.getRuntime().exec("taskkill /F /IM Winium.Desktop.Driver.exe")
//            //
//            driver.windowHandle[1]
//            Thread.sleep(1500)
//            driver.windowHandle[0]
//            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
//            Log.WriteLine("Сохранить")
//            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())

        }finally {
            Stop1()
        }
    }


    @Test
    fun AdminkaCategory(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка-новая-Модерация-Категория проектов-Создать категорию" + VersionBrowser(driver))





            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[2]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[3]", driver).click()
            Log.WriteLine("Категория проектов")
            //
            DriverFindElement("//*[@class='mat-toolbar mat-toolbar-multiple-rows']//*[@class='mat-button-wrapper']", driver).click()
            Log.WriteLine("Создать категорию проекта")
            //
            Thread.sleep(1000)
            DriverFindElement("(//input)[1]", driver).click()
            DriverFindElement("(//input)[1]", driver).sendKeys("Категория"+ GetRandomString())
            Log.WriteLine("Алиас")
            //
            DriverFindElement("(//input)[2]", driver).sendKeys(GetRandomString())
            Log.WriteLine("Название")
            //
            DriverFindElement("(//input)[3]", driver).sendKeys("Ass")
            Log.WriteLine("Англ название")
            //
            DriverFindElement("(//input)[4]", driver).clear()
            DriverFindElement("(//input)[4]", driver).sendKeys("-999")
            Log.WriteLine("Порядковый номер")
            //
            for(i in 1..4)
            {
                DriverFindElement("(//*[@class='mat-checkbox-inner-container'])[" + i.toString() + "]", driver).click()

            }
            Log.WriteLine("Включить все чек боксы")
            //
            DriverFindElement("(//textarea)[1]", driver).sendKeys("1")
            Log.WriteLine("Коментарий")
            //
            DriverFindElement("(//textarea)[2]", driver).sendKeys("Ass")
            Log.WriteLine("HTML")
            //
            DriverFindElement("(//textarea)[3]", driver).sendKeys("10")
            Log.WriteLine("Тултип")
            //
            fun text() : Boolean
            {
                return try {
                    DriverFindElement("//*[@class='mat-error ng-star-inserted']", driver)
                    true
                } catch(e:Exception) {
                    false
                }
            }

            if(text())
            {
                driver.quit()
            }
            //
            DriverFindElement("(//*[@class='mat-raised-button mat-primary'])[1]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    // Дописать!!!!
    @Test
    fun CategoryProjectPage(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()





            Log.WriteLine("Админка-Проверка наличия категории на странице" + VersionBrowser(driver))





            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            driver.quit()





        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

}