
import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*


class AdminkaModerationProject1 : SetupParallel() {

    @Test
    fun SaveStateProject(){
        try{
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка Модерация-Проекты-Состояние проектов" + VersionBrowser(driver))




            Avtorization_Adminka(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']//li[2]", driver).click()
            Log.WriteLine("Проекты")
            //
            for (i in 1..9) {
                DriverFindElement("(//*[@class='form-control input-sm'])[1]//option[" + i.toString() + "]", driver).click()
                //
                DriverFindElement("(//*[@class='btn btn-light btn-block'])[1]", driver).click()
                //
                DriverFindElement("//*[@class='alert alert-success']//*[@class='close']", driver).click()
            }
            Log.WriteLine("Перебор и сохранение состояния")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }


    @Test
    fun  Status(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка Модерация-Проекты-Статус проектов" + VersionBrowser(driver))





            Avtorization_Adminka(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']//li[2]", driver).click()
            Log.WriteLine("Проекты")
            //
            for (i in 2..10) {
                DriverFindElement("//*[@name='campaignStatus']//option[" + i.toString() + "]", driver).click()
                //
                DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            }
            //
            Log.WriteLine("Перебор статусов")
            //
            DriverFindElement("//*[@name='campaignStatus']//option[1]", driver).click()
            //
            DriverFindElement("//*[@name='campaignStatus']//option[2]", driver).click()
            Log.WriteLine("Все проекты")
            //
            DriverFindElement("//*[@name='campaignStatus']//option[1]", driver).click()
            Log.WriteLine("Все")
            //
            for (i in 1..10) {
                System.out.println(i)
                DriverFindElement("//*[@name='campaignState']//option[" + i.toString() + "]", driver).click()
                //
                DriverFindElement("//*[@class='btn btn-primary']", driver).click()

//                if (i == 2) {
//                    DriverFindElement("//*[contains(text(), 'Не установлено')]", driver)
//                }
//                if (i == 3) {
//                    DriverFindElement("//*[contains(text(), 'Будет продлеваться')]", driver)
//                }
//                if (i == 4) {
//                    DriverFindElement("//*[contains(text(), 'Аннулирован')]", driver)
//                }
//                if (i == 5) {
//                    DriverFindElement("//*[contains(text(), 'Продлен')]", driver)
//                }
//                if (i == 6) {
//                    DriverFindElement("//*[contains(text(), 'Подготовка закрывающих документов')]", driver)
//                }
//                if (i == 7) {
//                    DriverFindElement("//*[contains(text(), 'Ждём документы от автора')]", driver)
//                }
//                if (i == 8) {
//                    DriverFindElement("//*[contains(text(), 'Проблема с документами')]", driver)
//                }
//                if (i == 9) {
//                    DriverFindElement("//*[contains(text(), 'Отдали в оплату')]", driver)
//                }
//                if (i == 10) {
//                    DriverFindElement("//*[contains(text(), 'Деньги получены автором')]", driver)
//                }
            }
            Log.WriteLine("Перебор состояний проекта проверка на наличие статуса")
            //
            for (i in 1..44) {
                DriverFindElement("//*[@name='managerId']//option[" + i.toString() + "]", driver).click()
                //
                DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            }
            Log.WriteLine("Менеджеры")
            //
            for (i in 1..39) {
                DriverFindElement("//*[@name='campaignTagId']//option[" + i.toString() + "]", driver).click()
                //
                DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            }
            Log.WriteLine("Категории")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun Time(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка Модерация-Проекты-Время проектов" + VersionBrowser(driver))





            Avtorization_Adminka(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']//li[2]", driver).click()
            Log.WriteLine("Проекты")
            //
            for (i in 1..6) {
                DriverFindElement("//*[@class='btn-group col-lg-8']//*[@class='btn btn-default btn-outline'][" + i.toString() + "]", driver).click()
            }
            //
            Log.WriteLine("Время проектов")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }


    @Test
    fun Data(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка Модерация-Проекты-Время проектов" + VersionBrowser(driver))





            Avtorization_Adminka(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']//li[2]", driver).click()
            Log.WriteLine("Проекты")
            //
            DriverFindElement("//*[@id='date-box-from']", driver).clear()
            //
            DriverFindElement("//*[@id='date-box-from']", driver).sendKeys("01012010")
            Log.WriteLine("Дата от")
            //
            DriverFindElement("//*[@id='date-box-to']", driver).clear()
            //
            DriverFindElement("//*[@id='date-box-to']", driver).sendKeys("01062018")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Поиск")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun MyProject(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Админка Модерация-Мои проекты-редактирование проекта" + VersionBrowser(driver))





            Avtorization_Adminka(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']//li[4]", driver).click()
            Log.WriteLine("Мои Проекты")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-outline'])[1]", driver).click()
            Log.WriteLine("Редактирование проекта")
            //
            DriverFindElement("(//*[@class='form-control'])[3]", driver).sendKeys("asdasdsdsvdfhgj")
            Log.WriteLine("Ввод метаданных")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
            Log.WriteLine("Сохранить")
            //
            //   DriverFindElement("//*[contains(text(), 'Добавить доставку')]", driver); //Пока это не работает задача заведена
            Log.WriteLine("Добавить доставку вознаграждения")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun PageMyProject(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка Модерация-Мои проекты - редактирование проекта - Страница проекта" + VersionBrowser(driver))





            Avtorization_Adminka(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']//li[4]", driver).click()
            Log.WriteLine("Мои Проекты")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-outline'])[1]", driver).click()
            Log.WriteLine("Редактирование проекта")
            //
            DriverFindElement("//*[@class='btn btn-info btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Страница проета н планете")
            //
            DriverFindElement("//*[@class='btn btn-nd-success js-start-campaign']", driver)
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }


    @Test
    fun Edit(){
        try{
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка Модерация-Мои проекты - редактирование проекта - Страница проекта" + VersionBrowser(driver))





            Avtorization_Adminka(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']//li[4]", driver).click()
            Log.WriteLine("Мои Проекты")
            //
            DriverFindElement("(//*[@class='btn btn-success btn-outline'])[2]", driver).click()
            Log.WriteLine("Модерация проекта")
            //
            DriverFindElement("//*[contains(text(), 'На модерации')]", driver)
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun CreateKontrAgent1(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            var world = 10
            val r = Random()
            var number = StringBuilder()
            for (i in 0 until world) {
                val tmp = r.nextInt(10)
                number.append(tmp)
            }

            var world1 = 9
            val r1 = Random()
            var number1 = StringBuilder()
            for (i in 0 until world1) {
                val tmp = r1.nextInt(9)
                number1.append(tmp)
            }



            Log.WriteLine("Админка Модерация-КонтрАгент-Cоздать контрагента - Физ лицо" + VersionBrowser(driver))





            Avtorization_Adminka(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']//li[5]", driver).click()
            Log.WriteLine("КонтрАгент")
            //
            DriverFindElement("//*[@class='form-control']", driver).sendKeys("Project_User")
            Log.WriteLine("Ввод названия проекта")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Найти")
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Добавить контр-агента")
            //
            DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[2]", driver).sendKeys("КонтрАгент")
            Log.WriteLine("ФИО")
            //
            DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[3]", driver).sendKeys("КонтрАгент Агентович")
            Log.WriteLine("ФИО + Инициалы")
            //
            DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[4]", driver).sendKeys("КонтрАгент Агент Агентович")
            Log.WriteLine("Ответственное лицо в родительном падеже(для Физ. и Юр. лиц)")
            //
            DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[5]", driver).sendKeys("89776662313")
            Log.WriteLine("Телефон")
            //
            DriverFindElement("//*[@id='cityNameRus']", driver).sendKeys("Москва")
            Log.WriteLine("Москва")
            //
            DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[8]", driver).click()
            for (i in 1..3) {
                DriverFindElement("(//*[@class='picker-switch'])[" + i.toString() + "]", driver).click()
            }
            //
            DriverFindElement("(//tbody)[4]//tr//td//span[1]", driver).click()
            DriverFindElement("(//tbody)[3]//tr//td//span[1]", driver).click()
            DriverFindElement("(//tbody)[2]//tr//td//span[1]", driver).click()
            DriverFindElement("(//tbody)[1]//tr[3]//td[3]", driver).click()
            Log.WriteLine("Дата рождения")
            //
            DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[9]", driver).sendKeys(number)
            Log.WriteLine("ИНН")
            //
            DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[10]", driver).sendKeys(number)
            Log.WriteLine("Номер паспорта")
            //
            DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[12]", driver).sendKeys("jnfjgjdfhdgsyufrtd")
            Log.WriteLine("Прочие данные")
            //
            DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[13]", driver).sendKeys("jnfjgjdfhdgsyufrtdjnfjgjdfhdgsyufrtd")
            Log.WriteLine("Юридический адрес  Зарег")
            //
            DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[14]", driver).sendKeys("jnfjgjdfhdgsyufrtdjnfjgjdfhdgsyufrtd")
            Log.WriteLine("Фактический  адрес")
            //
            DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[15]", driver).sendKeys("jnfjgjdfhdgsyufrtdjnfjgjdfhdgsyufrtd")
            Log.WriteLine("Кем выдан паспорт")
            //
            DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[16]", driver).sendKeys("770-060")
            Log.WriteLine("Кем выдан паспорт")
            //
            DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[18]", driver).click()
            //
            for (i in 1..3) {
                DriverFindElement("(//*[@class='picker-switch'])[" + i.toString() + "]", driver).click()
            }
            //
            DriverFindElement("(//tbody)[4]//tr//td//span[1]", driver).click()
            DriverFindElement("(//tbody)[3]//tr//td//span[1]", driver).click()
            DriverFindElement("(//tbody)[2]//tr//td//span[1]", driver).click()
            DriverFindElement("(//tbody)[1]//tr[3]//td[3]", driver).click()
            Log.WriteLine("Когда выдан паспорт")
            //
            DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[19]", driver).sendKeys(number1.toString())
            Log.WriteLine("Банк получателя")
            //
            DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[20]", driver).sendKeys(number1.toString())
            Log.WriteLine("БИК")
            //
            DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[21]", driver).sendKeys(number1.toString())
            Log.WriteLine("КПП")
            //
            DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[22]", driver).sendKeys(number.toString() + number.toString())
            Log.WriteLine("Расчётный счет")
            //
            DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[23]", driver).sendKeys(number.toString() + number.toString())
            Log.WriteLine("Кор.Счет")
            //
            DriverFindElement("//*[@class='btn btn-primary js-want-save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@class='form-control']", driver).sendKeys("КонтрАгент")
            Log.WriteLine("Ввод названия проекта")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Найти")
            //
            DriverFindElement("//tbody//tr[1]//a[2]", driver).click()
            Log.WriteLine("Прикрепить проект")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("OK")
            //
            val text = driver.findElement(By.xpath("(//tbody//tr[5]//a)[1]"))
            text.getAttribute("href")
            val Text1 = text.getAttribute("href")
            println(Text1)
            //
            DriverFindElement("(//tbody//tr//*[@class='fa fa-paperclip'])[1]", driver).click()
            Log.WriteLine("Прикрепить к проекту")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverNavigate(Text1, driver)
            //
            DriverFindElement("(//*[@class='project-author-controls_name'])[2]", driver).click()
            Log.WriteLine("Редактировать")
            //
            DriverFindElement("(//*[@class='project-create_tab_link'])[4]", driver).click()
            Log.WriteLine("Контрагент")
            //
            DriverFindElement("(//*[contains(text(), 'КонтрАгент')])[2]", driver)
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun CreateKontrAgent2(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка Модерация-КонтрАгент-Cоздать контрагента" + VersionBrowser(driver))





            Avtorization_Adminka(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']//li[5]", driver).click()
            Log.WriteLine("КонтрАгент")
            //
            DriverFindElement("//*[@class='form-control']", driver).sendKeys("Project_User")
            Log.WriteLine("Ввод названия проекта")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Найти")
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Добавить контр-агента")
            //

            DriverFindElement("//*[@id='type']//option[2]", driver).click()
            Log.WriteLine("ИП")
            //
            ContrAgent(driver, Log)
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Добавить контр-агента")
            //
            DriverFindElement("//*[@id='type']//option[3]", driver).click()
            Log.WriteLine("OOO")
            //
            ContrAgent(driver, Log)
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Добавить контр-агента")
            //
            DriverFindElement("//*[@id='type']//option[4]", driver).click()
            Log.WriteLine("НКО(в том числе благотворительный фонд")
            //
            ContrAgent(driver, Log)
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Добавить контр-агента")
            //
            DriverFindElement("//*[@id='type']//option[4]", driver).click()
            Log.WriteLine("Другое ЮР лицо")
            //
            ContrAgent(driver, Log)
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun StatusOrders(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Админка Модерация - Заказы" + VersionBrowser(driver))




            Avtorization_Adminka(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']//li[7]", driver).click()
            Log.WriteLine("Заказы")
            //
            for (i in 1..6) {
                DriverFindElement("(//*[@class='order-action-block order-status form-control'])[2]//option[" + i.toString() + "]", driver).click()
            }
            Log.WriteLine("Перебор статусов")
            //
            for (i in 1..5) {

                DriverFindElement("(//*[@class='btn btn-default btn-outline'])[" + i.toString() + "]", driver).click()
            }
            Log.WriteLine("Перебор дат времени")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }



    @Test
    fun MailDownloadExcel(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка Модерация - Заказы" + VersionBrowser(driver))





            Avtorization_Adminka(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']//li[7]", driver).click()
            Log.WriteLine("Заказы")
            //
            DriverFindElement("//*[@class='btn btn-danger']", driver).click()
            Log.WriteLine("Почта (для больших выгрузок)")
            //
//            DriverFindElement("//*[@class='btn btn-info']", driver).click()  // 503 ошибка  не может выгрузить все данные из базы
//            Log.WriteLine("CSV")
//            //
//            DriverFindElement("//*[@class='btn btn-success']", driver).click()
//            Log.WriteLine("Excel")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun Status_Zakaz(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка Модерация - Заказы инвестинга - Статусы" + VersionBrowser(driver))





            Avtorization_Adminka(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']//li[6]", driver).click()
            Log.WriteLine("Заказы инвестинга")
            //
            for (i in 1..4) {
                DriverFindElement("//*[@id='status']//option[" + i.toString() + "]", driver).click()
                //
                DriverFindElement("//*[@id='search']", driver).click()
            }

            Log.WriteLine("Перебор статустов")
            //
            DriverFindElement("//*[@id='status']//option[1]", driver).click()
            //
            for (i in 1..5) {

                DriverFindElement("(//*[@class='btn btn-default btn-outline'])[" + i.toString() + "]", driver).click()
            }
            Log.WriteLine("Перебор дат времени")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }



    @Test
    fun DownloadCheckEdit(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\A_Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\A_Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка Модерация - Заказы инвестинга - Чек" + VersionBrowser(driver))




            Avtorization_Adminka(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']//li[6]", driver).click()
            Log.WriteLine("Заказы инвестинга")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-outline'])[1]", driver).click()
            Log.WriteLine("Редактировать")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-circle btn-outline btn-lg'])[1]", driver).click()
            Log.WriteLine("Скачать чек")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
            Log.WriteLine("Сохранить")
            //
//            val file = ("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\winium\\Winium.Desktop.Driver.exe")
//            Runtime.getRuntime().exec(file)
//
//            val dc =  DesiredCapabilities()
//            dc.setCapability("app", "C:/Windows/system32/cmd.exe")
//            val driver1 = RemoteWebDriver(java.net.URL("http://localhost:9999"), dc)
//            Thread.sleep(500)
//            Runtime.getRuntime().exec("taskkill /F /IM cmd.exe")
//
//            driver1.findElement(By.name("Действия")).click()
//            //
//            driver1.findElement(By.name("Открыть")).click()
//            //
//            Runtime.getRuntime().exec("taskkill /F /IM Winium.Desktop.Driver.exe")
//            //
            Thread.sleep(2500)
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }




}