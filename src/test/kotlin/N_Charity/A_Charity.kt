import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import java.nio.file.Files
import java.nio.file.Paths

class A_Charity : Setup() {


    @Test
    fun CharityMenu() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Charity")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Charity\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Благотворительность Меню" + VersionBrowser(driver))






            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(1500)
            //
            DriverNavigate(Charity, driver)
            //
            DriverFindElement("//*[@class='h-menu header_menu']//li[1]", driver).click()
            Log.WriteLine("Благотворительные проекты")
            //
            DriverFindElement("//*[@class='h-menu header_menu']//li[2]", driver).click()
            Log.WriteLine("О нас")
            //
            DriverFindElement("//*[@class='h-menu header_menu']//li[3]", driver).click()
            Log.WriteLine("Магазин")
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e: Exception) {
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }


    @Test
    fun CharityMenu2() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Charity")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Charity\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Благотворительность Меню -2" + VersionBrowser(driver))





            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(1500)
            //
            DriverNavigate(Charity, driver)
            //
            DriverFindElement("(//*[@class='charity-menu_list']//*[@class='charity-menu_link'])[1]", driver).click()
            Log.WriteLine("Благотворительные проекты")
            //
            DriverFindElement("(//*[@class='charity-menu_list']//*[@class='charity-menu_link'])[2]", driver).click()
            Log.WriteLine("О нас")
            //
            DriverFindElement("(//*[@class='charity-menu_list']//*[@class='charity-menu_link'])[3]", driver).click()
            Log.WriteLine("Новости")
            //
            DriverFindElement("(//*[@class='charity-menu_list']//*[@class='charity-menu_link'])[4]", driver).click()
            Log.WriteLine("Магазин")
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e: Exception) {
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }


    @Test
    fun HelpProject() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Charity")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Charity\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            fun Find(): Boolean {
                return try {
                    Thread.sleep(6000)
                    driver.findElement(By.xpath("(//*[@class='project-wide-carousel-names_i'][3]//*[@class='project-wide-carousel-names_title active'])"))

                    true
                } catch (e: Exception) {
                    false
                }
            }






            Log.WriteLine("Помочь проекту" + VersionBrowser(driver))





            Avtorization_Uz4name(driver, Log)
            //
            DriverNavigate(Charity, driver)
            //
            for (i in 0..29) {
                if (Find()) {
                    Thread.sleep(1000)
                    driver.findElement(By.xpath("(//*[@class='btn btn-primary'])[5]")).click()
                    break
                }
            }
            Log.WriteLine("Помочь проекту")

            Thread.sleep(3000)
            Log.Close()
            Stop()
            driver.quit()
        } catch (e: Exception) {
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun HelpSupportProject() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Charity")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Charity\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Помочь проекту При выборе проекта" + VersionBrowser(driver))







            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(1500)
            //
            DriverNavigate(Charity, driver)
            //
            DriverFindElement("(//*[@class='project-card-item'])[8]", driver)
            Log.WriteLine("Проверка карусели проекта")
            //
            DriverFindElement("(//*[@class='project-card-list project-card-list__item-3']//*[@class='project-card-item'])[1]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Помочь проекту")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary action-card_btn js-navigate-to-purchase'])[1]", driver).click()
            Log.WriteLine("Перейти к оплате")
            //
            fun FindEl(): Boolean {
                return try {
                    DriverFindElement("(//*[@class='form-ui-label'])[2]", driver)
                    //Согласен на условия
                    false
                } catch (e: Exception) {
                    true
                }
            }

            for (i in 0..60) {  //Клауд
                if (FindEl()) {

                    DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                    Log.WriteLine("Даю свое согласие")

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver).click()
                    Log.WriteLine("Карты других банков")
                    //
                    DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
                    Log.WriteLine("Оплатить покупку")
                    //
                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
                    Log.WriteLine("Оплата картой")
                    //
                    driver.switchTo().frame(0)
                    DriverFindElement("(//*[@class='row'])[1]//input", driver).click()
                    Log.WriteLine("клик по строке")
                    DriverFindElement("(//*[@class='row'])[1]//input", driver).sendKeys("4111 1111 1111 1111")
                    //
                    DriverFindElement("(//*[@id='inputMonth'])[1]", driver).sendKeys("12")
                    //
                    DriverFindElement("(//*[@id='inputYear'])[1]", driver).sendKeys("19")
                    //
                    DriverFindElement("(//*[@data-mask='000'])[1]", driver).sendKeys("123")
                    //
                    DriverFindElement("(//*[@id='cardHolder'])[1]", driver).sendKeys("QWERTY")
                    //
                    DriverFindElement("(//*[@class='button'])[1]", driver).click()
                    DriverFindElement("(//*[@class='button'])[2]", driver).click()
                    Log.WriteLine("OK")
                    driver.switchTo().defaultContent()
                    break
                }
            }


//            DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
//            Log.WriteLine("Оплатить покупку")
//            //
//            //DriverFindElement("(//*[@id='email'])[1]", driver).SendKeys("pain-net@yandex.ru");
//            //Log.WriteLine("Сбер ввод email");
//            ////
//            DriverFindElement("(//*[@id='cardnumber'])[1]", driver).sendKeys("4111 1111 1111 1111")
//            Log.WriteLine("Ваш банк ввод номера карты")
//            //
//            DriverFindElement("(//*[@name='expdate'])[1]", driver).sendKeys("1219")
//            Log.WriteLine("Ваш банк ввод месяц год")
//            //
//            DriverFindElement("(//*[@id='cvc'])[1]", driver).sendKeys("123")
//            Log.WriteLine("Ваш банк ввод CVC2")
//            //
//            DriverFindElement("(//*[@class='sbersafe-pay-button'])", driver).click()
//            Log.WriteLine("Оплатить")
//            //
//            DriverFindElement("(//*[@name='password'])", driver).sendKeys("12345678")
//            Log.WriteLine("password")
//            //
//            DriverFindElement("(//*[@value='Submit'])", driver).click()
//            Log.WriteLine("Submit")
            //
            Thread.sleep(1500)
            //
            driver.navigate().to(Charity)
            //
            DriverFindElement("(//*[@class='project-card-list project-card-list__item-3']//*[@class='project-card-item'])[1]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("//*[@data-tooltip='Вы приобрели это вознаграждение']", driver)
            Log.WriteLine("Вы поддержали проект")
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e: Exception) {
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

}







