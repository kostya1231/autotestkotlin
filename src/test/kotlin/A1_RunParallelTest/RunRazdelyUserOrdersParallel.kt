import org.junit.Test
import org.junit.experimental.ParallelComputer
import org.junit.runner.JUnitCore

class RunRazdelyUserOrdersParallel {

    @Test
    fun StartRazdelyUserOrders(){

        val cls = arrayOf(RazdelyUserOrders::class.java)
        JUnitCore.runClasses(ParallelComputer.methods(),*cls)
    }
}