import org.junit.Test
import org.junit.experimental.ParallelComputer
import org.junit.runner.JUnitCore

class G_RunAdminkaShop {

    @Test
    fun StartAdminkaShop() {

        val cls = arrayOf(AdminkaShop1::class.java)
        JUnitCore.runClasses(ParallelComputer.methods(), *cls)
    }

}