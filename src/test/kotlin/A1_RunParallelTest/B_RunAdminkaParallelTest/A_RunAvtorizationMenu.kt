import org.junit.Test
import org.junit.experimental.ParallelComputer
import org.junit.runner.JUnitCore

class A_RunAvtorizationMenu {

    @Test
    fun StartAvtorizationMenu() {

        val cls = arrayOf(AvtorizationMenu::class.java)
        JUnitCore.runClasses(ParallelComputer.methods(), *cls)
    }
}