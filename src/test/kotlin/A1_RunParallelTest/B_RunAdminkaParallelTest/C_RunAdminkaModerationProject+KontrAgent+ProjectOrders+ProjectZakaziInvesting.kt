import org.junit.Test
import org.junit.experimental.ParallelComputer
import org.junit.runner.JUnitCore

class `C_RunAdminkaModerationProject+KontrAgent+ProjectOrders+ProjectZakaziInvesting` {

    @Test
    fun StartAdminkaModerationProject1() {

        val cls = arrayOf(AdminkaModerationProject1::class.java)
        JUnitCore.runClasses(ParallelComputer.methods(), *cls)
    }

}