import org.junit.Test
import org.junit.experimental.ParallelComputer
import org.junit.runner.JUnitCore

class B_RunAdminkaModeration {

    @Test
    fun StartAdminkaModeration1() {

        val cls = arrayOf(AdminkaModeration1::class.java)
        JUnitCore.runClasses(ParallelComputer.methods(), *cls)
    }
}