import org.junit.Test
import org.junit.experimental.ParallelComputer
import org.junit.runner.JUnitCore

class RunAnonymousParallel {

    @Test
    fun StartAvtorizationService() {


        val cls = arrayOf(Anonymous::class.java)
        JUnitCore.runClasses(ParallelComputer.methods(), *cls)
    }

}