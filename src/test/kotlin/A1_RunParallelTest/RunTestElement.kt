import org.junit.Test
import org.junit.experimental.ParallelComputer
import org.junit.runner.JUnitCore

class RunTestElement {

    @Test
    fun StartTestElement1() {

        val cls = arrayOf(TestElement1::class.java)
        JUnitCore.runClasses(ParallelComputer.methods(), *cls)
    }


}