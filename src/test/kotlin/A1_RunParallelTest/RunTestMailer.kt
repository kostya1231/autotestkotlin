import org.junit.Test
import org.junit.experimental.ParallelComputer
import org.junit.runner.JUnitCore

class RunTestMailer {

    @Test
    fun StartTestMailer1() {

        val cls = arrayOf(TestMailer1::class.java)
        JUnitCore.runClasses(ParallelComputer.methods(), *cls)
    }



}