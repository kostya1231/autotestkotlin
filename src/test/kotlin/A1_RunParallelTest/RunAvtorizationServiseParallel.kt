import org.junit.Test
import org.junit.experimental.ParallelComputer
import org.junit.runner.JUnitCore


class RunAvtorizationServiseParallel {

    @Test
    fun StartAvtorizationService() {

        val cls = arrayOf(Service::class.java)
        JUnitCore.runClasses(ParallelComputer.methods(), *cls)
    }

}
