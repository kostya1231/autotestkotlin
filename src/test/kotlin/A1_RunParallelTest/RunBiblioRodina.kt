import org.junit.Test
import org.junit.experimental.ParallelComputer
import org.junit.runner.JUnitCore

class RunBiblioRodina {

    @Test
    fun StartBiblioRodina1(){

        val cls = arrayOf(BiblioRodina1::class.java)
        JUnitCore.runClasses(ParallelComputer.methods(),*cls)
    }
}