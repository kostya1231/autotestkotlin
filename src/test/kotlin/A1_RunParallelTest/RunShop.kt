import org.junit.Test
import org.junit.experimental.ParallelComputer
import org.junit.runner.JUnitCore

class RunShop {

    @Test
    fun StartShop1() {

        val cls = arrayOf(Shop1::class.java)
        JUnitCore.runClasses(ParallelComputer.methods(), *cls)
    }

}