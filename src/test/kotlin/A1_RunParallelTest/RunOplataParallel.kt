package A9_ParallelTest

import Oplata
import org.junit.Test
import org.junit.experimental.ParallelComputer
import org.junit.runner.JUnitCore

class RunOplataParallel {

    @Test
    fun StartOplata(){

        val cls = arrayOf(Oplata::class.java)
        JUnitCore.runClasses(ParallelComputer.methods(),*cls)
    }

}