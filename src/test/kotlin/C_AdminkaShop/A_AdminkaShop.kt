import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

class A_AdminkaShop : Setup() {


    @Test
    fun DeliveryService(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Админка-Товары магазина Служба доставки" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[3]", driver).click()
            Log.WriteLine("Магазин")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[3]", driver).click()
            Log.WriteLine("Служба доставки")
            //
            DriverFindElement("//*[@class='main-page-actions']", driver).click()
            Log.WriteLine("Создать службу доставки")
            //
            DriverFindElement("//*[@name='deliveryService']", driver).click()
            //
            DriverFindElement("//*[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front']//li[2]", driver).click()
            Log.WriteLine("Доставка почтой в пределах России (Россия)")
            //
            Thread.sleep(300)
            //
            DriverFindElement("//*[@name='publicNote']", driver).click()
            //
            DriverFindElement("//*[@name='publicNote']", driver).sendKeys("Служба доставки")
            Log.WriteLine("Описание для покупателя")
            //
            DriverFindElement("//*[@name='price']", driver).clear()
            //
            DriverFindElement("//*[@name='price']", driver).sendKeys("100")
            Log.WriteLine("Стоимость")
            //
            DriverFindElement("//*[@id='save-linked-service']", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun FindProductShop(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка-Товары магазина запуск товара" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[3]", driver).click()
            Log.WriteLine("Магазин")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[2]", driver).click()
            Log.WriteLine("Товары магазина")
            //
            DriverFindElement("//*[@name='query']", driver).sendKeys("Товар")
            Log.WriteLine("Ввод названия товара")
            //
            Thread.sleep(200)
            //
            DriverFindElement("(//*[@class='fa fa-play'])[1]", driver).click()
            Log.WriteLine("Запуск товара")
            //
            DriverNavigate(ShopPlaneta, driver)
            //
            DriverFindElement("(//*[@class='s-icon s-icon-search'])[1]", driver).click()
            //
            DriverFindElement("(//*[@id='search-input'])[1]", driver).sendKeys("Товар Товарович")
            Log.WriteLine("Название товара")
            //
            DriverFindElement("//*[contains(text(), 'Товар_Товарович')]", driver)
            Log.WriteLine("Проверка наличия товара")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception) {
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun SettingsButton(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка-Товары магазина запуск Клонирование редактирование удаление товара" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[3]", driver).click()
            Log.WriteLine("Магазин")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[2]", driver).click()
            Log.WriteLine("Товары магазина")
            //
            DriverFindElement("//*[@name='query']", driver).sendKeys("Товар")
            Log.WriteLine("Ввод названия товара")
            //
            Thread.sleep(200)
            //
            DriverFindElement("(//*[@class='btn btn-outline btn-info js-clone'])[1]", driver).click()
            Log.WriteLine("Клонировать товар")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("ОК")
            //
            DriverFindElement("(//*[@class='fa arrow'])[3]", driver).click()
            Log.WriteLine("Магазин")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[2]", driver).click()
            Log.WriteLine("Товары магазина")
            //
            DriverFindElement("//*[@name='query']", driver).sendKeys("Товар_Товарович [clone]")
            Log.WriteLine("Ввод названия товара")
            //
            DriverFindElement("//*[contains(text(), 'Товар_Товарович [clone]')]", driver)
            Log.WriteLine("Проверка наличия товара")
            //
            DriverFindElement("//*[@class='btn btn-outline btn-primary']", driver).click()
            Log.WriteLine("Редактировать")
            //
            DriverFindElement("(//*[@class='fa arrow'])[3]", driver).click()
            Log.WriteLine("Магазин")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[2]", driver).click()
            Log.WriteLine("Товары магазина")
            //
            DriverFindElement("//*[@name='query']", driver).sendKeys("Товар_Товарович [clone]")
            Log.WriteLine("Ввод названия товара")
            //
            DriverFindElement("//*[contains(text(), 'Товар_Товарович [clone]')]", driver)
            Log.WriteLine("Проверка наличия товара")
            //
            DriverFindElement("//*[@class='btn btn-danger btn-outline js-delete-product']", driver).click()
            Log.WriteLine("Удалить товар")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("OK")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun PromeCode1(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            var world = 1
            val r = Random()
            var number = StringBuilder()
            for (i in 0 until world) {
                val tmp = r.nextInt(16)
                number.append(tmp)
            }



            Log.WriteLine("Админка-Магазин-Прокмокод - рубли" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[3]", driver).click()
            Log.WriteLine("Магазин")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[4]", driver).click()
            Log.WriteLine("ПромоКоды")
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Создать промокод")
            //
            DriverFindElement("//*[@id='title']", driver).sendKeys("Код")
            Log.WriteLine("Название промо кода")
            //
            DriverFindElement("//*[@id='discountAmount']", driver).clear()
            //
            DriverFindElement("//*[@id='discountAmount']", driver).sendKeys("30")
            Log.WriteLine("Скидка в %")
            //
            DriverFindElement("//*[@id='usageCount']", driver).clear()
            //
            DriverFindElement("//*[@id='usageCount']", driver).sendKeys("1000")
            Log.WriteLine("Количество использований")
            //
            DriverFindElement("//*[@id='productTagId']//option[2]", driver).click()
            Log.WriteLine("Категория товара вещи")
            //
            DriverFindElement("//*[@id='date-box-begin']", driver).click()
            //
            DriverFindElement("(//*[@class='table-condensed'])[1]//tbody//tr[1]//td[5]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("//*[@id='date-box-end']", driver).click()
            //
            DriverFindElement("(//*[@class='table-condensed'])[1]//tbody//tr[5]//td[5]", driver).click()
            Log.WriteLine("Дата окончания")
            //
            DriverFindElement("//*[@name='code']", driver).sendKeys("Code123")
            Log.WriteLine("Код")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Сгенерить код")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[2]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Thread.sleep(500)
            //
            val element:String = DriverFindElement("//*[@name='code']", driver).getAttribute("value")

            //
            DriverNavigate(ShopPlaneta, driver)
            //
            Thread.sleep(2900)
            //
            DriverFindElement("//*[@class='shop-menu_arr']", driver).click()
            Log.WriteLine("Все товары")
            //
            DriverFindElement("(//*[@class='shop-menu_list']//*[@class='shop-menu_link'])[2]", driver).click()
            Log.WriteLine("Вещи")
            //
            for (i in 1..20)
            {
                val scrol = 500
                val js1 = driver as JavascriptExecutor
                Thread.sleep(50)
                js1.executeScript("window.scrollBy(0,$scrol)")
            }
            //
            DriverFindElement("(//*[@class='product-card_btn'])[" + number.toString() + "]", driver).click()
            Log.WriteLine("Выбор товара")
            //
            DriverFindElement("(//*[@class='shop-btn shop-btn-add pln-d-switch js-add-to-cart'])[1]", driver).click()
            Log.WriteLine("Добавить в корзину")
            //

            DriverFindElement("//*[@class='shop-basket_link pln-d-switch']", driver).click()
            Log.WriteLine("Перейти в корзину")
            //
            DriverFindElement("//*[@class='shop-btn shop-btn-primary js-purchase-cart']", driver).click()
            Log.WriteLine("Оформить заказ")
            //
            DriverFindElement("(//*[@class='radiobox '])[3]", driver).click()
            Log.WriteLine("Самовывоз")
            //
            DriverFindElement("//*[@name='phoneNumber']", driver).sendKeys("9778665883")
            Log.WriteLine("Ввод номера телефона")
            //
            DriverFindElement("//*[@name='comment']", driver).sendKeys("sataneem")
            Log.WriteLine("Комментарий")
            //
            DriverFindElement("//*[@class='js-submit shop-order-action_next shop-btn shop-btn-primary']", driver).click()
            Log.WriteLine("Далее")
            //
            DriverFindElement("//*[@name='promoCode']", driver).sendKeys(element)
            Log.WriteLine("Ввод промкода")
            //
            DriverFindElement("//*[@class='shop-btn shop-btn-secondary btn-block js-promo-button']", driver).click()
            Log.WriteLine("Применить")
            //
            DriverFindElement("//*[@class='shop-checkout_promocode']", driver)
            Log.WriteLine("Проверка на наличие элемента")
            //
            DriverFindElement("(//*[@class='ppt-item'])[2]",driver).click()
            Log.WriteLine("Клауд")
            //
            DriverFindElement("//*[@class='js-submit shop-order-action_next shop-btn shop-btn-primary']", driver).click()
            Log.WriteLine("Далее")
            //
            DriverFindElement("//*[@class='shop-btn shop-btn-primary js-shop-confirm-order-button']", driver).click()
            Log.WriteLine("Подтвердить оформление  заказа")


            fun FindEl() : Boolean{
                return try{
                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver)
                    //Согласен на условия
                    true
                }catch (e:Exception){
                    false
                }
            }

            for(i in 0..60){  //Клауд
                if(FindEl()){

                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
                    Log.WriteLine("Оплата картой")
                    //
                    driver.switchTo().frame(0)
                    DriverFindElement("(//*[@class='row'])[1]//input",driver).click()
                    Log.WriteLine("клик по строке")
                    DriverFindElement("(//*[@class='row'])[1]//input",driver).sendKeys("4111 1111 1111 1111")
                    //
                    DriverFindElement("(//*[@id='inputMonth'])[1]",driver).sendKeys("12")
                    //
                    DriverFindElement("(//*[@id='inputYear'])[1]",driver).sendKeys("19")
                    //
                    DriverFindElement("(//*[@data-mask='000'])[1]",driver).sendKeys("123")
                    //
                    DriverFindElement("(//*[@id='cardHolder'])[1]",driver).sendKeys("QWERTY")
                    //
                    DriverFindElement("(//*[@class='button'])[1]",driver).click()
                    DriverFindElement("(//*[@class='button'])[2]",driver).click()
                    Log.WriteLine("OK")
                    driver.switchTo().defaultContent()
                    break
                }
            }
//            DriverFindElement("(//*[@class='ppt-image'])[1]", driver).click()
//            Log.WriteLine("Оплата сбер")
//            //
//            DriverFindElement("//*[@class='js-submit shop-order-action_next shop-btn shop-btn-primary']", driver).click()
//            Log.WriteLine("Далее")
//            //
//            DriverFindElement("//*[@class='shop-btn shop-btn-primary js-shop-confirm-order-button']", driver).click()
//            Log.WriteLine("Подтвердить оформление  заказа")
//            //
//            //DriverFindElement("(//*[@id='email'])[1]", driver).SendKeys("pain-net@yandex.ru");
//            //Log.WriteLine("Сбер ввод email");
//            ////
//            DriverFindElement("(//*[@id='cardnumber'])[1]", driver).sendKeys("4111 1111 1111 1111")
//            Log.WriteLine("Ваш банк ввод номера карты")
//            //
//            DriverFindElement("(//*[@name='expdate'])[1]", driver).sendKeys("1219")
//            Log.WriteLine("Ваш банк ввод месяц год")
//            //
//            DriverFindElement("(//*[@id='cvc'])[1]", driver).sendKeys("123")
//            Log.WriteLine("Ваш банк ввод CVC2")
//            //
//            DriverFindElement("(//*[@class='sbersafe-pay-button'])", driver).click()
//            Log.WriteLine("Оплатить")
//            //
//            DriverFindElement("(//*[@name='password'])", driver).sendKeys("12345678")
//            Log.WriteLine("password")
//            //
//            DriverFindElement("(//*[@value='Submit'])", driver).click()
//            Log.WriteLine("Submit")
            //
            Thread.sleep(2000)
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[3]", driver).click()
            Log.WriteLine("Мои заказы")
            //
            try {
                DriverFindElement("(//*[@class='profile-nav_link'])[1]", driver).click()
            }
            catch(e:Exception)
            {
                DriverFindElement("(//*[@class='profile-nav_link'])[2]", driver).click()
            }
            //
            DriverFindElement("//*[contains(text(), '30 ₽')]", driver)
            Log.WriteLine("Проверка на наличие промкода в заказах")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){

            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun PromeCode123(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            var world = 1
            val r = Random()
            var number = StringBuilder()
            for (i in 0 until world) {
                val tmp = r.nextInt(16)
                number.append(tmp)
            }



            Log.WriteLine("Админка-Магазин-Прокмокод название 123 общий для всех товаров - рубли" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[3]", driver).click()
            Log.WriteLine("Магазин")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[4]", driver).click()
            Log.WriteLine("ПромоКоды")
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Создать промокод")
            //
            DriverFindElement("//*[@id='title']", driver).sendKeys("123")
            Log.WriteLine("Название промо кода")
            //
            DriverFindElement("//*[@id='discountAmount']", driver).clear()
            //
            DriverFindElement("//*[@id='discountAmount']", driver).sendKeys("30")
            Log.WriteLine("Скидка в %")
            //
            DriverFindElement("//*[@id='usageCount']", driver).clear()
            //
            DriverFindElement("//*[@id='usageCount']", driver).sendKeys("1000")
            Log.WriteLine("Количество использований")
            //
            DriverFindElement("//*[@id='date-box-begin']", driver).click()
            //
            DriverFindElement("(//*[@class='table-condensed'])[1]//tbody//tr[1]//td[5]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("//*[@id='date-box-end']", driver).click()
            //
            DriverFindElement("(//*[@class='table-condensed'])[1]//tbody//tr[5]//td[5]", driver).click()
            Log.WriteLine("Дата окончания")
            //
            DriverFindElement("//*[@name='code']", driver).sendKeys("123")
            Log.WriteLine("Код")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[2]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Thread.sleep(3500)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){

            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }


    @Test
    fun PromeCode2(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()

            var world = 1
            val r = Random()
            var number = StringBuilder()
            for (i in 0 until world) {
                val tmp = r.nextInt(16)
                number.append(tmp)
            }



            Log.WriteLine("Админка-Магазин-Прокмокод - проценты" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[3]", driver).click()
            Log.WriteLine("Магазин")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[4]", driver).click()
            Log.WriteLine("ПромоКоды")
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Создать промокод")
            //
            DriverFindElement("//*[@id='title']", driver).sendKeys("Код")
            Log.WriteLine("Название промо кода")
            //
            DriverFindElement("//*[@id='discountAmount']", driver).clear()
            //
            DriverFindElement("//*[@id='discountAmount']", driver).sendKeys("30")
            Log.WriteLine("Скидка в % рублях")
            //
            DriverFindElement("//*[@id='discountType']//option[2]", driver).click()
            Log.WriteLine("Относительная проценты")
            //
            DriverFindElement("//*[@id='usageCount']", driver).clear()
            //
            DriverFindElement("//*[@id='usageCount']", driver).sendKeys("1000")
            Log.WriteLine("Количество использований")
            //
            DriverFindElement("//*[@id='productTagId']//option[2]", driver).click()
            Log.WriteLine("Категория товара вещи")
            //
            DriverFindElement("//*[@id='date-box-begin']", driver).click()
            //
            DriverFindElement("(//*[@class='table-condensed'])[1]//tbody//tr[1]//td[4]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("//*[@id='date-box-end']", driver).click()
            //
            DriverFindElement("(//*[@class='table-condensed'])[1]//tbody//tr[5]//td[5]", driver).click()
            Log.WriteLine("Дата окончания")
            //
            DriverFindElement("//*[@name='code']", driver).sendKeys("Code123")
            Log.WriteLine("Код")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Сгенерить код")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[2]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Thread.sleep(500)
            //
            var element = DriverFindElement("//*[@name='code']", driver).getAttribute("value")

            //
            DriverNavigate(ShopPlaneta, driver)
            //
            Thread.sleep(2900)
            //
            DriverFindElement("//*[@class='shop-menu_arr']", driver).click()
            Log.WriteLine("Все товары")
            //
            DriverFindElement("(//*[@class='shop-menu_list']//*[@class='shop-menu_link'])[2]", driver).click()
            Log.WriteLine("Вещи")
            //
            for (i in 1..20)
            {
                val scrol = 500
                val js1 = driver as JavascriptExecutor
                Thread.sleep(50)
                js1.executeScript("window.scrollBy(0,$scrol)")
            }
            //
            DriverFindElement("(//*[@class='product-card_btn'])[" + number.toString() + "]", driver).click()
            Log.WriteLine("Выбор товара")
            //
            DriverFindElement("(//*[@class='shop-btn shop-btn-add pln-d-switch js-add-to-cart'])[1]", driver).click()
            Log.WriteLine("Добавить в корзину")
            //

            DriverFindElement("//*[@class='shop-basket_link pln-d-switch']", driver).click()
            Log.WriteLine("Перейти в корзину")
            //
            DriverFindElement("//*[@class='shop-btn shop-btn-primary js-purchase-cart']", driver).click()
            Log.WriteLine("Оформить заказ")
            //
            DriverFindElement("(//*[@class='radiobox '])[3]", driver).click()
            Log.WriteLine("Самовывоз")
            //
            DriverFindElement("//*[@name='phoneNumber']", driver).sendKeys("9778665883")
            Log.WriteLine("Ввод номера телефона")
            //
            DriverFindElement("//*[@name='comment']", driver).sendKeys("sataneem")
            Log.WriteLine("Комментарий")
            //
            DriverFindElement("//*[@class='js-submit shop-order-action_next shop-btn shop-btn-primary']", driver).click()
            Log.WriteLine("Далее")
            //
            DriverFindElement("//*[@name='promoCode']", driver).sendKeys(element)
            Log.WriteLine("Ввод промкода")
            //
            DriverFindElement("//*[@class='shop-btn shop-btn-secondary btn-block js-promo-button']", driver).click()
            Log.WriteLine("Применить")
            //
            DriverFindElement("//*[@class='shop-checkout_promocode']", driver)
            Log.WriteLine("Проверка на наличие элемента")
            //
            DriverFindElement("(//*[@class='ppt-item'])[2]",driver).click()
            Log.WriteLine("Клауд")
            //
            DriverFindElement("//*[@class='js-submit shop-order-action_next shop-btn shop-btn-primary']", driver).click()
            Log.WriteLine("Далее")
            //
            DriverFindElement("//*[@class='shop-btn shop-btn-primary js-shop-confirm-order-button']", driver).click()
            Log.WriteLine("Подтвердить оформление  заказа")


            fun FindEl() : Boolean{
                return try{
                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver)
                    //Согласен на условия
                    true
                }catch (e:Exception){
                    false
                }
            }

            for(i in 0..60){  //Клауд
                if(FindEl()){

                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
                    Log.WriteLine("Оплата картой")
                    //
                    driver.switchTo().frame(0)
                    DriverFindElement("(//*[@class='row'])[1]//input",driver).click()
                    Log.WriteLine("клик по строке")
                    DriverFindElement("(//*[@class='row'])[1]//input",driver).sendKeys("4111 1111 1111 1111")
                    //
                    DriverFindElement("(//*[@id='inputMonth'])[1]",driver).sendKeys("12")
                    //
                    DriverFindElement("(//*[@id='inputYear'])[1]",driver).sendKeys("19")
                    //
                    DriverFindElement("(//*[@data-mask='000'])[1]",driver).sendKeys("123")
                    //
                    DriverFindElement("(//*[@id='cardHolder'])[1]",driver).sendKeys("QWERTY")
                    //
                    DriverFindElement("(//*[@class='button'])[1]",driver).click()
                    DriverFindElement("(//*[@class='button'])[2]",driver).click()
                    Log.WriteLine("OK")
                    driver.switchTo().defaultContent()
                    break
                }
            }
//            DriverFindElement("(//*[@class='ppt-image'])[1]", driver).click()
//            Log.WriteLine("Оплата сбер")
//            //
//            DriverFindElement("//*[@class='js-submit shop-order-action_next shop-btn shop-btn-primary']", driver).click()
//            Log.WriteLine("Далее")
//            //
//            DriverFindElement("//*[@class='shop-btn shop-btn-primary js-shop-confirm-order-button']", driver).click()
//            Log.WriteLine("Подтвердить оформление  заказа")
//            //
//            //DriverFindElement("(//*[@id='email'])[1]", driver).SendKeys("pain-net@yandex.ru");
//            //Log.WriteLine("Сбер ввод email");
//            ////
//            DriverFindElement("(//*[@id='cardnumber'])[1]", driver).sendKeys("4111 1111 1111 1111")
//            Log.WriteLine("Ваш банк ввод номера карты")
//            //
//            DriverFindElement("(//*[@name='expdate'])[1]", driver).sendKeys("1219")
//            Log.WriteLine("Ваш банк ввод месяц год")
//            //
//            DriverFindElement("(//*[@id='cvc'])[1]", driver).sendKeys("123")
//            Log.WriteLine("Ваш банк ввод CVC2")
//            //
//            DriverFindElement("(//*[@class='sbersafe-pay-button'])", driver).click()
//            Log.WriteLine("Оплатить")
//            //
//            DriverFindElement("(//*[@name='password'])", driver).sendKeys("12345678")
//            Log.WriteLine("password")
//            //
//            DriverFindElement("(//*[@value='Submit'])", driver).click()
//            Log.WriteLine("Submit")
            //
            Thread.sleep(2000)
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[3]", driver).click()
            Log.WriteLine("Мои заказы")
            //
            try
            {
                DriverFindElement("(//*[@class='profile-nav_link'])[1]", driver).click()
            }
            catch(e:Exception)
            {
                DriverFindElement("(//*[@class='profile-nav_link'])[2]", driver).click()
            }
            //
            DriverFindElement("//*[contains(text(), '30 %')]", driver)
            Log.WriteLine("Проверка на наличие промкода в заказах")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun CategoryProduct(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка-Магазин-Категории товаров" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[3]", driver).click()
            Log.WriteLine("Магазин")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[5]", driver).click()
            Log.WriteLine("Категории товаров")
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Создать категорию")
            //
            DriverFindElement("//*[@id='value']", driver).sendKeys("Товар")
            Log.WriteLine("Tовар название")
            //
            DriverFindElement("//*[@id='mnemonicName']", driver).sendKeys("TOVAR")
            Log.WriteLine("Алиас")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Добавить")
            //
            DriverNavigate(ShopPlaneta, driver)
            //
            DriverNavigate(ShopPlaneta, driver)
            //
            Thread.sleep(2900)
            //
            DriverFindElement("//*[@class='shop-menu_arr']", driver).click()
            Log.WriteLine("Все товары")
            //
            DriverFindElement("//*[@class='shop-menu_i']//*[contains(text(), 'Товар')]", driver)
            Log.WriteLine("Проверка на наличие раздела")
            //
            DriverFindElement("//*[@class='shop-menu_i']//*[contains(text(), 'Товар')]", driver).click()
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun LinksHome(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка-Магазин-Ссылки на главной" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[3]", driver).click()
            Log.WriteLine("Магазин")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[6]", driver).click()
            Log.WriteLine("Категории товаров")
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Создать ссылку на главной")
            //
            DriverFindElement("//*[@name='title']", driver).sendKeys("Что-то там")
            Log.WriteLine("Название")
            //
            DriverFindElement("//*[@name='titleEn']", driver).sendKeys("WWWWWWW")
            Log.WriteLine("Название на инглише")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("//*[@class='btn btn-xs btn-success ma-b-20']", driver).click()
            Log.WriteLine("Добавить ссылку")
            //
            DriverFindElement("(//*[@class='form-control'])[1]", driver).sendKeys("Заголовок")
            Log.WriteLine("Заголовок")
            //
            DriverFindElement("(//*[@class='form-control'])[2]", driver).sendKeys("Заголовок")
            Log.WriteLine("тип инглиш")
            //
            DriverFindElement("(//*[@class='form-control'])[3]", driver).sendKeys("Заголовок")
            Log.WriteLine("Ссылка")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }





}