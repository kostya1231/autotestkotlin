
import Z_Settings.Loger
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.apache.commons.io.FileUtils
import org.openqa.selenium.*
import org.openqa.selenium.logging.LogType
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.RemoteWebDriver
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import java.awt.Robot
import java.io.File
import java.io.FileWriter
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*
import java.util.logging.Level


open class SetupParallel {

    var TestName = ""
    var URL = "https://test.planeta.ru"
    var ShopPlaneta = "http://shop.test.planeta.ru"
    var SchoolPlaneta = "https://school.test.planeta.ru/"
    var prod = "https://planeta.ru"
    var VIP = "https://test.planeta.ru/welcome/bonuses.html"
    var Charity = "https://charity.test.planeta.ru/"
    var Mailer = "http://mailer-web.test.planeta.ru/campaign"
    var BiblioRodina = "https://biblio.test.planeta.ru/"
    var Kampus = "https://promo.test.planeta.ru/campus"
    var Tovar = "https://shop.test.planeta.ru/products/2816"
    var Order = "http://admin.test.planeta.ru/moderator/order/"
    var OrderVoznagrajdenie = "https://test.planeta.ru/campaigns/kalendar_nalichniki_2018/donate/278885"
    private val result by lazy { Int }
    var Stream1: Boolean = true
    var x = 1
    var userprofile = System.getenv("USERPROFILE")
    val robot = Robot()
    var CrowdProducerss ="https://school.test.planeta.ru/crowdproducers"
    var console =""
    var ConsoleLog = ""
    var line =""



    fun GetCurrentMethod(): String {

        return Throwable().stackTrace[1].methodName
    }


    //Генератор букв
    var wordLength = 10

    fun GetRandomString(): String {

        val r = Random() // Intialize a Random Number Generator with SysTime as the seed
        val sb = StringBuilder()
        for (i in 0 until wordLength) { // For each letter in the word
            val tmp = 'a' + r.nextInt('z' - 'a') // Generate a letter between a and z
            sb.append(tmp) // Add it to the String
        }
        return sb.toString()
    }




    //Генератор чисел
    var wordLength1 = 1
    fun GetRandomNumberString() : String {

        val r = Random()
        val sb = StringBuilder()
        for (i in 0 until wordLength1) {
            val tmp = r.nextInt(23)
            if(tmp == 0){
                println("Перезапуск")
                sb.append(1)
            }
            sb.append(tmp)
            println(sb)
        }
        return sb.toString()
    }



    fun  LogJS(driver: WebDriver) : String //Ошибки JS
    {
        try{
            while (Stream1){
                console = "C:\\Log Test\\Console log js\\$TestName"
                var writer = FileWriter("$console.txt",true)
                val logs = listOf(driver.manage().logs().get((LogType.BROWSER +  Level.SEVERE)).toList())

                var delete =logs.toString().trim('[',']',' ')
                // println(delete)

                writer.write(delete + System.getProperty("line.separator"))
                writer.close()


                if(!Stream1){

                    var nameFile = "$console.txt"

                    val lines = FileUtils.readLines(File(nameFile))
                    var i = lines.iterator()

                    val regex = ".*BsDatepickerModule.*read changelog.*".toRegex()

                    while(i.hasNext()){
                        var line = i.next()
                        if(line.toString().trim().isEmpty()){
                            i.remove()
                        }
                        if(regex.containsMatchIn(line.toString())){
                            i.remove()
                        }
                    }

                    FileUtils.writeLines(File(nameFile),lines)
                    break
                }
            }
        }catch (p:Exception){

        }
        return  ConsoleLog
    }

    fun Driver(): WebDriver {
//        val file = File("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Drivers\\chromedriver.exe")
//        System.setProperty("webdriver.chrome.driver", file.absolutePath)
//        val driver = ChromeDriver()

        val desiredcapabilites = DesiredCapabilities.chrome()
        desiredcapabilites.setCapability("enableVNC",true)
        desiredcapabilites.setCapability("enableVideo",false)
        desiredcapabilites.setCapability("name", TestName)
        desiredcapabilites.setCapability("videoName","$TestName.mp4")
        val driver = RemoteWebDriver(java.net.URL("http://vm-for-testers:4444/wd/hub"), desiredcapabilites)
        driver.manage().window().maximize()



        GlobalScope.launch { LogJS(driver) }
        GlobalScope.launch { Exception(driver) }

        return driver
    }


    fun Screen1(driver: WebDriver): Boolean {

        return try {

            driver.findElement(By.cssSelector(".toast-container .toast.toast-error")).getAttribute("#ff501a")
            true
        } catch (e: Exception) {
            false
        }
    }


    fun Exception(driver: WebDriver) {

        while (Stream1) {
            //println("Цикл старт")
            if (Screen1(driver)) {

                val save = "C:\\Log Test\\" + "Красный кирпич" + TestName + "______" + result + ".Png"
                val ss = (driver as TakesScreenshot).getScreenshotAs(OutputType.FILE)
                FileUtils.copyFile(ss, File(save))
            }
        }
    }

    fun Stop() {

        Stream1 = false
        println("Цикл стоп")
    }

    fun Stop1() {


    }


    fun DriverFindElement(xpath: String, driver: WebDriver): WebElement {

        try {
            val wait = WebDriverWait(driver, 8)
            Thread.sleep(500)
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)))
            driver.findElement(By.xpath(xpath))

        } catch (e: Exception) {
        }


        fun findEx() : Boolean //функа для ловли ошибок драйвера
        {
            return try {
                driver.findElement(By.xpath(xpath))
                false
            } catch(e:Exception) {
                true
            }
        }

        if (findEx()){
            println("Ошибка xpath$xpath")

            val ss1 = "C:\\Log Test\\Скрины\\$TestName.Png"
            val ss = (driver as TakesScreenshot).getScreenshotAs(OutputType.FILE)
            FileUtils.copyFile(ss, File(ss1))
        }


        val element = driver.findElement(By.xpath(xpath))



        return element
    }


    fun DriverFindElementCss(xpath: String, driver: WebDriver): WebElement {

        val wait = WebDriverWait(driver, 8)
        try {
            wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(xpath)))
            wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(xpath)))
        }catch (p:Exception){
        }
        Thread.sleep(400)


        val element: WebElement = driver.findElement(By.cssSelector(xpath))


        return element

    }

    fun DriverFindElementsCss(xpath: String, driver: WebDriver, num: Int): WebElement {

        val wait = WebDriverWait(driver, 8)
        try{
            wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(xpath)))
            wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(xpath)))[num]
        }catch (p:Exception){

        }
        Thread.sleep(400)


        val element = driver.findElements(By.cssSelector(xpath))[num]


        return element
    }

    fun DriverNavigate(URL: String, driver: WebDriver): WebDriver {


        driver.navigate().to(URL)

        return driver
    }


    fun VersionBrowser(driver: WebDriver): String {
        val capabilities = (driver as RemoteWebDriver).capabilities // версия браузера получение!
        var browser1 = "browserName"
        val name: String
        name = capabilities.getCapability(browser1).toString()


        if (name == "firefox") {
            browser1 = " = " + capabilities.getCapability("Имя Браузера") + "(Версия Браузера = " + capabilities.getCapability("browserVersion").toString() + ")"
        }
        if (name == "chrome") {
            browser1 = " = " + capabilities.getCapability("Имя Браузера") + "(Версия Браузера = " + capabilities.getCapability("version").toString() + ")"
        }
        return browser1
    }


    fun Temp() {

        for (i in 1..1) {

            try {
                val temp = File("$userprofile\\AppData\\Local\\Temp")
                println(temp)
                FileUtils.deleteDirectory(temp)

            } catch (ex: Exception) {

            }

        }
    }

    fun DeleteLogTest() {

        try {

            val delete = File("C:\\Log Test\\")
            FileUtils.deleteDirectory(delete)
        } catch (ex: Exception) {

        }

    }


    fun Clear() {

        try {

            Temp()

            DeleteLogTest()

            val folder = Paths.get("C:\\Log Test")
            Files.createDirectories(folder)

            val folder2 = Paths.get("C:\\Log Test\\Скрины")
            Files.createDirectories(folder2)

            val folder3 = Paths.get("C:\\Log Test\\Скрины Тестов")
            Files.createDirectories(folder3)

            val folder4 = Paths.get("C:\\Log Test\\Конечный результат")
            Files.createDirectories(folder4)

            val folder5 = Paths.get("C:\\Log Test\\Трафик тестов")
            Files.createDirectories(folder5)

            val folder6 = Paths.get("C:\\Log Test\\Console log js")
            Files.createDirectories(folder6)

            val folder7 = Paths.get("C:\\Log Test\\Скрины Паралельных тестов")
            Files.createDirectories(folder7)

            val folder8 = Paths.get("C:\\Log Test\\Возврат путей при ошибке")
            Files.createDirectories(folder8)
        } catch (ex: Exception) {


        }
    }


    //Шаблоны кода
    fun Avtorization_Adminka(driver: WebDriver, Log: Loger) {

        DriverNavigate(URL, driver)
        //
        DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
        Log.WriteLine("Вход")
        //
        DriverFindElement("//*[@class='login_title']", driver).click()
        Log.WriteLine("Сбросить евент")
        //
        DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
        Thread.sleep(4500)
        DriverFindElement("//*[@class='button button__def button__xs']",driver).click()
        Log.WriteLine("Хорошо")
    }


    fun Avtorization(driver: WebDriver, Log: Loger)  //uzname@yandex.ru
    {
        DriverNavigate(URL, driver)
        //
        DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
        Log.WriteLine("Вход")
        //
        DriverFindElement("//*[@class='login_title']", driver).click()
        Log.WriteLine("Сбросить евент")
        //
        DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uzname@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman9111") //Неправильный пароль
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
    }

    fun Avtorization_Uz4name(driver: WebDriver, Log: Loger)  //uz4name@yandex.ru
    {

        DriverNavigate(URL, driver)
        //
        DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
        Log.WriteLine("Вход")
        //
        DriverFindElement("//*[@class='login_title']", driver).click()
        Log.WriteLine("Сбросить евент")
        //
        DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz4name@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911") //Неправильный пароль
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
        Thread.sleep(3000)
        DriverFindElement("//*[@class='button button__def button__xs']",driver).click()
        Log.WriteLine("Хорошо")
    }

    fun Avtorization_Uz2name(driver: WebDriver, Log: Loger) {
        DriverNavigate(URL, driver)
        //
        DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
        Log.WriteLine("Вход")
        //
        DriverFindElement("//*[@class='login_title']", driver).click()
        Log.WriteLine("Сбросить евент")
        //
        DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz2name@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
        DriverFindElement("//*[@class='button button__def button__xs']",driver).click()
        //
        Log.WriteLine("Хорошо")
    }

    fun Avtorization_Uz3name(driver: WebDriver, Log: Loger) {
        DriverNavigate(URL, driver)
        //
        DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
        Log.WriteLine("Вход")
        //
        DriverFindElement("//*[@class='login_title']", driver).click()
        Log.WriteLine("Сбросить евент")
        //
        DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz3name@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
        DriverFindElement("//*[@class='button button__def button__xs']",driver).click()
        //
        Log.WriteLine("Хорошо")
    }

    fun Avtorization_Uzname_Prod(driver: WebDriver, Log: Loger)  //uzname@yandex.ru
    {

        DriverNavigate(prod, driver)
        //
        DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
        Log.WriteLine("Вход")
        //
        DriverFindElement("//*[@class='login_title']", driver).click()
        Log.WriteLine("Сбросить евент")
        //
        DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uzname@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911") //Неправильный пароль
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
    }

    fun Avtorization_Uz2name_ShopPlaneta(driver: WebDriver, Log: Loger)  //uz2name@yandex.ru
    {
        DriverNavigate(ShopPlaneta, driver)
        //
        DriverFindElement("//*[@class='header_user-link js-signup-link']", driver).click()
        Log.WriteLine("Вход")
        //
        //DriverFindElement("//*[@class='login_title']", driver).Click();
        //Log.WriteLine("Сбросить евент");
        ////
        Thread.sleep(100)
        DriverFindElement("//*[@class='form-control login-control_input js-input-email']", driver).sendKeys("uz2name@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911") //Неправильный пароль
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn js-btn-submit-login']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
        Thread.sleep(3000)
        DriverFindElement("//*[@class='button button__def button__xs']",driver).click()
        Log.WriteLine("Хорошо")
    }

    fun Avtorization_Uz4name_BiblioRodina(driver: WebDriver, Log: Loger) {
        DriverNavigate(BiblioRodina, driver)
        //
        DriverFindElement("//*[@class='header_user-link js-signup-link']", driver).click()
        Log.WriteLine("Вход")
        //
        //DriverFindElement("//*[@class='login_title']", driver).Click();
        //Log.WriteLine("Сбросить евент");
        ////
        DriverFindElement("//*[@class='form-control login-control_input js-input-email']", driver).sendKeys("uz4name@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn js-btn-submit-login']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
        Thread.sleep(500)
        DriverFindElement("//*[@class='button button__def button__xs']",driver).click()
        Log.WriteLine("Хорошо")
    }

    fun Avtorization_Uz5name(driver: WebDriver, Log: Loger) {
        DriverNavigate(URL, driver)
        //
        DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
        Log.WriteLine("Вход")
        //
        DriverFindElement("//*[@class='login_title']", driver).click()
        Log.WriteLine("Сбросить евент")
        //
        DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz5name@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
        Thread.sleep(500)
        DriverFindElement("//*[@class='button button__def button__xs']",driver).click()
        Log.WriteLine("Хорошо")
    }

    fun AvtirizationOther(driver: WebDriver,Log:Loger){

        DriverNavigate(URL, driver)
        //
        DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
        Log.WriteLine("Вход")
        //
        DriverFindElement("//*[@class='login_title']", driver).click()
        Log.WriteLine("Сбросить евент")
        //
        DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("pain-net+" + GetRandomString() + GetRandomNumberString() + "@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='login_btn-link']", driver).click()
        Log.WriteLine("Зарегестрироваться")
        //
        DriverFindElement("(//*[@class='form-control login-control_input ng-untouched ng-pristine ng-valid'])[3]", driver).sendKeys("nekroman911")
        Log.WriteLine("Подтвердите пароль")
        //
        DriverFindElement("//*[@class='form-ui-txt']", driver).click()
        Log.WriteLine("Я даю своё согласие")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
        Log.WriteLine("Зарегестрироваться")
        //
        Thread.sleep(1000)
        DriverFindElement("//*[@class='button button__def button__xs']",driver).click()
        Log.WriteLine("Хорошо")
    }

    fun Purhase_1(driver: WebDriver,Log:Loger){  //Купить в 1 м проекте

        DriverFindElement("//*[@class='header_search-btn']", driver).click()
        Log.WriteLine("Поиск")
        //
        DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
        Log.WriteLine("Название проекта")
        //
        DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[1]", driver).click()
        Log.WriteLine("Выбрать проект")
        //
        DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
        Log.WriteLine("Поддержать проект")
        //
        Thread.sleep(1000)
        DriverFindElement("(//*[@class='action-card_in'])", driver).click()
        Log.WriteLine("Выбрать вознаграждение")
        //
        Thread.sleep(1000)
        //
        DriverFindElement("//*[contains(text(), 'Купить (1 шт.)')]", driver).click()
        Log.WriteLine("Купить 1 шт")
        //
        fun FindEl(): Boolean {
            return try {
                DriverFindElement("(//*[@class='form-ui-label'])[1]", driver)
                //Согласен на условия
                true
            } catch (e: Exception) {
                false
            }
        }

        for (i in 0..60) {  //Клауд
            if (FindEl()) {


                DriverFindElement("//*[@id='payment-page.question-to-buyer-label']", driver).sendKeys("123")
                Log.WriteLine("Данные")
                //
                DriverFindElement("//*[@id='core.locality']", driver).sendKeys("Луна")
                Log.WriteLine("Населённый пункт")
                //
                DriverFindElement("(//*[@id='core.zip-code'])[1]", driver).sendKeys("11111111")
                Log.WriteLine("Индекс")
                //
                DriverFindElement("(//*[@id='core.address'])[1]", driver).sendKeys("Большой кратер")
                Log.WriteLine("Адрес")
                //
                DriverFindElement("(//*[@id='core.recipient-name'])[1]", driver).sendKeys("Николас кейдж")
                Log.WriteLine("ФИО получателя")
                //
                DriverFindElement("(//*[@id='core.contact-phone'])[1]", driver).sendKeys("84952222222")
                Log.WriteLine("контактный телефон")

                DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                Log.WriteLine("Даю свое согласие")

                DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver).click()
                Log.WriteLine("Карты других банков")
                //
                DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
                Log.WriteLine("Оплатить покупку")
                //
                DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
                Log.WriteLine("Оплата картой")
                //
                driver.switchTo().frame(0)
                DriverFindElement("(//*[@class='row'])[1]//input", driver).click()
                Log.WriteLine("клик по строке")
                DriverFindElement("(//*[@class='row'])[1]//input", driver).sendKeys("4111 1111 1111 1111")
                //
                DriverFindElement("(//*[@id='inputMonth'])[1]", driver).sendKeys("12")
                //
                DriverFindElement("(//*[@id='inputYear'])[1]", driver).sendKeys("19")
                //
                DriverFindElement("(//*[@data-mask='000'])[1]", driver).sendKeys("123")
                //
                DriverFindElement("(//*[@id='cardHolder'])[1]", driver).sendKeys("QWERTY")
                //
                DriverFindElement("(//*[@class='button'])[1]", driver).click()
                DriverFindElement("(//*[@class='button'])[2]", driver).click()
                Log.WriteLine("OK")
                driver.switchTo().defaultContent()
                break
            }
        }
//        DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
//        Log.WriteLine("Даю свое согласие")
//        //
//        DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
//        Log.WriteLine("Оплатить покупку")
//        //
//        DriverFindElement("(//*[@id='email'])[1]", driver).clear()
//        Log.WriteLine("clear")
//        //
//        DriverFindElement("(//*[@id='email'])[1]", driver).sendKeys("pain-net@yandex.ru")
//        //
//        DriverFindElement("(//*[@id='cardnumber'])[1]", driver).sendKeys("4111 1111 1111 1111")
//        Log.WriteLine("Ваш банк ввод номера карты")
//        //
//        DriverFindElement("(//*[@name='expdate'])[1]", driver).sendKeys("1219")
//        Log.WriteLine("Ваш банк ввод месяц год")
//        //
//        DriverFindElement("(//*[@id='cvc'])[1]", driver).sendKeys("123")
//        Log.WriteLine("Ваш банк ввод CVC2")
//        //
//        DriverFindElement("(//*[@class='sbersafe-pay-button'])", driver).click()
//        Log.WriteLine("Оплатить")
//        //
//        DriverFindElement("(//*[@name='password'])", driver).sendKeys("12345678")
//        Log.WriteLine("password")
//        //
//        DriverFindElement("(//*[@value='Submit'])", driver).click()
//        Log.WriteLine("Submit")
        //
        DriverFindElement("//*[@class='h-user_img']", driver).click()
        Log.WriteLine("Открыть меню")
        //
        DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
        Log.WriteLine("Выход")
        //
        DriverFindElement("//*[@class='btn btn-primary']", driver).click()
        Log.WriteLine("OK")
        //
        DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
        Log.WriteLine("Вход")
        //
        DriverFindElement("//*[@class='login_title']", driver).click()
        Log.WriteLine("Сбросить евент")
        //
        DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
        DriverFindElement("//*[@class='header_search-btn']", driver).click()
        Log.WriteLine("Поиск")
        //
        DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
        Log.WriteLine("Название проекта")
        //
        DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[1]", driver).click()
        Log.WriteLine("Выбрать проект")
        //
        DriverFindElement("(//*[@class='project-admin-controls_action-i'])[1]", driver).click()
        Log.WriteLine("Модерация")
        //
        DriverFindElement("(//*[@name='reason'])[1]", driver).sendKeys("1")
        Log.WriteLine("Введите причину аннулирования")
        //
        DriverFindElement("(//*[@class='btn btn-danger'])[1]", driver).click()
        Log.WriteLine("Аннулировать")
        //
        DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
        Log.WriteLine("Ok")
        //
        DriverNavigate(URL, driver)
        //
    }

    fun Purhase_2(driver: WebDriver,Log:Loger,textNew:String){  //Купить в 2 м проекте

        DriverFindElement("//*[@class='h-user_img']", driver).click()
        Log.WriteLine("Открыть меню")
        //
        DriverFindElement("(//*[@class='h-user-menu_ico'])[8]", driver).click()
        Log.WriteLine("Выход")
        //
        DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
        Log.WriteLine("Вход")
        //
        DriverFindElement("//*[@class='login_title']", driver).click()
        Log.WriteLine("Сбросить евент")
        //
        DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("$textNew@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
        DriverFindElement("//*[@class='header_search-btn']", driver).click()
        Log.WriteLine("Поиск")
        //
        DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
        Log.WriteLine("Название проекта")
        //
        DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[2]", driver).click()
        Log.WriteLine("Выбрать проект")
        //
        DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
        Log.WriteLine("Поддержать проект")
        //
        Thread.sleep(1000)
        DriverFindElement("(//*[@class='action-card_in'])", driver).click()
        Log.WriteLine("Выбрать вознаграждение")
        //
        Thread.sleep(1000)
        //
        DriverFindElement("//*[contains(text(), 'Купить (1 шт.)')]", driver).click()
        Log.WriteLine("Купить 1 шт")
        //
        fun FindEl(): Boolean {
            return try {
                DriverFindElement("(//*[@class='form-ui-label'])[1]", driver)
                //Согласен на условия
                true
            } catch (e: Exception) {
                false
            }
        }

        for (i in 0..60) {  //Клауд
            if (FindEl()) {

                DriverFindElement("//*[@id='payment-page.question-to-buyer-label']", driver).sendKeys("123")
                Log.WriteLine("Данные")
                DriverFindElement("//*[@id='core.locality']", driver).sendKeys("Луна")
                Log.WriteLine("Населённый пункт")
                //
                DriverFindElement("(//*[@id='core.zip-code'])[1]", driver).sendKeys("11111111")
                Log.WriteLine("Индекс")
                //
                DriverFindElement("(//*[@id='core.address'])[1]", driver).sendKeys("Большой кратер")
                Log.WriteLine("Адрес")
                //
                DriverFindElement("(//*[@id='core.recipient-name'])[1]", driver).sendKeys("Николас кейдж")
                Log.WriteLine("ФИО получателя")
                //
                DriverFindElement("(//*[@id='core.contact-phone'])[1]", driver).sendKeys("84952222222")
                Log.WriteLine("контактный телефон")

                DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
                Log.WriteLine("Даю свое согласие")

                DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver).click()
                Log.WriteLine("Карты других банков")
                //
                DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
                Log.WriteLine("Оплатить покупку")
                //
                DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
                Log.WriteLine("Оплата картой")
                //
                driver.switchTo().frame(0)
                DriverFindElement("(//*[@class='row'])[1]//input", driver).click()
                Log.WriteLine("клик по строке")
                DriverFindElement("(//*[@class='row'])[1]//input", driver).sendKeys("4111 1111 1111 1111")
                //
                DriverFindElement("(//*[@id='inputMonth'])[1]", driver).sendKeys("12")
                //
                DriverFindElement("(//*[@id='inputYear'])[1]", driver).sendKeys("19")
                //
                DriverFindElement("(//*[@data-mask='000'])[1]", driver).sendKeys("123")
                //
                DriverFindElement("(//*[@id='cardHolder'])[1]", driver).sendKeys("QWERTY")
                //
                DriverFindElement("(//*[@class='button'])[1]", driver).click()
                DriverFindElement("(//*[@class='button'])[2]", driver).click()
                Log.WriteLine("OK")
                driver.switchTo().defaultContent()
                break
            }
        }
//        DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
//        Log.WriteLine("Даю свое согласие")
//        //
//        DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
//        Log.WriteLine("Оплатить покупку")
//        //
//        DriverFindElement("(//*[@id='email'])[1]", driver).clear()
//        Log.WriteLine("clear")
//        //
//        DriverFindElement("(//*[@id='email'])[1]", driver).sendKeys("pain-net@yandex.ru")
//        //
//        DriverFindElement("(//*[@id='cardnumber'])[1]", driver).sendKeys("4111 1111 1111 1111")
//        Log.WriteLine("Ваш банк ввод номера карты")
//        //
//        DriverFindElement("(//*[@name='expdate'])[1]", driver).sendKeys("1219")
//        Log.WriteLine("Ваш банк ввод месяц год")
//        //
//        DriverFindElement("(//*[@id='cvc'])[1]", driver).sendKeys("123")
//        Log.WriteLine("Ваш банк ввод CVC2")
//        //
//        DriverFindElement("(//*[@class='sbersafe-pay-button'])", driver).click()
//        Log.WriteLine("Оплатить")
//        //
//        DriverFindElement("(//*[@name='password'])", driver).sendKeys("12345678")
//        Log.WriteLine("password")
//        //
//        DriverFindElement("(//*[@value='Submit'])", driver).click()
//        Log.WriteLine("Submit")
        //
        DriverFindElement("//*[@class='h-user_img']", driver).click()
        Log.WriteLine("Открыть меню")
        //
        DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
        Log.WriteLine("Выход")
        //
        DriverFindElement("//*[@class='btn btn-primary']", driver).click()
        Log.WriteLine("OK")
        //
        DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
        Log.WriteLine("Вход")
        //
        DriverFindElement("//*[@class='login_title']", driver).click()
        Log.WriteLine("Сбросить евент")
        //
        DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
        DriverFindElement("//*[@class='header_search-btn']", driver).click()
        Log.WriteLine("Поиск")
        //
        DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
        Log.WriteLine("Название проекта")
        //
        DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[2]", driver).click()
        Log.WriteLine("Выбрать проект")
        //
        DriverFindElement("(//*[@class='project-admin-controls_action-i'])[1]", driver).click()
        Log.WriteLine("Модерация")
        //
        DriverFindElement("(//*[@name='reason'])[1]", driver).sendKeys("1")
        Log.WriteLine("Введите причину аннулирования")
        //
        DriverFindElement("(//*[@class='btn btn-danger'])[1]", driver).click()
        Log.WriteLine("Аннулировать")
        //
        DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
        Log.WriteLine("Ok")
        //
        DriverNavigate(URL, driver)
        //
    }

    fun Purhase_3(driver: WebDriver,Log:Loger,textNew:String){  //Купить в 3 м проекте

        DriverFindElement("//*[@class='h-user_img']", driver).click()
        Log.WriteLine("Открыть меню")
        //
        DriverFindElement("(//*[@class='h-user-menu_ico'])[8]", driver).click()
        Log.WriteLine("Выход")
        //
        DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
        Log.WriteLine("Вход")
        //
        DriverFindElement("//*[@class='login_title']", driver).click()
        Log.WriteLine("Сбросить евент")
        //
        DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("$textNew@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
        DriverFindElement("//*[@class='header_search-btn']", driver).click()
        Log.WriteLine("Поиск")
        //
        DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
        Log.WriteLine("Название проекта")
        //
        DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[3]", driver).click()
        Log.WriteLine("Выбрать проект")
        //
        DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
        Log.WriteLine("Поддержать проект")
        //
        Thread.sleep(1000)
        DriverFindElement("(//*[@class='action-card_in'])", driver).click()
        Log.WriteLine("Выбрать вознаграждение")
        //
        Thread.sleep(1000)
        //
        DriverFindElement("//*[contains(text(), 'Купить (1 шт.)')]", driver).click()
        Log.WriteLine("Купить 1 шт")
        //
        //
        fun FindEl(): Boolean {
            return try {
                DriverFindElement("(//*[@class='form-ui-label'])[1]", driver)
                //Согласен на условия
                true
            } catch (e: Exception) {
                false
            }
        }

        for (i in 0..60) {  //Клауд
            if (FindEl()) {

                DriverFindElement("//*[@id='payment-page.question-to-buyer-label']", driver).sendKeys("123")
                Log.WriteLine("Данные")

                DriverFindElement("//*[@id='core.locality']", driver).sendKeys("Луна")
                Log.WriteLine("Населённый пункт")
                //
                DriverFindElement("(//*[@id='core.zip-code'])[1]", driver).sendKeys("11111111")
                Log.WriteLine("Индекс")
                //
                DriverFindElement("(//*[@id='core.address'])[1]", driver).sendKeys("Большой кратер")
                Log.WriteLine("Адрес")
                //
                DriverFindElement("(//*[@id='core.recipient-name'])[1]", driver).sendKeys("Николас кейдж")
                Log.WriteLine("ФИО получателя")
                //
                DriverFindElement("(//*[@id='core.contact-phone'])[1]", driver).sendKeys("84952222222")
                Log.WriteLine("контактный телефон")

                DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
                Log.WriteLine("Даю свое согласие")

                DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver).click()
                Log.WriteLine("Карты других банков")
                //
                DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
                Log.WriteLine("Оплатить покупку")
                //
                DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
                Log.WriteLine("Оплата картой")
                //
                driver.switchTo().frame(0)
                DriverFindElement("(//*[@class='row'])[1]//input", driver).click()
                Log.WriteLine("клик по строке")
                DriverFindElement("(//*[@class='row'])[1]//input", driver).sendKeys("4111 1111 1111 1111")
                //
                DriverFindElement("(//*[@id='inputMonth'])[1]", driver).sendKeys("12")
                //
                DriverFindElement("(//*[@id='inputYear'])[1]", driver).sendKeys("19")
                //
                DriverFindElement("(//*[@data-mask='000'])[1]", driver).sendKeys("123")
                //
                DriverFindElement("(//*[@id='cardHolder'])[1]", driver).sendKeys("QWERTY")
                //
                DriverFindElement("(//*[@class='button'])[1]", driver).click()
                DriverFindElement("(//*[@class='button'])[2]", driver).click()
                Log.WriteLine("OK")
                driver.switchTo().defaultContent()
                break
            }
        }
//        DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
//        Log.WriteLine("Даю свое согласие")
//        //
//        DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
//        Log.WriteLine("Оплатить покупку")
//        //
//        DriverFindElement("(//*[@id='email'])[1]", driver).clear()
//        Log.WriteLine("clear")
//        //
//        DriverFindElement("(//*[@id='email'])[1]", driver).sendKeys("pain-net@yandex.ru")
//        //
//        DriverFindElement("(//*[@id='cardnumber'])[1]", driver).sendKeys("4111 1111 1111 1111")
//        Log.WriteLine("Ваш банк ввод номера карты")
//        //
//        DriverFindElement("(//*[@name='expdate'])[1]", driver).sendKeys("1219")
//        Log.WriteLine("Ваш банк ввод месяц год")
//        //
//        DriverFindElement("(//*[@id='cvc'])[1]", driver).sendKeys("123")
//        Log.WriteLine("Ваш банк ввод CVC2")
//        //
//        DriverFindElement("(//*[@class='sbersafe-pay-button'])", driver).click()
//        Log.WriteLine("Оплатить")
//        //
//        DriverFindElement("(//*[@name='password'])", driver).sendKeys("12345678")
//        Log.WriteLine("password")
//        //
//        DriverFindElement("(//*[@value='Submit'])", driver).click()
//        Log.WriteLine("Submit")
        //
        DriverFindElement("//*[@class='h-user_img']", driver).click()
        Log.WriteLine("Открыть меню")
        //
        DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
        Log.WriteLine("Выход")
        //
        DriverFindElement("//*[@class='btn btn-primary']", driver).click()
        Log.WriteLine("OK")
        //
        DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
        Log.WriteLine("Вход")
        //
        DriverFindElement("//*[@class='login_title']", driver).click()
        Log.WriteLine("Сбросить евент")
        //
        DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
        DriverFindElement("//*[@class='header_search-btn']", driver).click()
        Log.WriteLine("Поиск")
        //
        DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
        Log.WriteLine("Название проекта")
        //
        DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[3]", driver).click()
        Log.WriteLine("Выбрать проект")
        //
        DriverFindElement("(//*[@class='project-admin-controls_action-i'])[1]", driver).click()
        Log.WriteLine("Модерация")
        //
        DriverFindElement("(//*[@name='reason'])[1]", driver).sendKeys("1")
        Log.WriteLine("Введите причину аннулирования")
        //
        DriverFindElement("(//*[@class='btn btn-danger'])[1]", driver).click()
        Log.WriteLine("Аннулировать")
        //
        DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
        Log.WriteLine("Ok")
        //
        DriverNavigate(URL, driver)
        //
    }


    fun ContrAgent(driver: WebDriver, Log: Loger) {


        var world = 10
        val r = Random()
        var number = StringBuilder()
        for (i in 0 until world) {
            val tmp = r.nextInt(10)
            number.append(tmp)
        }

        var world1 = 9
        val r1 = Random()
        var number1 = StringBuilder()
        for (i in 0 until world1) {
            val tmp = r1.nextInt(9)
            number1.append(tmp)
        }



        DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[2]", driver).sendKeys("КонтрАгент")
        Log.WriteLine("ФИО")
        //
        DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[3]", driver).sendKeys("КонтрАгент Агентович")
        Log.WriteLine("ФИО + Инициалы")
        //
        DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[4]", driver).sendKeys("КонтрАгент Агент Агентович")
        Log.WriteLine("Ответственное лицо в родительном падеже(для Физ. и Юр. лиц)")
        //
        DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[5]", driver).sendKeys("89776662313")
        Log.WriteLine("Телефон")
        //
        DriverFindElement("//*[@id='cityNameRus']", driver).sendKeys("Москва")
        Log.WriteLine("Москва")
        //
        DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[8]", driver).click()
        for (i in 1..3) {
            DriverFindElement("(//*[@class='picker-switch'])[" + i.toString() + "]", driver).click()
        }
        //
        DriverFindElement("(//tbody)[4]//tr//td//span[1]", driver).click()
        DriverFindElement("(//tbody)[3]//tr//td//span[1]", driver).click()
        DriverFindElement("(//tbody)[2]//tr//td//span[1]", driver).click()
        DriverFindElement("(//tbody)[1]//tr[3]//td[3]", driver).click()
        Log.WriteLine("Дата рождения")
        //
        DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[9]", driver).sendKeys(number.toString())
        Log.WriteLine("ИНН")
        //
        DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[11]", driver).sendKeys(number.toString() + "123")
        Log.WriteLine("ОГРН")
        //
        DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[12]", driver).sendKeys("jnfjgjdfhdgsyufrtd")
        Log.WriteLine("Прочие данные")
        //
        DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[13]", driver).sendKeys("jnfjgjdfhdgsyufrtdjnfjgjdfhdgsyufrtd")
        Log.WriteLine("Юридический адрес  Зарег")
        //
        DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[14]", driver).sendKeys("jnfjgjdfhdgsyufrtdjnfjgjdfhdgsyufrtd")
        Log.WriteLine("Фактический  адрес")
        //
        DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[15]", driver).sendKeys("jnfjgjdfhdgsyufrtdjnfjgjdfhdgsyufrtd")
        Log.WriteLine("Кем выдан паспорт")
        //
        DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[16]", driver).sendKeys("770-060")
        Log.WriteLine("Кем выдан паспорт")
        //
        DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[18]", driver).click()
        //
        for (i in 1..3) {
            DriverFindElement("(//*[@class='picker-switch'])[" + i.toString() + "]", driver).click()
        }
        //
        DriverFindElement("(//tbody)[4]//tr//td//span[1]", driver).click()
        DriverFindElement("(//tbody)[3]//tr//td//span[1]", driver).click()
        DriverFindElement("(//tbody)[2]//tr//td//span[1]", driver).click()
        DriverFindElement("(//tbody)[1]//tr[3]//td[3]", driver).click()
        Log.WriteLine("Когда выдан паспорт")
        //
        DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[19]", driver).sendKeys(number1.toString())
        Log.WriteLine("Банк получателя")
        //
        DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[20]", driver).sendKeys(number1.toString())
        Log.WriteLine("БИК")
        //
        DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[21]", driver).sendKeys(number1.toString())
        Log.WriteLine("КПП")
        //
        DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[22]", driver).sendKeys(number.toString() + number.toString())
        Log.WriteLine("Расчётный счет")
        //
        DriverFindElement("(//*[@id='parse-form']//*[@class='form-control'])[23]", driver).sendKeys(number.toString() + number.toString())
        Log.WriteLine("Кор.Счет")
        //
        DriverFindElement("//*[@class='btn btn-primary js-want-save']", driver).click()
        Log.WriteLine("Сохранить")
        //
        DriverFindElement("//*[@class='btn btn-primary']", driver).click()
        Log.WriteLine("OK")
        //
    }
}






