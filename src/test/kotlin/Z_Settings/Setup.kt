
import Z_Settings.Loger
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.apache.commons.io.FileUtils
import org.openqa.selenium.*
import org.openqa.selenium.logging.LogType
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.LocalFileDetector
import org.openqa.selenium.remote.RemoteWebDriver
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import java.awt.Robot
import java.awt.Toolkit
import java.awt.datatransfer.Clipboard
import java.awt.datatransfer.StringSelection
import java.awt.event.KeyEvent
import java.awt.event.MouseEvent
import java.io.*
import java.nio.file.Files
import java.nio.file.Paths
import java.text.SimpleDateFormat
import java.util.*


open class Setup {
    var URL = "http://test.planeta.ru"
    var ShopPlaneta = "http://shop.test.planeta.ru"
    var SchoolPlaneta = "https://school.test.planeta.ru/"
    var prod = "https://planeta.ru"
    var VIP = "https://test.planeta.ru/welcome/bonuses.html"
    var Charity = "https://charity.test.planeta.ru/"
    var Mailer = "http://mailer-web.test.planeta.ru/campaign"
    var BiblioRodina = "https://biblio.test.planeta.ru/"
    var Kampus = "https://promo.test.planeta.ru/campus"
    var Job = "https://test.planeta.ru/api/admin/banner-reload.html"
    var JobVIP = "http://test.planeta.ru:8096/main/"
    var OrderVoznagrajdenie = "https://test.planeta.ru/campaigns/ponasenkov_vmv/donate/343231"
    var Stream1: Boolean = true
    var TestName = ""
    var result = 1
    var sourcePath: String = "C:\\Log Test\\Скрины\\"
    var targetPath = "C:\\Log Test\\Скрины Тестов\\"
    var  numberproject:String = ""
    var project: String =""
    val robot = Robot()
    var userprofile = System.getenv("USERPROFILE")
    var xpath:String =""
    var console =""
    var ConsoleLog = ""



    fun GetCurrentMethod(): String {

        return Throwable().stackTrace[1].methodName
    }


    fun LeftClick(x: Int, y: Int) {
        robot.mouseMove(x, y)
        robot.delay(3)
        robot.mousePress(MouseEvent.BUTTON1_MASK)
        robot.mouseRelease(MouseEvent.BUTTON1_MASK)
    }


    //Генератор букв
    var wordLength = 3
    fun GetRandomString(): String {
        val r = Random()
        val sb = StringBuilder()
        for (i in 0 until wordLength) {
            val tmp = 'a' + r.nextInt('z' - 'a')
            sb.append(tmp)
            println(sb.toString())
        }
        return sb.toString()
    }


    //Генератор чисел
    var wordLength1 = 1
    fun GetRandomNumberString() : String {

        val r = Random()
        val sb = StringBuilder()
        for (i in 0 until wordLength1) {
            val tmp = r.nextInt(23)
            if(tmp == 0){
                println("Перезапуск")
                sb.append(1)
            }
            sb.append(tmp)
            println(sb)
        }
       return sb.toString()
    }


    fun robot() {
        Thread.sleep(1000)
        var str = StringSelection("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")

        val tol = Toolkit.getDefaultToolkit()
        val cl: Clipboard = tol.systemClipboard
        cl.setContents(str, null)

        robot.keyPress(KeyEvent.VK_CONTROL)
        robot.keyPress(KeyEvent.VK_V)
        robot.keyRelease(KeyEvent.VK_V)
        robot.keyRelease(KeyEvent.VK_CONTROL)
        Thread.sleep(500)
        robot.keyPress(KeyEvent.VK_ENTER)
        robot.keyRelease(KeyEvent.VK_ENTER)
    }


    fun  LogJS(driver: WebDriver) : String //Ошибки JS
    {
        try{
            while (Stream1){
                console = "C:\\Log Test\\Console log js\\$TestName"
                var writer = FileWriter("$console.txt",true)
                val logs = listOf(driver.manage().logs().get((LogType.BROWSER)).toList())

                var delete =logs.toString().trim('[',']',' ')

                writer.write(delete + System.getProperty("line.separator"))
                writer.close()


                if(!Stream1){

                    var nameFile = "$console.txt"

                    val lines = FileUtils.readLines(File(nameFile))
                    var i = lines.iterator()

                    val regex = ".*BsDatepickerModule.*read changelog.*".toRegex()


                    while(i.hasNext()){
                        var line = i.next()
                        if(line.toString().trim().isEmpty()){
                            i.remove()
                        }
                        if(regex.containsMatchIn(line.toString())){
                            i.remove()
                        }
                    }

                    FileUtils.writeLines(File(nameFile),lines)
                    break
                }
            }
        }catch (p:Exception){

        }
        return  ConsoleLog
    }

    fun Driver(): WebDriver {

//        val file = File("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Drivers\\chromedriver.exe")
//        System.setProperty("webdriver.chrome.driver", file.absolutePath)
//        val driver = ChromeDriver()

        val desiredcapabilites = DesiredCapabilities.chrome()
        desiredcapabilites.setCapability("enableVNC",true)
      //  desiredcapabilites.setCapability("enableVideo",true)
        desiredcapabilites.setCapability("name",TestName)
     ///   desiredcapabilites.setCapability("videoName","$TestName.mp4")
        val driver = RemoteWebDriver(java.net.URL("http://vm-for-testers:4444/wd/hub"), desiredcapabilites)
        driver.manage().window().maximize()
        driver.fileDetector = LocalFileDetector()

        GlobalScope.launch { LogJS(driver) }

        try {
            driver.manage().window().maximize()
            GlobalScope.launch { Exception(driver) }
        }
        catch (e:Exception){

        }

        return  driver
    }


    fun Good(driver: WebDriver) {


        while (Stream1) {

            try {
                Thread.sleep(100)
                driver.findElement(By.xpath("//*[@class='message message__ico message__ico-info cookies ng-star-inserted']//button")).click()
            } catch (e: Exception) {

            }

//            if (good(driver)) {
//
//
//            }
        }
    }

    fun good(driver: WebDriver): Boolean {


        return try {

            driver.findElement(By.xpath("//*[@class='button button__def button__xs']"))
            true
        } catch (e: Exception) {

            false
        }

    }

    fun Screen1(driver: WebDriver): Boolean {

        return try {

            driver.findElement(By.cssSelector(".toast-container .toast.toast-error")).getAttribute("#ff501a")
            true
        } catch (e: Exception) {
            false
        }
    }

    fun Exception(driver: WebDriver) {

        while (Stream1) {
            // System.out.println("Цикл старт")
            if (Screen1(driver)) {

                val save = "C:\\Log common\\" + "Красный кирпич" + TestName + "______" + result + ".Png"
                val ss = (driver as TakesScreenshot).getScreenshotAs(OutputType.FILE)
                FileUtils.copyFile(ss, File(save))
            }
        }
    }

    fun Stop() {

        Stream1 = false
        System.out.println("Цикл стоп")


        val driver: WebDriver

        val endresult: String = "C:\\Log Test\\Конечный результат\\Конечный результат Теста!!$TestName!!!!.Png"


//        val ss = (driver as TakesScreenshot).getScreenshotAs(OutputType.FILE)
//        FileUtils.copyFile(ss, File(endresult))


//        File(targetPath).walk().forEach {
//            System.out.println(it)
//
//            File(sourcePath).copyTo(it)
//        }
//
//
//        System.out.println()

    }


    fun Stop1() {


    }


    private var count = 1
    fun DriverFindElement(xpath: String, driver: WebDriver): WebElement {
        try {
            val wait = WebDriverWait(driver, 8)
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)))
            Thread.sleep(300)

            val point: Point = driver.findElement(By.xpath(xpath)).location

            fun Scroll() {
                try{
                    val js: JavascriptExecutor = driver as JavascriptExecutor
                    val js1 = String.format("window.scroll(0," + point.getY() + ")", point)
                    js.executeScript(js1)
                    Thread.sleep(200)
                }
                catch (p:Exception){

                }
            }
            try{
                GlobalScope.launch { Scroll() }
            }catch (d:Exception){
                GlobalScope.launch { Scroll() }
            }
        } catch (e: Exception) {
        }


        fun findEx() : Boolean //функа для ловли ошибок драйвера
        {
            return try {
               driver.findElement(By.xpath(xpath))
                false
            } catch(e:Exception) {
                true
            }
        }

        if (findEx()){
            println("Ошибка xpath$xpath")

            val ss1 = "C:\\Log Test\\Скрины\\$TestName.Png"
            val ss = (driver as TakesScreenshot).getScreenshotAs(OutputType.FILE)
            FileUtils.copyFile(ss, File(ss1))
        }

        val element = driver.findElement(By.xpath(xpath))


        count++
        return element
    }

//    fun TestFile(){
//
//        try {
//            var lines = 1
//            var buf = BufferedReader(FileReader("C:\\Log Test\\Файлик с путями xpath.txt"))
//            var sb = StringBuilder()
//            var line = buf.readLine()
//
//            while (line != null) {
//                sb.append(line)
//                sb.append(System.lineSeparator())
//                line = buf.readLine()
//
//                val arr = line.split(":")
//                println(arr)
//
//                //  lines++
//                // println("$lines")
//            }
//        }catch (er:Exception){
//
//        }
//    }



    fun DriverFindElementCss(xpath: String, driver: WebDriver): WebElement {

        val wait = WebDriverWait(driver, 8)
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)))
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(xpath)))
        Thread.sleep(400)


        val element: WebElement = driver.findElement(By.xpath(xpath))
        // Scroll(driver, element)

        return element

    }

    fun DriverFindElementsCss(xpath: String, driver: WebDriver, num: Int): WebElement {

        val wait = WebDriverWait(driver, 8)
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)))
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(xpath)))[num]
        Thread.sleep(400)


         val element = driver.findElement(By.xpath(xpath)) //Доделать числовой параметр
//        Scroll(driver, element)

        return element

    }

    fun DriverNavigate(URL: String, driver: WebDriver): WebDriver {


        driver.navigate().to(URL)

        return driver
    }

    fun VersionBrowser(driver: WebDriver): String {
        val capabilities = (driver as RemoteWebDriver).capabilities // версия браузера получение!
        var browser1 = "browserName"
        val name: String
        name = capabilities.getCapability(browser1).toString()


        if (name == "firefox") {
            browser1 = " = " + capabilities.getCapability("Имя Браузера") + "(Версия Браузера = " + capabilities.getCapability("browserVersion").toString() + ")"
        }
        if (name == "chrome") {
            browser1 = " = " + capabilities.getCapability("Имя Браузера") + "(Версия Браузера = " + capabilities.getCapability("version").toString() + ")"
        }
        return browser1
    }

    fun Date(): String {

        val timedate = SimpleDateFormat("yyyy_MM_dd_HH:mm:ss").format(Calendar.getInstance().time)
        System.out.println(timedate)

        return timedate
    }

    fun Foto(driver: WebDriver): String {

        val ss1: String = "C:\\Log Test\\Скрины\\" + TestName + "_" + result + ".Png"
        val path: String = "C:\\Log Test\\Скрины Тестов\\" + TestName
        val dir = Paths.get(path)
        Files.createDirectories(dir)

        val ss = (driver as TakesScreenshot).getScreenshotAs(OutputType.FILE)
        FileUtils.copyFile(ss, File(ss1))

        result++

        return ss1
    }



    fun HTMLBanner(driver: WebDriver){

        val file = File("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\HTMLBanner\\HTMLBanner.txt")


        var  buf = BufferedReader(FileReader(file))
        var sb = StringBuilder()

        var line =buf.readLine()
        while(line != null){
            sb.append(line)
            sb.append(System.lineSeparator())
            line = buf.readLine()
        }

        var ev = sb.toString()
        val el = DriverFindElement("//*[@formcontrolname='html']", driver)
        el.sendKeys(ev)
        buf.close()


        val file2 = File("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\HTMLBanner\\HTMLBanner2.txt")

        var  buf2 = FileInputStream(file2)
        var sb2 = StringBuilder()
       val r = BufferedReader(InputStreamReader(buf2, "Cp1251"))
        var line2 =r.readLine()

        while(line2 != null){
            sb2.append(line2)
            sb2.append(System.lineSeparator())
            line2 = r.readLine()
        }

        var ev2 = sb2.toString()
        val el2 = DriverFindElement("//*[@formcontrolname='html']", driver)
        el2.sendKeys(ev2)
        buf2.close()

        //  val elActions = Actions(driver)
      //  elActions.keyDown(Keys.CONTROL).sendKeys(el,"v").keyUp(Keys.CONTROL).build().perform()
    }


    fun HTMLBanner2(driver: WebDriver){

        val file = File("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\HTMLBanner\\HTMLBanner.txt")


        var  buf = BufferedReader(FileReader(file))
        var sb = StringBuilder()

        var line =buf.readLine()
        while(line != null){
            sb.append(line)
            sb.append(System.lineSeparator())
            line = buf.readLine()
        }

        var ev = sb.toString()
        val el = DriverFindElement("//*[@id='advertismentBannerHtml']", driver)
        el.sendKeys(ev)
        buf.close()


        val file2 = File("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\HTMLBanner\\HTMLBanner2.txt")

        var  buf2 = FileInputStream(file2)
        var sb2 = StringBuilder()
        val r = BufferedReader(InputStreamReader(buf2, "Cp1251"))
        var line2 =r.readLine()

        while(line2 != null){
            sb2.append(line2)
            sb2.append(System.lineSeparator())
            line2 = r.readLine()
        }

        var ev2 = sb2.toString()
        val el2 = DriverFindElement("//*[@id='advertismentBannerHtml']", driver)
        el2.sendKeys(ev2)
        buf2.close()
    }


    //Шаблоны авторизации
    fun Avtorization_Adminka(driver: WebDriver, Log: Loger) {

        DriverNavigate(URL, driver)
        //
        DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
        Log.WriteLine("Вход")
        //
        DriverFindElement("//*[@class='login_title']", driver).click()
        Log.WriteLine("Сбросить евент")
        //
        DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
        Thread.sleep(500)
        DriverFindElement("//*[@class='button button__def button__xs']",driver).click()
        Log.WriteLine("Хорошо")
    }


    fun Avtorization_Uz2name(driver: WebDriver, Log: Loger) {
        DriverNavigate(URL, driver)
        //
        DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
        Log.WriteLine("Вход")
        //
        DriverFindElement("//*[@class='login_title']", driver).click()
        Log.WriteLine("Сбросить евент")
        //
        DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz2name@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
        Thread.sleep(500)
        DriverFindElement("//*[@class='button button__def button__xs']",driver).click()
        Log.WriteLine("Хорошо")
    }

    fun Avtorization_Uz3name(driver: WebDriver, Log: Loger) {
        DriverNavigate(URL, driver)
        //
        DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
        Log.WriteLine("Вход")
        //
        DriverFindElement("//*[@class='login_title']", driver).click()
        Log.WriteLine("Сбросить евент")
        //
        DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz3name@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
        Thread.sleep(500)
        DriverFindElement("//*[@class='button button__def button__xs']",driver).click()
        Log.WriteLine("Хорошо")
    }

    fun Avtorization_Uz4name(driver: WebDriver, Log: Loger)  //uz4name@yandex.ru
    {

        DriverNavigate(URL, driver)
        //
        DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
        Log.WriteLine("Вход")
        //
        DriverFindElement("//*[@class='login_title']", driver).click()
        Log.WriteLine("Сбросить евент")
        //
        DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz4name@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911") //Неправильный пароль
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
        Thread.sleep(3500)
        //
        DriverFindElement("//*[@class='button button__def button__xs']",driver).click()
        Log.WriteLine("Хорошо")
    }

    fun Avtorization_Uz5name(driver: WebDriver, Log: Loger) {
        DriverNavigate(URL, driver)
        //
        DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
        Log.WriteLine("Вход")
        //
        DriverFindElement("//*[@class='login_title']", driver).click()
        Log.WriteLine("Сбросить евент")
        //
        DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz5name@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
        Thread.sleep(500)
        DriverFindElement("//*[@class='button button__def button__xs']",driver).click()
        Log.WriteLine("Хорошо")
    }


    fun Avtorization_Uz2name_ShopPlaneta(driver: WebDriver, Log: Loger)  //uz2name@yandex.ru
    {
        DriverNavigate(ShopPlaneta, driver)
        //
        DriverFindElement("//*[@class='header_user-link js-signup-link']", driver).click()
        Log.WriteLine("Вход")
        //
        //DriverFindElement("//*[@class='login_title']", driver).Click();
        //Log.WriteLine("Сбросить евент");
        ////
        Thread.sleep(100)
        DriverFindElement("//*[@class='form-control login-control_input js-input-email']", driver).sendKeys("uz2name@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911") //Неправильный пароль
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn js-btn-submit-login']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
        DriverFindElement("//*[@class='button button__def button__xs']",driver).click()
        //
        Log.WriteLine("Хорошо")
    }

    fun Avtorization_Uz4name_BiblioRodina(driver: WebDriver, Log: Loger) {
        DriverNavigate(BiblioRodina, driver)
        //
        DriverFindElement("//*[@class='header_user-link js-signup-link']", driver).click()
        Log.WriteLine("Вход")
        //
        //DriverFindElement("//*[@class='login_title']", driver).Click();
        //Log.WriteLine("Сбросить евент");
        ////
        DriverFindElement("//*[@class='form-control login-control_input js-input-email']", driver).sendKeys("uz4name@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn js-btn-submit-login']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
        DriverFindElement("//*[@class='button button__def button__xs']",driver).click()
        Log.WriteLine("Хорошо")
    }

    fun AvtirizationOther(driver: WebDriver,Log:Loger){

        DriverNavigate(URL, driver)
        //
        DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
        Log.WriteLine("Вход")
        //
        DriverFindElement("//*[@class='login_title']", driver).click()
        Log.WriteLine("Сбросить евент")
        //
        DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("pain-net+" + GetRandomString() + GetRandomNumberString() + "@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='login_btn-link']", driver).click()
        Log.WriteLine("Зарегестрироваться")
        //
        DriverFindElement("(//*[@class='form-control login-control_input ng-untouched ng-pristine ng-valid'])[3]", driver).sendKeys("nekroman911")
        Log.WriteLine("Подтвердите пароль")
        //
        DriverFindElement("//*[@class='form-ui-txt']", driver).click()
        Log.WriteLine("Я даю своё согласие")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
        Log.WriteLine("Зарегестрироваться")
        //
        Thread.sleep(1000)
        DriverFindElement("//*[@class='button button__def button__xs']",driver).click()
        Log.WriteLine("Хорошо")
    }

    fun AvtorizationOtherUz4name(driver: WebDriver,Log:Loger){
        DriverNavigate(URL, driver)
        //
        DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
        Log.WriteLine("Вход")
        //
        DriverFindElement("//*[@class='login_title']", driver).click()
        Log.WriteLine("Сбросить евент")
        //
        DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz4name+" + GetRandomString() + "@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='login_btn-link']", driver).click()
        Log.WriteLine("Зарегестрироваться")
        //
        DriverFindElement("(//*[@class='form-control login-control_input ng-untouched ng-pristine ng-valid'])[3]", driver).sendKeys("nekroman911")
        Log.WriteLine("Подтвердите пароль")
        //
        DriverFindElement("//*[@class='form-ui-txt']", driver).click()
        Log.WriteLine("Я даю своё согласие")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
        Log.WriteLine("Зарегестрироваться")
        Thread.sleep(3000)
        DriverFindElement("//*[@class='button button__def button__xs']",driver).click()
        Log.WriteLine("Хорошо")
    }

    fun AvtorProject(driver: WebDriver,Log:Loger){
        DriverNavigate(URL, driver)
        //
        DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
        Log.WriteLine("Вход")
        //
        DriverFindElement("//*[@class='login_title']", driver).click()
        Log.WriteLine("Сбросить евент")
        //
        DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("avtor2.avtorproject@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
        Thread.sleep(3000)
        DriverFindElement("//*[@class='button button__def button__xs']",driver).click()
        Log.WriteLine("Хорошо")
    }


    fun AvtorProject23666(driver: WebDriver,Log: Loger){

        DriverNavigate(URL, driver)
        //
        DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
        Log.WriteLine("Вход")
        //
        DriverFindElement("//*[@class='login_title']", driver).click()
        Log.WriteLine("Сбросить евент")
        //
        DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("avtor2313666@yandex.ru")
        Log.WriteLine("Ввод логина")
        //
        DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
        Log.WriteLine("Ввод пароля")
        //
        DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
        Log.WriteLine("Войти на сайт")
        //
        Thread.sleep(1000)
        DriverFindElement("//*[@class='button button__def button__xs']",driver).click()
        Log.WriteLine("Хорошо")
    }

}
