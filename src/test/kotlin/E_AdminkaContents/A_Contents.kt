import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.WebDriver
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

class A_Contents : Setup(){

    @Test
    fun Posts(){

        try{
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка-Контент-Проект" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[5]", driver).click()
            Log.WriteLine("Контент")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[1]", driver).click()
            Log.WriteLine("Посты")
            //
            DriverFindElement("//*[@class='form-control']", driver).sendKeys("90448") //Id взять в админке
            Log.WriteLine("Номер проекта")
            //
            DriverFindElement("//*[@id='search']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-outline']", driver).click()
            Log.WriteLine("Редактировать")
            //
//            DriverFindElement("//*[@id='upload-no_image']", driver).click()
//            Log.WriteLine("Добавить изображение")
//            //
//            DriverFindElement("//*[@class='btn btn-primary fileinput-button']", driver).click()
//            Log.WriteLine("Выбрать фото с компа")
//            //
//            robot()
//            //
//            DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
//            Log.WriteLine("OK")
//            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun Project(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Админка-Контент-Проекты" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[5]", driver).click()
            Log.WriteLine("Контент")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[2]", driver).click()
            Log.WriteLine("Проекты")
            //
            DriverFindElement("//*[@id='search']", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun TV(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка-Контент-TV" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[5]", driver).click()
            Log.WriteLine("Контент")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[3]", driver).click()
            Log.WriteLine("TV")
            //
            DriverFindElement("//*[@id='search']", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun Charity(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка-Контент-Благотворительность" + VersionBrowser(driver))




            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[5]", driver).click()
            Log.WriteLine("Контент")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[4]", driver).click()
            Log.WriteLine("Благотворительность")
            //
            for (i in 1..3) {
                DriverFindElement("(//*[@id='search'])[" + i.toString() + "]", driver).click()
            }
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun AboutUs(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка-Контент-О нас" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[5]", driver).click()
            Log.WriteLine("Контент")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[6]", driver).click()
            Log.WriteLine("О нас")
            //
            DriverFindElement("(//*[@id='search'])[1]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun Partners(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка-Контент-Партнёры" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[5]", driver).click()
            Log.WriteLine("Контент")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[7]", driver).click()
            Log.WriteLine("Партнёры")
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Создать партнёра")
            //
            DriverFindElement("//*[@id='imageUrl']", driver).sendKeys("Партнёр скайнет")
            Log.WriteLine("Название партнёра")
            //
            DriverFindElement("//*[@id='upload-no_image']", driver).click()
            Log.WriteLine("Добавить изображение")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@id='name']", driver).sendKeys("Партнёр")
            Log.WriteLine("Название партнёра")
            //
            DriverFindElement("//*[@id='originalUrl']", driver).sendKeys("https://dev.planeta.ru")
            Log.WriteLine("Ссылка")
            //
            DriverFindElement("//*[@id='welcome1']", driver).click()
            Log.WriteLine("Партнер на главной")
            //
            DriverFindElement("//*[@id='curator1']", driver).click()
            Log.WriteLine("Куратор на главной")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[7]", driver).click()
            Log.WriteLine("Партнёры")
            //
            DriverFindElement("(//*[@class='btn btn-danger btn-outline promo-remove'])[12]", driver).click()
            Log.WriteLine("Удалить партнёра")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("ОК")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }


    @Test
    fun Redirect(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка-Контент-Редиректы" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[5]", driver).click()
            Log.WriteLine("Контент")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[8]", driver).click()
            Log.WriteLine("Редиректы")
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Доавить")
            //
            DriverFindElement("//*[@id='edit_title']", driver).sendKeys("короткая ссылка")
            Log.WriteLine("короткая ссылка")
            //
            DriverFindElement("//*[@id='redirectAddressUrl']", driver).sendKeys("https://dev.planeta.ru")
            Log.WriteLine("Адрес перехода")
            //
            DriverFindElement("//*[@id='cookieName']", driver).sendKeys("кука")
            Log.WriteLine("Имя кукки")
            //
            DriverFindElement("//*[@id='cookieValue']", driver).sendKeys("значение куки")
            Log.WriteLine("значение куки")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Добавить")
            //
            DriverFindElement("(//*[@class='fa arrow'])[5]", driver).click()
            Log.WriteLine("Контент")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[8]", driver).click()
            Log.WriteLine("Редиректы")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Мот ссылки")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[8]", driver).click()
            Log.WriteLine("Редиректы")
            //
            DriverFindElement("(//*[@class='admin-table-actions'])[1]//button[1]", driver).click()
            Log.WriteLine("Выключить")
            //
            DriverFindElement("(//*[@class='fa arrow'])[5]", driver).click()
            Log.WriteLine("Контент")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[8]", driver).click()
            Log.WriteLine("Редиректы")
            //
            DriverFindElement("(//*[@class='admin-table-actions'])[1]//button[2]", driver).click()
            Log.WriteLine("Удалить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){

        }finally {
            Stop1()
        }
    }


    @Test
    fun DebugPerepost(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка-Контент-Отладчик перепостов" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[5]", driver).click()
            Log.WriteLine("Контент")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[9]", driver).click()
            Log.WriteLine("Отладчик перепостов")
            //
            DriverFindElement("//*[@class='btn btn-primary js-scan-url']", driver).click()
            Log.WriteLine("Поиск")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }
    @Test
    fun DownloadImage(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Админка-Контент-Загрузить картинку" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[5]", driver).click()
            Log.WriteLine("Контент")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[10]", driver).click()
            Log.WriteLine("Загрузить картинку")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-circle btn-outline btn-lg upload-file']", driver).click()
            Log.WriteLine("Загрузить")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
            Log.WriteLine("OK")
            //
            for (i in 1..6)
            {
                DriverFindElement("//*[@class='btn btn-primary dropdown-toggle']", driver).click()
                //
                DriverFindElement("//*[@class='dropdown-menu']//li[" + i.toString() + "]", driver).click()
                //
                val tabs = ArrayList(driver.windowHandles)

                driver.switchTo().window(tabs[0])
            }

            for (i  in  1..6)
            {
                val tabs = ArrayList(driver.windowHandles)

                driver.switchTo().window(tabs[i])
                Thread.sleep(100)
            }
            val tabs = ArrayList(driver.windowHandles)
            driver.switchTo().window(tabs[0])
            //
            DriverFindElement("//*[@class='images-preview']//button[1]", driver).click()
            Log.WriteLine("Удалить фото")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun DownloadFile(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Админка-Контент-Загрузить файл" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[5]", driver).click()
            Log.WriteLine("Контент")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[11]", driver).click()
            Log.WriteLine("Загрузить файл")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-circle btn-outline btn-lg upload-file']", driver).click()
            Log.WriteLine("Загрузить")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
            Log.WriteLine("OK")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun DownloadDocument(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка-Контент-Загрузить Документ" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[5]", driver).click()
            Log.WriteLine("Контент")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[12]", driver).click()
            Log.WriteLine("Загрузить Документ")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-circle btn-outline btn-lg upload-file']", driver).click()
            Log.WriteLine("Загрузить")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[12]", driver).click()
            Log.WriteLine("Загрузить Документ")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-outline']", driver).click()
            Log.WriteLine("Редактировать")
            //
            DriverFindElement("//*[@class='btn btn-danger']", driver).click()
            Log.WriteLine("Удалить")
            //
            Thread.sleep(100)
            driver.switchTo().alert().accept()
            Thread.sleep(2000)
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

}