import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.WebDriver
import java.nio.file.Files
import java.nio.file.Paths

class A_AdminkaOther : Setup() {

    @Test
    fun ManagerProjectAndCaterory(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Админка-Прочее-Менеджеры-категории проектов" + VersionBrowser(driver))




            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[7]", driver).click()
            Log.WriteLine("Прочее")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[1]", driver).click()
            Log.WriteLine("Менеджеры проектов и категории")
            //
            DriverFindElement("//*[@id='profileId']//option[2]", driver).click()
            Log.WriteLine("Профиль")
            //
            DriverFindElement("//*[@id='name']", driver).sendKeys("Обращение")
            Log.WriteLine("Обращение")
            //
            DriverFindElement("//*[@id='fullName']", driver).sendKeys("Полное имя")
            Log.WriteLine("Полное имя")
            //
            DriverFindElement("//*[@class='btn btn-success']", driver).click()
            Log.WriteLine("Добавить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop()
        }
    }

    @Test
    fun StatusProject(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка-Прочее-Статусы Проектов" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[2]", driver).click()
            Log.WriteLine("Проекты")
            //
            DriverFindElement("//*[@name='campaignStatus']//option[6]", driver).click()
            Log.WriteLine("Статус активный")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//tbody//tr[1]//td[3]//a", driver).click()
            Log.WriteLine("Выбор проекта")
            //
            DriverFindElement("(//*[@class='project-admin-controls_action-i'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            var Text = DriverFindElement("(//*[@class='page-header'])[1]", driver).text
            Text = Text.replace("№", "")
            val txt = Text.trim('П', 'р', 'о', 'е', 'к', 'т')
            //
            DriverFindElement("(//*[@class='fa arrow'])[7]", driver).click()
            Log.WriteLine("Прочее")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[2]", driver).click()
            Log.WriteLine("Статусы Проектов")
            //
            DriverFindElement("(//*[@name='campaignId'])[1]", driver).sendKeys(txt)
            Log.WriteLine("Ввод номера проекта")
            //
            DriverFindElement("(//*[@class='input-group-btn'])[1]", driver).click()
            Log.WriteLine("Найти")
            //
            DriverFindElement("//*[@class='btn confirm js-change-action btn-danger']", driver)
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun ControlCity(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка-Прочее-Управление городами" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[7]", driver).click()
            Log.WriteLine("Прочее")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[3]", driver).click()
            Log.WriteLine("Управление городами")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }


    @Test
    fun DeliveryService(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка-Прочее-Служба доставки" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[7]", driver).click()
            Log.WriteLine("Прочее")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[4]", driver).click()
            Log.WriteLine("Служба доставки")
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg add-base-service']", driver).click()
            Log.WriteLine("Создать службу доставки")
            //
            DriverFindElement("//*[@id='name']", driver).sendKeys("Служба доставки")
            Log.WriteLine("Название")
            //
            DriverFindElement("//*[@id='description']", driver).sendKeys("1000")
            Log.WriteLine("Ориентировочная стоимость")
            //
            DriverFindElement("//*[@class='dropdown-toggle btn btn-small btn-success']", driver).click()
            //
            DriverFindElement("//*[@class='dropdown-menu pre-scrollable']//li[3]", driver).click()
            Log.WriteLine("Выбор региона")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
            DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun StikerAdressFile(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка-Прочее-Наклейки" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[7]", driver).click()
            Log.WriteLine("Прочее")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[5]", driver).click()
            Log.WriteLine("Наклейки")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Xls\\campaign_shares_report_854599.xls")

            Thread.sleep(1000)
//            var str = StringSelection("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Xls\\campaign_shares_report_854599.xls")
//
//            val tol = Toolkit.getDefaultToolkit()
//            val cl: Clipboard = tol.systemClipboard
//            cl.setContents(str, null)
//
//            robot.keyPress(KeyEvent.VK_CONTROL)
//            robot.keyPress(KeyEvent.VK_V)
//            robot.keyRelease(KeyEvent.VK_V)
//            robot.keyRelease(KeyEvent.VK_CONTROL)
//            Thread.sleep(500)
//            robot.keyPress(KeyEvent.VK_ENTER)
//            //
            DriverFindElement("//*[@class='btn btn-success']", driver).click()
            Log.WriteLine("Загрузить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun ConfigurePayments(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка-Прочее-Конфигурации платёжек" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[7]", driver).click()
            Log.WriteLine("Прочее")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[6]", driver).click()
            Log.WriteLine("Конфигурации платёжек")
            //
            DriverFindElement("(//*[@title='Сохранить'])[1]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun Sponsors(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка-Прочее-Спонсоры" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[7]", driver).click()
            Log.WriteLine("Прочее")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[7]", driver).click()
            Log.WriteLine("Спонсоры")
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Создать спонсора")
            //
            DriverFindElement("(//*[@class='form-control'])[1]", driver).sendKeys("Alias")
            Log.WriteLine("Алиас")
            //
            DriverFindElement("(//*[@class='form-control'])[2]", driver).sendKeys("1")
            Log.WriteLine("Множитель")
            //
            DriverFindElement("(//*[@class='form-control'])[3]", driver).sendKeys("1")
            Log.WriteLine("Максимальная допустимая сумма")
            //
            DriverFindElement("(//*[@class='form-control'])[4]", driver).sendKeys("1")
            Log.WriteLine("Код для карточки проекта")
            //
            DriverFindElement("(//*[@class='form-control'])[5]", driver).sendKeys("1")
            Log.WriteLine("Код для страницы проекта")
            //
            DriverFindElement("(//*[@class='form-control'])[6]", driver).sendKeys("1")
            Log.WriteLine("Код для страницы поиска")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit() //тут 500 ошибка её  не исправят
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun Campus(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка-Прочее-Кампусы" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[7]", driver).click()
            Log.WriteLine("Прочее")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[8]", driver).click()
            Log.WriteLine("Кампусы")
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Создать кампус")
            //
            DriverFindElement("(//*[@class='form-control'])[1]", driver).sendKeys("Название")
            Log.WriteLine("Название")
            //
            DriverFindElement("(//*[@class='form-control'])[2]", driver).sendKeys("Короткое название")
            Log.WriteLine("Короткое название")
            //
            DriverFindElement("//*[@class='img-no_preview js-ni-imageUrl']", driver).click()
            Log.WriteLine("Загрузить Фото")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun Mailing(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка-Прочее-Рассылка" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[7]", driver).click()
            Log.WriteLine("Прочее")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[9]", driver).click()
            Log.WriteLine("Рассылка")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-large']", driver)
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun Temolate(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка-Прочее-Транзакционные писем" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[7]", driver).click()
            Log.WriteLine("Прочее")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[10]", driver).click()
            Log.WriteLine("Шаблоны писем")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver)
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun Poll(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка-Прочее-Опрос" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[7]", driver).click()
            Log.WriteLine("Прочее")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[11]", driver).click()
            Log.WriteLine("Опрос")
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Добавить опрос")
            //
            DriverFindElement("(//*[@class='form-control'])[1]", driver).sendKeys("Название")
            Log.WriteLine("Название")
            //
            DriverFindElement("(//*[@class='form-control'])[2]", driver).sendKeys(GetRandomString())
            Log.WriteLine("Алиас")
            //
            driver.switchTo().frame(0)
            //
            DriverFindElement("(//*[@id='tinymce'])[1]", driver).sendKeys("ZZZzzzzzZZZzzzzzZZZZ!!!1")
            Log.WriteLine("Ввод значения")
            //
            try{
                DriverFindElement("//*[@id='enabled']//option[1]", driver).click()
                Log.WriteLine("Да")

                DriverFindElement("(//*[@class='btn-group'])[1]//button", driver).click()
                Log.WriteLine("Сохранить")
            }catch (p:Exception){

            }

            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun PromoMail(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка-Прочее-Промо письма" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[7]", driver).click()
            Log.WriteLine("Прочее")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[12]", driver).click()
            Log.WriteLine("Промо-письма")
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Добавить промо")
            //
            DriverFindElement("//*[@id='name']", driver).sendKeys("Промо")
            Log.WriteLine("Название")
            //
            DriverFindElement("//*[@id='usageCount']", driver).sendKeys("100")
            Log.WriteLine("Количество использований")
            //
            DriverFindElement("//*[@id='date-box-start']", driver).click()
            //
            DriverFindElement("(//*[@class='ui-datepicker-calendar'])[1]//tbody//tr[1]//td[6]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("//*[@id='date-box-finish']", driver).click()
            //
            DriverFindElement("(//*[@class='ui-datepicker-calendar'])[1]//tbody//tr[5]//td[3]", driver).click()
            Log.WriteLine("Дата окончания")
            //
            DriverFindElement("//*[@id='priceCondition']", driver).sendKeys("10")
            Log.WriteLine("Минимальная покупка")
            //
            DriverFindElement("//*[@id='campaignTags']//option[2]", driver).click()
            Log.WriteLine("Выбор категории")
            //
            DriverFindElement("//*[@id='hasPromocode']", driver).click()
            Log.WriteLine("Есть промокоды")
            //
            DriverFindElement("//*[@id='promoCodesString']", driver).sendKeys("test123")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Добавить")
            //
            DriverFindElement("(//*[@class='fa arrow'])[7]", driver).click()
            Log.WriteLine("Прочее")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[12]", driver).click()
            Log.WriteLine("Промо-письма")
            //
            DriverFindElement("(//*[@class='asdmin-table-actions'])[1]//button[1]", driver).click()
            Log.WriteLine("На паузе")
            //
            DriverFindElement("(//*[@class='asdmin-table-actions'])[1]//button[1]", driver).click()
            Log.WriteLine("Запустить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {

        }
    }



}