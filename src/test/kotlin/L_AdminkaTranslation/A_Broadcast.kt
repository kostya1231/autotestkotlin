import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.WebDriver
import java.nio.file.Files
import java.nio.file.Paths

class A_Broadcast : Setup() {

    @Test
    fun Broadcast(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка - Создать трансляцию" + VersionBrowser(driver))




            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='menu-item-text'])[2]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[8]", driver).click()
            Log.WriteLine("Трансляции")
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Создать трансляцию")
            //
            DriverFindElement("(//*[@class='form-control'])[1]", driver).sendKeys("qwerty")
            Log.WriteLine("Название")
            //
            DriverFindElement("//*[@id='date-box-start']", driver).click()
            //
            DriverFindElement("(//*[@class='glyphicon glyphicon-chevron-right'])[1]", driver).click()
            //
            DriverFindElement("(//tbody)[1]//tr[1]//td[6]", driver).click()
            Log.WriteLine("Время начала")
            //
            DriverFindElement("//*[@id='date-box-finish']", driver).click()
            //
            DriverFindElement("(//*[@class='glyphicon glyphicon-chevron-right'])[1]", driver).click()
            DriverFindElement("(//*[@class='glyphicon glyphicon-chevron-right'])[1]", driver).click()
            //
            DriverFindElement("(//tbody)[1]//tr[4]//td[6]", driver).click()
            Log.WriteLine("Время Окончания")
            //
            DriverFindElement("//*[@id='broadcastCategory']//option[4]", driver).click()
            Log.WriteLine("Школа краудфантинга")
            //
            DriverFindElement("//*[@id='description']", driver).sendKeys("qwerty")
            Log.WriteLine("Описание")
            //
            HTMLBanner2(driver)
            Log.WriteLine("Баннер HTML")
            //
            DriverFindElement("//*[@id='closedImageUrl']", driver).sendKeys("http://www.animacity.ru/sites/default/files/blog/82069/14624.jpg")
            Log.WriteLine("Заглушка для ограничений")
            //
            DriverFindElement("//*[@id='imageUrl']", driver).sendKeys("http://www.animacity.ru/sites/default/files/blog/82069/14624.jpg")
            Log.WriteLine("Заглушка до начала")
            //
            DriverFindElement("//*[@id='pausedImageUrl']", driver).sendKeys("http://www.animacity.ru/sites/default/files/blog/82069/14624.jpg")
            Log.WriteLine("Заглушка на паузе")
            //
            DriverFindElement("//*[@id='finishedImageUrl']", driver).sendKeys("http://www.animacity.ru/sites/default/files/blog/82069/14624.jpg")
            Log.WriteLine("Заглушка после")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("(//*[@class='btn btn-success'])[1]", driver).click()
            Log.WriteLine("Добавить поток")
            //
            DriverFindElement("//*[@id='name']", driver).sendKeys("qwerty")
            Log.WriteLine("Название")
            //
            DriverFindElement("//*[@id='broadcastUrl']", driver).sendKeys("https://www.youtube.com/watch?v=izFACT-l41Y")
            Log.WriteLine("Ссылка")
            //
            DriverFindElement("//*[@id='imageUrl']", driver).sendKeys("http://www.animacity.ru/sites/default/files/blog/82069/14624.jpg")
            Log.WriteLine("Заглушка")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Запустить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }




    }



}