import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.WebDriver
import org.openqa.selenium.interactions.Actions
import java.nio.file.Files
import java.nio.file.Paths

class A_Bonusses : Setup() {

    @Test
    fun CreateBonusses(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка-Бонусы" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[4]", driver).click()
            Log.WriteLine("Бонусы")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[1]", driver).click()
            Log.WriteLine("Бонусы")
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Создать бонус")
            //
            DriverFindElement("//*[@name='name']", driver).sendKeys("Бонус")
            Log.WriteLine("Название бонуса")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Фото")
            //
            DriverFindElement("//*[@name='description']", driver).sendKeys("Описание")
            Log.WriteLine("Описание")
            //
            DriverFindElement("//*[@name='available']", driver).clear()
            //
            DriverFindElement("//*[@name='available']", driver).sendKeys("500")
            Log.WriteLine("Количество")
            //
            Thread.sleep(500)
            DriverFindElement("//*[@name='buttonText']", driver).sendKeys("Текст кнонки")
            Log.WriteLine("Текст кнопки")
            //
            val el = DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver)
            val elActions = Actions(driver)
            elActions.moveToElement(el).click().build().perform()
            Log.WriteLine("Сохранить")
            //
            Thread.sleep(300)
            //
            driver.navigate().refresh()
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun BonussesOrders(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка-Бонусы-Заказы бонусов" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[4]", driver).click()
            Log.WriteLine("Бонусы")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[2]", driver).click()
            Log.WriteLine("Заказы бонусов")
            //
            for (i in 1..4) {
                DriverFindElement("//*[@id='status']//option[" + i.toString() + "]", driver).click()
                //
                DriverFindElement("//*[@id='search']", driver).click()
            }
            //
            Log.WriteLine("Перебор статусов поиск")
            //
            DriverFindElement("//*[@class='btn btn-success']", driver).click()
            Log.WriteLine("Скачать отчёты в формате excel")
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun  VipBonusses(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка-Бонусы-Вип спонсоры" + VersionBrowser(driver))







            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("pain-net+" + GetRandomString() + "@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='login_btn-link']", driver).click()
            Log.WriteLine("Зарегестрироваться")
            //
            DriverFindElement("(//*[@class='form-control login-control_input ng-untouched ng-pristine ng-valid'])[3]", driver).sendKeys("nekroman911")
            Log.WriteLine("Подтвердите пароль")
            //
            DriverFindElement("//*[@class='form-ui-txt']", driver).click()
            Log.WriteLine("Я даю своё согласие")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Зарегестрироваться")
            //
            val text = DriverFindElement("//*[@class='h-user_name']", driver) // Вытащить текст из элемента
            var Text1 = text.text
            Text1 = Text1.replace("\\s".toRegex(), "")//Удаление пробелов
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).clear()
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина админа")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[4]", driver).click()
            Log.WriteLine("Бонусы")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[3]", driver).click()
            Log.WriteLine("Вид Спонсоры")
            //
            DriverFindElement("//*[@class='form-control']", driver).sendKeys("pain-net+")
            Log.WriteLine("Ввести логин юзера")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("(//*[@class='btn btn-success btn-outline action invite'])[1]", driver).click()
            Log.WriteLine("Сделать випом")
            //
            DriverFindElement("(//*[@class='btn btn-danger btn-outline action kickOut'])[1]", driver).click()
            Log.WriteLine("Исключить из випов")
            //
            DriverFindElement("(//*[@class='btn btn-primary confirm'])[1]", driver).click()
            Log.WriteLine("Исключить")
            //
            Thread.sleep(1500)
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

}