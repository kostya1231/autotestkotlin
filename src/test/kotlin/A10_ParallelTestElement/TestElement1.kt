
import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

class TestElement1 : SetupParallel() {

    @Test
    fun TestElement1(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Test_Element")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Test_Element\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Отображение нулевого баланса на бекбоне" + VersionBrowser(driver))





            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz2name@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            Thread.sleep(1500)
            DriverFindElement("//*[@class='button button__def button__xs']",driver).click()
            Log.WriteLine("Хорошо")
            //
            DriverFindElement("//*[@class='h-menu_i h-menu_i__dropdown']", driver).click()
            Log.WriteLine("Проекты")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("//*[@class='h-user-menu_balance']//*[contains(text(), '0')]", driver)
            Log.WriteLine("Проверка на наличие нуля в списке")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun UrlVoznagragdenie(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Test_Element")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Test_Element\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Проверка на наличие ссылки на вознаграждение" + VersionBrowser(driver))





            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz4name@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("(//*[@class='h-menu_link'])[1]", driver)
            Log.WriteLine("Проекты")
            //
            //DriverFindElement("(//*[@class='h-menu_link'])[2]", driver).Click();
            //Log.WriteLine("Вознаграждения");
            ////
            //DriverFindElement("(//*[@class='reward-card_i'])[1]", driver).Click();
            //Log.WriteLine("Выбор вознаграждения");
            ////
            DriverNavigate(OrderVoznagrajdenie, driver)
            //
      //      DriverFindElement("//*[@class='action-card  inactive  focus active']//*[contains(text(), 'Ссылка на это вознаграждение')]", driver).click()
            Log.WriteLine("ссылка на это вознаграждение")
            //
     //       DriverFindElement("(//*[@class='btn btn-nd-primary action-card_btn js-navigate-to-purchase'])[1]", driver)
            Log.WriteLine("Проверка открыто ли вознаграждение")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun ErrorPassword(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Test_Element")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Test_Element\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Проверка на наличие ошибки что ссылка устарела" + VersionBrowser(driver))




            DriverNavigate("https://yandex.ru", driver)
            Log.WriteLine("Вход на яндекс")
            //
            DriverFindElement("(//*[@class='home-link home-link_black_yes'])[1]", driver).click()
            Log.WriteLine("Вход на яндекс")
            //
            fun FindWindowYandex() : Boolean
            {
                return try {
                    Thread.sleep(1500)
                    driver.findElement(By.xpath("//*[@class='passp-page-overlay']"))
                    true
                } catch(e:Exception) {
                    false
                }
            }

            if (!FindWindowYandex())
            {
                DriverFindElement("//*[@name='login']", driver).sendKeys("uz2name")
                Log.WriteLine("Ввод Логина")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passport-Button-Text']", driver).click()
                Log.WriteLine("Войти")
                //
                DriverFindElement("//*[contains(text(), 'Планета – Восстановление пароля')]", driver).click()
                Log.WriteLine("Выбор письма")
                //
                DriverFindElement("(//*[@rel='noopener noreferrer'])[2]", driver).click()
                Log.WriteLine("Перейти по Ссылке")
                //
            }

            if (FindWindowYandex())
            {
                DriverFindElement("//*[@name='login']", driver).sendKeys("uz2name")
                Log.WriteLine("Ввод Логина")
                //
                DriverFindElement("//*[@class='passp-login-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passp-password-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                //
                DriverFindElement("//*[contains(text(), 'Планета – Восстановление пароля')]", driver).click()
                Log.WriteLine("Выбор письма")
                //
                DriverFindElement("(//*[@rel='noopener noreferrer'])[2]", driver).click()
                Log.WriteLine("Перейти по Ссылке")
                //
            }
            val tabs = ArrayList(driver.windowHandles)

            driver.switchTo().window(tabs[1])
            //
            DriverFindElement("//*[@name='password']", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод нового пароля")
            //
            DriverFindElement("//*[@name='confirmation']", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод нового пароля ещё раз")
            //
            DriverFindElement("//*[@class='login_form ng-untouched ng-pristine ng-valid']//button[1]", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("//*[contains(text(), 'Ваш регистрационный код не действителен')]", driver).click()
            Log.WriteLine("Ваш регистрационный код не действителен")
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun NewAdminkaReklamaBanners(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Test_Element")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Test_Element\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Новая админка раздел Релкама Создать рекламную кампанию без баннера" + VersionBrowser(driver))






            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            DriverFindElement("//*[@class='mat-toolbar mat-toolbar-multiple-rows']//*[@class='mat-button-wrapper']", driver).click()
            Log.WriteLine("Создать новую рекламную компанию")
            //
            DriverFindElement("(//*[@id='mat-input-1'])[1]", driver).sendKeys("Тестовая РК")
            Log.WriteLine("Код рекламной кампании")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[3]//td[1]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[4]//td[1]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-trigger'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Статус запущена")
            //
            DriverFindElement("//*[contains(text(), 'Необходимо добавить хотя бы один баннер')]", driver)
            Log.WriteLine("Необходимо добавить хотя бы один баннер")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun NewAdminkaReklamaBannersData(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Test_Element")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Test_Element\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Новая админка раздел Релкама Создать рекламную кампанию без баннера" + VersionBrowser(driver))






            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            Thread.sleep(400)
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            DriverFindElement("//*[contains(text(), 'Создать новую РК')]", driver).click()
            Log.WriteLine("Создать новую рекламную компанию")
            //
            DriverFindElement("(//*[@id='mat-input-1'])[1]", driver).sendKeys("Тестовая РК")
            Log.WriteLine("Код рекламной кампании")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[3]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[5]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]", driver).click()
            //
            DriverFindElement("//*[@aria-label='Next month']", driver).click()
            Log.WriteLine("Следующий месяц")
            //
            DriverFindElement("(//*[@class='mat-calendar-table'])[1]//tr[4]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]", driver).click()
            //
            DriverFindElement("//*[@aria-label='Previous month']", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[5]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("//*[contains(text(), 'Дата начала не может быть больше даты окончания')]", driver)
            //
            DriverFindElement("//*[contains(text(), ' Добавить баннер ')]", driver).click()
            Log.WriteLine("Добавть баннер")
            //

            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[3]", driver).click()
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun KontrAgentEditPosition(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Test_Element")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Test_Element\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Контр агент проверка на сохранение названия должности" + VersionBrowser(driver))






            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("avtor2.avtorproject@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            Thread.sleep(400)
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[1]", driver).click()
            Log.WriteLine("Созданные проекты")
            //
            fun find() : Boolean
            {
                return try {
                    DriverFindElement("//*[@class='status-badge status-badge__info']", driver)
                    true
                } catch(e:Exception) {
                    false
                }
            }
            if (find())
            {
                try
                {
                    DriverFindElement("//*[@class='profile-nav']/li[2]", driver).click()
                }
                catch(e:Exception)
                {

                }
            }
            DriverFindElement("//*[@class='profile-project-change_link']", driver).click()
            Log.WriteLine("Редактировать")
            //
            DriverFindElement("(//*[@class='project-create_tab_link'])[4]", driver).click()
            Log.WriteLine("Контр-Агент")
            //
            DriverFindElement("(//*[@class='select-cont'])[3]", driver).click()
            Log.WriteLine("Должность руководителя")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[3]/li[2]", driver).click()
            Log.WriteLine("Другая")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[2]", driver).clear()
            Log.WriteLine("Очистить поле")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[2]", driver).sendKeys("Другая")
            Log.WriteLine("Ввод должности")
            //
            for (i in 1..5)
            {
                val scrol = 1000
                val js1 = driver as JavascriptExecutor
                Thread.sleep(50)
                js1.executeScript("window.scrollBy(0,$scrol)")
            }
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            Thread.sleep(1500)
            //
            driver.navigate().refresh()
            //
            DriverFindElement("//*[@value='Другая']", driver)
            //
            Log.Close()
            Stop()
            driver.quit()

        } catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }

    }


    @Test
    fun FindBannersName(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Test_Element")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Test_Element\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Новая админка раздел Релкама поиск баннера по имени" + VersionBrowser(driver))






            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            DriverFindElement("//*[@class='mat-content']", driver).click()
            Log.WriteLine("Фильтр")
            //
            Thread.sleep(150)
            DriverFindElement("(//*[@class='mat-form-field-infix'])[1]//input", driver).sendKeys("Тестовая РК")
            Log.WriteLine("Поиск по имени")
            //
            for (i in 1..4) {
                Thread.sleep(50)
                DriverFindElement("(//*[@class='mat-select-arrow'])[1]", driver).click()
                //
                Thread.sleep(50)
                DriverFindElement("(//*[@class='cdk-overlay-pane'])[1]//mat-option[" + i.toString() + "]", driver).click()
                Thread.sleep(50)
            }
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun FindUser(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Test_Element")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Test_Element\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Админка модерация пользователи поиск по номеру проекта почте и алиасу" + VersionBrowser(driver))






            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Пользователи")
            //
            DriverFindElement("//*[@class='form-control']", driver).sendKeys("94587")
            Log.WriteLine("Поиск по номеру проекта")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Поиск по юзеру")
            //
            DriverFindElement("//tbody//tr//*[contains(text(), '94587')]", driver)
            Log.WriteLine("Проверка на наличие юзера")
            //
            DriverFindElement("//*[@class='form-control']", driver).clear()
            //
            DriverFindElement("//*[@class='form-control']", driver).sendKeys("857066")
            Log.WriteLine("Ввод номер юзера")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Поиск по юзеру")
            //
            DriverFindElement("//tbody//tr//*[contains(text(), '857066')]", driver)
            Log.WriteLine("Проверка на наличие юзера")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }


}