
import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.interactions.Actions
import java.awt.event.KeyEvent
import java.nio.file.Files
import java.nio.file.Paths

class School1 : SetupParallel() {

    @Test
    fun Details() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\School")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\School\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("School-Подробности" + VersionBrowser(driver))





            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(1000)
            DriverNavigate(SchoolPlaneta, driver)
            //
            DriverFindElement("//*[@class='school-btn school-btn-default school-btn-lg']", driver).click()
            Log.WriteLine("Подробности")
            //
            Thread.sleep(1500)
            Log.Close()
            Stop()
            driver.quit()
        } catch (e: Exception) {
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }


    @Test
    fun DownloadPosobie() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\School")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\School\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("School-Скачать пособие" + VersionBrowser(driver))




            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(1000)
            DriverNavigate(SchoolPlaneta, driver)
            //
            DriverFindElement("//*[@class='school-book_download-link-text']", driver).click()
            Log.WriteLine("Скачать пособие")
            //
            Thread.sleep(500)
            Log.Close()
            Stop()
            driver.quit()
        } catch (e: Exception) {
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun SchoolLessons() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\School")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\School\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("School-Уроки" + VersionBrowser(driver))





            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(1000)
            DriverNavigate(SchoolPlaneta, driver)
            //
            DriverFindElement("//*[@href='https://tv.test.planeta.ru/broadcast/367732']", driver).click()
            Log.WriteLine("Вводное занятие")
            //
            Thread.sleep(1000)
            driver.navigate().to(SchoolPlaneta)
            //
            DriverFindElement("//*[@href='https://tv.test.planeta.ru/broadcast/370296']", driver).click()
            Log.WriteLine("Текстовое описание проекта")
            //
            Thread.sleep(1000)
            driver.navigate().to(SchoolPlaneta)
            //
            DriverFindElement("//*[@href='https://tv.test.planeta.ru/broadcast/375931']", driver).click()
            Log.WriteLine("Видеообращение")
            //
            Thread.sleep(1000)
            driver.navigate().to(SchoolPlaneta)
            //
            DriverFindElement("//*[@href='https://tv.test.planeta.ru/broadcast/377870']", driver).click()
            Log.WriteLine("Акции")
            //
            Thread.sleep(1000)
            driver.navigate().to(SchoolPlaneta)
            //
            DriverFindElement("//*[@href='https://tv.test.planeta.ru/broadcast/381404']", driver).click()
            Log.WriteLine("Акции")
            //
            Thread.sleep(1000)
            driver.navigate().to(SchoolPlaneta)
            //
            DriverFindElement("//*[@href='https://tv.test.planeta.ru/broadcast/385875']", driver).click()
            Log.WriteLine("Продвижение. Часть 2")
            //
            Thread.sleep(1000)
            driver.navigate().to(SchoolPlaneta)
            //
            DriverFindElement("//*[@href='https://tv.test.planeta.ru/broadcast/390157']", driver).click()
            Log.WriteLine("Продвижение. Часть 2")
            //
            Thread.sleep(1000)
            driver.navigate().to(SchoolPlaneta)
            //
            Log.Close()
            Stop()
            driver.quit()
        } finally {
            Stop1()
        }
    }


    @Test
    fun SocSeti() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\School")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\School\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("School-Sharik" + VersionBrowser(driver))





            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(1000)
            DriverNavigate(SchoolPlaneta, driver)
            //
            DriverFindElement("(//*[@class='sharing-popup-social sharing-mini']//*[@class='spsb-item'])[1]", driver).click()
            Log.WriteLine("VK")
            //
            Thread.sleep(1000)
            DriverNavigate(SchoolPlaneta, driver)
            //
            DriverFindElement("(//*[@class='sharing-popup-social sharing-mini']//*[@class='spsb-item'])[2]", driver).click()
            Log.WriteLine("FB")
            //
            Thread.sleep(1000)
            DriverNavigate(SchoolPlaneta, driver)
            //
            DriverFindElement("(//*[@class='sharing-popup-social sharing-mini']//*[@class='spsb-item'])[3]", driver).click()
            Log.WriteLine("Twitter")
            //
            Thread.sleep(1000)
            DriverNavigate(SchoolPlaneta, driver)
            //
            DriverFindElement("(//*[@class='sharing-popup-social sharing-mini']//*[@class='spsb-item'])[4]", driver).click()
            Log.WriteLine("Google+")
            //
            Thread.sleep(1000)
            DriverNavigate(SchoolPlaneta, driver)
            //
            DriverFindElement("//*[@class='school-interactive_top-link']", driver).click()
            Log.WriteLine("Подробнее о курсе")
            //
            Thread.sleep(500)
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e: Exception) {
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }


    @Test
    fun SchoolLessonsSharing() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\School")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\School\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("School-Уроки-Шаринг-vk" + VersionBrowser(driver))






            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(1000)
            DriverNavigate(SchoolPlaneta, driver)
            //
            DriverFindElement("//*[@href='https://tv.test.planeta.ru/broadcast/367732']", driver).click()
            Log.WriteLine("Вводное занятие")
            //
            DriverFindElement("(//*[@class='sps-button']//*[@class='spsb-item'][1])", driver).click()
            Log.WriteLine("VK")
            //
            val han = driver.windowHandles
            val numberOfTabs = han.size
            val newTabIndex = numberOfTabs - 1
            driver.switchTo().window(han.toTypedArray()[newTabIndex].toString())
            //
            DriverFindElement("//*[@name='email']", driver).sendKeys("89778665883")
            Log.WriteLine("Логин вк")
            //
            DriverFindElement("//*[@name='pass']", driver).sendKeys("nekroman911")
            Log.WriteLine("Пароль")
            //
            DriverFindElement("//*[@id='install_allow']", driver).click()
            Log.WriteLine("Войти")
            //
            DriverFindElement("//*[@class='flat_button fl_r']", driver).click()
            Log.WriteLine("Отправить")
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e: Exception) {
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun SchoolLessonsSharing2() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\School")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\School\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("School-Уроки-Шаринг-FB" + VersionBrowser(driver))






            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(1000)
            DriverNavigate(SchoolPlaneta, driver)
            //
            DriverFindElement("//*[@href='https://tv.test.planeta.ru/broadcast/367732']", driver).click()
            Log.WriteLine("Вводное занятие")
            //
            DriverFindElement("(//*[@class='sps-button']//*[@class='spsb-item'][2])", driver).click()
            Log.WriteLine("FB")
            //
            val han = driver.windowHandles
            val numberOfTabs = han.size
            val newTabIndex = numberOfTabs - 1
            driver.switchTo().window(han.toTypedArray()[newTabIndex].toString())
            //
            DriverFindElement("//*[@id='email']", driver).sendKeys("pain-net@yandex.ru")
            Log.WriteLine("Логин fb")
            //
            DriverFindElement("//*[@id='pass']", driver).sendKeys("nekroman911")
            Log.WriteLine("Пароль")
            //
            DriverFindElement("//*[@name='login']", driver).click()
            Log.WriteLine("Войти")
            //
            DriverFindElement("//*[@id='u_0_1w']", driver).click()
            Log.WriteLine("На Facebook")
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e: Exception) {
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun SchoolLessonsSharing3(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\School")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\School\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("School-Уроки-Шаринг-TW" + VersionBrowser(driver))





            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(1000)
            DriverNavigate(SchoolPlaneta, driver)
            //
            DriverFindElement("//*[@href='https://tv.test.planeta.ru/broadcast/367732']", driver).click()
            Log.WriteLine("Вводное занятие")
            //
            DriverFindElement("(//*[@class='sps-button']//*[@class='spsb-item'][3])", driver).click()
            Log.WriteLine("TW")
            //
            val han = driver.windowHandles
            val numberOfTabs = han.size
            val newTabIndex = numberOfTabs - 1
            driver.switchTo().window(han.toTypedArray()[newTabIndex].toString())
            //
            val Element = DriverFindElement("//*[@class='sign-in']//*[@class='row user']", driver)
            Log.WriteLine("Логин tw")
            //
            val actions = Actions(driver)
            actions.moveToElement(Element)
            actions.click()
            actions.sendKeys("89778665883")
            actions.build().perform()
            //
            DriverFindElement("//*[@id='password']", driver).sendKeys("nekroman911")
            Log.WriteLine("Пароль")
            //
            DriverFindElement("//*[@class='button selected submit']", driver).click()
            Log.WriteLine("Войти")
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }


    @Test
    fun SchoolLessonsSharing4(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\School")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\School\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("School-Уроки-Шаринг-OD" + VersionBrowser(driver))





            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(1000)
            DriverNavigate(SchoolPlaneta, driver)
            //
            DriverFindElement("//*[@href='https://tv.test.planeta.ru/broadcast/367732']", driver).click()
            Log.WriteLine("Вводное занятие")
            //
            DriverFindElement("(//*[@class='sps-button']//*[@class='spsb-item'][4])", driver).click()
            Log.WriteLine("OD")
            //
            Thread.sleep(1500)
            val han = driver.windowHandles
            val numberOfTabs = han.size
            val newTabIndex = numberOfTabs - 1
            driver.switchTo().window(han.toTypedArray()[newTabIndex].toString())
            //
            DriverFindElement("//*[@id='field_email']", driver).sendKeys("89778665883")
            Log.WriteLine("Логин")
            //
            DriverFindElement("//*[@id='field_password']", driver).sendKeys("nekroman911")
            Log.WriteLine("Пароль")
            //
            DriverFindElement("//*[@class='button-pro __wide form-actions_yes']", driver).click()
            Log.WriteLine("Войти")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun SchoolLessonsSharing5(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\School")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\School\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("School-Уроки-Шаринг-Mail" + VersionBrowser(driver))





            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(1000)
            DriverNavigate(SchoolPlaneta, driver)
            //
            DriverFindElement("//*[@href='https://tv.test.planeta.ru/broadcast/367732']", driver).click()
            Log.WriteLine("Вводное занятие")
            //
            DriverFindElement("(//*[@class='sps-button']//*[@class='spsb-item'][5])", driver).click()
            Log.WriteLine("Mail")
            //
            Thread.sleep(1500)
            val han = driver.windowHandles
            val numberOfTabs = han.size
            val newTabIndex = numberOfTabs - 1
            driver.switchTo().window(han.toTypedArray()[newTabIndex].toString())
            //
            DriverFindElement("//*[@name='Login']", driver).sendKeys("kostyasus")
            Log.WriteLine("Ввод Логина")
            //
            DriverFindElement("//*[@name='Password']", driver).sendKeys("nekroman924")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='ui-button-main']", driver).click()
            Log.WriteLine("Войти")
            //
            DriverFindElement("//*[@class='ui-button-main']", driver).click()
            Log.WriteLine("Поделиться")
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun SchoolLessonsSharing6(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\School")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\School\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("School-Уроки-Шаринг-google +" + VersionBrowser(driver))




            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(1000)
            DriverNavigate(SchoolPlaneta, driver)
            //
            DriverFindElement("//*[@href='https://tv.test.planeta.ru/broadcast/367732']", driver).click()
            Log.WriteLine("Вводное занятие")
            //
            DriverFindElement("(//*[@class='sps-button']//*[@class='spsb-item'][6])", driver).click()
            Log.WriteLine("Google +")
            //
            Thread.sleep(2500)
            val han = driver.windowHandles
            val numberOfTabs = han.size
            val newTabIndex = numberOfTabs - 1
            driver.switchTo().window(han.toTypedArray()[newTabIndex].toString())
            Thread.sleep(500)
            DriverFindElement("//*[@name='identifier']", driver).sendKeys("opp23123@gmail.com")
            Log.WriteLine("Ввод почты")
            //
            DriverFindElement("(//*[@class='RveJvd snByac'])[2]", driver).click()
            Log.WriteLine("Далее")
            //
            //DriverFindElement("//*[@name='password']", driver).SendKeys("nekroman911");
            //Log.WriteLine("пароль");
            ////
            //DriverFindElement("(//*[@class='CwaK9'])", driver).Click();
            //Log.WriteLine("Далее");
            ////
            //DriverFindElement("//*[@id='pg7w8']", driver).SendKeys("adasdasdasdas");
            //Log.WriteLine("Ввод комментария");
            ////
            //DriverFindElement("//*[@class='U26fgb O0WRkf zZhnYe e3Duub C0oVfc']", driver).Click();
            //Log.WriteLine("Опубликовать");
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun SchoolLessonsSharing7(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\School")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\School\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("School-Уроки-Шаринг-SurfingBird" + VersionBrowser(driver))




            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(1000)
            DriverNavigate(SchoolPlaneta, driver)
            //
            DriverFindElement("//*[@href='https://tv.test.planeta.ru/broadcast/367732']", driver).click()
            Log.WriteLine("Вводное занятие")
            //
            DriverFindElement("(//*[@class='sps-button']//*[@class='spsb-item'][7])", driver).click()
            Log.WriteLine("SurfinBird +")
            //
            Thread.sleep(2500)
            val han = driver.windowHandles
            val numberOfTabs = han.size
            val newTabIndex = numberOfTabs - 1
            driver.switchTo().window(han.toTypedArray()[newTabIndex].toString())
            Thread.sleep(500)
            DriverFindElement("//*[@id='login__input']", driver).sendKeys("pain-net@yandex.ru")
            Log.WriteLine("Ввод почты")
            //
            DriverFindElement("//*[@id='password__input']", driver).sendKeys("nekroman911")
            Log.WriteLine("пароль")
            //
            DriverFindElement("(//*[@class='b-button__text'])", driver).click()
            Log.WriteLine("Далее")
            //
            DriverFindElement("//*[@id='add-page__title']", driver).sendKeys("adasdasdasdas")
            Log.WriteLine("Заголовок")
            ////
            DriverFindElement("//*[@id='add-page__desc']", driver).sendKeys("dasdasdasdasdwewew")
            Log.WriteLine("Описание")
            //
            DriverFindElement("//*[@id='categories-dropdown']", driver).click()
            //
            DriverFindElement("//*[@class='categories-list']/li[1]", driver).click()
            Log.WriteLine("Выбор категории")
            //
            DriverFindElement("//*[@class='b-button__text']", driver).click()
            Log.WriteLine("Добавить")
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun SchoolLessonsSharing8(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\School")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\School\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("School-Уроки-Шаринг-Telegram" + VersionBrowser(driver))





            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(1000)
            DriverNavigate(SchoolPlaneta, driver)
            //
            DriverFindElement("//*[@href='https://tv.test.planeta.ru/broadcast/367732']", driver).click()
            Log.WriteLine("Вводное занятие")
            //
            DriverFindElement("(//*[@class='sps-button']//*[@class='spsb-item'][8])", driver).click()
            Log.WriteLine("Telegram")
            //
            Thread.sleep(1500)
            val han = driver.windowHandles
            val numberOfTabs = han.size
            val newTabIndex = numberOfTabs - 1
            driver.switchTo().window(han.toTypedArray()[newTabIndex].toString())
            Thread.sleep(1000)
            robot.keyPress(KeyEvent.VK_ESCAPE)
            robot.keyRelease(KeyEvent.VK_ESCAPE)
            //
            DriverFindElement("//*[@class='tgme_action_button']", driver).click()
            Log.WriteLine("Share")
            //

            //DriverFindElement("//*[@id='password__input']").SendKeys("nekroman911");
            //Log.WriteLine("пароль");
            ////
            //DriverFindElement("(//*[@class='b-button__text'])").Click();
            //Log.WriteLine("Далее");
            ////
            //DriverFindElement("//*[@id='add-page__title']").SendKeys("adasdasdasdas");
            //Log.WriteLine("Заголовок");
            //////
            //DriverFindElement("//*[@id='add-page__desc']").SendKeys("dasdasdasdasdwewew");
            //Log.WriteLine("Описание");
            ////
            //DriverFindElement("//*[@id='categories-dropdown']").Click();
            ////
            //DriverFindElement("//*[@class='categories-list']/li[1]").Click();
            //Log.WriteLine("Выбор категории");
            ////
            //DriverFindElement("//*[@class='b-button__text']").Click();
            //Log.WriteLine("Добавить");
            ////
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }


    @Test
    fun CrowdProducerss(){

        try{
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\School")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Test_Element\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Страница Краудпродюссерс Оставить заявку + проверка страницы на кликабельность элементов" + VersionBrowser(driver))



            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(1000)
            DriverNavigate(SchoolPlaneta, driver)
            //
            DriverNavigate(CrowdProducerss,driver)
            //
            DriverFindElement("//*[@class='ed-welcome_btn ed-button']",driver).click()
            Log.WriteLine("Оставить заявку")
            //
            Thread.sleep(3000)
            //
            DriverFindElement("//*[@id='ed-form_name']",driver).sendKeys("Лысый Продюссер из браззерс")
            Log.WriteLine("Имя")
            //
            DriverFindElement("//*[@id='ed-form_mail']",driver).sendKeys("uz4name@yandex.ru")
            Log.WriteLine("Email")
            //
            DriverFindElement("//*[@id='ed-form_city']",driver).sendKeys("Москва")
            Log.WriteLine("Город")
            //
            DriverFindElement("(//*[@class='ed-form_check-label'])[1]",driver).click()
            Log.WriteLine("Хочу получить все бесплатно!")
            //
            val ac = Actions(driver)
            ac.sendKeys(Keys.TAB).build().perform()
            //
            val ac2 = Actions(driver)
            ac2.sendKeys(Keys.SPACE).build().perform()
            Log.WriteLine("Согласен с условиями")
            //
            DriverFindElement("(//*[@class='ed-button ed-form_submit'])[1]",driver).click()
            Log.WriteLine("Отправить")
            //
            DriverFindElement("//*[contains(text(), 'Заявка принята, спасибо!')]", driver)
            //
            DriverFindElement("//*[@class='ed-header_nav-toggle ed-header_nav-toggle__scroll']",driver).click()
            //
            for(i in 1..7){
                Thread.sleep(500)
                DriverFindElement("//*[@class='ed-header_nav-list']//li[$i]",driver).click()
                Thread.sleep(500)
                DriverFindElement("//*[@class='ed-header_nav-toggle ed-header_nav-toggle__scroll']",driver).click()
            }
            Log.WriteLine("Перебор списка ссылок")
            //
            DriverFindElement("//*[@class='ed-header_nav-toggle ed-header_nav-toggle__scroll']",driver).click()
            Log.WriteLine("Меню")
            //
            DriverFindElement("//*[@class='ed-welcome_btn ed-button']",driver).click()
            Log.WriteLine("Отправить заявку")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            e.printStackTrace()
        }finally {
            Stop1()
        }
    }
}