import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.interactions.Actions
import java.nio.file.Files
import java.nio.file.Paths



class A_UserLK : Setup() {

    @Test
    fun CreateProject(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            fun TextFound() : Boolean
            {
                return try {
                    driver.findElement(By.xpath("//*[contains(text(), 'создать')]"))
                    true
                } catch(e:Exception) {
                    false
                }
            }
            fun TextFound2() : Boolean
            {
                return try {
                    driver.findElement(By.xpath("//*[@class='header_create-link']"))
                    true
                } catch(e:Exception) {
                    false
                }
            }



            Log.WriteLine("Создать проект" + VersionBrowser(driver))





            Avtorization_Uz4name(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[1]", driver).click()
            Log.WriteLine("Созданные проекты")
            //
           // LeftClick(0, 801)
            //
            fun find() : Boolean
            {
                return try {
                    DriverFindElement("//*[@class='status-badge status-badge__info']", driver)
                    true
                } catch(e:Exception) {
                    false
                }

            }
            if (find())
            {
                try
                {
                    DriverFindElement("//*[@class='profile-nav']/li[2]", driver).click()

                }
                catch(e:Exception)
                {

                }
            }
            //
            while (true)
            {
                try
                {
                    Thread.sleep(500)
                    DriverFindElement("//*[@class='profile-project-info_delete-link']",driver).click()
                    Log.WriteLine("Удалить черновик")
                    Thread.sleep(500)
                    DriverFindElement("//*[@class='btn btn-primary']",driver).click()
                    Log.WriteLine("Да")
                    Thread.sleep(200)
                }
                catch (e:Exception)
                {
                    break
                }
            }


            if (TextFound())
            {
                Thread.sleep(500)
                driver.findElement(By.xpath("//*[contains(text(), 'создать')]")).click()
                Log.WriteLine("Создать")
            }
            if (TextFound2())
            {
                Thread.sleep(500)
                driver.findElement(By.xpath("//*[@class='header_create-link']")).click()
                Log.WriteLine("Создать проект")
            }
            //
            DriverFindElement("//*[@class='checkbox']", driver).click()
            Log.WriteLine("Я согласен с условиями пользования")
            //
            DriverFindElement("//*[@class='btn btn-primary frc-i-accept-btn create-campaign']", driver).click()
            Log.WriteLine("Создать проект")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        finally {
            Stop1()
        }
    }


    @Test
    fun CreateProject2(){
        try {
            Thread.sleep(60000)
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            fun TextFound() : Boolean
            {
                return try {
                    driver.findElement(By.xpath("//*[contains(text(), 'создать')]"))
                    true
                } catch(e:Exception) {
                    false
                }
            }
            fun TextFound2() : Boolean
            {
                return try {
                    driver.findElement(By.xpath("//*[@class='header_create-link']"))
                    true
                } catch(e:Exception) {
                    false
                }
            }



            Log.WriteLine("Создать проект 2 часть" + VersionBrowser(driver))






            Avtorization_Uz4name(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[1]", driver).click()
            Log.WriteLine("Созданные проекты")
            //
           // LeftClick(0, 801)
            //
            fun find() : Boolean
            {
                return try {
                    DriverFindElement("//*[@class='status-badge status-badge__info']", driver)
                    true
                } catch(e:Exception) {
                    false
                }

            }
            if (find())
                try
                {
                    DriverFindElement("//*[@class='profile-nav']/li[2]", driver).click()

                }
                catch(e:Exception)
                {

                }
            //
            while (true)
            {
                try
                {
                    Thread.sleep(500)
                    DriverFindElement("//*[@class='profile-project-info_delete-link']",driver).click()
                    Log.WriteLine("Удалить черновик")
                    Thread.sleep(500)
                    DriverFindElement("//*[@class='btn btn-primary']",driver).click()
                    Log.WriteLine("Да")
                    Thread.sleep(200)
                }
                catch (e:Exception)
                {
                    break
                }
            }

            //
            try
            {
                if (TextFound())
                {
                    driver.findElement(By.xpath("//*[contains(text(), 'создать')]")).click()
                    Log.WriteLine("Создать")
                }
                if (TextFound2())
                {
                    driver.findElement(By.xpath("//*[@class='header_create-link']")).click()
                    Log.WriteLine("Создать проект")
                }
            }
            catch(e:Exception)
            {

            }
            //
            DriverFindElement("//*[@class='checkbox']", driver).click()
            Log.WriteLine("Я согласен с условиями пользования")
            //
            DriverFindElement("//*[@class='btn btn-primary frc-i-accept-btn create-campaign']", driver).click()
            Log.WriteLine("Создать проект")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("//*[@class='header_create-link js-create-button']", driver).click()
            Log.WriteLine("Создать проект")
            //
            DriverFindElement("//*[@class='ctba-modal-draft-item']", driver).click()
            Log.WriteLine("Войти в созданный проект")
            //
            Thread.sleep(500)
            //
            DriverFindElement("//*[@class='header_create-link js-create-button']", driver).click()
            Log.WriteLine("Создать проект")
            //
            DriverFindElement("//*[@class='flat-btn']", driver).click()
            Log.WriteLine("Создать новый проект")
            //
            DriverFindElement("//*[@class='checkbox']", driver).click()
            Log.WriteLine("Я согласен с условиями пользования")
            //
            DriverFindElement("//*[@class='btn btn-primary frc-i-accept-btn create-campaign']", driver).click()
            Log.WriteLine("Создать проект")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }finally {
            Stop1()
        }
    }

    @Test
    fun DeleteProject(){
        try {
            Thread.sleep(125000)
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Удалить проект" + VersionBrowser(driver))






            Avtorization_Uz4name(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[1]", driver).click()
            Log.WriteLine("Созданные проекты")
            //
          //  LeftClick(0, 801)
            //
            fun find() : Boolean
            {
                return try {
                    DriverFindElement("//*[@class='status-badge status-badge__info']", driver)
                    true
                } catch(e:Exception) {
                    false
                }

            }
            if (find())
            {
                try
                {
                    DriverFindElement("//*[@class='profile-nav']/li[2]", driver).click()

                }
                catch(e:Exception)
                {

                }
            }
            //
            while (true)
            {
                try
                {
                    Thread.sleep(500)
                    DriverFindElement("//*[@class='profile-project-info_delete-link']",driver).click()
                    Log.WriteLine("Удалить черновик")
                    Thread.sleep(500)
                    DriverFindElement("//*[@class='btn btn-primary']",driver).click()
                    Log.WriteLine("Да")
                    Thread.sleep(200)
                }
                catch (e:Exception)
                {
                    break
                }
            }
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        finally {
            Stop1()
        }
    }


    @Test
    fun UserProjectFindFiltr(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("User-Меню Проекты + фильтры" + VersionBrowser(driver))





            Avtorization_Uz4name(driver,Log)
            //
            DriverFindElement("(//*[@class='h-menu_link'])[1]", driver)
            Log.WriteLine("Проекты")
            //
            val actions = Actions(driver)
            actions.moveToElement(DriverFindElement("(//*[@class='h-menu_link'])[1]",driver)).perform()
            //
            DriverFindElement("//*[@class='h-menu_sub']//li[1]", driver).click()
            Log.WriteLine("Последние Обновлённые")
            //
            DriverFindElement("(//*[@class='h-menu_link'])[1]", driver)
            Log.WriteLine("Проекты")
            //
            val actions1 = Actions(driver)
            actions1.moveToElement( DriverFindElement("(//*[@class='h-menu_link'])[1]", driver)).perform()
            //
            DriverFindElement("//*[@class='h-menu_sub']//li[2]", driver).click()
            Log.WriteLine("Новые")
            //
            DriverFindElement("(//*[@class='h-menu_link'])[1]", driver)
            Log.WriteLine("Проекты")
            //
            val actions3 = Actions(driver)
            actions3.moveToElement(DriverFindElement("(//*[@class='h-menu_link'])[1]", driver)).perform()
            //
            DriverFindElement("//*[@class='h-menu_sub']//li[3]", driver).click()
            Log.WriteLine("Активные")
            //
            DriverFindElement("(//*[@class='h-menu_link'])[1]", driver)
            Log.WriteLine("Проекты")
            //
            val actions4 = Actions(driver)
            actions4.moveToElement(  DriverFindElement("(//*[@class='h-menu_link'])[1]", driver)).perform()
            //
            DriverFindElement("//*[@class='h-menu_sub']//li[4]", driver).click()
            Log.WriteLine("Обсуждаемые")
            //
            DriverFindElement("(//*[@class='h-menu_link'])[1]", driver)
            Log.WriteLine("Проекты")
            //
            val actions5 = Actions(driver)
            actions5.moveToElement(DriverFindElement("(//*[@class='h-menu_link'])[1]", driver)).perform()
            //
            DriverFindElement("//*[@class='h-menu_sub']//li[5]", driver).click()
            Log.WriteLine("Близкие к завершению")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        finally {
            Stop1()
        }
    }


    @Test
    fun UserProjectCVoznagragdeniya(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("User-Меню Вознаграждения" + VersionBrowser(driver))






            Avtorization_Uz4name(driver,Log)
            //
            DriverFindElement("(//*[@class='h-menu_link'])[2]", driver).click()
            Log.WriteLine("Вознаграждения")
            //
            for (i in 1..16) {
                Thread.sleep(400)
                DriverFindElement("(//*[@class='rewards-filter-list']//*[@class='form-ui-txt'])[" + i.toString() + "]", driver).click()
                DriverFindElement("(//*[@class='form-ui-txt'])[" + i.toString() + "]", driver).click()
            }

            for (i in 1..16) {
                Thread.sleep(400)
                DriverFindElement("(//*[@class='rewards-filter-list']//*[@class='form-ui-txt'])[" + i.toString() + "]", driver).click()
            }

            DriverFindElement("//*[@class='button button__b button__act button__sm']", driver).click()
            Log.WriteLine("Сбросить")
            //
            DriverFindElement("(//*[@id='search-rewards-from'])[1]", driver).sendKeys("1000")
            Log.WriteLine("Стоимость от")
            //
            DriverFindElement("(//*[@id='search-rewards-to'])[1]", driver).click()
            //
            DriverFindElement("(//*[@id='search-rewards-to'])[1]", driver).sendKeys("2000")
            Log.WriteLine("Стоимость До")
            //
            DriverFindElement("(//*[@class='button button__b button__act button__sm'])[1]", driver).click()
            Log.WriteLine("Сбросить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        finally {
            Stop1()
        }
    }



}