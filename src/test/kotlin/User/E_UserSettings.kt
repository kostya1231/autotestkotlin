import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.interactions.Actions
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

class E_UserSettings : Setup() {

    @Test
    fun Settings(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("User-Настройки" + VersionBrowser(driver))





            Avtorization_Uz4name(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[6]", driver).click()
            Log.WriteLine("Настройки")
            //
            DriverFindElement("//*[@id='profile-page.settings-block.name']", driver).clear()
            Log.WriteLine("Стереть имя в поле")
            //
            DriverFindElement("//*[@id='profile-page.settings-block.name']", driver).sendKeys("uzname")
            Log.WriteLine("Ввод имени")
            ////
            //DriverFindElement("//*[@id='profile-page.settings-block.alias']").SendKeys("azazazaza");
            //Log.WriteLine("Адрес страницы");
            ////
            DriverFindElement("(//*[@class='select-btn'])[1]", driver).click()
            Log.WriteLine("Пол")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])//li[1]", driver).click()
            Log.WriteLine("Мужской")
            //
            DriverFindElement("//*[@id='core.birth-date']", driver).sendKeys("29.04.1993")
            Log.WriteLine("Ввод даты рождения")
            //
            DriverFindElement("//*[@id='profile-page.settings-block.name']", driver).click()
            Log.WriteLine("Клик по полю имя")
            //
            DriverFindElement("//*[@id='profile-page.settings-block.phone']", driver).clear()
            //
            DriverFindElement("//*[@id='profile-page.settings-block.phone']", driver).sendKeys("89778665882")
            Log.WriteLine("Ввод телефона")
            //
            DriverFindElement("(//*[@class='select-btn'])[2]", driver).click()
            Log.WriteLine("Страна")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[2]//li[1]", driver).click()
            Log.WriteLine("Россия")
            //
            DriverFindElement("//*[@id='core.city']", driver).sendKeys("Москва")
            Log.WriteLine("Москва")
            Thread.sleep(500)
            DriverFindElement("(//*[@class='dropdown-menu'])[3]//li[1]", driver).click()
            //
            DriverFindElement("//*[@class='profile-settings_photo-link']", driver).click()
            Log.WriteLine("Изменить аватарку")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            DriverFindElement("//*[@class='button button__def button__xs']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary'])[2]", driver).click()
            Log.WriteLine("Сохранить изменения")
            //
            DriverFindElement("//*[@class='form-control pl-input-control_val pl-input-control_val-inner ng-untouched ng-pristine ng-valid ng-star-inserted']", driver).clear()
            Log.WriteLine("Очистить поле Раскажите о себе")
            //
            DriverFindElement("//*[@placeholder='Расскажите о себе в двух предложениях']", driver).sendKeys("dasdasdasdasdasdas" +
                    "dasdasda" +
                    "sdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasda" +
                    "" +
                    "dasdasdasdasdasdasdasdasdasdasdasdas" +
                    "dasdasdas" +
                    "dasdasdasdasdasdasdasdasdasdasdasdas" +
                    "dasdasdasdasdasdasdasdasdasdasdasdas" +
                    "dasdasdasdasdasdassdasdasdasdasdasdasda" +
                    "sdas")
            Log.WriteLine("Ввод значения")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary']", driver).click()
            Log.WriteLine("Сохранить изменения")
            //
            Thread.sleep(1500)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }


    @Test
    fun  SettingsContact(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("User-Настройки-Контакты" + VersionBrowser(driver))





            Avtorization_Uz4name(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[6]", driver).click()
            Log.WriteLine("Настройки")
            //
            DriverFindElement("//*[@class='profile-nav']//li[2]", driver).click()
            Log.WriteLine("Контакты")
            //

            DriverFindElement("//*[@id='profile-page.settings-block.site']", driver).clear()
            DriverFindElement("(//*[@id='instagram'])", driver).clear()
            DriverFindElement("//*[@id='profile-page.settings-block.vk']", driver).clear()
            DriverFindElement("//*[@id='facebook']", driver).clear()
            DriverFindElement("//*[@id='twitter']", driver).clear()
            DriverFindElement("//*[@id='google+']", driver).clear()
            DriverFindElement("//*[@id='Телеграм']", driver).clear()
            //

            DriverFindElement("//*[@id='profile-page.settings-block.site']", driver).sendKeys("Qwerty")
            Log.WriteLine("Сайт")
            //
            DriverFindElement("(//*[@id='instagram'])", driver).sendKeys("Qwerty")
            Log.WriteLine("Instagram")
            //
            DriverFindElement("//*[@id='profile-page.settings-block.vk']", driver).sendKeys("Qwerty")
            Log.WriteLine("Vkontakte")
            //
            DriverFindElement("//*[@id='facebook']", driver).sendKeys("Qwerty")
            Log.WriteLine("Facebook")
            //
            DriverFindElement("//*[@id='twitter']", driver).sendKeys("Qwerty")
            Log.WriteLine("Twitter")
            //
            DriverFindElement("//*[@id='google+']", driver).sendKeys("Qwerty")
            Log.WriteLine("Google+")
            //
            DriverFindElement("//*[@id='Телеграм']", driver).sendKeys("Qwerty")
            Log.WriteLine("Телеграм")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary'])", driver).click()
            Log.WriteLine("Сохранить изменения")
            //
            Thread.sleep(1500)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun SettingsDeleteAccaunt(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("User-Настройки-Удалить аккаунт" + VersionBrowser(driver))






            AvtirizationOther(driver,Log)
            //
            var text = DriverFindElement("//*[@class='h-user_name']", driver) // Вытащить текст из элемента
            var Text1:String = text.text
            Text1 = Text1.replace("\\s".toRegex(),"")
            //
            Thread.sleep(4000)
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Личный кабинет")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[6]", driver).click()
            Log.WriteLine("Настройки")
            //
            Thread.sleep(500)
            DriverFindElement("//*[@class='profile-settings_profile-actions-link profile-settings_remove-profile-link']", driver).click()
            Log.WriteLine("Удалить аккаунт")
            //
            DriverFindElement("//*[@class='btn btn-nd-default']", driver).click()
            Log.WriteLine("Нет")
            //
            Thread.sleep(500)
            DriverFindElement("//*[@class='profile-settings_profile-actions-link profile-settings_remove-profile-link']", driver).click()
            Log.WriteLine("Удалить аккаунт")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary'])[2]", driver).click()
            Log.WriteLine("Да")
            //
            Thread.sleep(3500)
            //
            driver.navigate().to(URL)
            //
            Thread.sleep(1500)
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("$Text1@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[contains(text(), 'Неверный логин или пароль')]", driver)
            Log.WriteLine("Неверный логин или пароль")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun SettingsDeleteAccountProject(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            //Генератор чисел
            var world = 4
            val r = Random()
            val number4 = StringBuilder()
            for (i in 0 until world) {
                val tmp = r.nextInt(3)
                number4.append(tmp)
            }

            var world10 = 10
            val r2 = Random()
            var number10 = StringBuilder()
            for (i in 0 until world10) {
                val tmp = r2.nextInt(10)
                number10.append(tmp)
            }

            var world1 = 9
            val r1 = Random()
            var number9 = StringBuilder()
            for (i in 0 until world1) {
                val tmp = r1.nextInt(9)
                number9.append(tmp)
            }





            Log.WriteLine("User-Настройки-Удалить аккаунт наличие на акк-те вознаграждения + на модерации + Запрос в Support" + VersionBrowser(driver))






            AvtirizationOther(driver,Log)
            //
            val text = DriverFindElement("//*[@class='h-user_name']", driver) // Вытащить текст из элемента
            var Text1:String = text.text
            Text1.replace("\\s".toRegex(),"")
            //
            Thread.sleep(4000)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[1]", driver).click()
            Log.WriteLine("Созданные проекты")
            //
            try
            {
                driver.findElement(By.xpath("//*[contains(text(), 'создать')]")).click()
                Log.WriteLine("Создать")
                Thread.sleep(500)
            }
            catch (e:Exception)
            {
                Thread.sleep(1500)
                driver.findElement(By.xpath("//*[@class='header_create-link']")).click()
                Log.WriteLine("Создать Проект")
                Thread.sleep(500)
            }

            Thread.sleep(4500)
            //
            DriverFindElement("//*[@class='checkbox']", driver).click()
            Log.WriteLine("Checkbox")
            //

            DriverFindElement("//*[@class='btn btn-primary frc-i-accept-btn create-campaign']", driver).click()
            Log.WriteLine("Создать проект")
            //
            DriverFindElement("//*[@class='h-reg-close-icon']",driver).click()
            Log.WriteLine("Убрать уведомление о напоминание письма")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[1]", driver).sendKeys("Z_Settings" + Date())
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='form-control control-xlg borderless'])", driver).sendKeys("web$number4$number4$number4")
            Log.WriteLine("Красивый адрес проекта")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            try{
                DriverFindElement("//*[@class='btn btn-primary']",driver).click()
                Log.WriteLine("Сохранить")
            }catch (e:Exception){

            }
            //
            Thread.sleep(2800)
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")
            //
            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Фото по ссылке")
            //
            DriverFindElement("//*[@name='img_url']", driver).sendKeys("https://2x2tv.ru/upload/medialibrary/5ed/5ed31ef87f88f58dca8056fa951be36b.png")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@class='btn btn-primary']",driver).click()
            Log.WriteLine("Готово")
            //
            try{
                DriverFindElement("//*[@class='btn btn-primary']",driver).click()
                Log.WriteLine("Сохранить")
            }catch (e:Exception){
                Log.WriteLine("Сохранить не выскочило т к размер фото меньше")
            }
            //
            Thread.sleep(2800)
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[2]", driver).sendKeys("cxcdsfdsfqq")
            Log.WriteLine("Коротко о проекте")
            //
            DriverFindElement("//*[@class='icon-select']", driver).click()
            Log.WriteLine("Страна реализции")
            //
            DriverFindElement("//*[@class='dropdown-menu']/li[2]", driver).click()
            Log.WriteLine("Выбор страны")
            //
            DriverFindElement("//*[@class='form-control control-xlg prj-crt-input ac_input']", driver).sendKeys("Москва")
            Log.WriteLine("Регион реализации")
            //
            DriverFindElement("//*[@class='ac_results']//li[1]", driver).click()
            Log.WriteLine("Москва")
            //
            DriverFindElement("//*[@name='cityNameRus']", driver).sendKeys("Алабино")
            Log.WriteLine("Город реализации")
            //
            DriverFindElement("//*[@class='ac_results']//*[@class='ac_even ac_over']", driver).click()
            //
            DriverFindElement("//*[@data-parse='number']", driver).clear()
            //
            DriverFindElement("//*[@data-parse='number']", driver).sendKeys("500000")
            Log.WriteLine("Финансовая цель")
            //
            DriverFindElement("//*[@class='project-create_col-val_col']", driver).click()
            Log.WriteLine("Срок окончания проекта")
            //
            DriverFindElement("(//*[@data-parse='number'])[2]", driver).click()
            //
            DriverFindElement("//*[@class='ui-datepicker-next ui-corner-all']", driver).click()
            Log.WriteLine("Следующий месяц")
            //
            DriverFindElement("//*[@class='ui-datepicker-calendar']//tr[4]//td[5]", driver).click()
            Log.WriteLine("Выбор даты")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("(//*[@class='project-create_tab_link'])[2]", driver).click()
            Log.WriteLine("Детали")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            try{
                DriverFindElement("//*[@class='btn btn-primary']",driver).click()
                Log.WriteLine("Сохранить")
            }catch (e:Exception){
                Log.WriteLine("Сохранить не выскочило т к размер фото меньше")
            }
            //
            Thread.sleep(1500)
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")
            //
            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Видео с Ютуба")
            //
            DriverFindElement("//*[@name='img_url']", driver).sendKeys("https://www.youtube.com/watch?v=VVtZVabutYM")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")
            //
            DriverFindElement("(//*[@class='mceIcon mce_planetaphoto'])", driver).click()
            Log.WriteLine("Загрузить картинку в поле text-area")
            //
            DriverFindElement("//*[@class='link']", driver).click()
            Log.WriteLine("Загрузить картинку по ссылке")
            //
            DriverFindElement("//*[@name='img_url']", driver).sendKeys("https://i.kym-cdn.com/photos/images/original/001/252/564/772.jpg")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("(//*[@class='project-create_tab_link'])[3]", driver).click()
            Log.WriteLine("Вознаграждение")
            //
            DriverFindElement("//*[@class='form-control prj-crt-input js-share-name-limit']", driver).sendKeys("SATANA" + Date())
            Log.WriteLine("Название вознаграждения")
            //
            driver.switchTo().frame(0)
            //
            DriverFindElement("//*[@class='mceContentBody  mceCampaignEditorBody']", driver).sendKeys("666")
            Log.WriteLine("Описание вознаграждения")
            //
            driver.switchTo().defaultContent()
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[1]", driver).sendKeys("Прямо в жерло преисподни")
            Log.WriteLine("Способы получения")
            //
            DriverFindElement("(//*[@name='price'])", driver).clear()
            //
            DriverFindElement("(//*[@name='price'])", driver).sendKeys("100000")
            Log.WriteLine("Цена вознаграждения")
            //
            DriverFindElement("//*[@class='form-control prj-crt-input prj-crt-input__date hasDatepicker']", driver).click()
            Log.WriteLine("Примерная дата доставки")
            //
            DriverFindElement("//*[@class='ui-datepicker-calendar']//tr[4]//td[5]", driver).click()
            Log.WriteLine("Выбор даты")
            //
            val el3 = DriverFindElement("//*[@type='file']", driver)
            el3.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")
            //
            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Фото по ссылке")
            //
            DriverFindElement("//*[@name='img_url']", driver).sendKeys("https://i.kym-cdn.com/photos/images/original/001/252/564/772.jpg")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")
            //
            DriverFindElement("(//*[@class='checkbox'])[1]", driver).click()
            Log.WriteLine("Запрашивать адрес доставки")
            //
            DriverFindElement("(//*[@class='checkbox'])[1]", driver).click()
            Log.WriteLine("Возможен самовывоз")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[4]", driver).sendKeys("Москва")
            Log.WriteLine("Город")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[5]", driver).sendKeys("Ул Малая Большая д 23 кв 666")
            Log.WriteLine("Адрес и условия самовывоза")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[6]", driver).sendKeys("74956661323")
            Log.WriteLine("Телефон")
            //
            DriverFindElement("(//*[@class='checkbox'])[1]", driver).click()
            Log.WriteLine("Задать вопрос покупателю")
            Thread.sleep(2500)
            driver.findElement(By.xpath("//*[@class='select-cont']")).click()
            //
            DriverFindElement("//*[@class='dropdown-menu']//li[1]", driver).click()
            Log.WriteLine("Выбор вопроса")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-lg']", driver).click()
            Log.WriteLine("Добавить изображение")
            //
            try{
                DriverFindElement("//*[@class='btn btn-primary']",driver).click()
                Log.WriteLine("Сохранить")
            }catch (p:Exception){
                Log.WriteLine("Сохранить не выскочило т к размер фото меньше")
            }
            //
            DriverFindElement("(//*[@class='project-create_tab_link'])[4]", driver).click()
            Log.WriteLine("Контр-Агент")
            //
            DriverFindElement("(//*[@class='select-cont'])[1]", driver).click()
            Log.WriteLine("Выбор нового контрАгента")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[1]//li//*[contains(text(), '                      Новый контрагент                 ')]", driver)
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[1]//li//*[contains(text(), '                      Новый контрагент                 ')]", driver).click()
            Log.WriteLine("Выбор нового контр агента")
            //
            DriverFindElement("(//*[@class='icon-select'])[2]", driver).click()
            Log.WriteLine("Тип")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[2]//*[contains(text(), 'ООО')]", driver).click()
            Log.WriteLine("Выбор ООО")
            //
            DriverFindElement("(//*[@class='select-cont'])[3]", driver).click()
            Log.WriteLine("Должность руководителя")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[3]/li[3]", driver).click()
            Log.WriteLine("Генеральный директор")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[1]", driver).sendKeys("фывфывфывфывфывфывфывфы")
            Log.WriteLine("ФИО физического лица ИП или наименование организации для юридического лица")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[2]", driver).sendKeys("И.И ИВанов")
            Log.WriteLine("Фио Руководителя")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[3]", driver).sendKeys("Иван.Иванович  ИВанов")
            Log.WriteLine("Инициалы и фамилия для подписи в договоре")
            //
            DriverFindElement("(//*[@class='icon-select'])[4]", driver).click()
            Log.WriteLine("Страна регистрации")
            //
            DriverFindElement("//*[@class='form-control control-xlg prj-crt-input js-city-name-input ac_input']", driver).sendKeys("Москва")
            Log.WriteLine("Город")
            //
            DriverFindElement("//*[@class='ac_results']//*[@class='ac_even ac_over']", driver).click()
            Log.WriteLine("Москва")
            //
            val ac = Actions(driver)
            ac.sendKeys(Keys.ENTER).build().perform()
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[4]", driver).sendKeys("84955555535")
            Log.WriteLine("Телефон")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[5]", driver).sendKeys(number10.toString() + "666")
            Log.WriteLine("ОГРН")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[6]", driver).sendKeys(number10.toString())
            Log.WriteLine("ИНН")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[7]", driver).sendKeys("123!@#fsdfавыа")
            Log.WriteLine("КПП")
            //
            DriverFindElement("(//*[@class='btn upload-file mrg-t-5'])", driver).click()
            Log.WriteLine("Необходимые документы для оформления договора - Загрузить")
            //
            val el4 = DriverFindElement("//*[@type='file']", driver)
            el4.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input js-save-model'])[1]", driver).sendKeys("Qkfdsfdkjhkjdfhkjdkjhfgkjdfhkjghfkjdh")
            Log.WriteLine("Юридический адрес")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[8]", driver).sendKeys("Фактический адрес")
            Log.WriteLine("Фактический адрес")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[9]", driver).sendKeys(number10.toString() + number10.toString())
            Log.WriteLine("Расчётный счет")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[10]", driver).sendKeys("Банк Анимэ и кавая")
            Log.WriteLine("Банк получателя")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[11]", driver).sendKeys(number10.toString() + number10.toString())
            Log.WriteLine("Корреспондентский счет")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[12]", driver).sendKeys(number9.toString())
            Log.WriteLine("БИК")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input js-save-model'])[2]", driver).sendKeys("Ашкараагалдыыыыыыыыыыыыыыыыыыы")
            Log.WriteLine("Прочие данные")
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            Thread.sleep(3500)
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn btn-primary project-create_next']", driver).click()
            Log.WriteLine("Отправить на модерацию")
            //
            Thread.sleep(4000)
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Личный кабинет")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[6]", driver).click()
            Log.WriteLine("Настройки")
            //
            DriverFindElement("//*[@class='profile-settings_profile-actions-link profile-settings_remove-profile-link']", driver).click()
            Log.WriteLine("Удалить аккаунт")
            //
            DriverFindElement("//*[@class='btn btn-nd-default']", driver).click()
            Log.WriteLine("Нет")
            //
            DriverFindElement("//*[@class='profile-settings_profile-actions-link profile-settings_remove-profile-link']", driver).click()
            Log.WriteLine("Удалить аккаунт")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary'])[2]", driver).click()
            Log.WriteLine("Да")
            //
            DriverFindElement("//*[@class='profile-settings_profile-actions-link profile-settings_remove-profile-link']", driver).click()
            Log.WriteLine("Удалить аккаунт")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary'])[2]", driver).click()
            Log.WriteLine("Да")
            //
            DriverFindElement("//*[@class='toast-message ng-star-inserted']", driver).isDisplayed
            Log.WriteLine("Запрос на удаление вашего аккаунта принят")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun SettingsDeleteAccountSupport(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



          AvtirizationOther(driver,Log)
            //
            val text = DriverFindElement("//*[@class='h-user_name']", driver) // Вытащить текст из элемента
            var Text1:String = text.text
            Text1 = Text1.replace("\\s".toRegex(),"")
            //
            Thread.sleep(4000)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[4]", driver).click()
            Log.WriteLine("Баланс")
            //
            DriverFindElement("//*[@class='button button__def button__sm']", driver).click()
            Log.WriteLine("Пополнить баланс")
            //
            DriverFindElement("//*[@id='core.total-amount']", driver).clear()
            Log.WriteLine("Очистить поле")
            //
            DriverFindElement("//*[@id='core.total-amount']", driver).sendKeys("1000")
            Log.WriteLine("Ввод в поле значения 1000")
            //
            DriverFindElement("(//*[@class='project-payment-choice_name'])[2]",driver).click()
            Log.WriteLine("Клауд")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary project-payment_btn']", driver).click()
            Log.WriteLine("Перейти к оплате Клауд")
            //
            fun FindEl() : Boolean{
                return try{
                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver)
                    //Согласен на условия
                    true
                }catch (e:Exception){
                    false
                }
            }

            for(i in 0..60){  //Клауд
                if(FindEl()){

                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
                    Log.WriteLine("Оплата картой")
                    //
                    driver.switchTo().frame(0)
                    DriverFindElement("(//*[@class='row'])[1]//input",driver).click()
                    Log.WriteLine("клик по строке")
                    DriverFindElement("(//*[@class='row'])[1]//input",driver).sendKeys("4111 1111 1111 1111")
                    //
                    DriverFindElement("(//*[@id='inputMonth'])[1]",driver).sendKeys("12")
                    //
                    DriverFindElement("(//*[@id='inputYear'])[1]",driver).sendKeys("19")
                    //
                    DriverFindElement("(//*[@data-mask='000'])[1]",driver).sendKeys("123")
                    //
                    DriverFindElement("(//*[@id='cardHolder'])[1]",driver).sendKeys("QWERTY")
                    //
                    DriverFindElement("(//*[@class='button'])[1]",driver).click()
                    DriverFindElement("(//*[@class='button'])[2]",driver).click()
                    Log.WriteLine("OK")
                    driver.switchTo().defaultContent()
                    break
                }
            }



//            DriverFindElement("(//*[@id='email'])[1]", driver).clear()
//            DriverFindElement("(//*[@id='email'])[1]", driver).sendKeys("pain-net@yandex.ru")
//            Log.WriteLine("Сбер ввод email")
//            ////
//            DriverFindElement("(//*[@id='cardnumber'])[1]", driver).sendKeys("4111 1111 1111 1111")
//            Log.WriteLine("Ваш банк ввод номера карты")
//            //
//            DriverFindElement("(//*[@name='expdate'])[1]", driver).sendKeys("1219")
//            Log.WriteLine("Ваш банк ввод месяц год")
//            //
//            DriverFindElement("(//*[@id='cvc'])[1]", driver).sendKeys("123")
//            Log.WriteLine("Ваш банк ввод CVC2")
//            //
//            DriverFindElement("(//*[@class='sbersafe-pay-button'])", driver).click()
//            Log.WriteLine("Оплатить")
//            //
//            DriverFindElement("(//*[@name='password'])", driver).sendKeys("12345678")
//            Log.WriteLine("password")
//            //
//            DriverFindElement("(//*[@value='Submit'])", driver).click()
//            Log.WriteLine("Submit")
            Thread.sleep(3500)
            DriverFindElement("//*[contains(text(), 'успешно')]", driver)
            //
            driver.navigate().to(URL)
            //
            Thread.sleep(4000)
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Личный кабинет")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[6]", driver).click()
            Log.WriteLine("Настройки")
            //
            DriverFindElement("//*[@class='profile-settings_profile-actions-link profile-settings_remove-profile-link']", driver).click()
            Log.WriteLine("Удалить аккаунт")
            //
            DriverFindElement("//*[@class='btn btn-nd-default']", driver).click()
            Log.WriteLine("Нет")
            //
            DriverFindElement("//*[@class='profile-settings_profile-actions-link profile-settings_remove-profile-link']", driver).click()
            Log.WriteLine("Удалить аккаунт")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary'])[2]", driver).click()
            Log.WriteLine("Да")
            //
            DriverFindElement("//*[@class='profile-settings_profile-actions-link profile-settings_remove-profile-link']", driver).click()
            Log.WriteLine("Удалить аккаунт")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary'])[2]", driver).click()
            Log.WriteLine("Да")
            //
            DriverFindElement("//*[@class='toast-message ng-star-inserted']", driver)
            Log.WriteLine("Запрос на удаление вашего аккаунта принят")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun SettingsDezautorization(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("User-Настройки-Выйти со всех устройств" + VersionBrowser(driver))





            Avtorization_Uz2name(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[6]", driver).click()
            Log.WriteLine("Настройки")
            //
            DriverFindElement("//*[@class='profile-settings_profile-actions-link']", driver).click()
            Log.WriteLine("Выйти со всех устройств")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary'])[2]", driver).click()
            Log.WriteLine("Да")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            //DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).SendKeys("uzname@yandex.ru");
            //Log.WriteLine("Ввод логина");
            ////
            //DriverFindElement("(//*[@name='password'])", driver).SendKeys("nekroman911");
            //Log.WriteLine("Ввод пароля");
            ////
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            Thread.sleep(2500)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun SettingsNotice(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("User-Настройки-Уведомлений" + VersionBrowser(driver))





            Avtorization_Uz4name(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[6]", driver).click()
            Log.WriteLine("Настройки")
            //
            DriverFindElement("//*[@class='profile-nav']//li[3]", driver).click()
            Log.WriteLine("Настройки уведомлений")
            //
            DriverFindElement("(//*[@class='form-ui-val'])[1]", driver).click()
            Log.WriteLine("Получать новости Planeta.ru-Отключить")
            //
            DriverFindElement("(//*[@class='form-ui-val'])[2]", driver).click()
            Log.WriteLine("Получать новости проектов, которые я поддержал-Отключить")
            //
            DriverFindElement("(//*[@class='form-ui-val'])[3]", driver).click()
            Log.WriteLine("Получать статистику созданных мной проектов-Отключить")
            //
            DriverFindElement("(//*[@class='form-ui-val'])[4]", driver).click()
            Log.WriteLine("Получать советы и рекомендации по созданию проекта от Planeta.ru-Отключить")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary']", driver).click()
            Log.WriteLine("Сохранить изменения")
            //
            DriverFindElement("(//*[@class='form-ui-val'])[1]", driver).click()
            Log.WriteLine("Получать новости Planeta.ru-Включить")
            //
            DriverFindElement("(//*[@class='form-ui-val'])[2]", driver).click()
            Log.WriteLine("Получать новости проектов, которые я поддержал-Включить")
            //
            DriverFindElement("(//*[@class='form-ui-val'])[3]", driver).click()
            Log.WriteLine("Получать статистику созданных мной проектов-Включить")
            //
            DriverFindElement("(//*[@class='form-ui-val'])[4]", driver).click()
            Log.WriteLine("Получать советы и рекомендации по созданию проекта от Planeta.ru-Включить")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary']", driver).click()
            Log.WriteLine("Сохранить изменения")
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun SettingsPassword(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()

            Log.WriteLine("User-Настройки-Изменение пароля" + VersionBrowser(driver))






            Avtorization_Uz4name(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[6]", driver).click()
            Log.WriteLine("Настройки")
            //
            DriverFindElement("//*[@class='profile-nav']//li[4]", driver).click()
            Log.WriteLine("Настройки Изменение пароля")
            //
            DriverFindElement("//*[@id='profile-page.settings-block.old-password']", driver).sendKeys("nekroman911")
            Log.WriteLine("Старый пароль")
            //
            DriverFindElement("//*[@id='profile-page.settings-block.new-password']", driver).sendKeys("nekroman911")
            Log.WriteLine("Новый пароль")
            //
            DriverFindElement("//*[@id='profile-page.settings-block.re-password']", driver).sendKeys("nekroman911")
            Log.WriteLine("Повторите  пароль")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary']", driver).click()
            Log.WriteLine("Сохранить изменения")
            //
            Log.Close()
            Thread.sleep(2000)
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }



}