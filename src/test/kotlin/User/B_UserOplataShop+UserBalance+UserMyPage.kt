import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import java.nio.file.Files
import java.nio.file.Paths

class `B_UserOplataShop+UserBalance+UserMyPage` : Setup() {

    @Test
    fun SupportedProjectOplataShopTovarDostavka(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Поддержанные проекты (Магазин) доставка почтой Оплата сбер" + VersionBrowser(driver))






            Avtorization_Uz4name(driver,Log)
            //
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[2]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            DriverFindElement("//*[contains(text(), '                                                                                                               Доставка почтой                                                                          ,                                                                                                                Самовывоз                                                                                                                                        ')]", driver).click()
            //
            DriverFindElement("//*[contains(text(), 'Купить (1 шт.)')]", driver).click()
            Log.WriteLine("Купить 1 шт")
            //
            DriverFindElement("//*[@id='payment-page.question-to-buyer-label']", driver).sendKeys("123")
            Log.WriteLine("Данные")
            DriverFindElement("//*[@id='core.locality']",driver).sendKeys("Луна")
            Log.WriteLine("Населённый пункт")
            //
            DriverFindElement("(//*[@id='core.zip-code'])[1]",driver).sendKeys("11111111")
            Log.WriteLine("Индекс")
            //
            DriverFindElement("(//*[@id='core.address'])[1]",driver).sendKeys("Большой кратер")
            Log.WriteLine("Адрес")
            //
            DriverFindElement("(//*[@id='core.recipient-name'])[1]",driver).sendKeys("Николас кейдж")
            Log.WriteLine("ФИО получателя")
            //
            DriverFindElement("(//*[@id='core.contact-phone'])[1]",driver).sendKeys("84952222222")
            Log.WriteLine("контактный телефон")
            //
            fun FindEl() : Boolean{
                return try{
                    DriverFindElement("(//*[@class='form-ui-label'])[1]", driver)
                    //Согласен на условия
                    true
                }catch (e:Exception){
                    false
                }
            }

            for(i in 0..60){  //Клауд
                if(FindEl()){

                    DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                    Log.WriteLine("Даю свое согласие")

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver).click()
                    Log.WriteLine("Карты других банков")
                    //
                    DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
                    Log.WriteLine("Оплатить покупку")
                    //
                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
                    Log.WriteLine("Оплата картой")
                    //
                    driver.switchTo().frame(0)
                    DriverFindElement("(//*[@class='row'])[1]//input",driver).click()
                    Log.WriteLine("клик по строке")
                    DriverFindElement("(//*[@class='row'])[1]//input",driver).sendKeys("4111 1111 1111 1111")
                    //
                    DriverFindElement("(//*[@id='inputMonth'])[1]",driver).sendKeys("12")
                    //
                    DriverFindElement("(//*[@id='inputYear'])[1]",driver).sendKeys("19")
                    //
                    DriverFindElement("(//*[@data-mask='000'])[1]",driver).sendKeys("123")
                    //
                    DriverFindElement("(//*[@id='cardHolder'])[1]",driver).sendKeys("QWERTY")
                    //
                    DriverFindElement("(//*[@class='button'])[1]",driver).click()
                    DriverFindElement("(//*[@class='button'])[2]",driver).click()
                    Log.WriteLine("OK")
                    driver.switchTo().defaultContent()
                    break
                }
            }
//            DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
//            Log.WriteLine("Даю свое согласие")
//            //
//            DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
//            Log.WriteLine("Оплатить покупку")
//            //
//            //DriverFindElement("(//*[@id='email'])[1]", driver).SendKeys("pain-net@yandex.ru");
//            //Log.WriteLine("Сбер ввод email");
//            ////
//            DriverFindElement("(//*[@id='cardnumber'])[1]", driver).sendKeys("4111 1111 1111 1111")
//            Log.WriteLine("Ваш банк ввод номера карты")
//            //
//            DriverFindElement("(//*[@name='expdate'])[1]", driver).sendKeys("1219")
//            Log.WriteLine("Ваш банк ввод месяц год")
//            //
//            DriverFindElement("(//*[@id='cvc'])[1]", driver).sendKeys("123")
//            Log.WriteLine("Ваш банк ввод CVC2")
//            //
//            DriverFindElement("(//*[@class='sbersafe-pay-button'])", driver).click()
//            Log.WriteLine("Оплатить")
//            //
//            DriverFindElement("(//*[@name='password'])", driver).sendKeys("12345678")
//            Log.WriteLine("password")
//            //
//            DriverFindElement("(//*[@value='Submit'])", driver).click()
//            Log.WriteLine("Submit")
            //
            DriverFindElement("(//*[@class='form-ui-txt'])[10]", driver).click()
            Log.WriteLine("Оценка качества сервиса 10")
            //
            DriverFindElement("(//*[@class='form-ui-txt'])[11]", driver).click()
            Log.WriteLine("я хочу принять участие в детальном опросе")
            //
            DriverFindElement("(//*[@class='quality-polling_btn'])", driver).click()
            Log.WriteLine("Отправить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }


    @Test
    fun SupportedProjectOplataShopTovarDostavkaSamovivoz(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Поддержанные проекты (Магазин) самовывоз Оплата" + VersionBrowser(driver))







            Avtorization_Uz4name(driver,Log)
            //
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[2]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            Thread.sleep(500)
            DriverFindElement("//*[contains(text(), '                                                                                                               Доставка почтой                                                                          ,                                                                                                                Самовывоз                                                                                                                                        ')]", driver).click()
            //
            DriverFindElement("//*[contains(text(), 'Купить (1 шт.)')]", driver).click()
            Log.WriteLine("Купить 1 шт")
            //
            DriverFindElement("//*[@class='project-payment-choice_i ng-star-inserted']", driver).click()
            Log.WriteLine("Самовывоз")
            //
            DriverFindElement("//*[@id='payment-page.question-to-buyer-label']", driver).sendKeys("123")
            Log.WriteLine("Телефон данные")
            //
            DriverFindElement("//*[@id='core.locality']",driver).sendKeys("Луна")
            Log.WriteLine("Населённый пункт")
            //
            DriverFindElement("(//*[@id='core.zip-code'])[1]",driver).sendKeys("11111111")
            Log.WriteLine("Индекс")
            //
            DriverFindElement("(//*[@id='core.address'])[1]",driver).sendKeys("Большой кратер")
            Log.WriteLine("Адрес")
            //
            DriverFindElement("(//*[@id='core.recipient-name'])[1]",driver).sendKeys("Николас кейдж")
            Log.WriteLine("ФИО получателя")
            //
            DriverFindElement("(//*[@id='core.contact-phone'])[1]",driver).sendKeys("84952222222")
            Log.WriteLine("контактный телефон")
            //
            DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
            Log.WriteLine("Даю свое согласие")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
            Log.WriteLine("Оплатить покупку")
            //
            fun FindEl() : Boolean{
                return try{
                    DriverFindElement("(//*[@class='form-ui-label'])[1]", driver)
                    //Согласен на условия
                    true
                }catch (e:Exception){
                    false
                }
            }

            for(i in 0..60){  //Клауд
                if(FindEl()){

                    DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                    Log.WriteLine("Даю свое согласие")

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver).click()
                    Log.WriteLine("Карты других банков")
                    //
                    DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
                    Log.WriteLine("Оплатить покупку")
                    //
                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
                    Log.WriteLine("Оплата картой")
                    //
                    driver.switchTo().frame(0)
                    DriverFindElement("(//*[@class='row'])[1]//input",driver).click()
                    Log.WriteLine("клик по строке")
                    DriverFindElement("(//*[@class='row'])[1]//input",driver).sendKeys("4111 1111 1111 1111")
                    //
                    DriverFindElement("(//*[@id='inputMonth'])[1]",driver).sendKeys("12")
                    //
                    DriverFindElement("(//*[@id='inputYear'])[1]",driver).sendKeys("19")
                    //
                    DriverFindElement("(//*[@data-mask='000'])[1]",driver).sendKeys("123")
                    //
                    DriverFindElement("(//*[@id='cardHolder'])[1]",driver).sendKeys("QWERTY")
                    //
                    DriverFindElement("(//*[@class='button'])[1]",driver).click()
                    DriverFindElement("(//*[@class='button'])[2]",driver).click()
                    Log.WriteLine("OK")
                    driver.switchTo().defaultContent()
                    break
                }
            }

//            //DriverFindElement("(//*[@id='email'])[1]", driver).SendKeys("pain-net@yandex.ru");
//            //Log.WriteLine("Сбер ввод email");
//            ////
//            DriverFindElement("(//*[@id='cardnumber'])[1]", driver).sendKeys("4111 1111 1111 1111")
//            Log.WriteLine("Ваш банк ввод номера карты")
//            //
//            DriverFindElement("(//*[@name='expdate'])[1]", driver).sendKeys("1219")
//            Log.WriteLine("Ваш банк ввод месяц год")
//            //
//            DriverFindElement("(//*[@id='cvc'])[1]", driver).sendKeys("123")
//            Log.WriteLine("Ваш банк ввод CVC2")
//            //
//            DriverFindElement("(//*[@class='sbersafe-pay-button'])", driver).click()
//            Log.WriteLine("Оплатить")
//            //
//            DriverFindElement("(//*[@name='password'])", driver).sendKeys("12345678")
//            Log.WriteLine("password")
//            //
//            DriverFindElement("(//*[@value='Submit'])", driver).click()
//            Log.WriteLine("Submit")
//            //
            DriverFindElement("(//*[@class='form-ui-txt'])[10]", driver).click()
            Log.WriteLine("Оценка качества сервиса 10")
            //
            DriverFindElement("(//*[@class='form-ui-txt'])[11]", driver).click()
            Log.WriteLine("я хочу принять участие в детальном опросе")
            //
            DriverFindElement("(//*[@class='quality-polling_btn'])", driver).click()
            Log.WriteLine("Отправить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun SupportedProjectsOplataShop(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Поддержанные проекты (Магазин) Оплата клауд" + VersionBrowser(driver))







            Avtorization_Uz4name(driver,Log)
            //
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[1]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            DriverFindElement("(//*[@class='action-card_check'])[2]", driver).click()
            Log.WriteLine("Выбор Товара")
            //
            for (i in 1..10)
            {
                driver.findElement(By.xpath("(//*[@class='action-card_counter-btn plus js-increase'])[1]")).click()
                Thread.sleep(300)
            }

            DriverFindElement("(//*[@class='btn btn-nd-primary action-card_btn js-navigate-to-purchase'])[2]", driver).click()
            Log.WriteLine("Купить")
            //
            try
            {
                DriverFindElement("//*[@id='payment-page.question-to-buyer-label']", driver).sendKeys("1")
                Log.WriteLine("Укажите номер фото")
                //
                DriverFindElement("//*[@id='core.contact-phone']", driver).sendKeys("89778665544")
                Log.WriteLine("Контактный телефон")

                DriverFindElement("//*[@id='core.locality']",driver).sendKeys("Луна")
                Log.WriteLine("Населённый пункт")
                //
                DriverFindElement("(//*[@id='core.zip-code'])[1]",driver).sendKeys("11111111")
                Log.WriteLine("Индекс")
                //
                DriverFindElement("(//*[@id='core.address'])[1]",driver).sendKeys("Большой кратер")
                Log.WriteLine("Адрес")
                //
                DriverFindElement("(//*[@id='core.recipient-name'])[1]",driver).sendKeys("Николас кейдж")
                Log.WriteLine("ФИО получателя")
                //
                DriverFindElement("(//*[@id='core.contact-phone'])[1]",driver).sendKeys("84952222222")
                Log.WriteLine("контактный телефон")
            }
            catch(e:Exception)
            {

            }
            //
            fun FindEl() : Boolean{
                return try{
                    DriverFindElement("(//*[@class='form-ui-label'])[1]", driver)
                    //Согласен на условия
                    true
                }catch (e:Exception){
                    false
                }
            }

            for(i in 0..60){  //Клауд
                if(FindEl()){

                    DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                    Log.WriteLine("Даю свое согласие")

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver).click()
                    Log.WriteLine("Карты других банков")
                    //
                    DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
                    Log.WriteLine("Оплатить покупку")
                    //
                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
                    Log.WriteLine("Оплата картой")
                    //
                    driver.switchTo().frame(0)
                    DriverFindElement("(//*[@class='row'])[1]//input",driver).click()
                    Log.WriteLine("клик по строке")
                    DriverFindElement("(//*[@class='row'])[1]//input",driver).sendKeys("4111 1111 1111 1111")
                    //
                    DriverFindElement("(//*[@id='inputMonth'])[1]",driver).sendKeys("12")
                    //
                    DriverFindElement("(//*[@id='inputYear'])[1]",driver).sendKeys("19")
                    //
                    DriverFindElement("(//*[@data-mask='000'])[1]",driver).sendKeys("123")
                    //
                    DriverFindElement("(//*[@id='cardHolder'])[1]",driver).sendKeys("QWERTY")
                    //
                    DriverFindElement("(//*[@class='button'])[1]",driver).click()
                    DriverFindElement("(//*[@class='button'])[2]",driver).click()
                    Log.WriteLine("OK")
                    driver.switchTo().defaultContent()
                    break
                }
            }
//            DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
//            Log.WriteLine("Даю свое согласие")
//            //
//            DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
//            Log.WriteLine("Оплатить покупку")
//            //
//            //DriverFindElement("(//*[@id='email'])[1]", driver).SendKeys("pain-net@yandex.ru");
//            //Log.WriteLine("Сбер ввод email");
//            ////
//            DriverFindElement("(//*[@id='cardnumber'])[1]", driver).sendKeys("4111 1111 1111 1111")
//            Log.WriteLine("Ваш банк ввод номера карты")
//            //
//            DriverFindElement("(//*[@name='expdate'])[1]", driver).sendKeys("1219")
//            Log.WriteLine("Ваш банк ввод месяц год")
//            //
//            DriverFindElement("(//*[@id='cvc'])[1]", driver).sendKeys("123")
//            Log.WriteLine("Ваш банк ввод CVC2")
//            //
//            DriverFindElement("(//*[@class='sbersafe-pay-button'])", driver).click()
//            Log.WriteLine("Оплатить")
//            //
//            DriverFindElement("(//*[@name='password'])", driver).sendKeys("12345678")
//            Log.WriteLine("password")
//            //
//            DriverFindElement("(//*[@value='Submit'])", driver).click()
//            Log.WriteLine("Submit")
            Thread.sleep(5000)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }


    @Test
    fun MyBalance(){

        try {
            Thread.sleep(72000)
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("User-Баланс + Частичная оплата" + VersionBrowser(driver))





            Avtorization_Uz4name(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[4]", driver).click()
            Log.WriteLine("Баланс")
            //
            DriverFindElement("//*[@class='button button__def button__sm']", driver).click()
            Log.WriteLine("Пополнить баланс")
            //
            DriverFindElement("//*[@id='core.total-amount']", driver).clear()
            Log.WriteLine("Очистить поле")
            //
            DriverFindElement("//*[@id='core.total-amount']", driver).sendKeys("1000")
            Log.WriteLine("Ввод в поле значения 1000")
            //
            DriverFindElement("(//*[@class='project-payment-choice_name'])[2]",driver).click()
            Log.WriteLine("Клауд")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary project-payment_btn']", driver).click()
            Log.WriteLine("Перейти к оплате")
            //
            fun FindEl() : Boolean{
                return try{
                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver)
                    //Согласен на условия
                    true
                }catch (e:Exception){
                    false
                }
            }

            for(i in 0..60){  //Клауд
                if(FindEl()){

                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
                    Log.WriteLine("Оплата картой")
                    //
                    driver.switchTo().frame(0)
                    DriverFindElement("(//*[@class='row'])[1]//input",driver).click()
                    Log.WriteLine("клик по строке")
                    DriverFindElement("(//*[@class='row'])[1]//input",driver).sendKeys("4111 1111 1111 1111")
                    //
                    DriverFindElement("(//*[@id='inputMonth'])[1]",driver).sendKeys("12")
                    //
                    DriverFindElement("(//*[@id='inputYear'])[1]",driver).sendKeys("19")
                    //
                    DriverFindElement("(//*[@data-mask='000'])[1]",driver).sendKeys("123")
                    //
                    DriverFindElement("(//*[@id='cardHolder'])[1]",driver).sendKeys("QWERTY")
                    //
                    DriverFindElement("(//*[@class='button'])[1]",driver).click()
                    DriverFindElement("(//*[@class='button'])[2]",driver).click()
                    Log.WriteLine("OK")
                    driver.switchTo().defaultContent()
                    break
                }
            }

//            //DriverFindElement("(//*[@id='email'])[1]", driver).SendKeys("pain-net@yandex.ru");
//            //Log.WriteLine("Сбер ввод email");
//            ////
//            DriverFindElement("(//*[@id='cardnumber'])[1]", driver).sendKeys("4111 1111 1111 1111")
//            Log.WriteLine("Ваш банк ввод номера карты")
//            //
//            DriverFindElement("(//*[@name='expdate'])[1]", driver).sendKeys("1219")
//            Log.WriteLine("Ваш банк ввод месяц год")
//            //
//            DriverFindElement("(//*[@id='cvc'])[1]", driver).sendKeys("123")
//            Log.WriteLine("Ваш банк ввод CVC2")
//            //
//            DriverFindElement("(//*[@class='sbersafe-pay-button'])", driver).click()
//            Log.WriteLine("Оплатить")
//            //
//            DriverFindElement("(//*[@name='password'])", driver).sendKeys("12345678")
//            Log.WriteLine("password")
//            //
//            DriverFindElement("(//*[@value='Submit'])", driver).click()
//            Log.WriteLine("Submit")
            Thread.sleep(3500)
            DriverFindElement("//*[contains(text(), 'успешно')]", driver)
            //
            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[2]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            DriverFindElement("(//*[@name='donateAmount'])[1]", driver).clear()
            Log.WriteLine("очистить поле")
            //
            DriverFindElement("(//*[@name='donateAmount'])[1]", driver).sendKeys("10000")
            Log.WriteLine("Ввод суммы")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary action-card_btn js-navigate-to-purchase'])[1]", driver).click()
            Log.WriteLine("Оплатить")
            //
            fun FindEl2() : Boolean{
                return try{
                    DriverFindElement("(//*[@class='form-ui-label'])[1]", driver)
                    //Согласен на условия
                    true
                }catch (e:Exception){
                    false
                }
            }

            for(i in 0..60){  //Клауд
                if(FindEl2()){

                    DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
                    Log.WriteLine("Даю свое согласие")

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver).click()
                    Log.WriteLine("Карты других банков")
                    //
                    DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
                    Log.WriteLine("Оплатить покупку")
                    //
                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
                    Log.WriteLine("Оплата картой")
                    //
                    driver.switchTo().frame(0)
                    DriverFindElement("(//*[@class='row'])[1]//input",driver).click()
                    Log.WriteLine("клик по строке")
                    DriverFindElement("(//*[@class='row'])[1]//input",driver).sendKeys("4111 1111 1111 1111")
                    //
                    DriverFindElement("(//*[@id='inputMonth'])[1]",driver).sendKeys("12")
                    //
                    DriverFindElement("(//*[@id='inputYear'])[1]",driver).sendKeys("19")
                    //
                    DriverFindElement("(//*[@data-mask='000'])[1]",driver).sendKeys("123")
                    //
                    DriverFindElement("(//*[@id='cardHolder'])[1]",driver).sendKeys("QWERTY")
                    //
                    DriverFindElement("(//*[@class='button'])[1]",driver).click()
                    DriverFindElement("(//*[@class='button'])[2]",driver).click()
                    Log.WriteLine("OK")
                    driver.switchTo().defaultContent()
                    break
                }
            }
//            DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
//            Log.WriteLine("Даю свое согласие")
//            //
//            DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
//            Log.WriteLine("Оплатить заказ, используя средства на моем счету")
//            //
//            DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
//            Log.WriteLine("Оплатить покупку")
//            //
////            DriverFindElement("(//*[@id='email'])[1]", driver).sendKeys("pain-net@yandex.ru")
////            Log.WriteLine("Сбер ввод email")
////            //
//            DriverFindElement("(//*[@id='cardnumber'])[1]", driver).sendKeys("4111 1111 1111 1111")
//            Log.WriteLine("Ваш банк ввод номера карты")
//            //
//            DriverFindElement("(//*[@name='expdate'])[1]", driver).sendKeys("1219")
//            Log.WriteLine("Ваш банк ввод месяц год")
//            //
//            DriverFindElement("(//*[@id='cvc'])[1]", driver).sendKeys("123")
//            Log.WriteLine("Ваш банк ввод CVC2")
//            //
//            DriverFindElement("(//*[@class='sbersafe-pay-button'])", driver).click()
//            Log.WriteLine("Оплатить")
//            //
//            DriverFindElement("(//*[@name='password'])", driver).sendKeys("12345678")
//            Log.WriteLine("password")
//            //
//            DriverFindElement("(//*[@value='Submit'])", driver).click()
//            Log.WriteLine("Submit")
            Thread.sleep(3800)
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Личный кабинет")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[4]", driver).click()
            Log.WriteLine("Баланс")
            Thread.sleep(4000)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun MyPage(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Моя страница" + VersionBrowser(driver))




            Avtorization_Uz4name(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[5]", driver).click()
            Log.WriteLine("Моя страница")
            //
            DriverFindElement("(//*[@class='n-card_info_val'])[1]", driver).click()
            Log.WriteLine("Подписчики")
            //
            DriverFindElement("//*[@class='close']", driver).click()
            Log.WriteLine("Закрыть окно")
            //
            DriverFindElement("(//*[@class='n-card_info_val'])[2]", driver).click()
            Log.WriteLine("Подписки")
            //
            DriverFindElement("//*[@class='close']", driver).click()
            Log.WriteLine("Закрыть окно")
            //

            for (i in 0..3) {
                driver.findElement(By.xpath("//*[@class='checkbox active ']")).click()
                Thread.sleep(200)
                driver.findElement(By.xpath("//*[@class='checkbox  ']")).click()
                Thread.sleep(200)
            }

            Log.WriteLine("Показывать всем")
            //
            DriverFindElement("//*[@class='tabs_i ']", driver).click()
            Log.WriteLine("Новости")
            Thread.sleep(2500)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }



}

