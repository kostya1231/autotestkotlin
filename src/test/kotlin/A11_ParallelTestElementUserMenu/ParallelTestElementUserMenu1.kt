
import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

class ParallelTestElementUserMenu1 : SetupParallel() {

    @Test
    fun UserMenu1(){
        try{
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("User-Авторизация" + VersionBrowser(driver))





            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz4name@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }


    @Test
    fun UserMenu2(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("User-User_Menu" + VersionBrowser(driver))






            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz4name@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[1]", driver).click()
            Log.WriteLine("Меню Созданные проекты")
            //
            DriverFindElement("(//*[@class='profile-tab_name'])[2]", driver).click()
            Log.WriteLine("Поддержанные проеткы")
            //
            DriverFindElement("(//*[@class='profile-tab_name'])[3]", driver).click()
            Log.WriteLine("Покупки и  вознаграждения")
            //
            DriverFindElement("(//*[@class='profile-tab_name'])[1]", driver).click()
            Log.WriteLine("Меню Созданные проекты")
            //
            DriverFindElement("(//*[@class='profile-balance_manage'])", driver).click()
            Log.WriteLine("Управление балансом")
            //
            DriverFindElement("//*[@class='profile-passport_settings']", driver).click()
            Log.WriteLine("Настройки")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")
            Thread.sleep(500)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun UserMenu2Contacts(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()
            driver.manage().window().maximize()


            Log.WriteLine("User-User_Menu-Контакты" + VersionBrowser(driver))






            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz4name@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("(//*[@class='h-menu_i'])[2]", driver).click()
            Log.WriteLine("Контакты")
            //
            DriverFindElement("(//*[@class='about-menu_link'])[5]", driver).click()
            Log.WriteLine("Партнёры и кураторы")
            //
            DriverFindElement("(//*[@class='about-h_nav-list'])[1]//li[2]", driver).click()
            Log.WriteLine("Наши партнёры")
            //
            DriverFindElement("(//*[@class='about-partners_list'])[1]//li[2]", driver)
            //
            DriverFindElement("(//*[@class='about-h_nav-list'])[1]//li[1]", driver).click()
            Log.WriteLine("Кто такие кураторы")
            //
            Thread.sleep(500)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun UserProjectFiltr(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("User-Меню Проекты + фильтры + выбор фильтров" + VersionBrowser(driver))





            Avtorization_Uz4name(driver, Log)
            //
            DriverFindElement("(//*[@class='h-menu_link'])[1]", driver).click()
            Log.WriteLine("Проекты")
            //
            DriverFindElement("(//*[@class='project-list-filter_i'])[1]", driver).click()
            Log.WriteLine("Все проекты")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[1]/li[2]", driver).click()
            Log.WriteLine("Последеие обновлённые")
            //
            Thread.sleep(700)
            DriverFindElement("(//*[@class='project-list-filter_i'])[1]", driver).click()
            Log.WriteLine("Все проекты")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[1]/li[3]", driver).click()
            Log.WriteLine("Успешные")
            //
            Thread.sleep(700)
            DriverFindElement("(//*[@class='project-list-filter_i'])[1]", driver).click()
            Log.WriteLine("Все проекты")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[1]/li[4]", driver).click()
            Log.WriteLine("Рекордсмены")
            //
            Thread.sleep(700)
            DriverFindElement("(//*[@class='project-list-filter_i'])[1]", driver).click()
            Log.WriteLine("Все проекты")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[1]/li[5]", driver).click()
            Log.WriteLine("Новые")
            //
            Thread.sleep(700)
            DriverFindElement("(//*[@class='project-list-filter_i'])[1]", driver).click()
            Log.WriteLine("Все проекты")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[1]/li[6]", driver).click()
            Log.WriteLine("Близкие к завершению")
            //
            Thread.sleep(700)
            DriverFindElement("(//*[@class='project-list-filter_i'])[1]", driver).click()
            Log.WriteLine("Все проекты")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[1]/li[7]", driver).click()
            Log.WriteLine("Обсуждаемые")
            //
            Thread.sleep(700)
            DriverFindElement("(//*[@class='project-list-filter_i'])[1]", driver).click()
            Log.WriteLine("Все проекты")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[1]/li[8]", driver).click()
            Log.WriteLine("Благотворительные")
            //
            Thread.sleep(700)
            DriverFindElement("(//*[@class='project-list-filter_i'])[1]", driver).click()
            Log.WriteLine("Все проекты")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[1]/li[7]", driver).click()
            Log.WriteLine("Активные")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun UserProjectUrlSocSeti(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()
            val tabs = ArrayList(driver.windowHandles)


            Log.WriteLine("Проект личная карточка юзера" + VersionBrowser(driver))




            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(500)
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[1]", driver).click()
            Log.WriteLine("Созданные проекты")
            //
            while (true)
            {
                try
                {
                    Thread.sleep(500)
                    driver.findElement(By.xpath("//*[@class='profile-project-info_delete-link']")).click()
                    Log.WriteLine("Удалить черновик")
                    Thread.sleep(500)
                    driver.findElement(By.xpath("//*[@class='btn btn-primary']")).click()
                    Log.WriteLine("Да")
                    Thread.sleep(200)
                }
                catch (e:Exception)
                {
                    break
                }
            }
            DriverFindElement("//*[@class='profile-project-info_text']//a", driver).click()
            Log.WriteLine("Войти в проект")
            //
            Thread.sleep(2000)
            DriverFindElement("//*[@class='project-author_ava']", driver).click()
            Log.WriteLine("Нажать на аватарку юзера")
            //
            DriverFindElement("//*[@class='project-author-info_social']//li[1]", driver).click()
            Log.WriteLine("Переадрессация-на-Вк")
            //
            driver.switchTo().window(tabs[0])
            DriverFindElement("//*[@class='project-author-info_social']//li[2]", driver).click()
            Log.WriteLine("Переадрессация-на-FB")
            //
            driver.switchTo().window(tabs[0])
            //
            DriverFindElement("//*[@class='project-author-info_social']//li[3]", driver).click()
            Log.WriteLine("Переадрессация-на-TW")
            //
            driver.switchTo().window(tabs[0])
            //
            DriverFindElement("//*[@class='project-author-info_social']//li[4]", driver).click()
            Log.WriteLine("Переадрессация-на-Google +")
            //
            driver.switchTo().window(tabs[0])
            //
            DriverFindElement("//*[@class='project-author-info_social']//li[5]", driver).click()
            Log.WriteLine("Переадрессация-на-Instagram")
            //
            driver.switchTo().window(tabs[0])
            //
            DriverFindElement("//*[@class='project-author-info_social']//li[6]", driver).click()
            Log.WriteLine("Переадрессация-на-Telegram")
            //
            val tabs2 = ArrayList(driver.windowHandles)

            for (i in 1..6){

                driver.switchTo().window(tabs2[i])
                Thread.sleep(800)
            }
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }





}