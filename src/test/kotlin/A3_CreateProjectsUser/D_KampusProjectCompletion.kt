import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.interactions.Actions
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

class D_KampusProjectCompletion : Setup() {


    @Test
    fun KampusProjectCompletion(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Kampus")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Kampus\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            var world = 1
            val r = Random()
            val number = StringBuilder()
            for (i in 0 until world) {
                val tmp = r.nextInt(9999)
                number.append(tmp)
            }

            var world1 = 10
            val r2 = Random()
            var number1 = StringBuilder()
            for (i in 0 until world1) {
                val tmp1 = r2.nextInt(10)
                number1.append(tmp1)
            }

            var world3 = 9
            val r3 = Random()
            var number2 = StringBuilder()
            for (i in 0 until world3) {
                val tmp2 = r3.nextInt(9)
                number2.append(tmp2)
            }




            Log.WriteLine("Создать проект" + VersionBrowser(driver))





            DriverNavigate(Kampus, driver)
            //
            DriverFindElement("//*[@class='header_user-link js-signup-link']", driver).click()
            Log.WriteLine("Вход")
            //
            //DriverFindElement("//*[@class='ng-tns-c3-0 ng-star-inserted']").Click();
            //Log.WriteLine("Сбросить евент");
            ////
            DriverFindElement("//*[@class='form-control login-control_input js-input-email']", driver).sendKeys("uz4name@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn js-btn-submit-login']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='button button__def button__xs']",driver).click()
            Log.WriteLine("Хорошо")
            //
            DriverFindElement("//*[@class='btn btn-campus-primary']", driver).click()
            Log.WriteLine("Создать проект")
            //
            DriverFindElement("//*[@class='checkbox']", driver).click()
            Log.WriteLine("Я Согласен на Условия")
            //
            DriverFindElement("//*[@class='btn btn-primary frc-i-accept-btn create-campaign']", driver).click()
            Log.WriteLine("Создать проект")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[1]", driver).sendKeys("Project_User_Kampus" + Date())
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='form-control control-xlg borderless'])", driver).sendKeys("web" + number.toString())
            Log.WriteLine("Красивый адрес проекта")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            try{
                DriverFindElement("//*[@class='btn btn-primary']",driver).click()
                Log.WriteLine("Сохранить")
            }catch (p:Exception){
                Log.WriteLine("Сохранить не выскочило т к размер фото меньше")
            }

            Thread.sleep(2800)
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")
            //
            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Фото по ссылке")
            //
            DriverFindElement("//*[@name='img_url']", driver).sendKeys("https://99px.ru/sstorage/53/2012/03/tmb_36848_3083.jpg")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")
            //
            try{
                DriverFindElement("//*[@class='btn btn-primary']",driver).click()
                Log.WriteLine("Сохранить")
            }catch (p:Exception){
                Log.WriteLine("Сохранить не выскочило т к размер фото меньше")
            }
            //
            Thread.sleep(1500)
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[2]", driver).sendKeys("cxcdsfdsfqq")
            Log.WriteLine("Коротко о проекте")
            //
            DriverFindElement("//*[@class='icon-select']", driver).click()
            Log.WriteLine("Страна реализции")
            //
            DriverFindElement("//*[@class='dropdown-menu']/li[2]", driver).click()
            Log.WriteLine("Выбор страны")
            //
            DriverFindElement("//*[@class='form-control control-xlg prj-crt-input ac_input']", driver).sendKeys("Москва")
            Log.WriteLine("Регион реализации")
            //
            DriverFindElement("//*[@class='ac_results']//li[1]", driver).click()
            Log.WriteLine("Москва")
            //
            DriverFindElement("//*[@name='cityNameRus']", driver).sendKeys("Алабино")
            Log.WriteLine("Город реализации")
            //
            DriverFindElement("//*[@class='ac_results']//*[@class='ac_even ac_over']", driver).click()
            //
            DriverFindElement("//*[@data-parse='number']", driver).clear()
            //
            DriverFindElement("//*[@data-parse='number']", driver).sendKeys("500000")
            Log.WriteLine("Финансовая цель")
            //
            DriverFindElement("//*[@class='project-create_col-val_col']", driver).click()
            Log.WriteLine("Срок окончания проекта")
            //
            DriverFindElement("(//*[@data-parse='number'])[2]", driver).click()
            //
            DriverFindElement("//*[@class='ui-datepicker-next ui-corner-all']", driver).click()
            Log.WriteLine("Следующий месяц")
            //
            DriverFindElement("//*[@class='ui-datepicker-calendar']//tr[4]//td[5]", driver).click()
            Log.WriteLine("Выбор даты")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            Thread.sleep(500)
            //
            DriverFindElement("(//*[@class='project-create_tab_link'])[2]", driver).click()
            Log.WriteLine("Детали")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            try{
                DriverFindElement("//*[@class='btn btn-primary']",driver).click()
                Log.WriteLine("Сохранить")
            }catch (p:Exception){
                Log.WriteLine("Сохранить не выскочило т к размер фото меньше")
            }
            //
            Thread.sleep(2000)
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")
            //
            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Видео с Ютуба")
            //
            DriverFindElement("//*[@name='img_url']", driver).sendKeys("https://www.youtube.com/watch?v=VVtZVabutYM")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")
            //
            DriverFindElement("(//*[@class='mceIcon mce_planetaphoto'])", driver).click()
            Log.WriteLine("Загрузить картинку в поле text-area")
            //
            DriverFindElement("//*[@class='link']", driver).click()
            Log.WriteLine("Загрузить картинку по ссылке")
            //
            DriverFindElement("//*[@name='img_url']", driver).sendKeys("http://hivemind.com.ua/wp-content/uploads/2012/04/main12.jpg")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("(//*[@class='project-create_tab_link'])[3]", driver).click()
            Log.WriteLine("Вознаграждение")
            //
            DriverFindElement("//*[@class='form-control prj-crt-input js-share-name-limit']", driver).sendKeys("SATANA" + Date())
            Log.WriteLine("Название вознаграждения")
            //
            driver.switchTo().frame(0)
            //
            DriverFindElement("//*[@class='mceContentBody  mceCampaignEditorBody']", driver).sendKeys("666")
            Log.WriteLine("Описание вознаграждения")
            //
            driver.switchTo().defaultContent()
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[1]", driver).sendKeys("Прямо в жерло преисподни")
            Log.WriteLine("Способы получения")
            //
            DriverFindElement("(//*[@name='price'])", driver).clear()
            //
            DriverFindElement("(//*[@name='price'])", driver).sendKeys("100000")
            Log.WriteLine("Цена вознаграждения")
            //
            DriverFindElement("//*[@class='form-control prj-crt-input prj-crt-input__date hasDatepicker']", driver).click()
            Log.WriteLine("Примерная дата доставки")
            //
            DriverFindElement("//*[@class='ui-datepicker-calendar']//tr[4]//td[5]", driver).click()
            Log.WriteLine("Выбор даты")
            //
            val el3 = DriverFindElement("//*[@type='file']", driver)
            el3.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")
            //
            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Фото по ссылке")
            //
            DriverFindElement("//*[@name='img_url']", driver).sendKeys("http://i84.beon.ru/49/62/2706249/5/111058305/308342421b1a64a83312136220f6f2a1_bp.jpeg")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")
            //
            DriverFindElement("(//*[@class='checkbox'])[1]", driver).click()
            Log.WriteLine("Запрашивать адрес доставки")
            //
            DriverFindElement("(//*[@class='checkbox'])[1]", driver).click()
            Log.WriteLine("Возможен самовывоз")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[4]", driver).sendKeys("Москва")
            Log.WriteLine("Город")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[5]", driver).sendKeys("Ул Малая Большая д 23 кв 666")
            Log.WriteLine("Адрес и условия самовывоза")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[6]", driver).sendKeys("74956661323")
            Log.WriteLine("Телефон")
            //
            DriverFindElement("(//*[@class='checkbox'])[1]", driver).click()
            Log.WriteLine("Задать вопрос покупателю")
            Thread.sleep(2500)
            driver.findElement(By.xpath("//*[@class='select-cont']")).click()
            //
            DriverFindElement("//*[@class='dropdown-menu']//li[1]", driver).click()
            Log.WriteLine("Выбор вопроса")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-lg']", driver).click()
            Log.WriteLine("Добавить изображение")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("(//*[@class='project-create_tab_link'])[4]", driver).click()
            Log.WriteLine("Контр-Агент")
            //
            Thread.sleep(1000)
            //
            DriverFindElement("(//*[@class='select-cont'])[1]", driver).click()
            Log.WriteLine("Выбор нового контрАгента")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[1]//li//*[contains(text(), '                      Новый контрагент                 ')]", driver).click()
            Log.WriteLine("Выбор нового контр агента")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[1]", driver).sendKeys("фывфывфывфывфывфывфывфы")
            Log.WriteLine("ФИО физического лица ИП или наименование организации для юридического лица")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[2]", driver).sendKeys("И.И ИВанов")
            Log.WriteLine("Инициалы и фамилия для подписи в договоре")
            //
            DriverFindElement("//*[@class='form-control prj-crt-input prj-crt-input__date hasDatepicker']", driver).sendKeys("20.9.1990")
            Log.WriteLine("Дата рождения")
            //
            val ac = Actions(driver)
            ac.sendKeys(Keys.ESCAPE).build().perform()
            //
            DriverFindElement("//*[@class='form-control control-xlg prj-crt-input js-city-name-input ac_input']", driver).sendKeys("Москва")
            Log.WriteLine("Город")
            val ac2 = Actions(driver)
            ac2.sendKeys(Keys.ESCAPE).build().perform()
            Log.WriteLine("Москва")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[3]", driver).sendKeys("84955555535")
            Log.WriteLine("Телефон")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[5]", driver).sendKeys(number1.toString())
            Log.WriteLine("Номер паспорта")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[6]", driver).sendKeys("Отделением ОУФМС по бла бла бла")
            Log.WriteLine("Кем выдан")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input prj-crt-input__date hasDatepicker'])[2]", driver).sendKeys("20.03.2007")
            Log.WriteLine("Когда выдан паспорт")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[7]", driver).sendKeys("770-060")
            Log.WriteLine("Код подразделения")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[6]", driver).click()
            //
            DriverFindElement("(//*[@class='btn upload-file mrg-t-5'])", driver).click()
            Log.WriteLine("Необходимые документы для оформления договора - Загрузить")
            //
            val el4 = DriverFindElement("//*[@type='file']", driver)
            el4.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[4]", driver).sendKeys(number1)
            Log.WriteLine("ИНН")
            //
            DriverFindElement("//*[@class='form-control prj-crt-input js-save-model']", driver).sendKeys("Ул большая нищебродская дом картонная коробка")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[9]", driver).sendKeys("Банк Анимэ и кавая")
            Log.WriteLine("Банк получателя")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[10]", driver).sendKeys(number1.toString() + number1.toString())
            Log.WriteLine("Корреспондентский счет")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[8]", driver).sendKeys(number1.toString() + number1.toString())
            Log.WriteLine("Расчётный счет")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[11]", driver).sendKeys(number2.toString())
            Log.WriteLine("БИК")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input js-save-model'])[2]", driver).sendKeys("Ашкараагалдыыыыыыыыыыыыыыыыыыы")
            Log.WriteLine("Прочие данные")
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            Thread.sleep(1000)
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn btn-primary project-create_next']", driver).click()
            Log.WriteLine("Отправить на модерацию")
            //
            Thread.sleep(3000)
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

}