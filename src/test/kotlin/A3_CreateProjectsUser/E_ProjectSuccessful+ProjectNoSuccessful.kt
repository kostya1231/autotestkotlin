import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

class `E_ProjectSuccessful+ProjectNoSuccessful` : Setup() {


    @Test
    fun projectSuccessful() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            var world = 10
            val r = Random()
            var number = StringBuilder()
            for (i in 0 until world) {
                val tmp = r.nextInt(10)
                number.append(tmp)
            }

            var world1 = 9
            val r1 = Random()
            var number1 = StringBuilder()
            for (i in 0 until world1) {
                val tmp = r1.nextInt(9)
                number1.append(tmp)
            }





            Log.WriteLine("Успешный проект" + VersionBrowser(driver))





            AvtirizationOther(driver, Log)
            //
            val text: WebElement = DriverFindElement("//*[@class='h-user_name']", driver) // Вытащить текст из элемента
            var Text1: String = text.text
            var textNew = Text1.replace("\\s".toRegex(), "")
            println(textNew)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[1]", driver).click()
            //
            try {
                driver.findElement(By.xpath("//*[contains(text(), 'создать')]")).click()
                Log.WriteLine("Создать")
            } catch (e: Exception) {
                driver.findElement(By.xpath("//*[@class='header_create-link']")).click()
                Log.WriteLine("Создать Проект")
            }

            DriverFindElement("//*[@class='checkbox']", driver).click()
            Log.WriteLine("Я Согласен на Условия")

            DriverFindElement("//*[@class='btn btn-primary frc-i-accept-btn create-campaign']", driver).click()
            Log.WriteLine("Создать проект")
            Thread.sleep(2000)
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[1]", driver).sendKeys("Project_User" + Date())
            Log.WriteLine("Название проекта")

            var str = driver.currentUrl

            var n: String = str.replace("\\s".toRegex(), "")

            var strProject: String = n.trim(' ')
            var spl = strProject.split('/')
            var indexSplit = spl[4]
            println(indexSplit)
            //
            DriverFindElement("(//*[@class='form-control control-xlg borderless'])", driver).sendKeys("web" + number.toString())
            Log.WriteLine("Красивый адрес проекта")

            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")

            try {
                DriverFindElement("//*[@class='btn btn-primary']", driver).click()
                Log.WriteLine("Сохранить")
            } catch (p: Exception) {
                Log.WriteLine("Сохранить не выскочило т к размер фото меньше")
            }

            Thread.sleep(2800)
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")

            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Фото по ссылке")

            DriverFindElement("//*[@name='img_url']", driver).sendKeys("http://www.animacity.ru/sites/default/files/blog/82069/14624.jpg")
            Log.WriteLine("Ввод ссылки")

            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")

            try {
                DriverFindElement("//*[@class='btn btn-primary']", driver).click()
                Log.WriteLine("Сохранить")
            } catch (p: Exception) {
                Log.WriteLine("Сохранить не выскочило т к размер фото меньше")
            }

            Thread.sleep(1800)

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[2]", driver).sendKeys("cxcdsfdsfqq")
            Log.WriteLine("Коротко о проекте")

            DriverFindElement("//*[@class='icon-select']", driver).click()
            Log.WriteLine("Страна реализции")

            DriverFindElement("//*[@class='dropdown-menu']/li[2]", driver).click()
            Log.WriteLine("Выбор страны")

            DriverFindElement("//*[@class='form-control control-xlg prj-crt-input ac_input']", driver).sendKeys("Москва")
            Log.WriteLine("Регион реализации")

            DriverFindElement("//*[@class='ac_results']//li[1]", driver).click()
            Log.WriteLine("Москва")

            DriverFindElement("//*[@name='cityNameRus']", driver).sendKeys("Алабино")
            Log.WriteLine("Город реализации")

            DriverFindElement("//*[@class='ac_results']//*[@class='ac_even ac_over']", driver).click()

            DriverFindElement("//*[@data-parse='number']", driver).clear()

            DriverFindElement("//*[@data-parse='number']", driver).sendKeys("500000")
            Log.WriteLine("Финансовая цель")

            DriverFindElement("//*[@class='project-create_col-val_col']", driver).click()
            Log.WriteLine("Срок окончания проекта")

            DriverFindElement("(//*[@data-parse='number'])[2]", driver).click()

            DriverFindElement("//*[@class='ui-datepicker-next ui-corner-all']", driver).click()
            Log.WriteLine("Следующий месяц")

            DriverFindElement("//*[@class='ui-datepicker-calendar']//tr[4]//td[5]", driver).click()
            Log.WriteLine("Выбор даты")

            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")

            Thread.sleep(800)

            DriverFindElement("(//*[@class='project-create_tab_link'])[2]", driver).click()
            Log.WriteLine("Детали")

            val el4 = DriverFindElement("//*[@type='file']", driver)
            el4.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")

            try {
                DriverFindElement("//*[@class='btn btn-primary']", driver).click()
                Log.WriteLine("Сохранить")
            } catch (p: Exception) {
                Log.WriteLine("Сохранить не выскочило т к размер фото меньше")
            }

            Thread.sleep(1800)
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")

            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Видео с Ютуба")

            DriverFindElement("//*[@name='img_url']", driver).sendKeys("https://www.youtube.com/watch?v=VVtZVabutYM")
            Log.WriteLine("Ввод ссылки")

            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")

            DriverFindElement("(//*[@class='mceIcon mce_planetaphoto'])", driver).click()
            Log.WriteLine("Загрузить картинку в поле text-area")

            DriverFindElement("//*[@class='link']", driver).click()
            Log.WriteLine("Загрузить картинку по ссылке")

            DriverFindElement("//*[@name='img_url']", driver).sendKeys("http://www.animacity.ru/sites/default/files/blog/82069/14624.jpg")
            Log.WriteLine("Ввод ссылки")

            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")

            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")

            DriverFindElement("(//*[@class='project-create_tab_link'])[3]", driver).click()
            Log.WriteLine("Вознаграждение")

            DriverFindElement("//*[@class='form-control prj-crt-input js-share-name-limit']", driver).sendKeys("SATANA")
            Log.WriteLine("Название вознаграждения")

            driver.switchTo().frame(0)

            DriverFindElement("//*[@class='mceContentBody  mceCampaignEditorBody']", driver).sendKeys("666")
            Log.WriteLine("Описание вознаграждения")

            driver.switchTo().defaultContent()

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[1]", driver).sendKeys("Прямо в жерло преисподни")
            Log.WriteLine("Способы получения")

            DriverFindElement("(//*[@name='price'])", driver).clear()

            DriverFindElement("(//*[@name='price'])", driver).sendKeys("100000")
            Log.WriteLine("Цена вознаграждения")

            DriverFindElement("//*[@class='checkbox active']", driver).click()
            Log.WriteLine("Отключить ЧекБокс")

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[3]", driver).clear()

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[3]", driver).sendKeys("100")

            DriverFindElement("//*[@class='form-control prj-crt-input prj-crt-input__date hasDatepicker']", driver).click()
            Log.WriteLine("Примерная дата доставки")

            DriverFindElement("//*[@class='ui-datepicker-calendar']//tr[4]//td[5]", driver).click()
            Log.WriteLine("Выбор даты")

            val el5 = DriverFindElement("//*[@type='file']", driver)
            el5.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")

            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")

            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Фото по ссылке")

            DriverFindElement("//*[@name='img_url']", driver).sendKeys("http://www.animacity.ru/sites/default/files/blog/82069/14624.jpg")
            Log.WriteLine("Ввод ссылки")

            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")

            DriverFindElement("(//*[@class='checkbox'])[1]", driver).click()
            Log.WriteLine("Запрашивать адрес доставки")

            Thread.sleep(1500)
            //
            val elAction = DriverFindElement("(//*[@class='checkbox-row flat-ui-control'])[2]", driver)
            val action = Actions(driver)
            action.moveToElement(elAction).click().build().perform()

            val elActions2 = DriverFindElement("(//*[@class='checkbox-row flat-ui-control'])[3]", driver)
            val action2 = Actions(driver)
            action2.moveToElement(elActions2).click().build().perform()
            Log.WriteLine("Возможен самовывоз")

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[4]", driver).sendKeys("Москва")
            Log.WriteLine("Город")

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[5]", driver).sendKeys("Ул Малая Большая д 23 кв 666")
            Log.WriteLine("Адрес и условия самовывоза")

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[6]", driver).sendKeys("74956661323")
            Log.WriteLine("Телефон")

            val elActions3 = DriverFindElement("(//*[@class='checkbox'])[1]", driver)
            val action3 = Actions(driver)
            action3.moveToElement(elActions3).click().build().perform()
            Log.WriteLine("Задать вопрос покупателю")
            Thread.sleep(2500)
            driver.findElement(By.xpath("//*[@class='select-cont']")).click()

            DriverFindElement("//*[@class='dropdown-menu']//li[1]", driver).click()
            Log.WriteLine("Выбор вопроса")

            DriverFindElement("//*[@class='btn btn-primary btn-lg']", driver).click()
            Log.WriteLine("Добавить изображение")

            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")

            Thread.sleep(500)

            DriverFindElement("(//*[@class='project-create_tab_link'])[4]", driver).click()
            Log.WriteLine("Контр-Агент")

            DriverFindElement("(//*[@class='select-cont'])[1]", driver).click()
            Log.WriteLine("Выбор нового контрАгента")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[1]//li//*[contains(text(), '                      Новый контрагент                 ')]", driver).click()
            Log.WriteLine("Выбор нового контр агента")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[1]", driver).sendKeys("фывфывфывфывфывфывфывфы")
            Log.WriteLine("ФИО физического лица ИП или наименование организации для юридического лица")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[2]", driver).sendKeys("И.И ИВанов")
            Log.WriteLine("Инициалы и фамилия для подписи в договоре")
            //
            DriverFindElement("//*[@class='form-control prj-crt-input prj-crt-input__date hasDatepicker']", driver).sendKeys("20.9.1990")
            Log.WriteLine("Дата рождения")
            //
            Thread.sleep(300)
            val ac = Actions(driver)
            ac.sendKeys(Keys.ESCAPE).build().perform()
            //
            DriverFindElement("//*[@class='form-control control-xlg prj-crt-input js-city-name-input ac_input']", driver).sendKeys("Москва")
            Log.WriteLine("Город")
            //
            Thread.sleep(500)
            val ac2 = Actions(driver)
            ac2.sendKeys(Keys.ENTER).build().perform()
            Log.WriteLine("Москва")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[3]", driver).sendKeys("84955555535")
            Log.WriteLine("Телефон")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[5]", driver).sendKeys(number.toString())
            Log.WriteLine("Номер паспорта")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[6]", driver).sendKeys("Отделением ОУФМС по бла бла бла")
            Log.WriteLine("Кем выдан")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input prj-crt-input__date hasDatepicker'])[2]", driver).sendKeys("20.03.2007")
            Log.WriteLine("Когда выдан паспорт")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[7]", driver).sendKeys("770-060")
            Log.WriteLine("Код подразделения")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[5]", driver).click()//Для линукса! у винды закомитить
            //
            DriverFindElement("(//*[@class='btn upload-file mrg-t-5'])", driver).click()
            Log.WriteLine("Необходимые документы для оформления договора - Загрузить")
            //
            val el3 = DriverFindElement("//*[@type='file']", driver)
            el3.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[4]", driver).sendKeys(number.toString())
            Log.WriteLine("ИНН")
            //
            DriverFindElement("//*[@class='form-control prj-crt-input js-save-model']", driver).sendKeys("Ул большая нищебродская дом картонная коробка")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[9]", driver).sendKeys("Банк Анимэ и кавая")
            Log.WriteLine("Банк получателя")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[10]", driver).sendKeys(number.toString() + number.toString())
            Log.WriteLine("Корреспондентский счет")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[8]", driver).sendKeys(number.toString() + number.toString())
            Log.WriteLine("Расчётный счет")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[11]", driver).sendKeys(number1.toString())
            Log.WriteLine("БИК")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input js-save-model'])[2]", driver).sendKeys("Ашкараагалдыыыыыыыыыыыыыыыыыыы")
            Log.WriteLine("Прочие данные")
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            Thread.sleep(1000)
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn btn-primary project-create_next']", driver).click()
            Log.WriteLine("Отправить на модерацию")
            //
            Thread.sleep(1500)
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("ОК")
            //
            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[2]", driver).click()
            Log.WriteLine("Проекты")
            //
            DriverFindElement("(//*[@class='form-control'])[1]", driver).sendKeys(indexSplit)
            Log.WriteLine("Ввод номера проекта")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//tbody//tr//td[3]//a", driver).click()
            Log.WriteLine("выбор проекта")
            //
            DriverFindElement("(//*[@class='project-author-controls_name'])[2]", driver).click()
            Log.WriteLine("Редактировать")
            //
            DriverFindElement("(//*[@class='select2-container select2-container-multi select-tags js-select2'])", driver).click()
            //
            DriverFindElement("//*[@class='select2-results']//li[1]", driver).click()
            Log.WriteLine("Выбор категории")
            //
            DriverFindElement("(//*[@class='btn project-create_save'])[1]", driver).click()
            Log.WriteLine("Сохранить")
            //
            indexSplit = spl[0] + "/" + spl[1] + "/" + spl[2] + "/" + spl[3] + "/" + spl[4]
            DriverNavigate(indexSplit, driver)
            //
            DriverFindElement("//*[@class='btn btn-nd-success js-start-campaign']", driver).click()
            Log.WriteLine("Запустить")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("ОК")
            //
            DriverFindElement("//*[@class='button button__b button__def']", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            DriverFindElement("(//*[@name='donateAmount'])[1]", driver).clear()
            Log.WriteLine("Очистить поле")
            //
            DriverFindElement("(//*[@name='donateAmount'])[1]", driver).sendKeys("500000")
            Log.WriteLine("Ввод в поле значения 500000")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary action-card_btn js-navigate-to-purchase'])[1]", driver).click()
            Log.WriteLine("Оплатить")
            //
            fun FindEl(): Boolean {
                return try {
                    DriverFindElement("(//*[@class='form-ui-label'])[2]", driver)
                    //Согласен на условия
                    false
                } catch (e: Exception) {
                    true
                }
            }

            for (i in 0..60) {  //Клауд
                if (FindEl()) {

                    DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                    Log.WriteLine("Даю свое согласие")

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver).click()
                    Log.WriteLine("Карты других банков")
                    //
                    DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
                    Log.WriteLine("Оплатить покупку")
                    //
                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
                    Log.WriteLine("Оплата картой")
                    //
                    driver.switchTo().frame(0)
                    DriverFindElement("(//*[@class='row'])[1]//input", driver).click()
                    Log.WriteLine("клик по строке")
                    DriverFindElement("(//*[@class='row'])[1]//input", driver).sendKeys("4111 1111 1111 1111")
                    //
                    DriverFindElement("(//*[@id='inputMonth'])[1]", driver).sendKeys("12")
                    //
                    DriverFindElement("(//*[@id='inputYear'])[1]", driver).sendKeys("19")
                    //
                    DriverFindElement("(//*[@data-mask='000'])[1]", driver).sendKeys("123")
                    //
                    DriverFindElement("(//*[@id='cardHolder'])[1]", driver).sendKeys("QWERTY")
                    //
                    DriverFindElement("(//*[@class='button'])[1]", driver).click()
                    DriverFindElement("(//*[@class='button'])[2]", driver).click()
                    Log.WriteLine("OK")
                    driver.switchTo().defaultContent()
                    break
                }
            }
            //
//            DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
//            Log.WriteLine("Оплатить покупку")
//            //
//            DriverFindElement("(//*[@id='email'])[1]", driver).clear()
//            //
//            DriverFindElement("(//*[@id='email'])[1]", driver).sendKeys("pain-net@yandex.ru")
//            Log.WriteLine("Сбер ввод email")
//            //
//            DriverFindElement("(//*[@id='cardnumber'])[1]", driver).sendKeys("4111 1111 1111 1111")
//            Log.WriteLine("Ваш банк ввод номера карты")
//            //
//            DriverFindElement("(//*[@name='expdate'])[1]", driver).sendKeys("1219")
//            Log.WriteLine("Ваш банк ввод месяц год")
//            //
//            DriverFindElement("(//*[@id='cvc'])[1]", driver).sendKeys("123")
//            Log.WriteLine("Ваш банк ввод CVC2")
//            //
//            DriverFindElement("(//*[@class='sbersafe-pay-button'])", driver).click()
//            Log.WriteLine("Оплатить")
//            //
//            DriverFindElement("(//*[@name='password'])", driver).sendKeys("12345678")
//            Log.WriteLine("password")
//            //
//            DriverFindElement("(//*[@value='Submit'])", driver).click()
//            Log.WriteLine("Submit")
            //
            DriverNavigate(indexSplit, driver)
            //
            DriverFindElement("(//*[@class='project-admin-controls_action-i'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[2]", driver).click()
            Log.WriteLine("Завершить")
            //
            DriverNavigate("https://yandex.ru", driver)
            //
            DriverFindElement("(//*[@class='home-link home-link_black_yes'])[1]", driver).click()
            Log.WriteLine("Вход на яндекс")

            fun FindWindowYandex(): Boolean {
                return try {

                    Thread.sleep(1500)
                    driver.findElement(By.xpath("//*[@class='passp-page-overlay']"))
                    true

                } catch (e: Exception) {

                    false
                }
            }

            if (!FindWindowYandex()) {
                DriverFindElement("//*[@name='login']", driver).sendKeys("pain-net")
                Log.WriteLine("Ввод Логина")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman924")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passport-Button-Text']", driver).click()
                Log.WriteLine("Войти")
                //
                Thread.sleep(1500)
                try{
                    driver.findElement(By.xpath("//*[contains(text(), 'Поздравляем с успешным завершением проекта на Planeta.ru!')]")).click()
                    Log.WriteLine("Выбор письма")
                    Thread.sleep(400)
                    driver.findElement(By.xpath("(//*[@class='mail-MessageSnippet-Content'])[2]")).click()
                    Log.WriteLine("Письмо")
                    Thread.sleep(400)
                    DriverFindElement("(//*[contains(text(), 'здесь')])[1]", driver).click()
                    Log.WriteLine("Здесь")
                    Thread.sleep(400)
                    DriverFindElement("(//*[contains(text(), 'Обновление и продвижение проекта')])[1]", driver)
                }catch (e:Exception){

                }
            }

            if (FindWindowYandex()) {

                DriverFindElement("//*[@name='login']", driver).sendKeys("pain-net")
                Log.WriteLine("Ввод Логина")
                //
                DriverFindElement("//*[@class='passp-login-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman924")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passp-password-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                //
                Thread.sleep(1500)
                driver.findElement(By.xpath("//*[contains(text(), 'Поздравляем с успешным завершением проекта на Planeta.ru!')]")).click()
                Log.WriteLine("Выбор письма")
                Thread.sleep(400)
                try {
                    driver.findElement(By.xpath("(//*[@class='mail-MessageSnippet-Content'])[2]")).click()
                    Log.WriteLine("Письмо")
                    Thread.sleep(400)
                    DriverFindElement("(//*[contains(text(), 'здесь')])[1]", driver).click()
                    Log.WriteLine("Здесь")
                    Thread.sleep(400)
                    DriverFindElement("(//*[contains(text(), 'Обновление и продвижение проекта')])[1]", driver)
                } catch (e: Exception) {

                }

            }
            Log.Close()
            Stop()
            driver.quit()
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            Stop1()
        }
    }


    @Test
    fun projectNoSuccessful() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            var world = 10
            val r = Random()
            var number = StringBuilder()
            for (i in 0 until world) {
                val tmp = r.nextInt(10)
                number.append(tmp)
            }

            var world1 = 9
            val r1 = Random()
            var number1 = StringBuilder()
            for (i in 0 until world1) {
                val tmp = r1.nextInt(9)
                number1.append(tmp)
            }





            Log.WriteLine("Не успешный проект" + VersionBrowser(driver))





            AvtirizationOther(driver, Log)
            //
            val text: WebElement = DriverFindElement("//*[@class='h-user_name']", driver) // Вытащить текст из элемента
            var Text1: String = text.text
            var textNew = Text1.replace("\\s".toRegex(), "")
            println(textNew)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[1]", driver).click()
            //
            try {
                driver.findElement(By.xpath("//*[contains(text(), 'создать')]")).click()
                Log.WriteLine("Создать")
            } catch (e: Exception) {
                driver.findElement(By.xpath("//*[@class='header_create-link']")).click()
                Log.WriteLine("Создать Проект")
            }

            DriverFindElement("//*[@class='checkbox']", driver).click()
            Log.WriteLine("Я Согласен на Условия")

            DriverFindElement("//*[@class='btn btn-primary frc-i-accept-btn create-campaign']", driver).click()
            Log.WriteLine("Создать проект")
            Thread.sleep(2000)
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[1]", driver).sendKeys("Project_User" + Date())
            Log.WriteLine("Название проекта")

            var str = driver.currentUrl

            var n: String = str.replace("\\s".toRegex(), "")

            var strProject: String = n.trim(' ')
            var spl = strProject.split('/')
            var indexSplit = spl[4]
            println(indexSplit)
            //
            DriverFindElement("(//*[@class='form-control control-xlg borderless'])", driver).sendKeys("web" + number.toString())
            Log.WriteLine("Красивый адрес проекта")

            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")

            try {
                DriverFindElement("//*[@class='btn btn-primary']", driver).click()
                Log.WriteLine("Сохранить")
            } catch (p: Exception) {
                Log.WriteLine("Сохранить не выскочило т к размер фото меньше")
            }

            Thread.sleep(2800)
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")

            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Фото по ссылке")

            DriverFindElement("//*[@name='img_url']", driver).sendKeys("http://www.animacity.ru/sites/default/files/blog/82069/14624.jpg")
            Log.WriteLine("Ввод ссылки")

            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")

            try {
                DriverFindElement("//*[@class='btn btn-primary']", driver).click()
                Log.WriteLine("Сохранить")
            } catch (p: Exception) {
                Log.WriteLine("Сохранить не выскочило т к размер фото меньше")
            }

            Thread.sleep(1800)

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[2]", driver).sendKeys("cxcdsfdsfqq")
            Log.WriteLine("Коротко о проекте")

            DriverFindElement("//*[@class='icon-select']", driver).click()
            Log.WriteLine("Страна реализции")

            DriverFindElement("//*[@class='dropdown-menu']/li[2]", driver).click()
            Log.WriteLine("Выбор страны")

            DriverFindElement("//*[@class='form-control control-xlg prj-crt-input ac_input']", driver).sendKeys("Москва")
            Log.WriteLine("Регион реализации")

            DriverFindElement("//*[@class='ac_results']//li[1]", driver).click()
            Log.WriteLine("Москва")

            DriverFindElement("//*[@name='cityNameRus']", driver).sendKeys("Алабино")
            Log.WriteLine("Город реализации")

            DriverFindElement("//*[@class='ac_results']//*[@class='ac_even ac_over']", driver).click()

            DriverFindElement("//*[@data-parse='number']", driver).clear()

            DriverFindElement("//*[@data-parse='number']", driver).sendKeys("500000")
            Log.WriteLine("Финансовая цель")

            DriverFindElement("//*[@class='project-create_col-val_col']", driver).click()
            Log.WriteLine("Срок окончания проекта")

            DriverFindElement("(//*[@data-parse='number'])[2]", driver).click()

            DriverFindElement("//*[@class='ui-datepicker-next ui-corner-all']", driver).click()
            Log.WriteLine("Следующий месяц")

            DriverFindElement("//*[@class='ui-datepicker-calendar']//tr[4]//td[5]", driver).click()
            Log.WriteLine("Выбор даты")

            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")

            Thread.sleep(800)

            DriverFindElement("(//*[@class='project-create_tab_link'])[2]", driver).click()
            Log.WriteLine("Детали")

            val el4 = DriverFindElement("//*[@type='file']", driver)
            el4.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")

            try {
                DriverFindElement("//*[@class='btn btn-primary']", driver).click()
                Log.WriteLine("Сохранить")
            } catch (p: Exception) {
                Log.WriteLine("Сохранить не выскочило т к размер фото меньше")
            }

            Thread.sleep(1800)
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")

            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Видео с Ютуба")

            DriverFindElement("//*[@name='img_url']", driver).sendKeys("https://www.youtube.com/watch?v=VVtZVabutYM")
            Log.WriteLine("Ввод ссылки")

            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")

            DriverFindElement("(//*[@class='mceIcon mce_planetaphoto'])", driver).click()
            Log.WriteLine("Загрузить картинку в поле text-area")

            DriverFindElement("//*[@class='link']", driver).click()
            Log.WriteLine("Загрузить картинку по ссылке")

            DriverFindElement("//*[@name='img_url']", driver).sendKeys("http://www.animacity.ru/sites/default/files/blog/82069/14624.jpg")
            Log.WriteLine("Ввод ссылки")

            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")

            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")

            DriverFindElement("(//*[@class='project-create_tab_link'])[3]", driver).click()
            Log.WriteLine("Вознаграждение")

            DriverFindElement("//*[@class='form-control prj-crt-input js-share-name-limit']", driver).sendKeys("SATANA")
            Log.WriteLine("Название вознаграждения")

            driver.switchTo().frame(0)

            DriverFindElement("//*[@class='mceContentBody  mceCampaignEditorBody']", driver).sendKeys("666")
            Log.WriteLine("Описание вознаграждения")

            driver.switchTo().defaultContent()

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[1]", driver).sendKeys("Прямо в жерло преисподни")
            Log.WriteLine("Способы получения")

            DriverFindElement("(//*[@name='price'])", driver).clear()

            DriverFindElement("(//*[@name='price'])", driver).sendKeys("100000")
            Log.WriteLine("Цена вознаграждения")

            DriverFindElement("//*[@class='checkbox active']", driver).click()
            Log.WriteLine("Отключить ЧекБокс")

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[3]", driver).clear()

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[3]", driver).sendKeys("100")

            DriverFindElement("//*[@class='form-control prj-crt-input prj-crt-input__date hasDatepicker']", driver).click()
            Log.WriteLine("Примерная дата доставки")

            DriverFindElement("//*[@class='ui-datepicker-calendar']//tr[4]//td[5]", driver).click()
            Log.WriteLine("Выбор даты")

            val el5 = DriverFindElement("//*[@type='file']", driver)
            el5.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")

            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")

            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Фото по ссылке")

            DriverFindElement("//*[@name='img_url']", driver).sendKeys("http://www.animacity.ru/sites/default/files/blog/82069/14624.jpg")
            Log.WriteLine("Ввод ссылки")

            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")

            DriverFindElement("(//*[@class='checkbox'])[1]", driver).click()
            Log.WriteLine("Запрашивать адрес доставки")

            Thread.sleep(1500)
            //
            val elAction = DriverFindElement("(//*[@class='checkbox-row flat-ui-control'])[2]", driver)
            val action = Actions(driver)
            action.moveToElement(elAction).click().build().perform()

            val elActions2 = DriverFindElement("(//*[@class='checkbox-row flat-ui-control'])[3]", driver)
            val action2 = Actions(driver)
            action2.moveToElement(elActions2).click().build().perform()
            Log.WriteLine("Возможен самовывоз")

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[4]", driver).sendKeys("Москва")
            Log.WriteLine("Город")

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[5]", driver).sendKeys("Ул Малая Большая д 23 кв 666")
            Log.WriteLine("Адрес и условия самовывоза")

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[6]", driver).sendKeys("74956661323")
            Log.WriteLine("Телефон")

            val elActions3 = DriverFindElement("(//*[@class='checkbox'])[1]", driver)
            val action3 = Actions(driver)
            action3.moveToElement(elActions3).click().build().perform()
            Log.WriteLine("Задать вопрос покупателю")
            Thread.sleep(2500)
            driver.findElement(By.xpath("//*[@class='select-cont']")).click()

            DriverFindElement("//*[@class='dropdown-menu']//li[1]", driver).click()
            Log.WriteLine("Выбор вопроса")

            DriverFindElement("//*[@class='btn btn-primary btn-lg']", driver).click()
            Log.WriteLine("Добавить изображение")

            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")

            Thread.sleep(500)

            DriverFindElement("(//*[@class='project-create_tab_link'])[4]", driver).click()
            Log.WriteLine("Контр-Агент")

            DriverFindElement("(//*[@class='select-cont'])[1]", driver).click()
            Log.WriteLine("Выбор нового контрАгента")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[1]//li//*[contains(text(), '                      Новый контрагент                 ')]", driver).click()
            Log.WriteLine("Выбор нового контр агента")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[1]", driver).sendKeys("фывфывфывфывфывфывфывфы")
            Log.WriteLine("ФИО физического лица ИП или наименование организации для юридического лица")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[2]", driver).sendKeys("И.И ИВанов")
            Log.WriteLine("Инициалы и фамилия для подписи в договоре")
            //
            DriverFindElement("//*[@class='form-control prj-crt-input prj-crt-input__date hasDatepicker']", driver).sendKeys("20.9.1990")
            Log.WriteLine("Дата рождения")
            //
            Thread.sleep(300)
            val ac = Actions(driver)
            ac.sendKeys(Keys.ESCAPE).build().perform()
            //
            DriverFindElement("//*[@class='form-control control-xlg prj-crt-input js-city-name-input ac_input']", driver).sendKeys("Москва")
            Log.WriteLine("Город")
            //
            Thread.sleep(500)
            val ac2 = Actions(driver)
            ac2.sendKeys(Keys.ENTER).build().perform()
            Log.WriteLine("Москва")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[3]", driver).sendKeys("84955555535")
            Log.WriteLine("Телефон")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[5]", driver).sendKeys(number.toString())
            Log.WriteLine("Номер паспорта")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[6]", driver).sendKeys("Отделением ОУФМС по бла бла бла")
            Log.WriteLine("Кем выдан")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input prj-crt-input__date hasDatepicker'])[2]", driver).sendKeys("20.03.2007")
            Log.WriteLine("Когда выдан паспорт")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[7]", driver).sendKeys("770-060")
            Log.WriteLine("Код подразделения")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[5]", driver).click()//Для линукса! у винды закомитить
            //
            DriverFindElement("(//*[@class='btn upload-file mrg-t-5'])", driver).click()
            Log.WriteLine("Необходимые документы для оформления договора - Загрузить")
            //
            val el3 = DriverFindElement("//*[@type='file']", driver)
            el3.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[4]", driver).sendKeys(number.toString())
            Log.WriteLine("ИНН")
            //
            DriverFindElement("//*[@class='form-control prj-crt-input js-save-model']", driver).sendKeys("Ул большая нищебродская дом картонная коробка")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[9]", driver).sendKeys("Банк Анимэ и кавая")
            Log.WriteLine("Банк получателя")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[10]", driver).sendKeys(number.toString() + number.toString())
            Log.WriteLine("Корреспондентский счет")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[8]", driver).sendKeys(number.toString() + number.toString())
            Log.WriteLine("Расчётный счет")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[11]", driver).sendKeys(number1.toString())
            Log.WriteLine("БИК")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input js-save-model'])[2]", driver).sendKeys("Ашкараагалдыыыыыыыыыыыыыыыыыыы")
            Log.WriteLine("Прочие данные")
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            Thread.sleep(1000)
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn btn-primary project-create_next']", driver).click()
            Log.WriteLine("Отправить на модерацию")
            //
            Thread.sleep(1500)
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("ОК")
            //
            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[2]", driver).click()
            Log.WriteLine("Проекты")
            //
            DriverFindElement("(//*[@class='form-control'])[1]", driver).sendKeys(indexSplit)
            Log.WriteLine("Ввод номера проекта")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//tbody//tr//td[3]//a", driver).click()
            Log.WriteLine("выбор проекта")
            //
            DriverFindElement("(//*[@class='project-author-controls_name'])[2]", driver).click()
            Log.WriteLine("Редактировать")
            //
            DriverFindElement("(//*[@class='select2-container select2-container-multi select-tags js-select2'])", driver).click()
            //
            DriverFindElement("//*[@class='select2-results']//li[1]", driver).click()
            Log.WriteLine("Выбор категории")
            //
            DriverFindElement("(//*[@class='btn project-create_save'])[1]", driver).click()
            Log.WriteLine("Сохранить")
            //
            indexSplit = spl[0] + "/" + spl[1] + "/" + spl[2] + "/" + spl[3] + "/" + spl[4]
            DriverNavigate(indexSplit, driver)
            //
            DriverFindElement("//*[@class='btn btn-nd-success js-start-campaign']", driver).click()
            Log.WriteLine("Запустить")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("ОК")
            //
            DriverFindElement("//*[@class='button button__b button__def']", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            DriverFindElement("(//*[@name='donateAmount'])[1]", driver).clear()
            Log.WriteLine("Очистить поле")
            //
            DriverFindElement("(//*[@name='donateAmount'])[1]", driver).sendKeys("200000")
            Log.WriteLine("Ввод в поле значения 200000")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary action-card_btn js-navigate-to-purchase'])[1]", driver).click()
            Log.WriteLine("Оплатить")
            //
            fun FindEl(): Boolean {
                return try {
                    DriverFindElement("(//*[@class='form-ui-label'])[2]", driver)
                    //Согласен на условия
                    false
                } catch (e: Exception) {
                    true
                }
            }

            for (i in 0..60) {  //Клауд
                if (FindEl()) {

                    DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                    Log.WriteLine("Даю свое согласие")

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver).click()
                    Log.WriteLine("Карты других банков")
                    //
                    DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
                    Log.WriteLine("Оплатить покупку")
                    //
                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
                    Log.WriteLine("Оплата картой")
                    //
                    driver.switchTo().frame(0)
                    DriverFindElement("(//*[@class='row'])[1]//input", driver).click()
                    Log.WriteLine("клик по строке")
                    DriverFindElement("(//*[@class='row'])[1]//input", driver).sendKeys("4111 1111 1111 1111")
                    //
                    DriverFindElement("(//*[@id='inputMonth'])[1]", driver).sendKeys("12")
                    //
                    DriverFindElement("(//*[@id='inputYear'])[1]", driver).sendKeys("19")
                    //
                    DriverFindElement("(//*[@data-mask='000'])[1]", driver).sendKeys("123")
                    //
                    DriverFindElement("(//*[@id='cardHolder'])[1]", driver).sendKeys("QWERTY")
                    //
                    DriverFindElement("(//*[@class='button'])[1]", driver).click()
                    DriverFindElement("(//*[@class='button'])[2]", driver).click()
                    Log.WriteLine("OK")
                    driver.switchTo().defaultContent()
                    break
                }
            }
            //
//            DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
//            Log.WriteLine("Оплатить покупку")
//            //
//            DriverFindElement("(//*[@id='email'])[1]", driver).clear()
//            //
//            DriverFindElement("(//*[@id='email'])[1]", driver).sendKeys("pain-net@yandex.ru")
//            Log.WriteLine("Сбер ввод email")
//            //
//            DriverFindElement("(//*[@id='cardnumber'])[1]", driver).sendKeys("4111 1111 1111 1111")
//            Log.WriteLine("Ваш банк ввод номера карты")
//            //
//            DriverFindElement("(//*[@name='expdate'])[1]", driver).sendKeys("1219")
//            Log.WriteLine("Ваш банк ввод месяц год")
//            //
//            DriverFindElement("(//*[@id='cvc'])[1]", driver).sendKeys("123")
//            Log.WriteLine("Ваш банк ввод CVC2")
//            //
//            DriverFindElement("(//*[@class='sbersafe-pay-button'])", driver).click()
//            Log.WriteLine("Оплатить")
//            //
//            DriverFindElement("(//*[@name='password'])", driver).sendKeys("12345678")
//            Log.WriteLine("password")
//            //
//            DriverFindElement("(//*[@value='Submit'])", driver).click()
//            Log.WriteLine("Submit")
//            //
            DriverNavigate(indexSplit, driver)
            //
            DriverFindElement("(//*[@class='project-admin-controls_action-i'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[2]", driver).click()
            Log.WriteLine("Завершить")
            //
            DriverNavigate("https://yandex.ru", driver)
            //
            DriverFindElement("(//*[@class='home-link home-link_black_yes'])[1]", driver).click()
            Log.WriteLine("Вход на яндекс")
            //
            fun FindWindowYandex(): Boolean {
                return try {

                    Thread.sleep(1500)
                    driver.findElement(By.xpath("//*[@class='passp-page-overlay']"))
                    true

                } catch (e: Exception) {

                    false
                }
            }

            if (!FindWindowYandex()) {
                DriverFindElement("//*[@name='login']", driver).sendKeys("pain-net")
                Log.WriteLine("Ввод Логина")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman924")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passport-Button-Text']", driver).click()
                Log.WriteLine("Войти")
                //
                Thread.sleep(1500)
                DriverFindElement("(//*[@class='mail-MessageSnippet-Content'])[1]", driver).click()
                Log.WriteLine("Выбор письма")
                //
                DriverFindElement("(//*[contains(text(), 'здесь')])[1]", driver).click()
                Log.WriteLine("Здесь")
                //
                DriverFindElement("(//*[contains(text(), 'Возврат денежных средств')])[1]", driver)
                //
            }

            if (FindWindowYandex()) {

                DriverFindElement("//*[@name='login']", driver).sendKeys("pain-net")
                Log.WriteLine("Ввод Логина")
                //
                DriverFindElement("//*[@class='passp-login-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman924")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passp-password-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                //
                Thread.sleep(1500)
                driver.findElement(By.xpath("(//*[@class='mail-MessageSnippet-Content'])[1]")).click()
                Log.WriteLine("Выбор письма")
                //
                //     DriverFindElement("(//*[contains(text(), 'здесь')])[1]",driver).click()
                //      Log.WriteLine("Здесь")
                //
                //        DriverFindElement("(//*[contains(text(), 'Возврат денежных средств')])[1]",driver)
                //
            }
            Log.Close()
            Stop()
            driver.quit()
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            Stop1()
        }
    }
}



