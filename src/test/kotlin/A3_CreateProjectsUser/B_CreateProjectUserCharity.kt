import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.interactions.Actions
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

class B_CreateProjectUserCharity : Setup() {


    @Test
    fun ProjectCompletionCharity1(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            var world = 4
            val r = Random()
            var number = StringBuilder()
            for (i in 0 until world) {
                val tmp = r.nextInt(4)
                number.append(tmp)
            }




            Log.WriteLine("Заполнение проект-Мой проект является благотворительным - Основные детали" + VersionBrowser(driver))






            Avtorization_Uz4name(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[1]", driver).click()
            Log.WriteLine("Созданные проекты")
            //
            fun find() : Boolean
            {
                return try {
                    DriverFindElement("//*[@class='status-badge status-badge__m status-badge__info']", driver)
                    true
                } catch(e:Exception) {
                    false
                }

            }
            if (find())
            {
                try
                {
                    DriverFindElement("//*[@class='profile-nav']//li[2]", driver).click()
                }
                catch(e:Exception)
                {

                }
            }
            //
            while (true)
            {
                try
                {
                    Thread.sleep(500)
                    DriverFindElement("//*[@class='profile-project-info_delete-link']",driver).click()
                    Log.WriteLine("Удалить черновик")
                    Thread.sleep(500)
                    DriverFindElement("//*[@class='btn btn-primary']",driver).click()
                    Log.WriteLine("Да")
                    Thread.sleep(200)
                }
                catch (e:Exception)
                {
                    break
                }
            }


            driver.findElement(By.xpath("//*[@class='header_create-link']")).click()
            Log.WriteLine("Создать Проект")
            //
            DriverFindElement("//*[@class='checkbox']", driver).click()
            Log.WriteLine("Checkbox")
            //
            DriverFindElement("//*[@class='btn btn-primary frc-i-accept-btn create-campaign']", driver).click()
            Log.WriteLine("Создать проект")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[1]", driver).sendKeys("Charity" + Date())
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='form-control control-xlg borderless'])", driver).sendKeys("web$number")
            Log.WriteLine("Красивый адрес проекта")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            try{
                DriverFindElement("//*[@class='btn btn-primary']",driver).click()
                Log.WriteLine("Сохранить")
            }catch (p:Exception){
                Log.WriteLine("Сохранить не выскочило т к размер фото меньше")
            }
            Thread.sleep(2800)
            //
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")
            //
            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Фото по ссылке")
            //
            DriverFindElement("//*[@name='img_url']", driver).sendKeys("https://2x2tv.ru/upload/medialibrary/5ed/5ed31ef87f88f58dca8056fa951be36b.png")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")
            //
            try{
                DriverFindElement("//*[@class='btn btn-primary']",driver).click()
                Log.WriteLine("Сохранить")
            }catch (p:Exception){
                Log.WriteLine("Сохранить не выскочило т к размер фото меньше")
            }
            Thread.sleep(1500)
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[2]", driver).sendKeys("cxcdsfdsfqq")
            Log.WriteLine("Коротко о проекте")
            //
            DriverFindElement("//*[@class='icon-select']", driver).click()
            Log.WriteLine("Страна реализции")
            //
            DriverFindElement("//*[@class='dropdown-menu']/li[2]", driver).click()
            Log.WriteLine("Выбор страны")
            //
            DriverFindElement("//*[@class='form-control control-xlg prj-crt-input ac_input']", driver).sendKeys("Москва")
            Log.WriteLine("Регион реализации")
            //
            DriverFindElement("//*[@class='ac_results']//li[1]", driver).click()
            Log.WriteLine("Москва")
            //
            DriverFindElement("//*[@name='cityNameRus']", driver).sendKeys("Алабино")
            Log.WriteLine("Город реализации")
            //
            DriverFindElement("//*[@class='ac_results']//*[@class='ac_even ac_over']", driver).click()
            //
            DriverFindElement("//*[@data-parse='number']", driver).clear()
            //
            DriverFindElement("//*[@data-parse='number']", driver).sendKeys("500000")
            Log.WriteLine("Финансовая цель")
            //
            DriverFindElement("//*[@class='project-create_col-val_col']", driver).click()
            Log.WriteLine("Срок окончания проекта")
            //
            DriverFindElement("(//*[@data-parse='number'])[2]", driver).click()
            //
            DriverFindElement("//*[@class='ui-datepicker-next ui-corner-all']", driver).click()
            Log.WriteLine("Следующий месяц")
            //
            DriverFindElement("//*[@class='ui-datepicker-calendar']//tr[4]//td[5]", driver).click()
            Log.WriteLine("Выбор даты")
            //
            DriverFindElement("//*[@class='checkbox js-charity-checkbox']", driver).click()
            Log.WriteLine("Мой проект является благотворительным")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()

            ProjectCompletionCharity2()
            ProjectCompletionCharity3()
            ProjectCompletionCharity4()

        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }


    fun ProjectCompletionCharity2(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Заполнение проекта Мой проект является благотворительным -Детали" + VersionBrowser(driver))





            Avtorization_Uz4name(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[1]", driver).click()
            Log.WriteLine("Созданные проекты")
            //
            fun find() : Boolean
            {
                try
                {
                    DriverFindElement("//*[@class='status-badge status-badge__info']", driver)
                    return true
                }
                catch(e:Exception)
                {
                    return false
                }

            }
            if (find())
            {
                DriverFindElement("//*[@class='profile-nav']//li[2]", driver).click()
            }
            //
            DriverFindElement("//*[@class='profile-project-change_link']", driver).click()
            Log.WriteLine("Редактировать")
            //
            DriverFindElement("(//*[@class='project-create_tab_link'])[2]", driver).click()
            Log.WriteLine("Детали")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            try{
                DriverFindElement("//*[@class='btn btn-primary']",driver).click()
                Log.WriteLine("Сохранить")
            }catch (p:Exception){
                Log.WriteLine("Сохранить не выскочило т к размер фото меньше")
            }
            //
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")
            //
            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Видео с Ютуба")
            //
            DriverFindElement("//*[@name='img_url']", driver).sendKeys("https://www.youtube.com/watch?v=VVtZVabutYM")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")
            //
            DriverFindElement("(//*[@class='mceIcon mce_planetaphoto'])", driver).click()
            Log.WriteLine("Загрузить картинку в поле text-area")
            //
            DriverFindElement("//*[@class='link']", driver).click()
            Log.WriteLine("Загрузить картинку по ссылке")
            //
            DriverFindElement("//*[@name='img_url']", driver).sendKeys("https://i.kym-cdn.com/photos/images/original/001/252/564/772.jpg")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }


    fun ProjectCompletionCharity3(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Заполнение проекта-Мой проект является благотворительным-Вознаграждение" + VersionBrowser(driver))





            Avtorization_Uz4name(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[1]", driver).click()
            Log.WriteLine("Созданные проекты")
            //
            fun find() : Boolean
            {
                return try {
                    DriverFindElement("//*[@class='status-badge status-badge__info']", driver)
                    true
                } catch(e:Exception) {
                    false
                }

            }
            if (find())
            {
                try
                {
                    DriverFindElement("//*[@class='profile-nav']//li[2]", driver).click()
                }
                catch(e:Exception)
                {

                }
            }
            //
            DriverFindElement("//*[@class='profile-project-change_link']", driver).click()
            Log.WriteLine("Редактировать")
            //
            DriverFindElement("(//*[@class='project-create_tab_link'])[3]", driver).click()
            Log.WriteLine("Вознаграждение")
            //
            DriverFindElement("//*[@class='form-control prj-crt-input js-share-name-limit']", driver).sendKeys("SATANA" + Date())
            Log.WriteLine("Название вознаграждения")
            //
            driver.switchTo().frame(0)
            //
            DriverFindElement("//*[@class='mceContentBody  mceCampaignEditorBody']", driver).sendKeys("666")
            Log.WriteLine("Описание вознаграждения")
            //
            driver.switchTo().defaultContent()
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[1]", driver).sendKeys("Прямо в жерло преисподни")
            Log.WriteLine("Способы получения")
            //
            DriverFindElement("(//*[@name='price'])", driver).clear()
            //
            DriverFindElement("(//*[@name='price'])", driver).sendKeys("100000")
            Log.WriteLine("Цена вознаграждения")
            //
            DriverFindElement("//*[@class='form-control prj-crt-input prj-crt-input__date hasDatepicker']", driver).click()
            Log.WriteLine("Примерная дата доставки")
            //
            DriverFindElement("//*[@class='ui-datepicker-calendar']//tr[4]//td[5]", driver).click()
            Log.WriteLine("Выбор даты")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")
            //
            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Фото по ссылке")
            //
            DriverFindElement("//*[@name='img_url']", driver).sendKeys("https://i.pinimg.com/originals/2c/33/03/2c33039aed01a190c8c7d1b1db60013a.jpg")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")
            //
            DriverFindElement("(//*[@class='checkbox'])[1]", driver).click()
            Log.WriteLine("Запрашивать адрес доставки")
            //
            DriverFindElement("(//*[@class='checkbox'])[1]", driver).click()
            Log.WriteLine("Возможен самовывоз")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[4]", driver).sendKeys("Москва")
            Log.WriteLine("Город")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[5]", driver).sendKeys("Ул Малая Большая д 23 кв 666")
            Log.WriteLine("Адрес и условия самовывоза")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[6]", driver).sendKeys("74956661323")
            Log.WriteLine("Телефон")
            //
            DriverFindElement("(//*[@class='checkbox'])[1]", driver).click()
            Log.WriteLine("Задать вопрос покупателю")
            Thread.sleep(2500)
            driver.findElement(By.xpath("//*[@class='select-cont']")).click()
            //
            DriverFindElement("//*[@class='dropdown-menu']//li[1]", driver).click()
            Log.WriteLine("Выбор вопроса")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-lg']", driver).click()
            Log.WriteLine("Добавить изображение")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }


    fun ProjectCompletionCharity4(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            var world = 10
            val r = Random()
            var number = StringBuilder()
            for (i in 0 until world) {
                val tmp = r.nextInt(10)
                number.append(tmp)
            }

            var world1 = 9
            val r1 = Random()
            var number1 = StringBuilder()
            for (i in 0 until world1) {
                val tmp = r1.nextInt(9)
                number1.append(tmp)
            }



            Log.WriteLine("Заполнение проекта-Мой проект является благотворительным-Контр-Агент-ООО-Неккомерческая орга-ция" + VersionBrowser(driver))




            Avtorization_Uz4name(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[1]", driver).click()
            Log.WriteLine("Созданные проекты")
            //
            fun find() : Boolean
            {
                return try {
                    DriverFindElement("//*[@class='status-badge status-badge__info']", driver)
                    true
                } catch(e:Exception) {
                    false
                }

            }
            if (find())
            {
                try
                {
                    DriverFindElement("//*[@class='profile-nav']//li[2]", driver).click()
                }
                catch(e:Exception)
                {
                }
            }
            //
            DriverFindElement("//*[@class='profile-project-change_link']", driver).click()
            Log.WriteLine("Редактировать")
            //
            DriverFindElement("(//*[@class='project-create_tab_link'])[4]", driver).click()
            Log.WriteLine("Контр-Агент")
            //
            Log.WriteLine("Некоммерческая организация")
            //
            //LeftClick(13, 829)
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[1]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[1]", driver).sendKeys("фывфывфывфывфывфывфывфы")
            Log.WriteLine("ФИО физического лица ИП или наименование организации для юридического лица")
            //
            DriverFindElement("(//*[@class='select-cont'])[3]", driver).click()
            Log.WriteLine("Должность руководителя")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[3]/li[3]", driver).click()
            Log.WriteLine("Генеральный директор")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[2]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[2]", driver).sendKeys("И.И ИВанов")
            Log.WriteLine("Фио Руководителя")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[3]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[3]", driver).sendKeys("Иван.Иванович  ИВанов")
            Log.WriteLine("Инициалы и фамилия для подписи в договоре")
            //
            DriverFindElement("(//*[@class='icon-select'])[4]", driver).click()
            Log.WriteLine("Страна регистрации")
            //
            DriverFindElement("//*[@class='form-control control-xlg prj-crt-input js-city-name-input ac_input']", driver).clear()
            //
            DriverFindElement("//*[@class='form-control control-xlg prj-crt-input js-city-name-input ac_input']", driver).sendKeys("Москва")
            Log.WriteLine("Город")
            //
            DriverFindElement("//*[@class='ac_results']//li[1]", driver).click()
            Log.WriteLine("Москва")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[4]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[4]", driver).sendKeys("84955555535")
            Log.WriteLine("Телефон")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[5]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[5]", driver).sendKeys(number.toString() + "666")
            Log.WriteLine("ОГРН")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[6]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[6]", driver).sendKeys(number.toString())
            Log.WriteLine("ИНН")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[7]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[7]", driver).sendKeys("123!@#fsdfавыа")
            Log.WriteLine("КПП")
            //
            DriverFindElement("(//*[@class='btn upload-file mrg-t-5'])", driver).click()
            Log.WriteLine("Необходимые документы для оформления договора - Загрузить")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input js-save-model'])[1]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input js-save-model'])[1]", driver).sendKeys("Qkfdsfdkjhkjdfhkjdkjhfgkjdfhkjghfkjdh")
            Log.WriteLine("Юридический адрес")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[8]", driver).clear()

            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[8]", driver).sendKeys("Фактический адрес")
            Log.WriteLine("Фактический адрес")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[9]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[9]", driver).sendKeys(number.toString() + number.toString())
            Log.WriteLine("Расчётный счет")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[10]", driver).clear()

            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[10]", driver).sendKeys("Банк Анимэ и кавая")
            Log.WriteLine("Банк получателя")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[11]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[11]", driver).sendKeys(number.toString() + number.toString())
            Log.WriteLine("Корреспондентский счет")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[12]", driver).clear()

            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[12]", driver).sendKeys(number1.toString())
            Log.WriteLine("БИК")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input js-save-model'])[2]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input js-save-model'])[2]", driver).sendKeys("Ашкараагалдыыыыыыыыыыыыыыыыыыы")
            Log.WriteLine("Прочие данные")
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("(//*[@class='select-cont'])[3]", driver).click()
            Log.WriteLine("Должность руководителя")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[3]/li[2]", driver).click()
            Log.WriteLine("Другая")
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver)
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[2]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[2]", driver).sendKeys("Адилович")
            Log.WriteLine("Название должности")
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver)
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("(//*[@class='select-cont'])[3]", driver).click()
            Log.WriteLine("Должность руководителя")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[3]/li[3]", driver).click()
            Log.WriteLine("Генеральный директор")
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("(//*[@class='select-cont'])[3]", driver).click()
            Log.WriteLine("Должность руководителя")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[3]/li[4]", driver).click()
            Log.WriteLine("Директор")
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("(//*[@class='select-cont'])[3]", driver).click()
            Log.WriteLine("Должность руководителя")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[3]/li[5]", driver).click()
            Log.WriteLine("Председатель")
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("(//*[@class='select-cont'])[3]", driver).click()
            Log.WriteLine("Должность руководителя")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[3]/li[6]", driver).click()
            Log.WriteLine("Президент")
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            Thread.sleep(1000)
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn btn-primary project-create_next']", driver).click()
            Log.WriteLine("Отправить на модерацию")
            //
            DriverFindElement("(//*[@class='form-ui-txt'])[10]", driver).click()
            Log.WriteLine("Оценка качества сервиса 10")
            //
            DriverFindElement("(//*[@class='form-ui form-ui-default'])[11]", driver).click()
            Log.WriteLine("я хочу принять участие в детальном опросе")
            //
            DriverFindElement("(//*[@class='quality-polling_btn'])", driver).click()
            Log.WriteLine("Отправить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun ProjectCompletionCharity5(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            var world = 4
            val r = Random()
            var number4 = StringBuilder()
            for (i in 0 until world) {
                val tmp = r.nextInt(4)
                number4.append(tmp)
            }


            var world1 = 10
            val r1 = Random()
            var number = StringBuilder()
            for (i in 0 until world1) {
                val tmp = r1.nextInt(10)
                number.append(tmp)
            }


            var world2 = 9
            val r2 = Random()
            var number2 = StringBuilder()
            for (i in 0 until world2) {
                val tmp = r2.nextInt(9)
                number2.append(tmp)
            }





            Log.WriteLine("Заполнение проект-Мой проект является благотворительным - Новый юзер" + VersionBrowser(driver))





            AvtirizationOther(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[1]", driver).click()
            Log.WriteLine("Созданные проекты")
            //
            while (true)
            {
                try
                {
                    Thread.sleep(500)
                    DriverFindElement("//*[@class='profile-project-info_delete-link']",driver).click()
                    Log.WriteLine("Удалить черновик")
                    Thread.sleep(500)
                    DriverFindElement("//*[@class='btn btn-primary']",driver).click()
                    Log.WriteLine("Да")
                    Thread.sleep(200)
                }
                catch (e:Exception)
                {
                    break
                }
            }
            //
            driver.findElement(By.xpath("//*[@class='header_create-link']")).click()
            Log.WriteLine("Создать Проект")
            //
            DriverFindElement("//*[@class='checkbox']", driver).click()
            Log.WriteLine("Checkbox")
            //
            DriverFindElement("//*[@class='btn btn-primary frc-i-accept-btn create-campaign']", driver).click()
            Log.WriteLine("Создать проект")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[1]", driver).sendKeys("Charity" + Date())
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='form-control control-xlg borderless'])", driver).sendKeys("web$number")
            Log.WriteLine("Красивый адрес проекта")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            try{
                DriverFindElement("//*[@class='btn btn-primary']",driver).click()
                Log.WriteLine("Сохранить")
            }catch (p:Exception){
                Log.WriteLine("Сохранить не выскочило т к размер фото меньше")
            }
            //
            Thread.sleep(2900)
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")
            //
            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Фото по ссылке")
            //
            DriverFindElement("//*[@name='img_url']", driver).sendKeys("https://image.animedigitalnetwork.fr/license/onepunchman/tv/web/affiche_300x428.jpg")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")
            //
            try{
                DriverFindElement("//*[@class='btn btn-primary']",driver).click()
                Log.WriteLine("Сохранить")
            }catch (p:Exception){
                Log.WriteLine("Сохранить не выскочило т к размер фото меньше")
            }
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[2]", driver).sendKeys("cxcdsfdsfqq")
            Log.WriteLine("Коротко о проекте")
            //
            DriverFindElement("//*[@class='icon-select']", driver).click()
            Log.WriteLine("Страна реализции")
            //
            DriverFindElement("//*[@class='dropdown-menu']/li[2]", driver).click()
            Log.WriteLine("Выбор страны")
            //
            DriverFindElement("//*[@class='form-control control-xlg prj-crt-input ac_input']", driver).sendKeys("Москва")
            Log.WriteLine("Регион реализации")
            //
            DriverFindElement("//*[@class='ac_results']//li[1]", driver).click()
            Log.WriteLine("Москва")
            //
            DriverFindElement("//*[@name='cityNameRus']", driver).sendKeys("Алабино")
            Log.WriteLine("Город реализации")
            //
            DriverFindElement("//*[@class='ac_results']//*[@class='ac_even ac_over']", driver).click()
            //
            DriverFindElement("//*[@data-parse='number']", driver).clear()
            //
            DriverFindElement("//*[@data-parse='number']", driver).sendKeys("500000")
            Log.WriteLine("Финансовая цель")
            //
            DriverFindElement("//*[@class='project-create_col-val_col']", driver).click()
            Log.WriteLine("Срок окончания проекта")
            //
            DriverFindElement("(//*[@data-parse='number'])[2]", driver).click()
            //
            DriverFindElement("//*[@class='ui-datepicker-next ui-corner-all']", driver).click()
            Log.WriteLine("Следующий месяц")
            //
            DriverFindElement("//*[@class='ui-datepicker-calendar']//tr[4]//td[5]", driver).click()
            Log.WriteLine("Выбор даты")
            //
            DriverFindElement("//*[@class='checkbox js-charity-checkbox']", driver).click()
            Log.WriteLine("Мой проект является благотворительным")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("(//*[@class='project-create_tab_link'])[2]", driver).click()
            Log.WriteLine("Детали")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            try{
                DriverFindElement("//*[@class='btn btn-primary']",driver).click()
                Log.WriteLine("Сохранить")
            }catch (p:Exception){
                Log.WriteLine("Сохранить не выскочило т к размер фото меньше")
            }
            //
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")
            //
            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Видео с Ютуба")
            //
            DriverFindElement("//*[@name='img_url']", driver).sendKeys("https://www.youtube.com/watch?v=VVtZVabutYM")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")
            //
            DriverFindElement("(//*[@class='mceIcon mce_planetaphoto'])", driver).click()
            Log.WriteLine("Загрузить картинку в поле text-area")
            //
            DriverFindElement("//*[@class='link']", driver).click()
            Log.WriteLine("Загрузить картинку по ссылке")
            //
            DriverFindElement("//*[@name='img_url']", driver).sendKeys("http://www.animacity.ru/sites/default/files/blog/82069/14624.jpg")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("(//*[@class='project-create_tab_link'])[3]", driver).click()
            Log.WriteLine("Вознаграждение")
            //
            DriverFindElement("//*[@class='form-control prj-crt-input js-share-name-limit']", driver).sendKeys("SATANA" + Date())
            Log.WriteLine("Название вознаграждения")
            //
            driver.switchTo().frame(0)
            //
            DriverFindElement("//*[@class='mceContentBody  mceCampaignEditorBody']", driver).sendKeys("666")
            Log.WriteLine("Описание вознаграждения")
            //
            driver.switchTo().defaultContent()
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[1]", driver).sendKeys("Прямо в жерло преисподни")
            Log.WriteLine("Способы получения")
            //
            DriverFindElement("(//*[@name='price'])", driver).clear()
            //
            DriverFindElement("(//*[@name='price'])", driver).sendKeys("100000")
            Log.WriteLine("Цена вознаграждения")
            //
            DriverFindElement("//*[@class='form-control prj-crt-input prj-crt-input__date hasDatepicker']", driver).click()
            Log.WriteLine("Примерная дата доставки")
            //
            DriverFindElement("//*[@class='ui-datepicker-calendar']//tr[4]//td[5]", driver).click()
            Log.WriteLine("Выбор даты")
            //
            val el3 = DriverFindElement("//*[@type='file']", driver)
            el3.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")
            //
            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Фото по ссылке")
            //
            DriverFindElement("//*[@name='img_url']", driver).sendKeys("https://i.pinimg.com/originals/b5/2a/3a/b52a3a47eff76ca03c0c736151656341.jpg")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")
            //
            DriverFindElement("(//*[@class='checkbox'])[1]", driver).click()
            Log.WriteLine("Запрашивать адрес доставки")
            //
            Thread.sleep(1500)
            //
            val elAction =  DriverFindElement("(//*[@class='checkbox-row flat-ui-control'])[2]", driver)
            val action = Actions(driver)
            action.moveToElement(elAction).click().build().perform()

            val elActions2 = DriverFindElement("(//*[@class='checkbox-row flat-ui-control'])[3]", driver)
            val action2 = Actions(driver)
            action2.moveToElement(elActions2).click().build().perform()
            Log.WriteLine("Возможен самовывоз")

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[4]", driver).sendKeys("Москва")
            Log.WriteLine("Город")

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[5]", driver).sendKeys("Ул Малая Большая д 23 кв 666")
            Log.WriteLine("Адрес и условия самовывоза")

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[6]", driver).sendKeys("74956661323")
            Log.WriteLine("Телефон")

            Thread.sleep(500)
            val elActions3 = DriverFindElement("(//*[@class='checkbox'])[1]", driver)
            val action3 = Actions(driver)
            action3.moveToElement(elActions3).click().build().perform()
            Log.WriteLine("Задать вопрос покупателю")
            Thread.sleep(2500)
            driver.findElement(By.xpath("//*[@class='select-cont']")).click()
            //
            DriverFindElement("//*[@class='dropdown-menu']//li[1]", driver).click()
            Log.WriteLine("Выбор вопроса")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-lg']", driver).click()
            Log.WriteLine("Добавить изображение")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("(//*[@class='project-create_tab_link'])[4]", driver).click()
            Log.WriteLine("Контр-Агент")
            //
            Log.WriteLine("Некоммерческая организация")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[1]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[1]", driver).sendKeys("фывфывфывфывфывфывфывфы")
            Log.WriteLine("ФИО физического лица ИП или наименование организации для юридического лица")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[2]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[2]", driver).sendKeys("И.И ИВанов")
            Log.WriteLine("Фио Руководителя")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[3]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[3]", driver).sendKeys("Иван.Иванович  ИВанов")
            Log.WriteLine("Инициалы и фамилия для подписи в договоре")
            //
            DriverFindElement("(//*[@class='icon-select'])[4]", driver).click()
            Log.WriteLine("Страна регистрации")
            //
            DriverFindElement("//*[@class='form-control control-xlg prj-crt-input js-city-name-input ac_input']", driver).clear()
            //
            DriverFindElement("//*[@class='form-control control-xlg prj-crt-input js-city-name-input ac_input']", driver).sendKeys("Москва")
            Log.WriteLine("Город")
            //
            DriverFindElement("(//*[@class='ac_results'][3]//li[1])", driver).click()
            Log.WriteLine("Москва")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[4]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[4]", driver).sendKeys("84955555535")
            Log.WriteLine("Телефон")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[5]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[5]", driver).sendKeys(number.toString() + "666")
            Log.WriteLine("ОГРН")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[6]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[6]", driver).sendKeys(number.toString())
            Log.WriteLine("ИНН")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[7]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[7]", driver).sendKeys("123!@#fsdfавыа")
            Log.WriteLine("КПП")
            //
            val ac = Actions(driver)
            ac.sendKeys(Keys.ESCAPE).build().perform()
            //
            DriverFindElement("(//*[@class='btn upload-file mrg-t-5'])", driver).click()
            Log.WriteLine("Необходимые документы для оформления договора - Загрузить")
            //
            val el5 = DriverFindElement("//*[@type='file']", driver)
            el5.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input js-save-model'])[1]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input js-save-model'])[1]", driver).sendKeys("Qkfdsfdkjhkjdfhkjdkjhfgkjdfhkjghfkjdh")
            Log.WriteLine("Юридический адрес")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[8]", driver).clear()

            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[8]", driver).sendKeys("Фактический адрес")
            Log.WriteLine("Фактический адрес")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[9]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[9]", driver).sendKeys(number.toString() + number.toString())
            Log.WriteLine("Расчётный счет")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[10]", driver).clear()

            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[10]", driver).sendKeys("Банк Анимэ и кавая")
            Log.WriteLine("Банк получателя")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[11]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[11]", driver).sendKeys(number.toString() + number.toString())
            Log.WriteLine("Корреспондентский счет")
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[12]", driver).clear()

            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[12]", driver).sendKeys(number2)
            Log.WriteLine("БИК")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input js-save-model'])[2]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input js-save-model'])[2]", driver).sendKeys("Ашкараагалдыыыыыыыыыыыыыыыыыыы")
            Log.WriteLine("Прочие данные")
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("(//*[@class='select-cont'])[3]", driver).click()
            Log.WriteLine("Должность руководителя")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[3]/li[2]", driver).click()
            Log.WriteLine("Другая")
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver)
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[2]", driver).clear()
            //
            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[2]", driver).sendKeys("Адилович")
            Log.WriteLine("Название должности")
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver)
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("(//*[@class='select-cont'])[3]", driver).click()
            Log.WriteLine("Должность руководителя")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[3]/li[3]", driver).click()
            Log.WriteLine("Генеральный директор")
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("(//*[@class='select-cont'])[3]", driver).click()
            Log.WriteLine("Должность руководителя")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[3]/li[4]", driver).click()
            Log.WriteLine("Директор")
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("(//*[@class='select-cont'])[3]", driver).click()
            Log.WriteLine("Должность руководителя")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[3]/li[5]", driver).click()
            Log.WriteLine("Председатель")
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("(//*[@class='select-cont'])[3]", driver).click()
            Log.WriteLine("Должность руководителя")
            //
            DriverFindElement("(//*[@class='dropdown-menu'])[3]/li[6]", driver).click()
            Log.WriteLine("Президент")
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            Thread.sleep(1000)
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("//*[@class='btn btn-primary project-create_next']", driver).click()
            Log.WriteLine("Отправить на модерацию")
            //
            DriverFindElement("(//*[@class='form-ui-txt'])[10]", driver).click()
            Log.WriteLine("Оценка качества сервиса 10")
            //
            DriverFindElement("(//*[@class='form-ui form-ui-default'])[11]", driver).click()
            Log.WriteLine("я хочу принять участие в детальном опросе")
            //
            Thread.sleep(1500)
            DriverFindElement("(//*[@class='quality-polling_btn'])", driver).click()
            Log.WriteLine("Отправить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }




}