import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.WebDriver
import java.nio.file.Files
import java.nio.file.Paths

class D_DeleteBanners : Setup() {

    @Test
    fun DeleteBanners(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Удаление баннеров" + VersionBrowser(driver))







            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            while (true)
            {
                try
                {
                    DriverFindElement("//*[@role='img']", driver).click()
                    //
                    Thread.sleep(1000)
                    DriverFindElement("//*[contains(text(), 'Удалить')]", driver).click()
                    //
                    Thread.sleep(1000)
                    DriverFindElement("//*[contains(text(), 'Подтвердить')]", driver).click()
                    //
                    Thread.sleep(1000)
                    driver.navigate().refresh()

                }
                catch(e:Exception)
                {
                    break
                }
            }
            DriverNavigate(URL, driver)
            //
            fun find() : Boolean
            {
                return try {
                    DriverFindElement("(//*[@class='advertise-block advertise-block__margin ng-star-inserted'])[1]", driver)
                    true
                } catch(e:Exception) {
                    false
                }
            }

            if(find())
            {
                throw  Exception("Баннер не удалился!")
            }

            Log.WriteLine("Проверка на наличие баннера")
            //
            Thread.sleep(1500)
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }


}