import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.RemoteWebDriver
import java.nio.file.Files
import java.nio.file.Paths

class B_Banners2 : Setup() {


    @Test
    fun MenuBanner16(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Новая админка раздел Релкама Создать рекламную кампанию +    Главная благо верхняя перетяжка - 1170 x 80" + VersionBrowser(driver))






            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            DriverFindElement("//*[contains(text(), 'Создать новую РК')]", driver).click()
            Log.WriteLine("Создать новую рекламную компанию")
            //
            DriverFindElement("(//*[@id='mat-input-1'])[1]", driver).sendKeys("Тестовая РК" + "---" + " Главная благо верхняя перетяжка - 1170 x 80")
            Log.WriteLine("Код рекламной кампании")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-trigger'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Статус запущена")
            //
            DriverFindElement("//*[contains(text(), ' Добавить баннер ')]", driver).click()
            Log.WriteLine("Добавть баннер")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[5]/input", driver).sendKeys("Тестовый баннер" + "---" + GetRandomString())
            Log.WriteLine("Название баннера")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[6]/input", driver).sendKeys("https://test.planeta.ru/Тест")
            Log.WriteLine("Адрес перехода")
            //
            DriverFindElement("(//*[@class='mat-select-arrow'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-select-content ng-trigger ng-trigger-fadeInContent'])[1]//mat-option[15]", driver).click() //
            Log.WriteLine("Тип баннера 15")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[3]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[4]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для десктопа")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для мобильных устройств")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@formcontrolname='mask']", driver).sendKeys("123")
            Log.WriteLine("Маска показа")
            //
            DriverFindElement("//*[@type='number']", driver).clear()
            //
            DriverFindElement("//*[@type='number']", driver).sendKeys("10000")
            Log.WriteLine("Вес баннера")
            //
            DriverFindElement("//*[@class='mat-raised-button mat-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@role='img']", driver).click()
            Log.WriteLine("Открыть меню баннера")
            //
            DriverFindElement("//*[contains(text(), 'Запустить')]", driver).click()
            Log.WriteLine("Запустить")
            //
            DriverFindElement("//*[contains(text(), 'Сохранить')]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun MenuBanner17(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Новая админка раздел Релкама Создать рекламную кампанию +   Главная благо нижняя перетяжка - 1170 x 80 " + VersionBrowser(driver))






            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            DriverFindElement("//*[contains(text(), 'Создать новую РК')]", driver).click()
            Log.WriteLine("Создать новую рекламную компанию")
            //
            DriverFindElement("(//*[@id='mat-input-1'])[1]", driver).sendKeys("Тестовая РК" + "---" + "Главная благо нижняя перетяжка - 1170 x 80")
            Log.WriteLine("Код рекламной кампании")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-trigger'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Статус запущена")
            //
            DriverFindElement("//*[contains(text(), ' Добавить баннер ')]", driver).click()
            Log.WriteLine("Добавть баннер")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[5]/input", driver).sendKeys("Тестовый баннер" + "---" + GetRandomString())
            Log.WriteLine("Название баннера")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[6]/input", driver).sendKeys("https://test.planeta.ru/Тест")
            Log.WriteLine("Адрес перехода")
            //
            DriverFindElement("(//*[@class='mat-select-arrow'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-select-content ng-trigger ng-trigger-fadeInContent'])[1]//mat-option[15]", driver).click() //
            Log.WriteLine("Тип баннера 15")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[3]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[4]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для десктопа")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для мобильных устройств")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@formcontrolname='mask']", driver).sendKeys("123")
            Log.WriteLine("Маска показа")
            //
            DriverFindElement("//*[@type='number']", driver).clear()
            //
            DriverFindElement("//*[@type='number']", driver).sendKeys("10000")
            Log.WriteLine("Вес баннера")
            //
            DriverFindElement("//*[@class='mat-raised-button mat-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@role='img']", driver).click()
            Log.WriteLine("Открыть меню баннера")
            //
            DriverFindElement("//*[contains(text(), 'Запустить')]", driver).click()
            Log.WriteLine("Запустить")
            //
            DriverFindElement("//*[contains(text(), 'Сохранить')]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun ReklamaStatistics(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Новая админка раздел Релкама Создать рекламную кампанию +   Главная благо нижняя перетяжка - 1170 x 80 " + VersionBrowser(driver))






            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            Thread.sleep(1500)
            //
            DriverFindElement("(//*[@class='mat-sort-header-button'])[1]", driver).click()
            Log.WriteLine("ID")
            //
            val element = DriverFindElement("//*[contains(text(), 'Тестовая РК---Верхняя перетяжка на главной - 1170 x 90')]", driver)
            val actions =  Actions(driver)
            actions.moveToElement(element).doubleClick()
            actions.perform()
            Log.WriteLine("Выбор баннера")
            //
            var text = DriverFindElement("(//*[@role='gridcell'])[2]", driver).text

            //
            DriverFindElement("//*[contains(text(), 'Статистика баннеров')]", driver).click()
            Log.WriteLine("Статистика  баннеров")
            //
            Thread.sleep(10000)
            //
            DriverFindElement("//*[contains(text(), ' Фильтры поиска ')]", driver).click()
            Log.WriteLine("Фильтр статистики")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[1]//input", driver).sendKeys(text)
            Log.WriteLine("Ввести название баннера")
            //
            DriverFindElement("(//*[@class='mat-raised-button mat-primary'])[1]", driver).click()
            Log.WriteLine("Применить")
            //

            fun Chrome() : WebDriver
            {
                val desiredcapabilites = DesiredCapabilities.chrome()
                desiredcapabilites.setCapability("enableVNC",true)
                val chrome = RemoteWebDriver(java.net.URL("http://192.168.10.42:4444/wd/hub"), desiredcapabilites)
                driver.manage().window().maximize()

                return chrome
            }

            val Chrome1 = Chrome()
            Thread.sleep(1000)

            //
            Chrome1.navigate().to(URL)
            //
            Chrome1.findElement(By.xpath("(//*[@class='wrap'])[1]")).click()
            //
            Chrome1.quit()

            var number = driver.findElement(By.xpath("//tbody//tr//td[4]")).text
            var value:Int = 1
            var Summa = number.toInt() + value
            var s:String = Summa.toString()

            //
            driver.findElement(By.xpath("(//*[@class='mat-raised-button mat-primary'])")).click()
            Log.WriteLine("Применить")
            //
            Thread.sleep(3000)
            //
            var new_number = driver.findElement(By.xpath("//tbody//tr//td[4]")).text
            val n = new_number


            driver.navigate().refresh()
            //
            Thread.sleep(5000)
            DriverFindElement("//*[contains(text(), ' Фильтры поиска ')]", driver).click()
            Log.WriteLine("Фильтр статистики")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[1]//input", driver).sendKeys(text)
            Log.WriteLine("Ввести название баннера")
            //
            DriverFindElement("(//*[@class='mat-raised-button mat-primary'])", driver).click()
            Log.WriteLine("Применить")
            //
            Thread.sleep(5000)

            println(n)
            println(s)

            if (s.toInt() < n.toInt())
                throw  Exception("Статистика не ведёться!")

            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }


    @Test
    fun CloneReklamaCompany(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Новая админка раздел Релкама Клонировать рекламную кампанию + архивирование" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            DriverFindElement("(//*[@class='mat-select-arrow-wrapper'])[2]", driver).click()
            Log.WriteLine("Выпадашка нижняя")
            //
            DriverFindElement("(//*[@class='mat-option-text'])[3]", driver).click()
            Log.WriteLine("100")
            //
            DriverFindElement("//tbody//tr[1]//td[6]//button", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("//*[contains(text(), 'Клонировать')]",driver).click()
            Log.WriteLine("Клонировать")
            //
//            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]", driver).click()
//            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[1]//td[1]", driver).click()
//            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]", driver).click()
            DriverFindElement("//*[@class='mat-calendar-next-button mat-icon-button']",driver).click()
            DriverFindElement("(//*[@class='mat-calendar-table'])[1]//tr[5]//td[4]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-trigger'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Статус запущена")
            //
            DriverFindElement("//*[contains(text(), 'Сохранить')]", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("//*[contains(text(), 'Рекламные кампании')]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            DriverFindElement("(//tbody)[1]//tr[19]//td[6]", driver).click()
            //
            DriverFindElement("//*[contains(text(), 'Архивировать')]", driver).click()
            Log.WriteLine("Архивировать")
            //
            DriverFindElement("//*[contains(text(), 'Подтвердить')]", driver).click()
            Log.WriteLine("Подтвердить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

}