import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.WebDriver
import java.nio.file.Files
import java.nio.file.Paths

class A_Banners : Setup() {

    fun NewAdminkaReklamaBannersCarusel(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Новая админка раздел Релкама Создать рекламную кампанию + Баннер карусель на главной" + VersionBrowser(driver))






            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            DriverFindElement("//*[contains(text(), 'Создать новую РК')]", driver).click()
            Log.WriteLine("Создать новую рекламную компанию")
            //
            DriverFindElement("(//*[@id='mat-input-1'])[1]", driver).sendKeys("Тестовая РК" + "---" + "Баннер карусель на главной")
            Log.WriteLine("Код рекламной кампании")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-trigger'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Статус запущена")
            //
            DriverFindElement("//*[contains(text(), ' Добавить баннер ')]", driver).click()
            Log.WriteLine("Добавть баннер")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[5]/input", driver).sendKeys("Тестовый баннер" + "---" + GetRandomString())
            Log.WriteLine("Название баннера")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[6]/input", driver).sendKeys("https://test.planeta.ru/Тест")
            Log.WriteLine("Адрес перехода")
            //
            DriverFindElement("(//*[@class='mat-select-arrow'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-select-content ng-trigger ng-trigger-fadeInContent'])[1]//mat-option[1]", driver).click()
            Log.WriteLine("Баннер в сдайдере на главной")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[3]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[4]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для десктопа")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для десктопа")

            val el3 = DriverFindElement("//*[@type='file']", driver)
            el3.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@formcontrolname='mask']", driver).sendKeys("123")
            Log.WriteLine("Маска показа")
            //
            DriverFindElement("//*[@type='number']", driver).clear()
            //
            DriverFindElement("//*[@type='number']", driver).sendKeys("10000")
            Log.WriteLine("Вес баннера")
            //
            HTMLBanner(driver)
            Log.WriteLine("Баннер HTML")
            //
            DriverFindElement("//*[@class='mat-raised-button mat-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@role='img']", driver).click()
            Log.WriteLine("Открыть меню баннера")
            //
            DriverFindElement("//*[contains(text(), 'Запустить')]", driver).click()
            Log.WriteLine("Запустить")
            //
            DriverFindElement("//*[contains(text(), 'Сохранить')]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }



    @Test
    fun MenuBanner2(){
        NewAdminkaReklamaBannersCarusel() // Цепочка т к если он запущен в паралели хост рвется и тест падает!

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Новая админка раздел Релкама Создать рекламную кампанию + Верхняя перетяжка на главной - 1170 x 90" + VersionBrowser(driver))






            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            DriverFindElement("//*[contains(text(), 'Создать новую РК')]", driver).click()
            Log.WriteLine("Создать новую рекламную компанию")
            //
            DriverFindElement("(//*[@id='mat-input-1'])[1]", driver).sendKeys("Тестовая РК" + "---" + "Верхняя перетяжка на главной - 1170 x 90")
            Log.WriteLine("Код рекламной кампании")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-trigger'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Статус запущена")
            //
            DriverFindElement("//*[contains(text(), ' Добавить баннер ')]", driver).click()
            Log.WriteLine("Добавть баннер")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[5]/input", driver).sendKeys("Тестовый баннер" + "---" + GetRandomString())
            Log.WriteLine("Название баннера")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[6]/input", driver).sendKeys("https://test.planeta.ru")
            Log.WriteLine("Адрес перехода")
            //
            DriverFindElement("(//*[@class='mat-select-arrow'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-select-content ng-trigger ng-trigger-fadeInContent'])[1]//mat-option[2]", driver).click() //
            Log.WriteLine("Верхняя перетяжка на главной - 1170 x 90")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[3]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[4]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для десктопа")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для мобильных устройств")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@formcontrolname='mask']", driver).sendKeys("123")
            Log.WriteLine("Маска показа")
            //
            DriverFindElement("//*[@type='number']", driver).clear()
            //
            DriverFindElement("//*[@type='number']", driver).sendKeys("10000")
            Log.WriteLine("Вес баннера")
            //
            DriverFindElement("//*[@class='mat-raised-button mat-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@role='img']", driver).click()
            Log.WriteLine("Открыть меню баннера")
            //
            DriverFindElement("//*[contains(text(), 'Запустить')]", driver).click()
            Log.WriteLine("Запустить")
            //
            DriverFindElement("//*[contains(text(), 'Сохранить')]", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverNavigate(URL,driver)
            //
            DriverFindElement("(//*[@class='advertise-block advertise-block__margin ng-star-inserted'])[1]", driver)
            Log.WriteLine("Проверка на наличие баннера")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }


    @Test
    fun MenuBanner3(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Новая админка раздел Релкама Создать рекламную кампанию +  Нижняя перетяжка на главной - 1170 x 90 " + VersionBrowser(driver))






            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            DriverFindElement("//*[contains(text(), 'Создать новую РК')]", driver).click()
            Log.WriteLine("Создать новую рекламную компанию")
            //
            DriverFindElement("(//*[@id='mat-input-1'])[1]", driver).sendKeys("Тестовая РК" + "---" + "Нижняя перетяжка на главной - 1170 x 90")
            Log.WriteLine("Код рекламной кампании")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-trigger'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Статус запущена")
            //
            DriverFindElement("//*[contains(text(), ' Добавить баннер ')]", driver).click()
            Log.WriteLine("Добавть баннер")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[5]/input", driver).sendKeys("Тестовый баннер" + "---" + GetRandomString())
            Log.WriteLine("Название баннера")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[6]/input", driver).sendKeys("https://test.planeta.ru/Тест")
            Log.WriteLine("Адрес перехода")
            //
            DriverFindElement("(//*[@class='mat-select-arrow'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-select-content ng-trigger ng-trigger-fadeInContent'])[1]//mat-option[3]", driver).click() //
            Log.WriteLine(" Нижняя перетяжка на главной - 1170 x 90 ")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[3]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[4]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для десктопа")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для мобильных устройств")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@formcontrolname='mask']", driver).sendKeys("123")
            Log.WriteLine("Маска показа")
            //
            DriverFindElement("//*[@type='number']", driver).clear()
            //
            DriverFindElement("//*[@type='number']", driver).sendKeys("10000")
            Log.WriteLine("Вес баннера")
            //
            DriverFindElement("//*[@class='mat-raised-button mat-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@role='img']", driver).click()
            Log.WriteLine("Открыть меню баннера")
            //
            DriverFindElement("//*[contains(text(), 'Запустить')]", driver).click()
            Log.WriteLine("Запустить")
            //
            DriverFindElement("//*[contains(text(), 'Сохранить')]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun MenuBanner4(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Новая админка раздел Релкама Создать рекламную кампанию +  Поиск проектов верхняя перетяжка - 870 x 150" + VersionBrowser(driver))






            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            DriverFindElement("//*[contains(text(), 'Создать новую РК')]", driver).click()
            Log.WriteLine("Создать новую рекламную компанию")
            //
            DriverFindElement("(//*[@id='mat-input-1'])[1]", driver).sendKeys("Тестовая РК" + "---" + "Поиск проектов верхняя перетяжка - 870 x 150")
            Log.WriteLine("Код рекламной кампании")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-trigger'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Статус запущена")
            //
            DriverFindElement("//*[contains(text(), ' Добавить баннер ')]", driver).click()
            Log.WriteLine("Добавть баннер")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[5]/input", driver).sendKeys("Тестовый баннер" + "---" + GetRandomString())
            Log.WriteLine("Название баннера")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[6]/input", driver).sendKeys("https://test.planeta.ru/Тест")
            Log.WriteLine("Адрес перехода")
            //
            DriverFindElement("(//*[@class='mat-select-arrow'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-select-content ng-trigger ng-trigger-fadeInContent'])[1]//mat-option[4]", driver).click() //
            Log.WriteLine(" Поиск проектов верхняя перетяжка - 870 x 150 ")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[3]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[4]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-arrow-wrapper'])[3]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Категория проекта")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для десктопа")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для мобильных устройств")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@formcontrolname='mask']", driver).sendKeys("123")
            Log.WriteLine("Маска показа")
            //
            DriverFindElement("//*[@type='number']", driver).clear()
            //
            DriverFindElement("//*[@type='number']", driver).sendKeys("10000")
            Log.WriteLine("Вес баннера")
            //
            DriverFindElement("//*[@class='mat-raised-button mat-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@role='img']", driver).click()
            Log.WriteLine("Открыть меню баннера")
            //
            DriverFindElement("//*[contains(text(), 'Запустить')]", driver).click()
            Log.WriteLine("Запустить")
            //
            DriverFindElement("//*[contains(text(), 'Сохранить')]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun H_MenuBanner5(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Новая админка раздел Релкама Создать рекламную кампанию +  Поиск проектов под категориями - 270 x 270" + VersionBrowser(driver))






            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            DriverFindElement("//*[contains(text(), 'Создать новую РК')]", driver).click()
            Log.WriteLine("Создать новую рекламную компанию")
            //
            DriverFindElement("(//*[@id='mat-input-1'])[1]", driver).sendKeys("Тестовая РК" + "---" + " Поиск проектов под категориями - 270 x 270 ")
            Log.WriteLine("Код рекламной кампании")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-trigger'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Статус запущена")
            //
            DriverFindElement("//*[contains(text(), ' Добавить баннер ')]", driver).click()
            Log.WriteLine("Добавть баннер")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[5]/input", driver).sendKeys("Тестовый баннер" + "---" + GetRandomString())
            Log.WriteLine("Название баннера")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[6]/input", driver).sendKeys("https://test.planeta.ru/Тест")
            Log.WriteLine("Адрес перехода")
            //
            DriverFindElement("(//*[@class='mat-select-arrow'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-select-content ng-trigger ng-trigger-fadeInContent'])[1]//mat-option[5]", driver).click() //
            Log.WriteLine("Тип баннера 5")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[3]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[4]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-arrow-wrapper'])[3]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Категория проекта")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для десктопа")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для мобильных устройств")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@formcontrolname='mask']", driver).sendKeys("123")
            Log.WriteLine("Маска показа")
            //
            DriverFindElement("//*[@type='number']", driver).clear()
            //
            DriverFindElement("//*[@type='number']", driver).sendKeys("10000")
            Log.WriteLine("Вес баннера")
            //
            DriverFindElement("//*[@class='mat-raised-button mat-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@role='img']", driver).click()
            Log.WriteLine("Открыть меню баннера")
            //
            DriverFindElement("//*[contains(text(), 'Запустить')]", driver).click()
            Log.WriteLine("Запустить")
            //
            DriverFindElement("//*[contains(text(), 'Сохранить')]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun MenuBAnner6(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Новая админка раздел Релкама Создать рекламную кампанию +   Страница проекта (для категории проекта) - 1170 x 80 " + VersionBrowser(driver))






            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            DriverFindElement("//*[contains(text(), 'Создать новую РК')]", driver).click()
            Log.WriteLine("Создать новую рекламную компанию")
            //
            DriverFindElement("(//*[@id='mat-input-1'])[1]", driver).sendKeys("Тестовая РК" + "---" + " Страница проекта (для категории проекта) - 1170 x 80 ")
            Log.WriteLine("Код рекламной кампании")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-trigger'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Статус запущена")
            //
            DriverFindElement("//*[contains(text(), ' Добавить баннер ')]", driver).click()
            Log.WriteLine("Добавть баннер")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[5]/input", driver).sendKeys("Тестовый баннер" + "---" + GetRandomString())
            Log.WriteLine("Название баннера")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[6]/input", driver).sendKeys("https://test.planeta.ru/Тест")
            Log.WriteLine("Адрес перехода")
            //
            DriverFindElement("(//*[@class='mat-select-arrow'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-select-content ng-trigger ng-trigger-fadeInContent'])[1]//mat-option[6]", driver).click() //
            Log.WriteLine("Тип баннера 6")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[3]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[4]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-arrow-wrapper'])[3]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[2]", driver).click()
            Log.WriteLine("Категория проекта Онлайн кампус")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для десктопа")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для мобильных устройств")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@formcontrolname='mask']", driver).sendKeys("123")
            Log.WriteLine("Маска показа")
            //
            DriverFindElement("//*[@type='number']", driver).clear()
            //
            DriverFindElement("//*[@type='number']", driver).sendKeys("10000")
            Log.WriteLine("Вес баннера")
            //
            DriverFindElement("//*[@class='mat-raised-button mat-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@role='img']", driver).click()
            Log.WriteLine("Открыть меню баннера")
            //
            DriverFindElement("//*[contains(text(), 'Запустить')]", driver).click()
            Log.WriteLine("Запустить")
            //
            DriverFindElement("//*[contains(text(), 'Сохранить')]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun MenuBanner7(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Новая админка раздел Релкама Создать рекламную кампанию +  Страница проекта (конкретный проект) - 1170 x 80" + VersionBrowser(driver))






            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            DriverFindElement("//*[contains(text(), 'Создать новую РК')]", driver).click()
            Log.WriteLine("Создать новую рекламную компанию")
            //
            DriverFindElement("(//*[@id='mat-input-1'])[1]", driver).sendKeys("Тестовая РК" + "---" + " Страница проекта (конкретный проект) - 1170 x 80")
            Log.WriteLine("Код рекламной кампании")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-trigger'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Статус запущена")
            //
            DriverFindElement("//*[contains(text(), ' Добавить баннер ')]", driver).click()
            Log.WriteLine("Добавть баннер")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[5]/input", driver).sendKeys("Тестовый баннер" + "---" + GetRandomString())
            Log.WriteLine("Название баннера")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[6]/input", driver).sendKeys("https://test.planeta.ru/Тест")
            Log.WriteLine("Адрес перехода")
            //
            DriverFindElement("(//*[@class='mat-select-arrow'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-select-content ng-trigger ng-trigger-fadeInContent'])[1]//mat-option[7]", driver).click() //
            Log.WriteLine("Тип баннера 7")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[3]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[4]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("//*[@formcontrolname='campaignId']", driver).sendKeys("123456")
            Log.WriteLine("Id Кампании")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для десктопа")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для мобильных устройств")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@formcontrolname='mask']", driver).sendKeys("123")
            Log.WriteLine("Маска показа")
            //
            DriverFindElement("//*[@type='number']", driver).clear()
            //
            DriverFindElement("//*[@type='number']", driver).sendKeys("10000")
            Log.WriteLine("Вес баннера")
            //
            DriverFindElement("//*[@class='mat-raised-button mat-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@role='img']", driver).click()
            Log.WriteLine("Открыть меню баннера")
            //
            DriverFindElement("//*[contains(text(), 'Запустить')]", driver).click()
            Log.WriteLine("Запустить")
            //
            DriverFindElement("//*[contains(text(), 'Сохранить')]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }


    @Test
    fun MenuBanner8(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Новая админка раздел Релкама Создать рекламную кампанию +  Стр. проекта под акциями (для категории проекта) - 340 x 340" + VersionBrowser(driver))






            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            DriverFindElement("//*[contains(text(), 'Создать новую РК')]", driver).click()
            Log.WriteLine("Создать новую рекламную компанию")
            //
            DriverFindElement("(//*[@id='mat-input-1'])[1]", driver).sendKeys("Тестовая РК" + "---" + " Стр. проекта под акциями (для категории проекта) - 340 x 340 ")
            Log.WriteLine("Код рекламной кампании")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-trigger'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Статус запущена")
            //
            DriverFindElement("//*[contains(text(), ' Добавить баннер ')]", driver).click()
            Log.WriteLine("Добавть баннер")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[5]/input", driver).sendKeys("Тестовый баннер" + "---" + GetRandomString())
            Log.WriteLine("Название баннера")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[6]/input", driver).sendKeys("https://test.planeta.ru/Тест")
            Log.WriteLine("Адрес перехода")
            //
            DriverFindElement("(//*[@class='mat-select-arrow'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-select-content ng-trigger ng-trigger-fadeInContent'])[1]//mat-option[8]", driver).click() //
            Log.WriteLine("Тип баннера 8")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[3]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[4]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-arrow-wrapper'])[3]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Категория проекта")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для десктопа")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для мобильных устройств")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@formcontrolname='mask']", driver).sendKeys("123")
            Log.WriteLine("Маска показа")
            //
            DriverFindElement("//*[@type='number']", driver).clear()
            //
            DriverFindElement("//*[@type='number']", driver).sendKeys("10000")
            Log.WriteLine("Вес баннера")
            //
            DriverFindElement("//*[@class='mat-raised-button mat-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@role='img']", driver).click()
            Log.WriteLine("Открыть меню баннера")
            //
            DriverFindElement("//*[contains(text(), 'Запустить')]", driver).click()
            Log.WriteLine("Запустить")
            //
            DriverFindElement("//*[contains(text(), 'Сохранить')]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun MenuBanner9(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Новая админка раздел Релкама Создать рекламную кампанию +  Стр. проекта под акциями (конкретный проект) - 340 x 340" + VersionBrowser(driver))






            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            DriverFindElement("//*[contains(text(), 'Создать новую РК')]", driver).click()
            Log.WriteLine("Создать новую рекламную компанию")
            //
            DriverFindElement("(//*[@id='mat-input-1'])[1]", driver).sendKeys("Тестовая РК" + "---" + "Стр. проекта под акциями (конкретный проект) - 340 x 340")
            Log.WriteLine("Код рекламной кампании")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-trigger'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Статус запущена")
            //
            DriverFindElement("//*[contains(text(), ' Добавить баннер ')]", driver).click()
            Log.WriteLine("Добавть баннер")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[5]/input", driver).sendKeys("Тестовый баннер" + "---" + GetRandomString())
            Log.WriteLine("Название баннера")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[6]/input", driver).sendKeys("https://test.planeta.ru/Тест")
            Log.WriteLine("Адрес перехода")
            //
            DriverFindElement("(//*[@class='mat-select-arrow'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-select-content ng-trigger ng-trigger-fadeInContent'])[1]//mat-option[9]", driver).click() //
            Log.WriteLine("Тип баннера 9")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[3]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[4]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("//*[@formcontrolname='campaignId']", driver).sendKeys("123456")
            Log.WriteLine("Id Кампании")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для десктопа")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для мобильных устройств")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@formcontrolname='mask']", driver).sendKeys("123")
            Log.WriteLine("Маска показа")
            //
            DriverFindElement("//*[@type='number']", driver).clear()
            //
            DriverFindElement("//*[@type='number']", driver).sendKeys("10000")
            Log.WriteLine("Вес баннера")
            //
            DriverFindElement("//*[@class='mat-raised-button mat-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@role='img']", driver).click()
            Log.WriteLine("Открыть меню баннера")
            //
            DriverFindElement("//*[contains(text(), 'Запустить')]", driver).click()
            Log.WriteLine("Запустить")
            //
            DriverFindElement("//*[contains(text(), 'Сохранить')]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }


    @Test
    fun MenuBanner10(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Новая админка раздел Релкама Создать рекламную кампанию +  Баннер на странице успешной оплаты" + VersionBrowser(driver))






            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            DriverFindElement("//*[contains(text(), 'Создать новую РК')]", driver).click()
            Log.WriteLine("Создать новую рекламную компанию")
            //
            DriverFindElement("(//*[@id='mat-input-1'])[1]", driver).sendKeys("Тестовая РК" + "---" + "Баннер на странице успешной оплаты")
            Log.WriteLine("Код рекламной кампании")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-trigger'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Статус запущена")
            //
            DriverFindElement("//*[contains(text(), ' Добавить баннер ')]", driver).click()
            Log.WriteLine("Добавть баннер")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[5]/input", driver).sendKeys("Тестовый баннер" + "---" + GetRandomString())
            Log.WriteLine("Название баннера")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[6]/input", driver).sendKeys("https://test.planeta.ru/Тест")
            Log.WriteLine("Адрес перехода")
            //
            DriverFindElement("(//*[@class='mat-select-arrow'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-select-content ng-trigger ng-trigger-fadeInContent'])[1]//mat-option[10]", driver).click() //
            Log.WriteLine("Тип баннера 10")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[3]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[4]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для десктопа")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для мобильных устройств")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@formcontrolname='mask']", driver).sendKeys("123")
            Log.WriteLine("Маска показа")
            //
            DriverFindElement("//*[@type='number']", driver).clear()
            //
            DriverFindElement("//*[@type='number']", driver).sendKeys("10000")
            Log.WriteLine("Вес баннера")
            //
            DriverFindElement("//*[@class='mat-raised-button mat-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@role='img']", driver).click()
            Log.WriteLine("Открыть меню баннера")
            //
            DriverFindElement("//*[contains(text(), 'Запустить')]", driver).click()
            Log.WriteLine("Запустить")
            //
            DriverFindElement("//*[contains(text(), 'Сохранить')]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }


    @Test
    fun MenuBanner11(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Новая админка раздел Релкама Создать рекламную кампанию +  Баннер на странице успешной оплаты для категории проекта" + VersionBrowser(driver))






            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            DriverFindElement("//*[contains(text(), 'Создать новую РК')]", driver).click()
            Log.WriteLine("Создать новую рекламную компанию")
            //
            DriverFindElement("(//*[@id='mat-input-1'])[1]", driver).sendKeys("Тестовая РК" + "---" + "Баннер на странице успешной оплаты для категории проекта")
            Log.WriteLine("Код рекламной кампании")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-trigger'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Статус запущена")
            //
            DriverFindElement("//*[contains(text(), ' Добавить баннер ')]", driver).click()
            Log.WriteLine("Добавть баннер")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[5]/input", driver).sendKeys("Тестовый баннер" + "---" + GetRandomString())
            Log.WriteLine("Название баннера")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[6]/input", driver).sendKeys("https://test.planeta.ru/Тест")
            Log.WriteLine("Адрес перехода")
            //
            DriverFindElement("(//*[@class='mat-select-arrow'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-select-content ng-trigger ng-trigger-fadeInContent'])[1]//mat-option[11]", driver).click() //
            Log.WriteLine("Тип баннера 11")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[3]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[4]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для десктопа")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для мобильных устройств")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='mat-select-arrow-wrapper'])[3]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Категория проекта")
            //
            DriverFindElement("//*[@formcontrolname='mask']", driver).sendKeys("123")
            Log.WriteLine("Маска показа")
            //
            DriverFindElement("//*[@type='number']", driver).clear()
            //
            DriverFindElement("//*[@type='number']", driver).sendKeys("10000")
            Log.WriteLine("Вес баннера")
            //
            DriverFindElement("//*[@class='mat-raised-button mat-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@role='img']", driver).click()
            Log.WriteLine("Открыть меню баннера")
            //
            DriverFindElement("//*[contains(text(), 'Запустить')]", driver).click()
            Log.WriteLine("Запустить")
            //
            DriverFindElement("//*[contains(text(), 'Сохранить')]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun MenuBanner12(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Новая админка раздел Релкама Создать рекламную кампанию +  Большой баннер на главной магазина - 880 x 400" + VersionBrowser(driver))






            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            DriverFindElement("//*[contains(text(), 'Создать новую РК')]", driver).click()
            Log.WriteLine("Создать новую рекламную компанию")
            //
            DriverFindElement("(//*[@id='mat-input-1'])[1]", driver).sendKeys("Тестовая РК" + "---" + " Большой баннер на главной магазина - 880 x 400")
            Log.WriteLine("Код рекламной кампании")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-trigger'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Статус запущена")
            //
            DriverFindElement("//*[contains(text(), ' Добавить баннер ')]", driver).click()
            Log.WriteLine("Добавть баннер")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[5]/input", driver).sendKeys("Тестовый баннер" + "---" + GetRandomString())
            Log.WriteLine("Название баннера")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[6]/input", driver).sendKeys("https://test.planeta.ru/Тест")
            Log.WriteLine("Адрес перехода")
            //
            DriverFindElement("(//*[@class='mat-select-arrow'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-select-content ng-trigger ng-trigger-fadeInContent'])[1]//mat-option[12]", driver).click() //
            Log.WriteLine("Тип баннера 12")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[3]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[4]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для десктопа")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для мобильных устройств")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@formcontrolname='mask']", driver).sendKeys("123")
            Log.WriteLine("Маска показа")
            //
            DriverFindElement("//*[@type='number']", driver).clear()
            //
            DriverFindElement("//*[@type='number']", driver).sendKeys("10000")
            Log.WriteLine("Вес баннера")
            //
            DriverFindElement("//*[@class='mat-raised-button mat-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@role='img']", driver).click()
            Log.WriteLine("Открыть меню баннера")
            //
            DriverFindElement("//*[contains(text(), 'Запустить')]", driver).click()
            Log.WriteLine("Запустить")
            //
            DriverFindElement("//*[contains(text(), 'Сохранить')]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun MenuBanner13(){
        try{
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Новая админка раздел Релкама Создать рекламную кампанию +   Малый баннер на главной магазина - 280 x 340" + VersionBrowser(driver))






            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            DriverFindElement("//*[contains(text(), 'Создать новую РК')]", driver).click()
            Log.WriteLine("Создать новую рекламную компанию")
            //
            DriverFindElement("(//*[@id='mat-input-1'])[1]", driver).sendKeys("Тестовая РК" + "---" + " Малый баннер на главной магазина - 280 x 340")
            Log.WriteLine("Код рекламной кампании")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-trigger'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Статус запущена")
            //
            DriverFindElement("//*[contains(text(), ' Добавить баннер ')]", driver).click()
            Log.WriteLine("Добавть баннер")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[5]/input", driver).sendKeys("Тестовый баннер" + "---" + GetRandomString())
            Log.WriteLine("Название баннера")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[6]/input", driver).sendKeys("https://test.planeta.ru/Тест")
            Log.WriteLine("Адрес перехода")
            //
            DriverFindElement("(//*[@class='mat-select-arrow'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-select-content ng-trigger ng-trigger-fadeInContent'])[1]//mat-option[13]", driver).click() //
            Log.WriteLine("Тип баннера 13")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[3]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[4]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для десктопа")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для мобильных устройств")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@formcontrolname='mask']", driver).sendKeys("123")
            Log.WriteLine("Маска показа")
            //
            DriverFindElement("//*[@type='number']", driver).clear()
            //
            DriverFindElement("//*[@type='number']", driver).sendKeys("10000")
            Log.WriteLine("Вес баннера")
            //
            DriverFindElement("//*[@class='mat-raised-button mat-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@role='img']", driver).click()
            Log.WriteLine("Открыть меню баннера")
            //
            DriverFindElement("//*[contains(text(), 'Запустить')]", driver).click()
            Log.WriteLine("Запустить")
            //
            DriverFindElement("//*[contains(text(), 'Сохранить')]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun MenuBanner14(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Новая админка раздел Релкама Создать рекламную кампанию +   Страница поиска товаров магазина - 880 x 170" + VersionBrowser(driver))






            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            DriverFindElement("//*[contains(text(), 'Создать новую РК')]", driver).click()
            Log.WriteLine("Создать новую рекламную компанию")
            //
            DriverFindElement("(//*[@id='mat-input-1'])[1]", driver).sendKeys("Тестовая РК" + "---" + "Страница поиска товаров магазина - 880 x 170")
            Log.WriteLine("Код рекламной кампании")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-trigger'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Статус запущена")
            //
            DriverFindElement("//*[contains(text(), ' Добавить баннер ')]", driver).click()
            Log.WriteLine("Добавть баннер")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[5]/input", driver).sendKeys("Тестовый баннер" + "---" + GetRandomString())
            Log.WriteLine("Название баннера")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[6]/input", driver).sendKeys("https://test.planeta.ru/Тест")
            Log.WriteLine("Адрес перехода")
            //
            DriverFindElement("(//*[@class='mat-select-arrow'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-select-content ng-trigger ng-trigger-fadeInContent'])[1]//mat-option[14]", driver).click() //
            Log.WriteLine("Тип баннера 14")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[3]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[4]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для десктопа")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для мобильных устройств")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@formcontrolname='mask']", driver).sendKeys("123")
            Log.WriteLine("Маска показа")
            //
            DriverFindElement("//*[@type='number']", driver).clear()
            //
            DriverFindElement("//*[@type='number']", driver).sendKeys("10000")
            Log.WriteLine("Вес баннера")
            //
            DriverFindElement("//*[@class='mat-raised-button mat-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@role='img']", driver).click()
            Log.WriteLine("Открыть меню баннера")
            //
            DriverFindElement("//*[contains(text(), 'Запустить')]", driver).click()
            Log.WriteLine("Запустить")
            //
            DriverFindElement("//*[contains(text(), 'Сохранить')]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun MenuBanner15(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Новая админка раздел Релкама Создать рекламную кампанию +   Главная благо под боковыми фильтрами - 280 x 280" + VersionBrowser(driver))






            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("//*[@id='side-menu']/li[10]", driver).click()
            Log.WriteLine("Реклама")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Рекламные кампании")
            //
            DriverFindElement("//*[contains(text(), 'Создать новую РК')]", driver).click()
            Log.WriteLine("Создать новую рекламную компанию")
            //
            DriverFindElement("(//*[@id='mat-input-1'])[1]", driver).sendKeys("Тестовая РК" + "---" + "Главная благо под боковыми фильтрами - 280 x 280")
            Log.WriteLine("Код рекламной кампании")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='mat-select-trigger'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-option-text'])[1]", driver).click()
            Log.WriteLine("Статус запущена")
            //
            DriverFindElement("//*[contains(text(), ' Добавить баннер ')]", driver).click()
            Log.WriteLine("Добавть баннер")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[5]/input", driver).sendKeys("Тестовый баннер" + "---" + GetRandomString())
            Log.WriteLine("Название баннера")
            //
            DriverFindElement("(//*[@class='mat-form-field-infix'])[6]/input", driver).sendKeys("https://test.planeta.ru/Тест")
            Log.WriteLine("Адрес перехода")
            //
            DriverFindElement("(//*[@class='mat-select-arrow'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-select-content ng-trigger ng-trigger-fadeInContent'])[1]//mat-option[15]", driver).click() //
            Log.WriteLine("Тип баннера 15")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[3]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата начала")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[4]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[7]", driver).click()
            Log.WriteLine("Дата Окончания")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для десктопа")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("(//*[@class='image-area_choose ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Изображение для мобильных устройств")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            //
            DriverFindElement("//*[@class='image-uploader_footer ng-star-inserted']/button[2]", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@formcontrolname='mask']", driver).sendKeys("123")
            Log.WriteLine("Маска показа")
            //
            DriverFindElement("//*[@type='number']", driver).clear()
            //
            DriverFindElement("//*[@type='number']", driver).sendKeys("10000")
            Log.WriteLine("Вес баннера")
            //
            DriverFindElement("//*[@class='mat-raised-button mat-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@role='img']", driver).click()
            Log.WriteLine("Открыть меню баннера")
            //
            DriverFindElement("//*[contains(text(), 'Запустить')]", driver).click()
            Log.WriteLine("Запустить")
            //
            DriverFindElement("//*[contains(text(), 'Сохранить')]", driver).click()
            Log.WriteLine("Сохранить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }




}