import A9_ParallelTest.RunOplataParallel
import org.junit.Test
import org.junit.experimental.ParallelComputer
import org.junit.runner.JUnitCore

class D_StartParallelTest {

    @Test
    fun start(){
        val cls = arrayOf( //Паралельные тесты админки и юзера  смесь
                RunAnonymousParallel::class.java,
                RunAvtorizationServiseParallel::class.java,
                RunBiblioRodina::class.java,
                RunOplataParallel::class.java,
                RunRazdelyUserOrdersParallel::class.java,
                RunSchool::class.java,
                RunShop::class.java,
                RunTestElement::class.java,
                RunTestElementUserMenu::class.java,
                RunTestMailer::class.java,
                A_RunAvtorizationMenu::class.java,
                B_RunAdminkaModeration::class.java,
                `C_RunAdminkaModerationProject+KontrAgent+ProjectOrders+ProjectZakaziInvesting`::class.java,
                G_RunAdminkaShop::class.java)
        JUnitCore.runClasses(ParallelComputer.methods(), *cls)
    }
}