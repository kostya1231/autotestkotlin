
import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.WebDriver
import java.nio.file.Files
import java.nio.file.Paths

class BiblioRodina1 : SetupParallel() {

    @Test
    fun Avtorization(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\BiblioRodina")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\BiblioRodina\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()






            Log.WriteLine("User-Авторизация" + VersionBrowser(driver))






            Avtorization_Uz4name_BiblioRodina(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Ок")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }


    @Test
    fun SendAnApplicationIzdanie(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\BiblioRodina")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\BiblioRodina\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()





            Log.WriteLine("Отправить заявку на участие-Издание" + VersionBrowser(driver))






            Avtorization_Uz4name_BiblioRodina(driver, Log)
            //
            DriverFindElement("//*[@id='send-request']", driver).click()
            Log.WriteLine("Отправить заявку на участие")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-lg btn-block js-open-request-form'])[1]", driver).click()
            Log.WriteLine("Издание")
            //
            DriverFindElement("(//*[@class='biblio-request']//*[@name='title'])[1]", driver).sendKeys("Тест" + GetRandomString())
            Log.WriteLine("Название издания")
            //
            DriverFindElement("(//*[@class='biblio-request']//*[@name='contact'])[1]", driver).sendKeys("Тест")
            Log.WriteLine("Контактное лицо")
            //
            DriverFindElement("(//*[@class='biblio-request']//*[@name='site'])[1]", driver).sendKeys("Тест")
            Log.WriteLine("Сайт издания")
            //
            DriverFindElement("(//*[@class='biblio-request']//*[@name='email'])[1]", driver).sendKeys("123@yandex.ru")
            Log.WriteLine("E-Mail для связи")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-lg btn-block'])", driver).click()
            Log.WriteLine("Отправить")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-lg btn-block'])[2]", driver).click()
            Log.WriteLine("Закрыть")
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e:Exception){
            println(e.printStackTrace())
        }
        finally
        {
            Stop1()
        }
    }

    @Test
    fun SendAnApplicationIzdanieNoAvtorization(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\BiblioRodina")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\BiblioRodina\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()





            Log.WriteLine("Отправить заявку на участие-Издание не авторизованным" + VersionBrowser(driver))






            DriverNavigate(BiblioRodina,driver)
            //
            DriverFindElement("//*[@id='send-request']", driver).click()
            Log.WriteLine("Отправить заявку на участие")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-lg btn-block js-open-request-form'])[1]", driver).click()
            Log.WriteLine("Издание")
            //
            DriverFindElement("(//*[@class='biblio-request']//*[@name='title'])[1]", driver).sendKeys("Тест" + GetRandomString())
            Log.WriteLine("Название издания")
            //
            DriverFindElement("(//*[@class='biblio-request']//*[@name='contact'])[1]", driver).sendKeys("Тест")
            Log.WriteLine("Контактное лицо")
            //
            DriverFindElement("(//*[@class='biblio-request']//*[@name='site'])[1]", driver).sendKeys("Тест")
            Log.WriteLine("Сайт издания")
            //
            DriverFindElement("(//*[@class='biblio-request']//*[@name='email'])[1]", driver).sendKeys("123@yandex.ru")
            Log.WriteLine("E-Mail для связи")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-lg btn-block'])", driver).click()
            Log.WriteLine("Отправить")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-lg btn-block'])[2]", driver).click()
            Log.WriteLine("Закрыть")
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e:Exception){
            println(e.printStackTrace())
        }
        finally
        {
            Stop1()
        }
    }


    @Test
    fun SendAnApplicationBiblioteka1(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\BiblioRodina")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\BiblioRodina\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Отправить заявку на участие-Библиотека-Публичные библиотеки" + VersionBrowser(driver))





            Avtorization_Uz4name_BiblioRodina(driver, Log)
            //
            DriverFindElement("//*[@id='send-request']", driver).click()
            Log.WriteLine("Отправить заявку на участие")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-lg btn-block js-open-request-form'])[2]", driver).click()
            Log.WriteLine("Библиотека")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[1]", driver).sendKeys("Test" + GetRandomString())
            Log.WriteLine("Название организации")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[2]", driver).sendKeys("Рашка Парашка")
            Log.WriteLine("Почтовый адрес")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[3]", driver).sendKeys("Рашка Парашка")
            Log.WriteLine("Почтовый адрес")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[4]", driver).sendKeys("Путиноид")
            Log.WriteLine("Контактное лицо")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[5]", driver).sendKeys("123@yandex.ru")
            Log.WriteLine("E-mail организации")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[6]", driver).sendKeys("https://123.com")
            Log.WriteLine("Сайт организации")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-lg btn-block'])", driver).click()
            Log.WriteLine("Отправить")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-lg btn-block'])[2]", driver).click()
            Log.WriteLine("Закрыть")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }


    @Test
    fun SendAn_ApplicationBiblioteka2(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\BiblioRodina")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\BiblioRodina\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()





            Log.WriteLine("Отправить заявку на участие-Библиотека-Школьные библиотеки" + VersionBrowser(driver))





            Avtorization_Uz4name_BiblioRodina(driver, Log)
            //
            DriverFindElement("//*[@id='send-request']", driver).click()
            Log.WriteLine("Отправить заявку на участие")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-lg btn-block js-open-request-form'])[2]", driver).click()
            Log.WriteLine("Библиотека")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[1]", driver).sendKeys("Test" + GetRandomString())
            Log.WriteLine("Название организации")
            //
            DriverFindElement("//*[@class='select-btn']", driver).click()
            //
            DriverFindElement("//*[@class='dropdown-menu']/li[2]", driver).click()
            Log.WriteLine("Тип организации")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[2]", driver).sendKeys("Рашка Парашка")
            Log.WriteLine("Почтовый адрес")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[3]", driver).sendKeys("Рашка Парашка")
            Log.WriteLine("Почтовый адрес")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[4]", driver).sendKeys("Путиноид")
            Log.WriteLine("Контактное лицо")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[5]", driver).sendKeys("123@yandex.ru")
            Log.WriteLine("E-mail организации")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[6]", driver).sendKeys("https://123.com")
            Log.WriteLine("Сайт организации")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-lg btn-block'])", driver).click()
            Log.WriteLine("Отправить")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-lg btn-block'])[2]", driver).click()
            Log.WriteLine("Закрыть")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun SendAnApplicationBiblioteka3(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\BiblioRodina")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\BiblioRodina\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Отправить заявку на участие-Библиотека-Научные библиотеки" + VersionBrowser(driver))






            Avtorization_Uz4name_BiblioRodina(driver, Log)
            //
            DriverFindElement("//*[@id='send-request']", driver).click()
            Log.WriteLine("Отправить заявку на участие")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-lg btn-block js-open-request-form'])[2]", driver).click()
            Log.WriteLine("Библиотека")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[1]", driver).sendKeys("Test" + GetRandomString())
            Log.WriteLine("Название организации")
            //
            DriverFindElement("//*[@class='select-btn']", driver).click()
            //
            DriverFindElement("//*[@class='dropdown-menu']/li[3]", driver).click()
            Log.WriteLine("Тип организации")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[2]", driver).sendKeys("Рашка Парашка")
            Log.WriteLine("Почтовый адрес")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[3]", driver).sendKeys("Рашка Парашка")
            Log.WriteLine("Почтовый адрес")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[4]", driver).sendKeys("Путиноид")
            Log.WriteLine("Контактное лицо")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[5]", driver).sendKeys("123@yandex.ru")
            Log.WriteLine("E-mail организации")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[6]", driver).sendKeys("https://123.com")
            Log.WriteLine("Сайт организации")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-lg btn-block'])", driver).click()
            Log.WriteLine("Отправить")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-lg btn-block'])[2]", driver).click()
            Log.WriteLine("Закрыть")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        finally {
            Stop1()
        }

    }


    @Test
    fun SendAnapplicationBiblioteka4(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\BiblioRodina")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\BiblioRodina\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Отправить заявку на участие-Библиотека-Детские семейные библиотеки" + VersionBrowser(driver))





            Avtorization_Uz4name_BiblioRodina(driver, Log)
            //
            DriverFindElement("//*[@id='send-request']", driver).click()
            Log.WriteLine("Отправить заявку на участие")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-lg btn-block js-open-request-form'])[2]", driver).click()
            Log.WriteLine("Библиотека")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[1]", driver).sendKeys("Test" + GetRandomString())
            Log.WriteLine("Название организации")
            //
            DriverFindElement("//*[@class='select-btn']", driver).click()
            //
            DriverFindElement("//*[@class='dropdown-menu']/li[4]", driver).click()
            Log.WriteLine("Тип организации")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[2]", driver).sendKeys("Рашка Парашка")
            Log.WriteLine("Почтовый адрес")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[3]", driver).sendKeys("Рашка Парашка")
            Log.WriteLine("Почтовый адрес")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[4]", driver).sendKeys("Путиноид")
            Log.WriteLine("Контактное лицо")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[5]", driver).sendKeys("123@yandex.ru")
            Log.WriteLine("E-mail организации")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[6]", driver).sendKeys("https://123.com")
            Log.WriteLine("Сайт организации")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-lg btn-block'])", driver).click()
            Log.WriteLine("Отправить")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-lg btn-block'])[2]", driver).click()
            Log.WriteLine("Закрыть")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun SendAnApplicationBiblioteka5(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\BiblioRodina")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\BiblioRodina\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("Отправить заявку на участие-Библиотека-Специализированные библиотеки" + VersionBrowser(driver))






            Avtorization_Uz4name_BiblioRodina(driver, Log)
            //
            DriverFindElement("//*[@id='send-request']", driver).click()
            Log.WriteLine("Отправить заявку на участие")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-lg btn-block js-open-request-form'])[2]", driver).click()
            Log.WriteLine("Библиотека")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[1]", driver).sendKeys("Test" + GetRandomString())
            Log.WriteLine("Название организации")
            //
            DriverFindElement("//*[@class='select-btn']", driver).click()
            //
            DriverFindElement("//*[@class='dropdown-menu']/li[5]", driver).click()
            Log.WriteLine("Тип организации")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[2]", driver).sendKeys("Рашка Парашка")
            Log.WriteLine("Почтовый адрес")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[3]", driver).sendKeys("Рашка Парашка")
            Log.WriteLine("Почтовый адрес")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[4]", driver).sendKeys("Путиноид")
            Log.WriteLine("Контактное лицо")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[5]", driver).sendKeys("123@yandex.ru")
            Log.WriteLine("E-mail организации")
            //
            DriverFindElement("(//*[@class='form-control control-lg js-form'])[6]", driver).sendKeys("https://123.com")
            Log.WriteLine("Сайт организации")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-lg btn-block'])", driver).click()
            Log.WriteLine("Отправить")
            //
            DriverFindElement("(//*[@class='btn btn-primary btn-lg btn-block'])[2]", driver).click()
            Log.WriteLine("Закрыть")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        finally {
            Stop1()
        }
    }




}