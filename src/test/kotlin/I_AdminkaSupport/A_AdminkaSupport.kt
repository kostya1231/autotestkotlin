import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.WebDriver
import java.nio.file.Files
import java.nio.file.Paths

class A_AdminkaSupport : Setup() {

    @Test
    fun Payments(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка-Поддержка-платежи" + VersionBrowser(driver))





            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).clear()
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина админа")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[8]", driver).click()
            Log.WriteLine("Поддержка")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[1]", driver).click()
            Log.WriteLine("Платежи")
            //
            DriverFindElement("(//*[@class='form-control'])[1]", driver).sendKeys("892816")
            Log.WriteLine("id юзера")
            //
            DriverFindElement("(//*[@id='search'])[1]", driver).click()
            Log.WriteLine("Поиск")
            //
            for (i in 1..26) {
                DriverFindElement("//*[@id='projectType']//option[" + i.toString() + "]", driver).click()
                //
                DriverFindElement("(//*[@id='search'])[1]", driver).click()
            }
            //
            Log.WriteLine("Перебор Проектов")
            //
            DriverFindElement("//*[@id='projectType']//option[1]", driver).click()
            //
            DriverFindElement("(//*[@id='search'])[1]", driver).click()
            Log.WriteLine("Поиск")
            //
            for (i in 1..7) {
                DriverFindElement("//*[@id='transactionStatus']//option[" + i.toString() + "]", driver).click()
                //
                DriverFindElement("(//*[@id='search'])[1]", driver).click()
            }
            //
            Log.WriteLine("Перебор статусов")
            //
            DriverFindElement("//*[@id='transactionStatus']//option[1]", driver).click()
            Log.WriteLine("Все")
            //
            DriverFindElement("(//*[@id='search'])[1]", driver).click()
            Log.WriteLine("Поиск")
            //
            for (i in 1..29) {
                DriverFindElement("//*[@id='paymentType']//option[" + i.toString() + "]", driver).click()
                //
                DriverFindElement("(//*[@id='search'])[1]", driver).click()
            }
            //
            Log.WriteLine("Перебор Платежный способ")
            //
            DriverFindElement("//*[@id='paymentType']//option[1]", driver).click()
            Log.WriteLine("Всё")
            //
            DriverFindElement("(//*[@id='search'])[1]", driver).click()
            Log.WriteLine("Поиск")
            //
            for (i in 1..15) {
                DriverFindElement("//*[@name='paymentProviderId']//option[" + i.toString() + "]", driver).click()
                //
                DriverFindElement("(//*[@id='search'])[1]", driver).click()
            }
            Log.WriteLine("Перебор Платёжная система")
            //
            DriverFindElement("//*[@name='paymentProviderId']//option[1]", driver).click()
            //
            DriverFindElement("(//*[@id='search'])[1]", driver).click()
            Log.WriteLine("Поиск")
            //
            for (i in 1..3) {
                DriverFindElement("//*[@id='transactionType']//option[" + i.toString() + "]", driver).click()
                //
                DriverFindElement("(//*[@id='search'])[1]", driver).click()
            }
            //
            DriverFindElement("//*[@id='transactionType']//option[1]", driver).click()
            Log.WriteLine("Все")
            //
            DriverFindElement("(//*[@id='search'])[1]", driver).click()
            Log.WriteLine("Поиск")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }


    @Test
    fun PaymentError(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка-Поддержка-платежи-платёжные ошибки" + VersionBrowser(driver))





            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).clear()
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина админа")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[8]", driver).click()
            Log.WriteLine("Поддержка")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[2]", driver).click()
            Log.WriteLine("Платёжные ошибки")
            //
            DriverFindElement("//*[@id='query']", driver).sendKeys("857066")
            Log.WriteLine("Ввод юзера")
            //
            DriverFindElement("//*[@id='date-box-from']", driver).clear()
            //
            DriverFindElement("//*[@id='date-box-from']", driver).sendKeys("01012010")
            Log.WriteLine("Ввод предыдущей даты")
            //
            DriverFindElement("(//*[@id='search'])[1]", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@id='transactionId']", driver).sendKeys("1023838")
            Log.WriteLine("id транзакции")
            //
            DriverFindElement("(//*[@id='search'])[1]", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@id='transactionId']", driver).clear()
            //
            DriverFindElement("(//*[@id='search'])[1]", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@id='orderId']", driver).sendKeys("1009066")
            Log.WriteLine("id заказа")
            //
            DriverFindElement("(//*[@id='search'])[1]", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@id='orderId']", driver).clear()
            //
            DriverFindElement("(//*[@id='search'])[1]", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@id='campaignId']", driver).sendKeys("94669")
            Log.WriteLine("id проекта")
            //
            DriverFindElement("(//*[@id='search'])[1]", driver).click()
            Log.WriteLine("Поиск")
            //
            Log.Close()
            Stop()
            driver.quit()
        } catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }


    @Test
    fun ApplicationForRefund(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка-Поддержка-платежи-заявления на возврат" + VersionBrowser(driver))





            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).clear()
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина админа")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[8]", driver).click()
            Log.WriteLine("Поддержка")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[3]", driver).click()
            Log.WriteLine("Платёжные ошибки")
            //
            Thread.sleep(8000)
            DriverFindElement("//*[contains(text(), ' Фильтры поиска ')]", driver).click()
            Log.WriteLine("Фильтр поиска")
            //
            DriverFindElement("//*[@formcontrolname='email']", driver).sendKeys("uz2name@yandex.ru")
            Log.WriteLine("Id")
            //
            DriverFindElement("//*[contains(text(), 'Поиск')]", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("(//*[@role='img'])[1]", driver).click()
            Log.WriteLine("Выбрать письмо")
            //
            DriverFindElement("(//*[@role='menuitem'])[1]//i", driver).click()
            Log.WriteLine("Отправить письмо ещё раз")
            //
            DriverFindElement("//*[contains(text(), ' Письмо успешно отправлено ')]", driver)
            Log.WriteLine("Письмо успешно отправлено")
            ////
            //DriverFindElement("(//*[@role='img'])[1]", driver).Click();
            //Log.WriteLine("Выбрать письмо");
            ////
            //DriverFindElement("(//*[@role='menuitem'])[1]//i[2]", driver).Click();
            //Log.WriteLine("Просмотр заявлния");
            ////
            //DriverFindElement("(//*[@role='img'])[1]", driver).Click();
            //Log.WriteLine("Выбрать письмо");
            ////
            //DriverFindElement("(//*[@role='menuitem'])[1]//i[1]", driver).Click();
            //Log.WriteLine("Обработан");
            //
            for (i in 1..4) {
                DriverFindElement("(//*[@class='mat-select-trigger'])[1]", driver).click()
                //
                DriverFindElement("(//*[@class='cdk-overlay-pane'])[1]//mat-option[" + i.toString() + "]", driver).click()
                //
                DriverFindElement("//*[contains(text(), 'Поиск')]", driver).click()
            }
            Log.WriteLine("Пересчёт статусов")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-table'])[1]//tr[2]//td[2]", driver).click()
            Log.WriteLine("Дата добавления от")
            //
            DriverFindElement("(//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]", driver).click()
            //
            DriverFindElement("(//*[@class='mat-calendar-body'])[1]//tr[5]//td[5]", driver).click()
            Log.WriteLine("Дата добавления до")
            //
            DriverFindElement("//*[contains(text(), 'Поиск')]", driver).click()
            Log.WriteLine("Поиск")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun CallReqest(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка-Поддержка-платежи-заявки на обратный звонок" + VersionBrowser(driver))





            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).clear()
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина админа")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[8]", driver).click()
            Log.WriteLine("Поддержка")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[5]", driver).click()
            Log.WriteLine("Заявки на обратный звонок")
            //
            for (i in 1..3) {
                DriverFindElement("//*[@id='processed']//option[" + i.toString() + "]", driver).click()
                //
                DriverFindElement("(//*[@id='search'])[1]", driver).click()
            }
            //
            DriverFindElement("//*[@id='processed']//option[1]", driver).click()
            //
            DriverFindElement("(//*[@id='search'])[1]", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@id='searchStr']", driver).sendKeys("831771")
            Log.WriteLine("Ввод id юзера")
            //
            DriverFindElement("(//*[@id='search'])[1]", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='label label-success']", driver)
            Log.WriteLine("Обработана")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }


    @Test
    fun Mail(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка-Поддержка-платежи-письма" + VersionBrowser(driver))






            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).clear()
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина админа")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[8]", driver).click()
            Log.WriteLine("Поддержка")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[4]", driver).click()
            Log.WriteLine("Отправленные письма")
            //
            Thread.sleep(10000)
            DriverFindElement("//*[contains(text(), ' Фильтры поиска ')]", driver).click()
            Log.WriteLine("Фильтр поиска")
            //
            for (i in 1..6) {
                Thread.sleep(1500)
                DriverFindElement("(//*[@class='mat-select-arrow'])[1]", driver).click()
                //
                DriverFindElement("(//*[@class='mat-select-content ng-trigger ng-trigger-fadeInContent'])[1]//mat-option[" + i.toString() + "]", driver).click()
                //
                DriverFindElement("//*[contains(text(), 'Поиск')]", driver).click()
            }
            Log.WriteLine("Перебор статусов")
            //
            driver.navigate().refresh()
            Thread.sleep(2000)
            val text = DriverFindElement("//tbody//tr[1]//td[1]", driver).text
            //
            DriverFindElement("//*[contains(text(), ' Фильтры поиска ')]", driver).click()
            Log.WriteLine("Фильтр поиска")
            //
            DriverFindElement("//*[@placeholder='Введите тему письма или id']", driver).sendKeys(text)
            Log.WriteLine("Ввод id")
            //
            DriverFindElement("//*[contains(text(), 'Поиск')]", driver).click()
            Log.WriteLine("Поиск")
            //
            Thread.sleep(3500)
            DriverFindElement("//tbody//tr[1]//td[1]", driver).click()
            Log.WriteLine("Открыть карточку с письмом")
            //
            Thread.sleep(500)
            DriverFindElement("//*[contains(text(), 'Сбросить')]", driver).click()
            Log.WriteLine("Сбросить")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }






}