import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import java.nio.file.Files
import java.nio.file.Paths

class A_Refund : SetupParallel() {

    @Test
    fun A_Balance(){
         try{
             TestName = GetCurrentMethod()
             val folder = Paths.get("C:\\Log Test\\Refund")
             Files.createDirectories(folder)
             val currentFile = "C:\\Log Test\\Refund\\$TestName"
             val Log = Loger(currentFile, ".txt")
             val driver: WebDriver = Driver()




             Log.WriteLine("User-Баланс-Возврат баланса через заполнение заявления + Списание со счёта" + VersionBrowser(driver))





             Avtorization_Uz2name(driver,Log)
             //
             DriverFindElement("//*[@class='h-user_img']", driver).click()
             Log.WriteLine("Открыть меню")
             //
             DriverFindElement("(//*[@class='h-user-menu_name'])[4]", driver).click()
             Log.WriteLine("Баланс")
             //
             DriverFindElement("//*[@class='button button__def button__sm']", driver).click()
             Log.WriteLine("Пополнить баланс")
             //
             DriverFindElement("//*[@id='core.total-amount']", driver).clear()
             Log.WriteLine("Очистить поле")
             //
             DriverFindElement("//*[@id='core.total-amount']", driver).sendKeys("1000")
             Log.WriteLine("Ввод в поле значения 1000")
             //
             DriverFindElement("(//*[@class='project-payment-choice_name'])[2]",driver).click()
             Log.WriteLine("Клауд")
             //
             DriverFindElement("//*[@class='btn btn-nd-primary project-payment_btn']", driver).click()
             Log.WriteLine("Перейти к оплате")
             //
             fun FindEl() : Boolean{
                 return try{
                     DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver)
                     //Согласен на условия
                     true
                 }catch (e:Exception){
                     false
                 }
             }

             for(i in 0..60){  //Клауд
                 if(FindEl()){

                     DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
                     Log.WriteLine("Оплата картой")
                     //
                     driver.switchTo().frame(0)
                     DriverFindElement("(//*[@class='row'])[1]//input",driver).click()
                     Log.WriteLine("клик по строке")
                     DriverFindElement("(//*[@class='row'])[1]//input",driver).sendKeys("4111 1111 1111 1111")
                     //
                     DriverFindElement("(//*[@id='inputMonth'])[1]",driver).sendKeys("12")
                     //
                     DriverFindElement("(//*[@id='inputYear'])[1]",driver).sendKeys("19")
                     //
                     DriverFindElement("(//*[@data-mask='000'])[1]",driver).sendKeys("123")
                     //
                     DriverFindElement("(//*[@id='cardHolder'])[1]",driver).sendKeys("QWERTY")
                     //
                     DriverFindElement("(//*[@class='button'])[1]",driver).click()
                     DriverFindElement("(//*[@class='button'])[2]",driver).click()
                     Log.WriteLine("OK")
                     driver.switchTo().defaultContent()
                     break
                 }
             }
//             DriverFindElement("//*[@class='btn btn-nd-primary project-payment_btn']", driver).click()
//             Log.WriteLine("Перейти к оплате Сбер")
//             //
//             //DriverFindElement("(//*[@id='email'])[1]", driver).SendKeys("pain-net@yandex.ru");
//             //Log.WriteLine("Сбер ввод email");
//             ////
//             DriverFindElement("(//*[@id='cardnumber'])[1]", driver).sendKeys("4111 1111 1111 1111")
//             Log.WriteLine("Ваш банк ввод номера карты")
//             //
//             DriverFindElement("(//*[@name='expdate'])[1]", driver).sendKeys("1219")
//             Log.WriteLine("Ваш банк ввод месяц год")
//             //
//             DriverFindElement("(//*[@id='cvc'])[1]", driver).sendKeys("123")
//             Log.WriteLine("Ваш банк ввод CVC2")
//             //
//             DriverFindElement("(//*[@class='sbersafe-pay-button'])", driver).click()
//             Log.WriteLine("Оплатить")
//             //
//             DriverFindElement("(//*[@name='password'])", driver).sendKeys("12345678")
//             Log.WriteLine("password")
//             //
//             DriverFindElement("(//*[@value='Submit'])", driver).click()
//             Log.WriteLine("Submit")
             Thread.sleep(2500)
             //
             DriverFindElement("//*[@class='h-user_img']", driver).click()
             Log.WriteLine("Открыть меню")
             //
             DriverFindElement("(//*[@class='h-user-menu_ico'])[4]", driver).click()
             Log.WriteLine("Баланс")
             //
             DriverFindElement("//*[@class='button button__act button__sm']", driver).click()
             Log.WriteLine("Вывести средства")
             //
             DriverFindElement("(//*[@class='link-to-modal'])[1]", driver).click()
             Log.WriteLine("Подробнее")
             //
             DriverFindElement("(//*[@class='button button__def button__xs'])[2]", driver).click()
             Log.WriteLine("OK")
             //
             DriverFindElement("(//*[@class='link-to-modal'])[2]", driver).click()
             Log.WriteLine("Пример заполненного бланка")
             //
             DriverFindElement("(//*[@class='button button__def button__xs'])[2]", driver).click()
             Log.WriteLine("OK")
             //
             Thread.sleep(500)
             //
             DriverFindElement("//*[@id='profile-page.refund.fullname-form-label']", driver).sendKeys("Гараж Петрович Самогонов")
             Log.WriteLine("Ваше полное имя фамилия и отчество")
             //
             DriverFindElement("//*[@id='profile-page.refund.zip-code-form-label']", driver).sendKeys("11111111")
             Log.WriteLine("Индекс")
             //
             DriverFindElement("(//*[@id='profile-page.refund.address-form-label'])", driver).sendKeys("Улица малая гаражная дом 23 кв 13")
             Log.WriteLine("Город улица дом квартира")
             //
             DriverFindElement("(//*[@class='form-control pl-input-control_val pl-input-control_val-inner ng-untouched ng-pristine ng-invalid'])[1]", driver).sendKeys("4515545445")
             Log.WriteLine("Серия и номер паспорта")
             //
             DriverFindElement("//*[@placeholder='ДД.ММ.ГГГГ']", driver).sendKeys("03.10.2019")
             Log.WriteLine("Дата выдачи")
             //
             DriverFindElement("(//*[@id='profile-page.refund.document-issued-label'])[1]", driver).sendKeys("Отделением УФМС по городу Москве по р-ну Подвальный")
             Log.WriteLine("Кем выдан")
             //
             DriverFindElement("(//*[@id='profile-page.refund.bank-account-number-label'])[1]", driver).sendKeys("23451356878980767665")
             Log.WriteLine("Счет")
             //
             DriverFindElement("(//*[@id='profile-page.refund.bank-name-label'])[1]", driver).sendKeys("Банк какой то там")
             Log.WriteLine("Наименование банка")
             //
             DriverFindElement("(//*[@id='profile-page.refund.bank-correspondent-account-label'])[1]", driver).sendKeys("2435465676784")
             Log.WriteLine("Коор счёт")
             //
             DriverFindElement("(//*[@id='profile-page.refund.bank-bik-number-label'])[1]", driver).sendKeys("6548957347896757")
             Log.WriteLine("БИК")
             //
             DriverFindElement("(//*[@class='form-ui-txt'])[1]", driver).click()
             Log.WriteLine("Я согласен на сбор и хранение и обработку данных")
             //
             DriverFindElement("(//*[@class='button button__def button__xs'])[1]", driver).click()
             Log.WriteLine("Одобряю")
             //
             DriverFindElement("//*[@class='button button__act button__xs']", driver).click()
             Log.WriteLine("Редактировать")
             //
             DriverFindElement("(//*[@class='form-ui-txt'])[1]", driver).click()
             Log.WriteLine("Я согласен на сбор и хранение и обработку данных")
             //
             DriverFindElement("(//*[@class='button button__def button__xs'])[1]", driver).click()
             Log.WriteLine("Одобряю")
             //
             DriverFindElement("//*[@class='button button__def button__xs']", driver).click()
             Log.WriteLine("Отправить заявление")
             //
             driver.navigate().to("https://yandex.ru")
             //
             DriverFindElement("(//*[@class='home-link home-link_black_yes'])[1]", driver).click()
             Log.WriteLine("Вход на яндекс")
             //
             fun FindWindowYandex() : Boolean
             {
                 return try {

                     Thread.sleep(1500)
                     driver.findElement(By.xpath("//*[@class='passp-page-overlay']"))
                     true

                 } catch(e:Exception) {

                     false
                 }
             }

             if (!FindWindowYandex())
             {
                 DriverFindElement("//*[@name='login']", driver).sendKeys("uz2name")
                 Log.WriteLine("Ввод Логина")
                 //
                 DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                 Log.WriteLine("Ввод пароля")
                 //
                 DriverFindElement("//*[@class='passport-Button-Text']", driver).click()
                 Log.WriteLine("Войти")
                 //
                 Thread.sleep(1500)
                 DriverFindElement("//*[contains(text(), 'Подтверждение заявления на возврат денежных средств')]", driver).click()
                 Log.WriteLine("Выбор письма")
                 //
                 DriverFindElement("(//*[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_sender js-message-snippet-sender'])[2]", driver).click()
                 Log.WriteLine("Выбор письма 2")
                 //
                 DriverFindElement("(//*[@class='daria-goto-anchor'])[2]", driver).click()
                 Log.WriteLine("Для подтверждения заявления на возврат денежных средств пройдите по ссылке")
                 //
             }

             if (FindWindowYandex()){

                 DriverFindElement("//*[@name='login']", driver).sendKeys("uz2name")
                 Log.WriteLine("Ввод Логина")
                 //
                 DriverFindElement("//*[@class='passp-login-form']//button[1]", driver).click()
                 Log.WriteLine("Войти")
                 //
                 DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                 Log.WriteLine("Ввод пароля")
                 //
                 DriverFindElement("//*[@class='passp-password-form']//button[1]", driver).click()
                 Log.WriteLine("Войти")
                 //
                 Thread.sleep(1500)
                 DriverFindElement("//*[contains(text(), 'Подтверждение заявления на возврат денежных средств')]", driver).click()
                 Log.WriteLine("Выбор письма")
                 //
                 DriverFindElement("(//*[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_sender js-message-snippet-sender'])[2]", driver).click()
                 Log.WriteLine("Выбор письма 2")
                 //
                 DriverFindElement("(//*[@class='daria-goto-anchor'])[2]", driver).click()
                 Log.WriteLine("Для подтверждения заявления на возврат денежных средств пройдите по ссылке")
                 //
             }
             //
             Thread.sleep(500)
             //
             val han = driver.windowHandles
             val numberOfTabs = han.size
             var newTabIndex = numberOfTabs
             newTabIndex = 1
             driver.switchTo().window(han.toTypedArray()[newTabIndex].toString())
             //
             DriverFindElement("//*[@class='button button__def button__xs']", driver).click()
             Log.WriteLine("Вернуться на главную профиля")
             //
             DriverFindElement("//*[@class='h-user_img']", driver).click()
             Log.WriteLine("Открыть меню")
             //
             DriverFindElement("(//*[@class='h-user-menu_ico'])[4]", driver).click()
             Log.WriteLine("Баланс")

             fun findbutton() : Boolean
             {
                 return try {
                     DriverFindElement("//*[contains(text(), ' Вывести средства ')]", driver)
                     Log.WriteLine(" Вывести средства ")
                     true
                 } catch(e:Exception) {
                     false
                 }
             }

             if (findbutton())
             {
                 throw  Exception()
             }

             DriverFindElement("//*[@class='h-user_img']", driver).click()
             Log.WriteLine("Открыть меню")
             //
             DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
             Log.WriteLine("Выход")
             //
             Thread.sleep(500)
             //
             DriverFindElement("//*[@class='login_title']", driver).click()
             Log.WriteLine("Сбросить евент")
             //
             DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
             Log.WriteLine("Ввод логина")
             //
             DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
             Log.WriteLine("Ввод пароля")
             //
             DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
             Log.WriteLine("Войти на сайт")
             //
             DriverFindElement("//*[@class='h-user_img']", driver).click()
             Log.WriteLine("Открыть меню")
             //
             DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
             Log.WriteLine("Администрирование")
             //
             DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
             Log.WriteLine("Модерация")
             //
             DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
             Log.WriteLine("Пользователи")
             //
             DriverFindElement("//*[@id='xlInput']", driver).sendKeys("uz2name@yandex.ru")
             Log.WriteLine("Ввод е-майла")
             //
             DriverFindElement("//*[@class='btn btn-primary']", driver).click()
             Log.WriteLine("Поск юзера")
             //
             DriverFindElement("//*[@class='btn btn-primary btn-outline']", driver).click()
             Log.WriteLine("Редактировать")
             //
             Thread.sleep(1500)
             //
             DriverFindElement("(//*[@class='btn btn-default js-all-frozen-amount'])[1]", driver).click()
             Log.WriteLine("Вся сумма")
             //
             DriverFindElement("(//*[@class='btn btn-default js-decrease'])[1]", driver).click()
             Log.WriteLine("Списать")
             //
             DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
             Log.WriteLine("OK")
             //
             Thread.sleep(1500)
             Log.Close()
             Stop()
             driver.quit()
         }
         catch (e:Exception){
             println(e.printStackTrace())
         }
         finally {
             Stop1()
         }
    }

    @Test
    fun  AnnulPurchasess(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Refund")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Refund\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("User-Баланс-Возврат баланса через Аннулирование покупки в админке + Найти этот заказ в админке платежи скачать заявление" + VersionBrowser(driver))





            Avtorization_Uz3name(driver,Log)
            //
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[2]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            Thread.sleep(1000)
            DriverFindElement("(//*[@class='action-card_in'])", driver).click()
            Log.WriteLine("Выбрать вознаграждение")
            //
            Thread.sleep(1000)
            //
            DriverFindElement("//*[contains(text(), 'Купить (1 шт.)')]", driver).click()
            Log.WriteLine("Купить 1 шт")
            //
            DriverFindElement("//*[@id='payment-page.question-to-buyer-label']", driver).sendKeys("123")
            Log.WriteLine("Данные")
            //
            DriverFindElement("//*[@id='core.locality']", driver).sendKeys("Луна")
            Log.WriteLine("Населённый пункт")
            //
            DriverFindElement("(//*[@id='core.zip-code'])[1]", driver).sendKeys("11111111")
            Log.WriteLine("Индекс")
            //
            DriverFindElement("(//*[@id='core.address'])[1]", driver).sendKeys("Большой кратер")
            Log.WriteLine("Адрес")
            //
            DriverFindElement("(//*[@id='core.recipient-name'])[1]", driver).sendKeys("Николас кейдж")
            Log.WriteLine("ФИО получателя")
            //
            DriverFindElement("(//*[@id='core.contact-phone'])[1]", driver).sendKeys("84952222222")
            Log.WriteLine("контактный телефон")
            //
            fun FindEl(): Boolean {
                return try {
                    DriverFindElement("(//*[@class='form-ui-label'])[1]", driver)
                    //Согласен на условия
                    true
                } catch (e: Exception) {
                    false
                }
            }

            for (i in 0..60) {  //Клауд
                if (FindEl()) {

                    try{
                        DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                        Log.WriteLine("Даю свое согласие")
                    }
                    catch (e:Exception){
                        DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
                        Log.WriteLine("Даю свое согласие")
                    }

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver).click()
                    Log.WriteLine("Карты других банков")
                    //
                    DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
                    Log.WriteLine("Оплатить покупку")
                    //
                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
                    Log.WriteLine("Оплата картой")
                    //
                    driver.switchTo().frame(0)
                    DriverFindElement("(//*[@class='row'])[1]//input", driver).click()
                    Log.WriteLine("клик по строке")
                    DriverFindElement("(//*[@class='row'])[1]//input", driver).sendKeys("4111 1111 1111 1111")
                    //
                    DriverFindElement("(//*[@id='inputMonth'])[1]", driver).sendKeys("12")
                    //
                    DriverFindElement("(//*[@id='inputYear'])[1]", driver).sendKeys("19")
                    //
                    DriverFindElement("(//*[@data-mask='000'])[1]", driver).sendKeys("123")
                    //
                    DriverFindElement("(//*[@id='cardHolder'])[1]", driver).sendKeys("QWERTY")
                    //
                    DriverFindElement("(//*[@class='button'])[1]", driver).click()
                    DriverFindElement("(//*[@class='button'])[2]", driver).click()
                    Log.WriteLine("OK")
                    driver.switchTo().defaultContent()
                    break
                }
            }
//            DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
//            Log.WriteLine("Даю свое согласие")
//            //
//            DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
//            Log.WriteLine("Оплатить покупку")
//
//            //DriverFindElement("(//*[@id='email'])[1]", driver).SendKeys("pain-net@yandex.ru");
//            //Log.WriteLine("Сбер ввод email");
//            ////
//            DriverFindElement("(//*[@id='cardnumber'])[1]", driver).sendKeys("4111 1111 1111 1111")
//            Log.WriteLine("Ваш банк ввод номера карты")
//            //
//            DriverFindElement("(//*[@name='expdate'])[1]", driver).sendKeys("1219")
//            Log.WriteLine("Ваш банк ввод месяц год")
//            //
//            DriverFindElement("(//*[@id='cvc'])[1]", driver).sendKeys("123")
//            Log.WriteLine("Ваш банк ввод CVC2")
//            //
//            DriverFindElement("(//*[@class='sbersafe-pay-button'])", driver).click()
//            Log.WriteLine("Оплатить")
//            //
//            DriverFindElement("(//*[@name='password'])", driver).sendKeys("12345678")
//            Log.WriteLine("password")
//            //
//            DriverFindElement("(//*[@value='Submit'])", driver).click()
//            Log.WriteLine("Submit")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[2]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='project-admin-controls_action-i'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("(//*[@name='reason'])[1]", driver).sendKeys("1")
            Log.WriteLine("Введите причину аннулирования")
            //
            DriverFindElement("(//*[@class='btn btn-danger'])[1]", driver).click()
            Log.WriteLine("Аннулировать")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
            Log.WriteLine("Ok")
            //
            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[8]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz3name@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[4]", driver).click()
            Log.WriteLine("Баланс")
            //
            Thread.sleep(1500)
            var numberorder = DriverFindElement("(//*[@class='profile-balance-table_name'])[1]", driver).text
            var n:String = numberorder.replace("\\s".toRegex(),"")
            //
            var number:String = n.trim('О', 'т', 'м', 'е', 'н', 'а', 'з', 'а', 'к', 'а', 'з', 'а','№')
            println(number)
            //
            DriverFindElement("//*[@class='button button__def button__xs ng-star-inserted']", driver).click()
            Log.WriteLine("Возврат Средств на карту")
            //
            DriverFindElement("//*[@class='button button__def button__xs']", driver).click()
            Log.WriteLine("Отправить заявлеие")
            //
            DriverFindElement("//*[@class='button button__def button__xs']", driver).click()
            Log.WriteLine("Подтвердить возврат")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[4]", driver).click()
            Log.WriteLine("Баланс")
            //
            fun findbutton() : Boolean
            {
                return try {
                    DriverFindElement("//*[@class='button button__def button__xs ng-star-inserted']", driver)
                    Log.WriteLine("Возврат Средств на карту")
                    true
                } catch(e:Exception) {
                    false
                }
            }

            if (findbutton())
            {
                DriverFindElement("//*[@class='button button__def button__xs ng-star-inserted']", driver).click()
                Log.WriteLine("Возврат Средств на карту")
                //
                DriverFindElement("//*[@class='button button__def button__xs']", driver).click()
                Log.WriteLine("Отправить заявлеие")
                //
                DriverFindElement("//*[@class='button button__def button__xs']", driver).click()
                Log.WriteLine("Подтвердить возврат")
                //
                DriverFindElement("//*[@class='button button__def button__xs']", driver).click()
            }
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[7]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[8]", driver).click()
            Log.WriteLine("Поддержка")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']//li[1]", driver).click()
            Log.WriteLine("Платежи")
            //
            DriverFindElement("//*[@id='orderOrTransactionId']", driver).sendKeys(number)
            Log.WriteLine("Ввод номера заказа")
            //
            DriverFindElement("//*[@id='search']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("(//*[@class='qa-transaction-container'])[1]//td[10]//a", driver).click()
            Log.WriteLine("Скачать заявление")
            //
            fun Error() : Boolean
            {
                return try {
                    DriverFindElement("//*[contains(text(), '404 - Page not found')]",driver)
                    true
                } catch(e:Exception) {
                    false
                }
            }

            if(Error())
            {
                throw  Exception("Отчёт не скачался")
            }


//            val file = ("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\winium\\Winium.Desktop.Driver.exe")
//            Runtime.getRuntime().exec(file)
//
//            val dc =  DesiredCapabilities()
//            dc.setCapability("app", "C:/Windows/system32/cmd.exe")
//            val driver1 = RemoteWebDriver(java.net.URL("http://localhost:9999"), dc)
//            Thread.sleep(500)
//            Runtime.getRuntime().exec("taskkill /F /IM cmd.exe")
//
//            driver1.findElement(By.name("Действия")).click()
//            //
//            driver1.findElement(By.name("Открыть")).click()
//            //
//            Runtime.getRuntime().exec("taskkill /F /IM Winium.Desktop.Driver.exe")
//            //
//            driver.windowHandle[1]
            Thread.sleep(2500)

            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun DeleteBalance(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Refund")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Refund\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()





            Log.WriteLine("User-Удаление баланса через админку проверка на отсутствие кнопки возврата средств в балансе" + VersionBrowser(driver))






            Avtorization_Uz4name(driver,Log)
            //
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[2]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            Thread.sleep(1000)
            DriverFindElement("(//*[@class='action-card_in'])", driver).click()
            Log.WriteLine("Выбрать вознаграждение")
            //
            Thread.sleep(1000)
            DriverFindElement("//*[contains(text(), 'Купить (1 шт.)')]", driver).click()
            Log.WriteLine("Купить 1 шт")
            //
            DriverFindElement("//*[@id='payment-page.question-to-buyer-label']", driver).sendKeys("123")
            Log.WriteLine("Данные")
            //
            DriverFindElement("//*[@id='core.locality']",driver).sendKeys("Луна")
            Log.WriteLine("Населённый пункт")
            //
            DriverFindElement("(//*[@id='core.zip-code'])[1]",driver).sendKeys("11111111")
            Log.WriteLine("Индекс")
            //
            DriverFindElement("(//*[@id='core.address'])[1]",driver).sendKeys("Большой кратер")
            Log.WriteLine("Адрес")
            //
            DriverFindElement("(//*[@id='core.recipient-name'])[1]",driver).sendKeys("Николас кейдж")
            Log.WriteLine("ФИО получателя")
            //
            DriverFindElement("(//*[@id='core.contact-phone'])[1]",driver).sendKeys("84952222222")
            Log.WriteLine("контактный телефон")
            //
            fun FindEl(): Boolean {
                return try {
                    DriverFindElement("(//*[@class='form-ui-label'])[1]", driver)
                    //Согласен на условия
                    true
                } catch (e: Exception) {
                    false
                }
            }

            for (i in 0..60) {  //Клауд
                if (FindEl()) {

                    try{
                        DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                        Log.WriteLine("Даю свое согласие")
                    }
                    catch (e:Exception){
                        DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
                        Log.WriteLine("Даю свое согласие")
                    }

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver).click()
                    Log.WriteLine("Карты других банков")
                    //
                    DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
                    Log.WriteLine("Оплатить покупку")
                    //
                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
                    Log.WriteLine("Оплата картой")
                    //
                    driver.switchTo().frame(0)
                    DriverFindElement("(//*[@class='row'])[1]//input", driver).click()
                    Log.WriteLine("клик по строке")
                    DriverFindElement("(//*[@class='row'])[1]//input", driver).sendKeys("4111 1111 1111 1111")
                    //
                    DriverFindElement("(//*[@id='inputMonth'])[1]", driver).sendKeys("12")
                    //
                    DriverFindElement("(//*[@id='inputYear'])[1]", driver).sendKeys("19")
                    //
                    DriverFindElement("(//*[@data-mask='000'])[1]", driver).sendKeys("123")
                    //
                    DriverFindElement("(//*[@id='cardHolder'])[1]", driver).sendKeys("QWERTY")
                    //
                    DriverFindElement("(//*[@class='button'])[1]", driver).click()
                    DriverFindElement("(//*[@class='button'])[2]", driver).click()
                    Log.WriteLine("OK")
                    driver.switchTo().defaultContent()
                    break
                }
            }
//            fun findelement() : Boolean
//            {
//                return try {
//                    DriverFindElement("(//*[@class='form-ui-label'])[2]", driver)
//                    true
//                } catch(e:Exception) {
//                    false
//                }
//            }
//
//            if (findelement())
//            {
//                DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
//                Log.WriteLine("Даю свое согласие")
//            }
//
//            if (!findelement())
//            {
//                DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
//                Log.WriteLine("Даю свое согласие")
//            }
//
//            DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
//            Log.WriteLine("Оплатить покупку")
//
//            //DriverFindElement("(//*[@id='email'])[1]", driver).SendKeys("pain-net@yandex.ru");
//            //Log.WriteLine("Сбер ввод email");
//            ////
//            DriverFindElement("(//*[@id='cardnumber'])[1]", driver).sendKeys("4111 1111 1111 1111")
//            Log.WriteLine("Ваш банк ввод номера карты")
//            //
//            DriverFindElement("(//*[@name='expdate'])[1]", driver).sendKeys("1219")
//            Log.WriteLine("Ваш банк ввод месяц год")
//            //
//            DriverFindElement("(//*[@id='cvc'])[1]", driver).sendKeys("123")
//            Log.WriteLine("Ваш банк ввод CVC2")
//            //
//            DriverFindElement("(//*[@class='sbersafe-pay-button'])", driver).click()
//            Log.WriteLine("Оплатить")
//            //
//            DriverFindElement("(//*[@name='password'])", driver).sendKeys("12345678")
//            Log.WriteLine("password")
//            //
//            DriverFindElement("(//*[@value='Submit'])", driver).click()
//            Log.WriteLine("Submit")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[2]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='project-admin-controls_action-i'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("(//*[@name='reason'])[1]", driver).sendKeys("1")
            Log.WriteLine("Введите причину аннулирования")
            //
            DriverFindElement("(//*[@class='btn btn-danger'])[1]", driver).click()
            Log.WriteLine("Аннулировать")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
            Log.WriteLine("Ok")
            //
            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[8]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz4name@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[4]", driver).click()
            Log.WriteLine("Баланс")
            //
            DriverFindElement("//*[@class='button button__def button__xs ng-star-inserted']", driver)
            Log.WriteLine("Проверка на наличии кнопки возврат средств")
            //
            Thread.sleep(500)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).clear()
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).clear()
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Пользователи")
            //
            DriverFindElement("//*[@id='xlInput']", driver).sendKeys("uz4name@yandex.ru")
            Log.WriteLine("Ввод е-майла")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Поск юзера")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-outline']", driver).click()
            Log.WriteLine("Редактировать")
            //
            val text = DriverFindElement("(//*[@class='form-group'])[8]", driver) // Вытащить текст из элемента
            var Text1 = text.text
            Text1 = Text1.replace("\\s".toRegex(),"")
            //
            var number = Text1.trim('Т', 'е', 'к', 'у', 'щ', 'и', 'й', 'б', 'а', 'л', 'а', 'н', 'с', ':','0', '0', '0', '0')
            var deletenumber = number.trim( '.' )
            println(deletenumber)
            //
            DriverFindElement("//*[@name='amount']", driver).sendKeys(deletenumber)
            Log.WriteLine("Ввод в поле для удаления баланса баланс")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[8]", driver).click()
            Log.WriteLine("Удалить")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
            Log.WriteLine("OK")
            //
            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[8]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).clear()
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz4name@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).clear()
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[4]", driver).click()
            Log.WriteLine("Баланс")
            //
            fun find() : Boolean
            {
                return try {
                    DriverFindElement("//*[@class='button button__def button__xs ng-star-inserted']", driver)
                    true
                } catch(e:Exception) {
                    false
                }
            }

            if (find())
            {
                throw  Exception()
            }
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }


    @Test
    fun ButtonRefundBalance(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Refund")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Refund\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()





            Log.WriteLine("User-Аннулирование заказа покупка на возвращённые деньги кнопка вернуть на карту отсутствует - Оплата Сбер" + VersionBrowser(driver))





            AvtirizationOther(driver,Log)
            //
            val text: WebElement = DriverFindElement("//*[@class='h-user_name']", driver) // Вытащить текст из элемента
            var Text1:String = text.text
            var textNew = Text1.replace("\\s".toRegex(),"")
            println(textNew)
            //
            Purhase_1(driver,Log)
            //
            Purhase_2(driver,Log,textNew)
            //
            Purhase_3(driver,Log,textNew)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[8]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("$textNew@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[4]", driver).click()
            Log.WriteLine("Баланс")
            //
            fun findElementButton() : Boolean{
                return try {
                    DriverFindElement("(//*[@class='button button__def button__xs ng-star-inserted'])[3]", driver)
                    true
                }catch (e:Exception){
                    false
                }
            }

            for(i in 0..40){
                if(findElementButton()){
                    DriverFindElement("(//*[@class='button button__def button__xs ng-star-inserted'])[3]", driver).click()
                    Log.WriteLine("Возврат Средств на карту")
                    break
                }
            }

            //
            DriverFindElement("//*[@class='button button__def button__xs']", driver).click()
            Log.WriteLine("Отправить заявлеие")
            //
            DriverFindElement("//*[@class='button button__def button__xs']", driver).click()
            Log.WriteLine("Подтвердить возврат")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[4]", driver).click()
            Log.WriteLine("Баланс")
            //
            fun findButton() : Boolean{
                return try {
                    DriverFindElement("(//*[@class='button button__def button__xs ng-star-inserted'])[3]",driver)
                    Log.WriteLine("3 я кнопка")
                    true
                }catch (p:Exception){
                    false
                }
            }

             if(findButton()){
                 throw  Exception("Условие не выполнено кнопка не исчезла")
             }
            //
            DriverFindElement("(//*[@class='button button__def button__xs ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Возврат Средств на карту")
            //
            DriverFindElement("//*[@class='button button__def button__xs']", driver).click()
            Log.WriteLine("Отправить заявлеие")
            //
            DriverFindElement("//*[@class='button button__def button__xs']", driver).click()
            Log.WriteLine("Подтвердить возврат")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[4]", driver).click()
            Log.WriteLine("Баланс")
            //
            fun findButton2() : Boolean{
                return try {
                    DriverFindElement("(//*[@class='button button__def button__xs ng-star-inserted'])[2]",driver)
                    Log.WriteLine("2 я кнопка")
                    true
                }catch (p:Exception){
                    false
                }
            }
            //
            if(findButton2()){
                throw  Exception("Условие не выполнено кнопка не исчезла")
            }
            //
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[1]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            Thread.sleep(1300)
            DriverFindElement("(//*[@class='btn btn-nd-primary action-card_btn js-navigate-to-purchase'])[1]",driver).click()
            Log.WriteLine("Оплатить")
            //
            DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
            Log.WriteLine("Даю свое согласие")
            //
            DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
            Log.WriteLine("Оплатить заказ, используя средства на моем счету")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
            Log.WriteLine("Оплатить покупку")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Личный кабинет")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[4]", driver).click()
            Log.WriteLine("Баланс")
            //
            fun findButtonOne() : Boolean{
                return try {
                    DriverFindElement("(//*[@class='button button__def button__xs ng-star-inserted'])[1]",driver)
                    Log.WriteLine("2 я кнопка")
                    true
                }catch (p:Exception){
                    false
                }
            }
            //
            if(findButtonOne()){
                throw  Exception("Условие не выполнено кнопка не исчезла")
            }

            //
            DriverFindElement("//*[@class='button button__def button__sm']", driver).click()
            Log.WriteLine("Пополнить баланс")
            //
            DriverFindElement("//*[@id='core.total-amount']", driver).clear()
            Log.WriteLine("Очистить поле")
            //
            DriverFindElement("//*[@id='core.total-amount']", driver).sendKeys("1000000")
            Log.WriteLine("Ввод в поле значения 1000")
            //
            DriverFindElement("(//*[@class='project-payment-choice_name'])[2]",driver).click()
            Log.WriteLine("Клауд")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary project-payment_btn']", driver).click()
            Log.WriteLine("Перейти к оплате")
            //
            fun FindEl() : Boolean{
                return try{
                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver)
                    //Согласен на условия
                    true
                }catch (e:Exception){
                    false
                }
            }

            for(i in 0..60){  //Клауд
                if(FindEl()){

                    DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
                    Log.WriteLine("Оплата картой")
                    //
                    driver.switchTo().frame(0)
                    DriverFindElement("(//*[@class='row'])[1]//input",driver).click()
                    Log.WriteLine("клик по строке")
                    DriverFindElement("(//*[@class='row'])[1]//input",driver).sendKeys("4111 1111 1111 1111")
                    //
                    DriverFindElement("(//*[@id='inputMonth'])[1]",driver).sendKeys("12")
                    //
                    DriverFindElement("(//*[@id='inputYear'])[1]",driver).sendKeys("19")
                    //
                    DriverFindElement("(//*[@data-mask='000'])[1]",driver).sendKeys("123")
                    //
                    DriverFindElement("(//*[@id='cardHolder'])[1]",driver).sendKeys("QWERTY")
                    //
                    DriverFindElement("(//*[@class='button'])[1]",driver).click()
                    DriverFindElement("(//*[@class='button'])[2]",driver).click()
                    Log.WriteLine("OK")
                    driver.switchTo().defaultContent()
                    break
                }
            }
//            DriverFindElement("//*[@class='btn btn-nd-primary project-payment_btn']", driver).click()
//            Log.WriteLine("Перейти к оплате Сбер")
//            //
//            DriverFindElement("(//*[@id='email'])[1]", driver).clear()
//            //
//            DriverFindElement("(//*[@id='email'])[1]", driver).sendKeys("pain-net@yandex.ru")
//            Log.WriteLine("Сбер ввод email")
//            //
//            DriverFindElement("(//*[@id='cardnumber'])[1]", driver).sendKeys("4111 1111 1111 1111")
//            Log.WriteLine("Ваш банк ввод номера карты")
//            //
//            DriverFindElement("(//*[@name='expdate'])[1]", driver).sendKeys("1219")
//            Log.WriteLine("Ваш банк ввод месяц год")
//            //
//            DriverFindElement("(//*[@id='cvc'])[1]", driver).sendKeys("123")
//            Log.WriteLine("Ваш банк ввод CVC2")
//            //
//            DriverFindElement("(//*[@class='sbersafe-pay-button'])", driver).click()
//            Log.WriteLine("Оплатить")
//            //
//            DriverFindElement("(//*[@name='password'])", driver).sendKeys("12345678")
//            Log.WriteLine("password")
//            //
//            DriverFindElement("(//*[@value='Submit'])", driver).click()
//            Log.WriteLine("Submit")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Личный кабинет")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[4]", driver).click()
            Log.WriteLine("Баланс")
            //
            if(findButton()){
                throw  Exception("Условие не выполнено кнопка не исчезла")
            }

            if(findButton2()){
                throw  Exception("Условие не выполнено кнопка не исчезла")
            }
            DriverFindElement("(//*[@class='button button__def button__xs ng-star-inserted'])[1]", driver).click()
            Log.WriteLine("Возврат Средств на карту")
            //
            DriverFindElement("//*[@class='button button__def button__xs']", driver).click()
            Log.WriteLine("Отправить заявлеие")
            //
            DriverFindElement("//*[@class='button button__def button__xs']", driver).click()
            Log.WriteLine("Подтвердить возврат")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[4]", driver).click()
            Log.WriteLine("Баланс")

            if(findButtonOne()){
                throw  Exception("Условие не выполнено кнопка не исчезла")
            }
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            e.printStackTrace()
        }finally {
            Stop1()
        }
    }


}