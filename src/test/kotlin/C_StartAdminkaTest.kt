import org.junit.Test
import org.junit.experimental.ParallelComputer
import org.junit.runner.JUnitCore

class C_StartAdminkaTest  {

    @Test
    fun Start(){

        val clsConsistent = arrayOf(//Последовательные
                `A_CreateAvtorProject+CreateShopProduct`::class.java,
                B_AdminNews::class.java,
                ProjectCategory::class.java,
                ProjectCompletionAdmin::class.java)
        JUnitCore.runClasses(ParallelComputer.methods(), *clsConsistent)


        val cls = arrayOf(//Паралельные
                A_AdminkaShop::class.java,
                A_Bonusses::class.java,
                A_Contents::class.java,
                A_Notifications::class.java,
                A_AdminkaOther::class.java,
                D_DeleteBanners::class.java,
                A_Banners::class.java,
                B_Banners2::class.java,
                A_AdminkaSupport::class.java,
                A_BiblioRodina::class.java
                ,A_Broadcast::class.java)
        JUnitCore.runClasses(ParallelComputer.methods(), *cls)
    }
}