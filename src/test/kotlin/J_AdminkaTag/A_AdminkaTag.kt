import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.WebDriver
import java.nio.file.Files
import java.nio.file.Paths

class A_AdminkaTag : Setup() {

    @Test
    fun Planeta(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка-Теги-Планета" + VersionBrowser(driver))





            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).clear()
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина админа")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[1]", driver).click()
            Log.WriteLine("Модерация")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in']//li[2]", driver).click()
            Log.WriteLine("Проекты")
            //
            DriverFindElement("//tbody//tr[1]//td[3]/a", driver).click()
            Log.WriteLine("Выбор проекта")
            //
            DriverFindElement("(//*[@class='project-author-controls_name'])[2]", driver).click()
            Log.WriteLine("Редактировать")
            //
            val text = DriverFindElement("//tbody[1]//tr[1]//td[2]//input", driver)
            text.getAttribute("value")
            val Text = text.getAttribute("value")

            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[10]", driver).click()
            Log.WriteLine("Теги")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[1]", driver).click()
            Log.WriteLine("Теги")
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Добавит тег")
            //
            DriverFindElement("//*[@name='tagPath']", driver).sendKeys("/campaings/$Text")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@name='keywords']", driver).sendKeys("Meeetttt")
            Log.WriteLine("Keywords")
            //
            DriverFindElement("//*[@name='title']", driver).sendKeys("title")
            Log.WriteLine("title")
            //
            DriverFindElement("//*[@name='description']", driver).sendKeys("description")
            Log.WriteLine("description")
            //
            DriverFindElement("//*[@name='ogTitle']", driver).sendKeys("Open Graph-title")
            Log.WriteLine("Open Graph-title")
            //
            DriverFindElement("//*[@name='ogDescription']", driver).sendKeys("Open Graph-Description")
            Log.WriteLine("Description")
            //
            DriverFindElement("//*[@class='btn btn-primary js-btn-save']", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverNavigate("$URL/campaings/$Text", driver)
            Log.WriteLine("открыть урлу по тегу")
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun Shop(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()

            Log.WriteLine("Админка-Теги-Магазин" + VersionBrowser(driver))





            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).clear()
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина админа")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[10]", driver).click()
            Log.WriteLine("Теги")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[2]", driver).click()
            Log.WriteLine("Магазин")
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Добавить тэг на товар")
            //

            //Дописать!




            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }




}