import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.WebDriver
import java.nio.file.Files
import java.nio.file.Paths



class A_Notifications : Setup() {

    @Test
    fun EnentsInProject(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка-Уведомления-События в проектах" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[6]", driver).click()
            Log.WriteLine("Уведомления")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[1]", driver).click()
            Log.WriteLine("Уведомления в проекте")
            //
            for (i in 1..17) {
                DriverFindElement("(//*[@class='form-control'])[2]//option[" + i.toString() + "]", driver).click()
            }
            Log.WriteLine("Перебор статусов")
            //
            DriverFindElement("(//*[@class='form-control'])[2]//option[1]", driver).click()
            //

            for (i in 1..27) {
                DriverFindElement("(//*[@class='form-control'])[3]//option[" + i.toString() + "]", driver).click()
            }
            Log.WriteLine("Перебор менеджеров")
            //
            DriverFindElement("//*[@class='btn btn-success']", driver).click()
            Log.WriteLine("Выгрузить Excel")
            //
            DriverFindElement("//*[@class='btn btn-info']", driver).click()
            Log.WriteLine("Выгрузить CSV")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun NewsProject(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка-Уведомления-Новости в проектах" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[6]", driver).click()
            Log.WriteLine("Уведомления")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[2]", driver).click()
            Log.WriteLine("Новости в проектах")
            //
            DriverFindElement("//*[@id='date-box-from']", driver).clear()
            //
            DriverFindElement("//*[@id='date-box-from']", driver).sendKeys("20012017")
            Log.WriteLine("Выбор даты")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Поиск")
            //
            for (i in 1..17) {
                DriverFindElement("(//*[@class='form-control'])[2]//option[" + i.toString() + "]", driver).click()
            }
            Log.WriteLine("Перебор статусов")
            //
            DriverFindElement("(//*[@class='form-control'])[2]//option[1]", driver).click()
            //

            for (i in 1..27) {
                DriverFindElement("(//*[@class='form-control'])[3]//option[" + i.toString() + "]", driver).click()
            }
            Log.WriteLine("Перебор менеджеров")
            //
            DriverFindElement("//*[@class='btn btn-success']", driver).click()
            Log.WriteLine("Выгрузить Excel")
            //
            DriverFindElement("//*[@class='btn btn-info']", driver).click()
            Log.WriteLine("Выгрузить CSV")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun CommentProject(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка-Уведомления-Комментарии в проектах" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[6]", driver).click()
            Log.WriteLine("Уведомления")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[3]", driver).click()
            Log.WriteLine("Комментарии в проектах")
            //
            DriverFindElement("//*[@id='date-box-from']", driver).clear()
            //
            DriverFindElement("//*[@id='date-box-from']", driver).sendKeys("20012017")
            Log.WriteLine("Выбор даты")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Поиск")
            //
            for (i in 1..17) {
                DriverFindElement("(//*[@class='form-control'])[2]//option[" + i.toString() + "]", driver).click()
            }
            Log.WriteLine("Перебор статусов")
            //
            DriverFindElement("(//*[@class='form-control'])[2]//option[1]", driver).click()
            //
            for (i in 1..27) {
                DriverFindElement("(//*[@class='form-control'])[3]//option[" + i.toString() + "]", driver).click()
            }
            Log.WriteLine("Перебор менеджеров")
            //
            DriverFindElement("//*[@class='btn btn-success']", driver).click()
            Log.WriteLine("Выгрузить Excel")
            //
            DriverFindElement("//*[@class='btn btn-info']", driver).click()
            Log.WriteLine("Выгрузить CSV")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun ProjectRewardsEvents(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Админка-Уведомления-События вознаграждений в проектах" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[6]", driver).click()
            Log.WriteLine("Уведомления")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[4]", driver).click()
            Log.WriteLine("События вознаграждений в проектах")
            //
            DriverFindElement("//*[@id='date-box-from']", driver).clear()
            //
            DriverFindElement("//*[@id='date-box-from']", driver).sendKeys("20012017")
            Log.WriteLine("Выбор даты")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Поиск")
            //
            for (i in 1..17) {
                DriverFindElement("(//*[@class='form-control'])[2]//option[" + i.toString() + "]", driver).click()
            }
            Log.WriteLine("Перебор статусов")
            //
            DriverFindElement("(//*[@class='form-control'])[2]//option[2]", driver).click()
            //
            for (i in 1..27) {
                DriverFindElement("(//*[@class='form-control'])[3]//option[" + i.toString() + "]", driver).click()
            }
            Log.WriteLine("Перебор менеджеров")
            //
            DriverFindElement("//*[@class='btn btn-success']", driver).click()
            Log.WriteLine("Выгрузить Excel")
            //
            DriverFindElement("//*[@class='btn btn-info']", driver).click()
            Log.WriteLine("Выгрузить CSV")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun CommentsProduct(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Админка-Уведомления-Комментарии к продуктам" + VersionBrowser(driver))





            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[6]", driver).click()
            Log.WriteLine("Уведомления")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[5]", driver).click()
            Log.WriteLine("Комментарии к продуктам")
            //
            Thread.sleep(1000)
            //
            DriverFindElement("//tbody", driver)
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }



}