
import Z_Settings.Loger
import org.junit.Test
import org.junit.experimental.ParallelComputer
import org.junit.runner.JUnitCore
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import java.nio.file.Files
import java.nio.file.Paths

class A_RunAll {

   @Test
   fun Start(){

       val clsConsistent = arrayOf(
               B_StartСonsistentTest::class.java)
       JUnitCore.runClasses(ParallelComputer.methods(), *clsConsistent)

       val clsConsistent2 = arrayOf(
               C_StartAdminkaTest::class.java)
       JUnitCore.runClasses(ParallelComputer.methods(), *clsConsistent2)


       val clsConsistent3 = arrayOf(
               D_StartParallelTest::class.java)
       JUnitCore.runClasses(ParallelComputer.methods(), *clsConsistent3)
   }



    @Test
    fun StartRegister(){

        val register = arrayOf(RegisterMail::class.java)        //Если учётки обезличены запустить и учётки восстановятся
       JUnitCore.runClasses(ParallelComputer.methods(),*register)
    }


   class RegisterMail : SetupParallel(){ //Восстановить учётки после перенакатки базы

       @Test
       fun RegisterAcc(){
           try {
               TestName = GetCurrentMethod()
               val folder = Paths.get("C:\\Log Test\\Register")
               Files.createDirectories(folder)
               val currentFile = "C:\\Log Test\\Register\\$TestName"
               val Log = Loger(currentFile, ".txt")
               val driver: WebDriver = Driver()


               Log.WriteLine("Регистрация" + VersionBrowser(driver))



               DriverNavigate(URL, driver)
               //
               DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
               Log.WriteLine("Вход")
               //
               DriverFindElement("//*[@class='login_title']", driver).click()
               Log.WriteLine("Сбросить евент")
               //
               DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz2name@yandex.ru")
               Log.WriteLine("Ввод логина")
               //
               DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
               Log.WriteLine("Ввод пароля")
               //
               DriverFindElement("//*[@class='login_btn-link']", driver).click()
               Log.WriteLine("Зарегестрироваться")
               //
               DriverFindElement("(//*[@class='form-control login-control_input ng-untouched ng-pristine ng-valid'])[3]", driver).sendKeys("nekroman911")
               Log.WriteLine("Подтвердите пароль")
               //
               DriverFindElement("//*[@class='form-ui-txt']", driver).click()
               Log.WriteLine("Я даю своё согласие")
               //
               DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
               Log.WriteLine("Зарегестрироваться")
               //
               val text: WebElement = DriverFindElement("//*[@class='h-user_name']", driver) // Вытащить текст из элемента
               var Text1:String = text.text
               Text1 = Text1.replace("\\s".toRegex(),"")
               //
               DriverFindElement("//*[@class='h-user_img']", driver).click()
               Log.WriteLine("Личный кабинет")
               //
               DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
               Log.WriteLine("Выход")
               //
               DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
               Log.WriteLine("Вход")
               //
               DriverFindElement("//*[@class='login_title']", driver).click()
               Log.WriteLine("Сбросить евент")
               //
               DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).clear()
               //
               println(Text1)
               DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("$Text1@yandex.ru")
               Log.WriteLine("Ввод логина")
               //
               //DriverFindElement("(//*[@name='password'])", driver).SendKeys("nekroman911")
               //Log.WriteLine("Ввод пароля");
               //
               DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
               Log.WriteLine("Войти на сайт")
               //
               Thread.sleep(1500)
               //
               driver.navigate().to("https://yandex.ru")
               //
               DriverFindElement("(//*[@class='home-link home-link_black_yes'])[1]", driver).click()
               Log.WriteLine("Вход на яндекс")
               //
               fun FindWindowYandex() : Boolean
               {
                   return try {

                       Thread.sleep(1500)
                       driver.findElement(By.xpath("//*[@class='passp-page-overlay']"))
                       true

                   } catch(e:Exception) {

                       false
                   }
               }

               if (!FindWindowYandex())
               {
                   DriverFindElement("//*[@name='login']", driver).sendKeys(Text1)
                   Log.WriteLine("Ввод Логина")
                   //
                   DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                   Log.WriteLine("Ввод пароля")
                   //
                   DriverFindElement("//*[@class='passport-Button-Text']", driver).click()
                   Log.WriteLine("Войти")
                   //
                   DriverFindElement("(//*[@class='mail-MessageSnippet-FromText'])[1]", driver).click()
                   Log.WriteLine("Выбор письма")
                   //
                   Thread.sleep(500)
                   DriverFindElement("(//*[contains(text(), 'Подтвердить регистрацию')])[2]", driver).click()
                   Log.WriteLine("Подтвердить регистрацию")
               }

               if (FindWindowYandex()){

                   DriverFindElement("//*[@name='login']", driver).sendKeys(Text1)
                   Log.WriteLine("Ввод Логина")
                   //
                   DriverFindElement("//*[@class='passp-login-form']//button[1]", driver).click()
                   Log.WriteLine("Войти")
                   //
                   DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                   Log.WriteLine("Ввод пароля")
                   //
                   DriverFindElement("//*[@class='passp-password-form']//button[1]", driver).click()
                   Log.WriteLine("Войти")
                   //
                   DriverFindElement("(//*[@class='mail-MessageSnippet-FromText'])[1]", driver).click()
                   Log.WriteLine("Выбор письма")
                   //
                   Thread.sleep(500)
                   DriverFindElement("(//*[contains(text(), 'Подтвердить регистрацию')])[2]", driver).click()
                   Log.WriteLine("Подтвердить регистрацию")
               }
               Thread.sleep(3500)
               Log.Close()
               Stop()
               driver.quit()
           }
           catch (e:Exception){
               println(e.printStackTrace())
           }
           finally {
               Stop1()
           }
       }

       @Test
       fun RegisterAcc2(){
           try {
               TestName = GetCurrentMethod()
               val folder = Paths.get("C:\\Log Test\\Register")
               Files.createDirectories(folder)
               val currentFile = "C:\\Log Test\\Register\\$TestName"
               val Log = Loger(currentFile, ".txt")
               val driver: WebDriver = Driver()


               Log.WriteLine("Регистрация" + VersionBrowser(driver))



               DriverNavigate(URL, driver)
               //
               DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
               Log.WriteLine("Вход")
               //
               DriverFindElement("//*[@class='login_title']", driver).click()
               Log.WriteLine("Сбросить евент")
               //
               DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz3name@yandex.ru")
               Log.WriteLine("Ввод логина")
               //
               DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
               Log.WriteLine("Ввод пароля")
               //
               DriverFindElement("//*[@class='login_btn-link']", driver).click()
               Log.WriteLine("Зарегестрироваться")
               //
               DriverFindElement("(//*[@class='form-control login-control_input ng-untouched ng-pristine ng-valid'])[3]", driver).sendKeys("nekroman911")
               Log.WriteLine("Подтвердите пароль")
               //
               DriverFindElement("//*[@class='form-ui-txt']", driver).click()
               Log.WriteLine("Я даю своё согласие")
               //
               DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
               Log.WriteLine("Зарегестрироваться")
               //
               val text: WebElement = DriverFindElement("//*[@class='h-user_name']", driver) // Вытащить текст из элемента
               var Text1:String = text.text
               Text1 = Text1.replace("\\s".toRegex(),"")
               //
               DriverFindElement("//*[@class='h-user_img']", driver).click()
               Log.WriteLine("Личный кабинет")
               //
               DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
               Log.WriteLine("Выход")
               //
               DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
               Log.WriteLine("Вход")
               //
               DriverFindElement("//*[@class='login_title']", driver).click()
               Log.WriteLine("Сбросить евент")
               //
               DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).clear()
               //
               println(Text1)
               DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("$Text1@yandex.ru")
               Log.WriteLine("Ввод логина")
               //
               //DriverFindElement("(//*[@name='password'])", driver).SendKeys("nekroman911")
               //Log.WriteLine("Ввод пароля");
               //
               DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
               Log.WriteLine("Войти на сайт")
               //
               Thread.sleep(1500)
               //
               driver.navigate().to("https://yandex.ru")
               //
               DriverFindElement("(//*[@class='home-link home-link_black_yes'])[1]", driver).click()
               Log.WriteLine("Вход на яндекс")
               //
               fun FindWindowYandex() : Boolean
               {
                   return try {

                       Thread.sleep(1500)
                       driver.findElement(By.xpath("//*[@class='passp-page-overlay']"))
                       true

                   } catch(e:Exception) {

                       false
                   }
               }

               if (!FindWindowYandex())
               {
                   DriverFindElement("//*[@name='login']", driver).sendKeys(Text1)
                   Log.WriteLine("Ввод Логина")
                   //
                   DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                   Log.WriteLine("Ввод пароля")
                   //
                   DriverFindElement("//*[@class='passport-Button-Text']", driver).click()
                   Log.WriteLine("Войти")
                   //
//                   DriverFindElement("//*[@class='user-account__name user-account__name_hasAccentLetter']",driver).click()
//                   Log.WriteLine("Клик на меню яндекса")
//                   //
//                   DriverFindElement("//*[@class='menu__group']//li[2]",driver).click()
//                   Log.WriteLine("Почта")
                   //
                   DriverFindElement("(//*[@class='mail-MessageSnippet-FromText'])[1]", driver).click()
                   Log.WriteLine("Выбор письма")
                   //
                   Thread.sleep(500)
                   DriverFindElement("(//*[contains(text(), 'Подтвердить регистрацию')])[2]", driver).click()
                   Log.WriteLine("Подтвердить регистрацию")
               }

               if (FindWindowYandex()){

                   DriverFindElement("//*[@name='login']", driver).sendKeys(Text1)
                   Log.WriteLine("Ввод Логина")
                   //
                   DriverFindElement("//*[@class='passp-login-form']//button[1]", driver).click()
                   Log.WriteLine("Войти")
                   //
                   DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                   Log.WriteLine("Ввод пароля")
                   //
                   DriverFindElement("//*[@class='passp-password-form']//button[1]", driver).click()
                   Log.WriteLine("Войти")
                   //
//                   DriverFindElement("//*[@class='user-account__name user-account__name_hasAccentLetter']",driver).click()
//                   Log.WriteLine("Клик на меню яндекса")
//                   //
//                   DriverFindElement("//*[@class='menu__group']//li[2]",driver).click()
//                   Log.WriteLine("Почта")
                   //
                   DriverFindElement("(//*[@class='mail-MessageSnippet-FromText'])[1]", driver).click()
                   Log.WriteLine("Выбор письма")
                   //
                   Thread.sleep(500)
                   DriverFindElement("(//*[contains(text(), 'Подтвердить регистрацию')])[2]", driver).click()
                   Log.WriteLine("Подтвердить регистрацию")
               }
               Thread.sleep(3500)
               Log.Close()
               Stop()
               driver.quit()
           }
           catch (e:Exception){
               println(e.printStackTrace())
           }
           finally {
               Stop1()
           }
       }

       @Test
       fun RegisterAcc4(){
           try {
               TestName = GetCurrentMethod()
               val folder = Paths.get("C:\\Log Test\\Register")
               Files.createDirectories(folder)
               val currentFile = "C:\\Log Test\\Register\\$TestName"
               val Log = Loger(currentFile, ".txt")
               val driver: WebDriver = Driver()


               Log.WriteLine("Регистрация" + VersionBrowser(driver))



               DriverNavigate(URL, driver)
               //
               DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
               Log.WriteLine("Вход")
               //
               DriverFindElement("//*[@class='login_title']", driver).click()
               Log.WriteLine("Сбросить евент")
               //
               DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz4name@yandex.ru")
               Log.WriteLine("Ввод логина")
               //
               DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
               Log.WriteLine("Ввод пароля")
               //
               DriverFindElement("//*[@class='login_btn-link']", driver).click()
               Log.WriteLine("Зарегестрироваться")
               //
               DriverFindElement("(//*[@class='form-control login-control_input ng-untouched ng-pristine ng-valid'])[3]", driver).sendKeys("nekroman911")
               Log.WriteLine("Подтвердите пароль")
               //
               DriverFindElement("//*[@class='form-ui-txt']", driver).click()
               Log.WriteLine("Я даю своё согласие")
               //
               DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
               Log.WriteLine("Зарегестрироваться")
               //
               val text: WebElement = DriverFindElement("//*[@class='h-user_name']", driver) // Вытащить текст из элемента
               var Text1:String = text.text
               Text1 = Text1.replace("\\s".toRegex(),"")
               //
               DriverFindElement("//*[@class='h-user_img']", driver).click()
               Log.WriteLine("Личный кабинет")
               //
               DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
               Log.WriteLine("Выход")
               //
               DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
               Log.WriteLine("Вход")
               //
               DriverFindElement("//*[@class='login_title']", driver).click()
               Log.WriteLine("Сбросить евент")
               //
               DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).clear()
               //
               println(Text1)
               DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("$Text1@yandex.ru")
               Log.WriteLine("Ввод логина")
               //
               //DriverFindElement("(//*[@name='password'])", driver).SendKeys("nekroman911")
               //Log.WriteLine("Ввод пароля");
               //
               DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
               Log.WriteLine("Войти на сайт")
               //
               Thread.sleep(1500)
               //
               driver.navigate().to("https://yandex.ru")
               //
               DriverFindElement("(//*[@class='home-link home-link_black_yes'])[1]", driver).click()
               Log.WriteLine("Вход на яндекс")
               //
               fun FindWindowYandex() : Boolean
               {
                   return try {

                       Thread.sleep(1500)
                       driver.findElement(By.xpath("//*[@class='passp-page-overlay']"))
                       true

                   } catch(e:Exception) {

                       false
                   }
               }

               if (!FindWindowYandex())
               {
                   DriverFindElement("//*[@name='login']", driver).sendKeys(Text1)
                   Log.WriteLine("Ввод Логина")
                   //
                   DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                   Log.WriteLine("Ввод пароля")
                   //
                   DriverFindElement("//*[@class='passport-Button-Text']", driver).click()
                   Log.WriteLine("Войти")

                   DriverFindElement("(//*[@class='mail-MessageSnippet-FromText'])[1]", driver).click()
                   Log.WriteLine("Выбор письма")
                   //
                   Thread.sleep(500)
                   DriverFindElement("(//*[contains(text(), 'Подтвердить регистрацию')])[2]", driver).click()
                   Log.WriteLine("Подтвердить регистрацию")
               }

               if (FindWindowYandex()){

                   DriverFindElement("//*[@name='login']", driver).sendKeys(Text1)
                   Log.WriteLine("Ввод Логина")
                   //
                   DriverFindElement("//*[@class='passp-login-form']//button[1]", driver).click()
                   Log.WriteLine("Войти")
                   //
                   DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                   Log.WriteLine("Ввод пароля")
                   //
                   DriverFindElement("//*[@class='passp-password-form']//button[1]", driver).click()
                   Log.WriteLine("Войти")
                   //
                   DriverFindElement("(//*[@class='mail-MessageSnippet-FromText'])[1]", driver).click()
                   Log.WriteLine("Выбор письма")
                   //
                   Thread.sleep(500)
                   DriverFindElement("(//*[contains(text(), 'Подтвердить регистрацию')])[2]", driver).click()
                   Log.WriteLine("Подтвердить регистрацию")
               }
               Thread.sleep(3500)
               Log.Close()
               Stop()
               driver.quit()
           }
           catch (e:Exception){
               println(e.printStackTrace())
           }
           finally {
               Stop1()
           }
       }


       @Test
       fun RegisterAcc5(){
           try {
               TestName = GetCurrentMethod()
               val folder = Paths.get("C:\\Log Test\\Register")
               Files.createDirectories(folder)
               val currentFile = "C:\\Log Test\\Register\\$TestName"
               val Log = Loger(currentFile, ".txt")
               val driver: WebDriver = Driver()


               Log.WriteLine("Регистрация" + VersionBrowser(driver))



               DriverNavigate(URL, driver)
               //
               DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
               Log.WriteLine("Вход")
               //
               DriverFindElement("//*[@class='login_title']", driver).click()
               Log.WriteLine("Сбросить евент")
               //
               DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz5name@yandex.ru")
               Log.WriteLine("Ввод логина")
               //
               DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
               Log.WriteLine("Ввод пароля")
               //
               DriverFindElement("//*[@class='login_btn-link']", driver).click()
               Log.WriteLine("Зарегестрироваться")
               //
               DriverFindElement("(//*[@class='form-control login-control_input ng-untouched ng-pristine ng-valid'])[3]", driver).sendKeys("nekroman911")
               Log.WriteLine("Подтвердите пароль")
               //
               DriverFindElement("//*[@class='form-ui-txt']", driver).click()
               Log.WriteLine("Я даю своё согласие")
               //
               DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
               Log.WriteLine("Зарегестрироваться")
               //
               val text: WebElement = DriverFindElement("//*[@class='h-user_name']", driver) // Вытащить текст из элемента
               var Text1:String = text.text
               Text1 = Text1.replace("\\s".toRegex(),"")
               //
               DriverFindElement("//*[@class='h-user_img']", driver).click()
               Log.WriteLine("Личный кабинет")
               //
               DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
               Log.WriteLine("Выход")
               //
               DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
               Log.WriteLine("Вход")
               //
               DriverFindElement("//*[@class='login_title']", driver).click()
               Log.WriteLine("Сбросить евент")
               //
               DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).clear()
               //
               println(Text1)
               DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("$Text1@yandex.ru")
               Log.WriteLine("Ввод логина")
               //
               //DriverFindElement("(//*[@name='password'])", driver).SendKeys("nekroman911")
               //Log.WriteLine("Ввод пароля");
               //
               DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
               Log.WriteLine("Войти на сайт")
               //
               Thread.sleep(1500)
               //
               driver.navigate().to("https://yandex.ru")
               //
               DriverFindElement("(//*[@class='home-link home-link_black_yes'])[1]", driver).click()
               Log.WriteLine("Вход на яндекс")
               //
               fun FindWindowYandex() : Boolean
               {
                   return try {

                       Thread.sleep(1500)
                       driver.findElement(By.xpath("//*[@class='passp-page-overlay']"))
                       true

                   } catch(e:Exception) {

                       false
                   }
               }

               if (!FindWindowYandex())
               {
                   DriverFindElement("//*[@name='login']", driver).sendKeys(Text1)
                   Log.WriteLine("Ввод Логина")
                   //
                   DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                   Log.WriteLine("Ввод пароля")
                   //
                   DriverFindElement("//*[@class='passport-Button-Text']", driver).click()
                   Log.WriteLine("Войти")
                   //
                   DriverFindElement("(//*[@class='mail-MessageSnippet-FromText'])[1]", driver).click()
                   Log.WriteLine("Выбор письма")
                   //
                   Thread.sleep(500)
                   DriverFindElement("(//*[contains(text(), 'Подтвердить регистрацию')])[2]", driver).click()
                   Log.WriteLine("Подтвердить регистрацию")
               }

               if (FindWindowYandex()){

                   DriverFindElement("//*[@name='login']", driver).sendKeys(Text1)
                   Log.WriteLine("Ввод Логина")
                   //
                   DriverFindElement("//*[@class='passp-login-form']//button[1]", driver).click()
                   Log.WriteLine("Войти")
                   //
                   DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                   Log.WriteLine("Ввод пароля")
                   //
                   DriverFindElement("//*[@class='passp-password-form']//button[1]", driver).click()
                   Log.WriteLine("Войти")
                   //
                   DriverFindElement("(//*[@class='mail-MessageSnippet-FromText'])[1]", driver).click()
                   Log.WriteLine("Выбор письма")
                   //
                   Thread.sleep(500)
                   DriverFindElement("(//*[contains(text(), 'Подтвердить регистрацию')])[2]", driver).click()
                   Log.WriteLine("Подтвердить регистрацию")
               }
               Thread.sleep(3500)
               Log.Close()
               Stop()
               driver.quit()
           }
           catch (e:Exception){
               println(e.printStackTrace())
           }
           finally {
               Stop1()
           }
       }


       @Test
       fun RegisterAcc6(){
           try {
               TestName = GetCurrentMethod()
               val folder = Paths.get("C:\\Log Test\\Register")
               Files.createDirectories(folder)
               val currentFile = "C:\\Log Test\\Register\\$TestName"
               val Log = Loger(currentFile, ".txt")
               val driver: WebDriver = Driver()


               Log.WriteLine("Регистрация" + VersionBrowser(driver))



               DriverNavigate(URL, driver)
               //
               DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
               Log.WriteLine("Вход")
               //
               DriverFindElement("//*[@class='login_title']", driver).click()
               Log.WriteLine("Сбросить евент")
               //
               DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("avtor2.avtorproject@yandex.ru")
               Log.WriteLine("Ввод логина")
               //
               DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
               Log.WriteLine("Ввод пароля")
               //
               DriverFindElement("//*[@class='login_btn-link']", driver).click()
               Log.WriteLine("Зарегестрироваться")
               //
               DriverFindElement("(//*[@class='form-control login-control_input ng-untouched ng-pristine ng-valid'])[3]", driver).sendKeys("nekroman911")
               Log.WriteLine("Подтвердите пароль")
               //
               DriverFindElement("//*[@class='form-ui-txt']", driver).click()
               Log.WriteLine("Я даю своё согласие")
               //
               DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
               Log.WriteLine("Зарегестрироваться")
               //
               val text: WebElement = DriverFindElement("//*[@class='h-user_name']", driver) // Вытащить текст из элемента
               var Text1:String = text.text
               Text1 = Text1.replace("\\s".toRegex(),"")
               //
               DriverFindElement("//*[@class='h-user_img']", driver).click()
               Log.WriteLine("Личный кабинет")
               //
               DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
               Log.WriteLine("Выход")
               //
               DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
               Log.WriteLine("Вход")
               //
               DriverFindElement("//*[@class='login_title']", driver).click()
               Log.WriteLine("Сбросить евент")
               //
               DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).clear()
               //
               println(Text1)
               DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("$Text1@yandex.ru")
               Log.WriteLine("Ввод логина")
               //
               //DriverFindElement("(//*[@name='password'])", driver).SendKeys("nekroman911")
               //Log.WriteLine("Ввод пароля");
               //
               DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
               Log.WriteLine("Войти на сайт")
               //
               Thread.sleep(1500)
               //
               driver.navigate().to("https://yandex.ru")
               //
               DriverFindElement("(//*[@class='home-link home-link_black_yes'])[1]", driver).click()
               Log.WriteLine("Вход на яндекс")
               //
               fun FindWindowYandex() : Boolean
               {
                   return try {

                       Thread.sleep(1500)
                       driver.findElement(By.xpath("//*[@class='passp-page-overlay']"))
                       true

                   } catch(e:Exception) {

                       false
                   }
               }

               if (!FindWindowYandex())
               {
                   DriverFindElement("//*[@name='login']", driver).sendKeys(Text1)
                   Log.WriteLine("Ввод Логина")
                   //
                   DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                   Log.WriteLine("Ввод пароля")
                   //
                   DriverFindElement("//*[@class='passport-Button-Text']", driver).click()
                   Log.WriteLine("Войти")
                   //
//                   DriverFindElement("//*[@class='user-account__name user-account__name_hasAccentLetter']",driver).click()
//                   Log.WriteLine("Клик на меню яндекса")
//                   //
//                   DriverFindElement("//*[@class='menu__group']//li[2]",driver).click()
//                   Log.WriteLine("Почта")
                   //
                   DriverFindElement("(//*[@class='mail-MessageSnippet-FromText'])[1]", driver).click()
                   Log.WriteLine("Выбор письма")
                   //
                   Thread.sleep(500)
                   DriverFindElement("(//*[contains(text(), 'Подтвердить регистрацию')])[2]", driver).click()
                   Log.WriteLine("Подтвердить регистрацию")
               }

               if (FindWindowYandex()){

                   DriverFindElement("//*[@name='login']", driver).sendKeys(Text1)
                   Log.WriteLine("Ввод Логина")
                   //
                   DriverFindElement("//*[@class='passp-login-form']//button[1]", driver).click()
                   Log.WriteLine("Войти")
                   //
                   DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                   Log.WriteLine("Ввод пароля")
                   //
                   DriverFindElement("//*[@class='passp-password-form']//button[1]", driver).click()
                   Log.WriteLine("Войти")
                   //
//                   DriverFindElement("//*[@class='user-account__name user-account__name_hasAccentLetter']",driver).click()
//                   Log.WriteLine("Клик на меню яндекса")
//                   //
//                   DriverFindElement("//*[@class='menu__group']//li[2]",driver).click()
//                   Log.WriteLine("Почта")
                   //
                   DriverFindElement("(//*[@class='mail-MessageSnippet-FromText'])[1]", driver).click()
                   Log.WriteLine("Выбор письма")
                   //
                   Thread.sleep(500)
                   DriverFindElement("(//*[contains(text(), 'Подтвердить регистрацию')])[2]", driver).click()
                   Log.WriteLine("Подтвердить регистрацию")
               }
               Thread.sleep(3500)
               Log.Close()
               Stop()
               driver.quit()
           }
           catch (e:Exception){
               println(e.printStackTrace())
           }
           finally {
               Stop1()
           }
       }

       @Test
       fun RegisterAcc7(){
           try {
               TestName = GetCurrentMethod()
               val folder = Paths.get("C:\\Log Test\\Register")
               Files.createDirectories(folder)
               val currentFile = "C:\\Log Test\\Register\\$TestName"
               val Log = Loger(currentFile, ".txt")
               val driver: WebDriver = Driver()


               Log.WriteLine("Регистрация" + VersionBrowser(driver))



               DriverNavigate(URL, driver)
               //
               DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
               Log.WriteLine("Вход")
               //
               DriverFindElement("//*[@class='login_title']", driver).click()
               Log.WriteLine("Сбросить евент")
               //
               DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("avtor2313666@yandex.ru")
               Log.WriteLine("Ввод логина")
               //
               DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
               Log.WriteLine("Ввод пароля")
               //
               DriverFindElement("//*[@class='login_btn-link']", driver).click()
               Log.WriteLine("Зарегестрироваться")
               //
               DriverFindElement("(//*[@class='form-control login-control_input ng-untouched ng-pristine ng-valid'])[3]", driver).sendKeys("nekroman911")
               Log.WriteLine("Подтвердите пароль")
               //
               DriverFindElement("//*[@class='form-ui-txt']", driver).click()
               Log.WriteLine("Я даю своё согласие")
               //
               DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
               Log.WriteLine("Зарегестрироваться")
               //
               val text: WebElement = DriverFindElement("//*[@class='h-user_name']", driver) // Вытащить текст из элемента
               var Text1:String = text.text
               Text1 = Text1.replace("\\s".toRegex(),"")
               //
               DriverFindElement("//*[@class='h-user_img']", driver).click()
               Log.WriteLine("Личный кабинет")
               //
               DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
               Log.WriteLine("Выход")
               //
               DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
               Log.WriteLine("Вход")
               //
               DriverFindElement("//*[@class='login_title']", driver).click()
               Log.WriteLine("Сбросить евент")
               //
               DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).clear()
               //
               println(Text1)
               DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("$Text1@yandex.ru")
               Log.WriteLine("Ввод логина")
               //
               //DriverFindElement("(//*[@name='password'])", driver).SendKeys("nekroman911")
               //Log.WriteLine("Ввод пароля");
               //
               DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
               Log.WriteLine("Войти на сайт")
               //
               Thread.sleep(1500)
               //
               driver.navigate().to("https://yandex.ru")
               //
               DriverFindElement("(//*[@class='home-link home-link_black_yes'])[1]", driver).click()
               Log.WriteLine("Вход на яндекс")
               //
               fun FindWindowYandex() : Boolean
               {
                   return try {

                       Thread.sleep(1500)
                       driver.findElement(By.xpath("//*[@class='passp-page-overlay']"))
                       true

                   } catch(e:Exception) {

                       false
                   }
               }

               if (!FindWindowYandex())
               {
                   DriverFindElement("//*[@name='login']", driver).sendKeys(Text1)
                   Log.WriteLine("Ввод Логина")
                   //
                   DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                   Log.WriteLine("Ввод пароля")
                   //
                   DriverFindElement("//*[@class='passport-Button-Text']", driver).click()
                   Log.WriteLine("Войти")
                   //
//                   DriverFindElement("//*[@class='user-account__name user-account__name_hasAccentLetter']",driver).click()
//                   Log.WriteLine("Клик на меню яндекса")
//                   //
//                   DriverFindElement("//*[@class='menu__group']//li[2]",driver).click()
//                   Log.WriteLine("Почта")
                   //
                   DriverFindElement("(//*[@class='mail-MessageSnippet-FromText'])[1]", driver).click()
                   Log.WriteLine("Выбор письма")
                   //
                   Thread.sleep(500)
                   DriverFindElement("(//*[contains(text(), 'Подтвердить регистрацию')])[2]", driver).click()
                   Log.WriteLine("Подтвердить регистрацию")
               }

               if (FindWindowYandex()){

                   DriverFindElement("//*[@name='login']", driver).sendKeys(Text1)
                   Log.WriteLine("Ввод Логина")
                   //
                   DriverFindElement("//*[@class='passp-login-form']//button[1]", driver).click()
                   Log.WriteLine("Войти")
                   //
                   DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                   Log.WriteLine("Ввод пароля")
                   //
                   DriverFindElement("//*[@class='passp-password-form']//button[1]", driver).click()
                   Log.WriteLine("Войти")
                   //
//                   DriverFindElement("//*[@class='user-account__name user-account__name_hasAccentLetter']",driver).click()
//                   Log.WriteLine("Клик на меню яндекса")
//                   //
//                   DriverFindElement("//*[@class='menu__group']//li[2]",driver).click()
//                   Log.WriteLine("Почта")
                   //
                   DriverFindElement("(//*[@class='mail-MessageSnippet-FromText'])[1]", driver).click()
                   Log.WriteLine("Выбор письма")
                   //
                   Thread.sleep(500)
                   DriverFindElement("(//*[contains(text(), 'Подтвердить регистрацию')])[2]", driver).click()
                   Log.WriteLine("Подтвердить регистрацию")
               }
               Thread.sleep(3500)
               Log.Close()
               Stop()
               driver.quit()
           }
           catch (e:Exception){
               println(e.printStackTrace())
           }
           finally {
               Stop1()
           }
       }

   }

}