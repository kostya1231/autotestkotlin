
import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.interactions.Actions
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

class TestMailer1 : SetupParallel() {

    @Test
    fun SettingsNoticeSendMail1(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Mailer")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Mailer\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("User-Настройки-Уведомлений-Получать новости Planeta.ru" + VersionBrowser(driver))






            Avtorization_Uz2name(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[6]", driver).click()
            Log.WriteLine("Настройки")
            //
            DriverFindElement("//*[@class='profile-nav']//li[3]", driver).click()
            Log.WriteLine("Настройки уведомлений")
            //
            val element1 = DriverFindElement("//*[contains(text(), 'Получать новости Planeta.ru')]", driver)
            Log.WriteLine("Получать новости Planeta.ru-Отключить")
            //
            val actions =  Actions(driver)
            actions.moveToElement(element1).click()
            actions.perform()
            //
            val element2 = DriverFindElement("(//*[@class='form-ui-val'])[2]", driver)
            Log.WriteLine("Получать новости проектов, которые я поддержал-Отключить")
            //
            val actions1 =  Actions(driver)
            actions1.moveToElement(element2).click()
            actions1.perform()
            //
            val element3 = DriverFindElement("(//*[@class='form-ui-val'])[3]", driver)
            Log.WriteLine("Получать статистику созданных мной проектов-Отключить")
            //
            val actions2 =  Actions(driver)
            actions2.moveToElement(element3).click()
            actions2.perform()
            //
            val element4 = DriverFindElement("(//*[@class='form-ui-val'])[4]", driver)
            Log.WriteLine("Получать советы и рекомендации по созданию проекта от Planeta.ru-Отключить")
            //
            val actions3 =  Actions(driver)
            actions3.moveToElement(element4).click()
            actions3.perform()
            //
            val element5 = DriverFindElement("(//*[@class='form-ui-val'])[1]", driver)
            Log.WriteLine("Получать новости Planeta.ru-Включить")
            //
            val actions4 =  Actions(driver)
            actions4.moveToElement(element5).click()
            actions4.perform()
            //
            DriverFindElement("//*[@class='btn btn-nd-primary']", driver).click()
            Log.WriteLine("Сохранить изменения")
            //
            Thread.sleep(500)
            //
            DriverNavigate(Mailer, driver)
            //
            DriverFindElement("//*[@id='name']", driver).sendKeys("Новая Тестовая почта")
            Log.WriteLine("Название рассылки")
            //
            DriverFindElement("//*[@id='subject']", driver).sendKeys("Тест")
            Log.WriteLine("Тема")
            //
            DriverFindElement("(//*[@class='select2-selection__arrow'])[1]", driver).click()
            Log.WriteLine("Ссылки")
            //
            DriverFindElement("(//*[@class='select2-search__field'])", driver).sendKeys("Новая Тестовая почта")
            Log.WriteLine("Список рассылки")
            //
            Thread.sleep(200)
            DriverFindElement("//*[contains(text(), 'Новая тестовая почта')]", driver).click()
            Log.WriteLine("Выбор тестовой почты")
            //
            DriverFindElement("(//*[@class='select2-selection__arrow'])[2]", driver).click()
            Log.WriteLine("Шаблон")
            //
            DriverFindElement("(//*[@class='select2-search__field'])", driver).sendKeys("1")
            //
            DriverFindElement("//*[contains(text(), '061015_дайджест')]", driver).click()
            Log.WriteLine("061015_дайджест")
            //
            DriverFindElement("(//*[@class='btn btn-success'])", driver).click()
            Log.WriteLine("Взять за основу")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[2]", driver).click()
            Log.WriteLine("Отправить")
            //
            DriverFindElement("//*[contains(text(), 'Адресаты рассылки')]", driver)
            Log.WriteLine("Адресаты рассылки")
            //
            DriverFindElement("//*[@name='confirmed']", driver).click()
            Log.WriteLine("Подтвердить и отправить")
            //
            DriverFindElement("//*[contains(text(), 'её статистику')]", driver).click()
            Log.WriteLine("её статистику")
            //
            Thread.sleep(3500)
            //
            DriverNavigate("https://yandex.ru", driver)
            //
            DriverFindElement("(//*[@class='home-link home-link_black_yes'])[1]", driver).click()
            Log.WriteLine("Вход на яндекс")
            //
            fun FindWindowYandex() : Boolean
            {
                return try {
                    Thread.sleep(1500)
                    driver.findElement(By.xpath("//*[@class='passp-page-overlay']"))
                    true
                } catch(e:Exception) {
                    false
                }
            }

            if (!FindWindowYandex())
            {
                DriverFindElement("//*[@name='login']", driver).sendKeys("uz2name")
                Log.WriteLine("Ввод логина")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passport-Button-Text']", driver).click()
                Log.WriteLine("Войти")
                //
                DriverFindElement("//*[@class='user-account__name user-account__name_hasAccentLetter']",driver).click()
                Log.WriteLine("Клик на меню яндекса")
                //
                DriverFindElement("//*[@class='menu__group']//li[2]",driver).click()
                Log.WriteLine("Почта")
                //
                DriverFindElement("(//*[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_sender js-message-snippet-sender'])", driver).click()
                Log.WriteLine("Тест")
                //
            }

            if (FindWindowYandex())
            {
                DriverFindElement("//*[@name='login']", driver).sendKeys("uz2name")
                Log.WriteLine("Ввод Логина")
                //
                DriverFindElement("//*[@class='passp-login-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passp-password-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                //
                DriverFindElement("//*[@class='user-account__name user-account__name_hasAccentLetter']",driver).click()
                Log.WriteLine("Клик на меню яндекса")
                //
                DriverFindElement("//*[@class='menu__group']//li[2]",driver).click()
                Log.WriteLine("Почта")
                //
                DriverFindElement("(//*[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_sender js-message-snippet-sender'])", driver).click()
                Log.WriteLine("Тест")
            }
            Thread.sleep(1500)
            //
            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[6]", driver).click()
            Log.WriteLine("Настройки")
            //
            DriverFindElement("//*[@class='profile-nav']//li[3]", driver).click()
            Log.WriteLine("Настройки уведомлений")
            //
            val element6 = DriverFindElement("//*[contains(text(), 'Получать новости Planeta.ru')]", driver)
            Log.WriteLine("Получать новости Planeta.ru-Отключить")
            //
            val actions6 =  Actions(driver)
            actions6.moveToElement(element6).click()
            actions6.perform()
            //
            DriverFindElement("//*[@class='btn btn-nd-primary']", driver).click()
            Log.WriteLine("Сохранить изменения")
            //
            DriverNavigate(Mailer, driver)
            //
            DriverFindElement("//*[@id='name']", driver).sendKeys("Новая Тестовая почта")
            Log.WriteLine("Название рассылки")
            //
            DriverFindElement("//*[@id='subject']", driver).sendKeys("Тест")
            Log.WriteLine("Тема")
            //
            DriverFindElement("(//*[@class='select2-selection__arrow'])[1]", driver).click()
            Log.WriteLine("Ссылки")
            //
            DriverFindElement("(//*[@class='select2-search__field'])", driver).sendKeys("Новая Тестовая почта")
            Log.WriteLine("Список рассылки")
            //
            DriverFindElement("//*[contains(text(), 'Новая Тестовая почта')]", driver).click()
            Log.WriteLine("Выбор тестовой почты")
            //
            DriverFindElement("(//*[@class='select2-selection__arrow'])[2]", driver).click()
            Log.WriteLine("Шаблон")
            //
            DriverFindElement("(//*[@class='select2-search__field'])", driver).sendKeys("1")
            //
            DriverFindElement("//*[contains(text(), '061015_дайджест')]", driver).click()
            Log.WriteLine("061015_дайджест")
            //
            DriverFindElement("(//*[@class='btn btn-success'])", driver).click()
            Log.WriteLine("Взять за основу")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[1]", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("(//*[@class='btn btn-primary'])[2]", driver).click()
            Log.WriteLine("Отправить")
            //
            DriverFindElement("//*[contains(text(), 'Адресаты рассылки')]", driver)
            Log.WriteLine("Адресаты рассылки")
            //
            DriverFindElement("//*[@name='confirmed']", driver).click()
            Log.WriteLine("Подтвердить и отправить")
            //
            DriverFindElement("//*[contains(text(), 'её статистику')]", driver).click()
            Log.WriteLine("её статистику")
            //
            Thread.sleep(2500)
            //
            DriverNavigate("https://yandex.ru", driver)
            //
            DriverFindElement("//*[@class='desk-notif-card__mail-title']", driver).click()
            Log.WriteLine("Вход на яндекс")
            //
            Thread.sleep(1500)
            //
            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[6]", driver).click()
            Log.WriteLine("Настройки")
            //
            DriverFindElement("//*[@class='profile-nav']//li[3]", driver).click()
            Log.WriteLine("Настройки уведомлений")
            //
            val element8 = DriverFindElement("//*[contains(text(), 'Получать новости Planeta.ru')]", driver)
            Log.WriteLine("Получать новости Planeta.ru-Отключить")
            //
            val actions8 =  Actions(driver)
            actions8.moveToElement(element8).click()
            actions8.perform()
            //
            val element9 = DriverFindElement("(//*[@class='form-ui-val'])[2]", driver)
            Log.WriteLine("Получать новости проектов, которые я поддержал-Отключить")
            //
            val actions9 =  Actions(driver)
            actions9.moveToElement(element9).click()
            actions9.perform()
            //
            val element10 = DriverFindElement("(//*[@class='form-ui-val'])[3]", driver)
            Log.WriteLine("Получать статистику созданных мной проектов-Отключить")
            //
            val actions10 =  Actions(driver)
            actions10.moveToElement(element10).click()
            actions10.perform()
            //
            val element11 = DriverFindElement("(//*[@class='form-ui-val'])[4]", driver)
            Log.WriteLine("Получать советы и рекомендации по созданию проекта от Planeta.ru-Отключить")
            //
            val actions11 = Actions(driver)
            actions11.moveToElement(element11).click()
            actions11.perform()
            //
            val element12 = DriverFindElement("(//*[@class='form-ui-val'])[1]", driver)
            Log.WriteLine("Получать новости Planeta.ru-Включить")
            //
            val actions12 =  Actions(driver)
            actions12.moveToElement(element12).click()
            actions12.perform()
            //
            DriverFindElement("//*[@class='btn btn-nd-primary']", driver).click()
            Log.WriteLine("Сохранить изменения")
            //
            Thread.sleep(300)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }


    @Test
    fun SettingsNoticeSendMail2(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Mailer")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Mailer\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()
            driver.manage().window().maximize()



            Log.WriteLine("User-Настройки-Уведомлений-Получать новости проектов которые я подержал" + VersionBrowser(driver))





            Avtorization_Uz3name(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[6]", driver).click()
            Log.WriteLine("Настройки")
            //
            DriverFindElement("//*[@class='profile-nav']//li[3]", driver).click()
            Log.WriteLine("Настройки уведомлений")
            //
            val element1 = DriverFindElement("//*[contains(text(), 'Получать новости Planeta.ru')]", driver)
            Log.WriteLine("Получать новости Planeta.ru-Отключить")
            //
            val actions =  Actions(driver)
            actions.moveToElement(element1).click()
            actions.perform()
            //
            val element2 = DriverFindElement("(//*[@class='form-ui-val'])[2]", driver)
            Log.WriteLine("Получать новости проектов, которые я поддержал-Отключить")
            //
            val actions1 =  Actions(driver)
            actions1.moveToElement(element2).click()
            actions1.perform()
            //
            val element3 = DriverFindElement("(//*[@class='form-ui-val'])[3]", driver)
            Log.WriteLine("Получать статистику созданных мной проектов-Отключить")
            //
            val actions2 =  Actions(driver)
            actions2.moveToElement(element3).click()
            actions2.perform()
            //
            val element4 = DriverFindElement("(//*[@class='form-ui-val'])[4]", driver)
            Log.WriteLine("Получать советы и рекомендации по созданию проекта от Planeta.ru-Отключить")
            //
            val actions3 =  Actions(driver)
            actions3.moveToElement(element4).click()
            actions3.perform()
            //
            val element5 = DriverFindElement("(//*[@class='form-ui-val'])[2]", driver)
            Log.WriteLine("Получать новости проектов, которые я поддержал - Включить")
            //
            val actions4 =  Actions(driver)
            actions4.moveToElement(element5).click()
            actions4.perform()
            //
            DriverFindElement("//*[@class='btn btn-nd-primary']", driver).click()
            Log.WriteLine("Сохранить изменения")
            //
            //
            Thread.sleep(500)
            //
            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("ЛК")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("planetaadm1n@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Project_User")
            //
            DriverFindElement("//*[contains(text(), 'Project_User')]", driver).click()
            Log.WriteLine("Выбор проекта")
            //
            Thread.sleep(10000)
            //
            DriverFindElement("//*[@class='project-tab_list']//li[3]", driver).click()
            Log.WriteLine("Новости проекта")
            //
            DriverFindElement("//*[@class='feed_cont']", driver).click()
            Log.WriteLine("Выбрать новость")
            //
            DriverFindElement("//*[@class='feed_action']//span[3]", driver).click()
            Log.WriteLine("Запустить рассылку")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Ок")
            //
            Thread.sleep(1500)
            //
            DriverNavigate("https://yandex.ru", driver)
            //
            DriverFindElement("(//*[@class='home-link home-link_black_yes'])[1]", driver).click()
            Log.WriteLine("Вход на яндекс")
            //
            fun FindWindowYandex() : Boolean
            {
                return try {
                    Thread.sleep(1500)
                    driver.findElement(By.xpath("//*[@class='passp-page-overlay']"))
                    true
                } catch(e:Exception) {
                    false
                }
            }

            if (!FindWindowYandex())
            {
                DriverFindElement("//*[@name='login']", driver).sendKeys("uz3name")
                Log.WriteLine("Ввод логина")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passport-Button-Text']", driver).click()
                Log.WriteLine("Войти")
                //
            }

            if (FindWindowYandex())
            {
                DriverFindElement("//*[@name='login']", driver).sendKeys("uz3name")
                Log.WriteLine("Ввод Логина")
                //
                DriverFindElement("//*[@class='passp-login-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passp-password-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                //
            }
            //
            Thread.sleep(1500)
            //
            //bool find()
            //{
            //    DriverFindElement("//*[contains(text(), 'Обновление проекта На спички: asd')]", driver);
            //    return true;
            //}
            ////
            //if (find())
            //{
            //    DriverFindElement("(//*[@class='mail-MessageSnippet-FromText'])[1]", driver).Click();
            //}
            ////
            //DriverFindElement("(//*[@class='daria-goto-anchor'])[4]", driver).Click();
            //Log.WriteLine("Перейти к проекту");
            ////
            //Thread.Sleep(1500);
            ////
            //driver.SwitchTo().Window(driver.WindowHandles[1]);
            ////
            //Thread.Sleep(4000);
            ////
            //DriverFindElement("//*[@class='h-user_img']", driver).Click();
            //Log.WriteLine("Открыть меню");
            ////
            //DriverFindElement("(//*[@class='h-user-menu_ico'])[8]", driver).Click();
            //Log.WriteLine("Выход");
            ////
            //DriverFindElement("//*[@class='btn btn-primary']", driver).Click();
            //Log.WriteLine("ОК");
            ////
            //DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).Click();
            //Log.WriteLine("Вход");
            ////
            //DriverFindElement("//*[@class='login_title']", driver).Click();
            //Log.WriteLine("Сбросить евент");
            ////
            //DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).SendKeys("uz2name@yandex.ru");
            //Log.WriteLine("Ввод логина");
            ////
            //DriverFindElement("(//*[@name='password'])", driver).SendKeys("nekroman911");
            //Log.WriteLine("Ввод пароля");
            ////
            //DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).Click();
            //Log.WriteLine("Войти на сайт");
            ////
            //Thread.Sleep(3500);
            ////
            //DriverFindElement("//*[@class='h-user_img']", driver).Click();
            //Log.WriteLine("Открыть меню");
            ////
            //DriverFindElement("(//*[@class='h-user-menu_name'])[6]", driver).Click();
            //Log.WriteLine("Настройки");
            ////
            //DriverFindElement("//*[@class='profile-nav']//li[3]", driver).Click();
            //Log.WriteLine("Настройки уведомлений");
            ////
            //DriverFindElement("(//*[@class='form-ui-val'])[2]", driver).Click();
            //Log.WriteLine("Получать новости проектов которые я подержал-Отключить");
            ////
            //DriverFindElement("//*[@class='btn btn-nd-primary']", driver).Click();
            //Log.WriteLine("Сохранить изменения");
            ////
            //Thread.Sleep(500);
            ////
            //DriverNavigate(URL, driver);
            ////
            //DriverFindElement("(//*[@class='project-card-item is-selected is-current'])[2]", driver).Click();
            //Log.WriteLine("Выбрать проект");
            ////

            //DriverNavigate(URL, driver);
            ////
            //DriverFindElement("//*[@class='h-user_img']", driver).Click();
            //Log.WriteLine("ЛК");
            ////
            //DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).Click();
            //Log.WriteLine("Выход");
            ////
            //DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).Click();
            //Log.WriteLine("Вход");
            ////
            //DriverFindElement("//*[@class='login_title']", driver).Click();
            //Log.WriteLine("Сбросить евент");
            ////
            //DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).SendKeys("planetaadm1n@yandex.ru");
            //Log.WriteLine("Ввод логина");
            ////
            //DriverFindElement("(//*[@name='password'])", driver).SendKeys("nekroman911");
            //Log.WriteLine("Ввод пароля");
            ////
            //DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).Click();
            //Log.WriteLine("Войти на сайт");
            ////
            //DriverFindElement("//*[@class='header_search-btn']", driver).Click();
            //Log.WriteLine("Поиск");
            ////
            //DriverFindElement("//*[@class='form-control h-search_input']", driver).SendKeys("на спички");
            //Log.WriteLine("на спички");
            ////
            //DriverFindElement("//*[contains(text(), 'На спички')]", driver).Click();
            //Log.WriteLine("На спички");
            ////
            //Thread.Sleep(4500);
            ////
            //DriverFindElement("//*[@class='project-tab_list']//li[3]", driver).Click();
            //Log.WriteLine("Новости проекта");
            ////
            //DriverFindElement("//*[@class='feed_cont']", driver).Click();
            //Log.WriteLine("Выбрать новость");
            ////
            //DriverFindElement("//*[@class='feed_action']//span[3]", driver).Click();
            //Log.WriteLine("Запустить рассылку");
            ////
            //DriverFindElement("//*[@class='btn btn-primary']", driver).Click();
            //Log.WriteLine("Ок");
            ////
            //DriverNavigate("https://yandex.ru", driver);
            ////
            //DriverFindElement("//*[@class='desk-notif-card__mail-title']", driver).Click();
            //Log.WriteLine("Вход на яндекс");
            ////

            //bool find1()
            //{
            //    driver.FindElement(By.XPath("//*[contains(text(), 'Обновление проекта На спички: asd')]"));
            //    return true;
            //}

            //for (int i = 0; i < 40; i++)
            //{
            //    try
            //    {
            //        sim.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.F5);
            //        Thread.Sleep(1000);

            //        if (find1())
            //        {
            //            driver.FindElement(By.XPath("//*[contains(text(), 'Обновление проекта На спички: asd')]"));
            //            break;
            //            throw new Exception("Ошибка");
            //        }
            //    }
            //    catch
            //    {

            //    }
            //}

            ////
            //DriverNavigate(URL, driver);
            ////
            //DriverFindElement("//*[@class='h-user_img']", driver).Click();
            //Log.WriteLine("ЛК");
            ////
            //DriverFindElement("(//*[@class='h-user-menu_ico'])[8]", driver).Click();
            //Log.WriteLine("Выход");
            ////
            //DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).Click();
            //Log.WriteLine("Вход");
            ////
            //DriverFindElement("//*[@class='login_title']", driver).Click();
            //Log.WriteLine("Сбросить евент");
            ////
            //DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).SendKeys("uz2name@yandex.ru");
            //Log.WriteLine("Ввод логина");
            ////
            //DriverFindElement("(//*[@name='password'])", driver).SendKeys("nekroman911");
            //Log.WriteLine("Ввод пароля");
            ////
            //DriverFindElement("//*[@class='h-user_img']", driver).Click();
            //Log.WriteLine("Открыть меню");
            ////
            //DriverFindElement("(//*[@class='h-user-menu_name'])[6]", driver).Click();
            //Log.WriteLine("Настройки");
            ////
            //DriverFindElement("//*[@class='profile-nav']//li[3]", driver).Click();
            //Log.WriteLine("Настройки уведомлений");
            ////
            //DriverFindElement("(//*[@class='form-ui-val'])[1]", driver).Click();
            //Log.WriteLine("Получать новости Planeta.ru-Включить");
            ////
            //DriverFindElement("(//*[@class='form-ui-val'])[2]", driver).Click();
            //Log.WriteLine("Получать новости проектов, которые я поддержал-Включить");
            ////
            //DriverFindElement("(//*[@class='form-ui-val'])[3]", driver).Click();
            //Log.WriteLine("Получать статистику созданных мной проектов-Включить");
            ////
            //DriverFindElement("(//*[@class='form-ui-val'])[4]", driver).Click();
            //Log.WriteLine("Получать советы и рекомендации по созданию проекта от Planeta.ru-Включить");
            ////
            //DriverFindElement("//*[@class='btn btn-nd-primary']", driver).Click();
            //Log.WriteLine("Сохранить изменения");
            ////
            //Log.Close();
            //Stop();
            //driver.Quit();
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun SettingsNoticeSendMail3(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Mailer")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Mailer\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            fun FindWindowYandex() : Boolean
            {
                return try {
                    Thread.sleep(1500)
                    driver.findElement(By.xpath("//*[@class='passp-page-overlay']"))
                    true
                } catch(e:Exception) {
                    false
                }
            }


            Log.WriteLine("User-Настройки-Уведомлений-Получать статистику созданных мной проектов" + VersionBrowser(driver))




            DriverNavigate("https://yandex.ru", driver)
            //
            DriverFindElement("(//*[@class='home-link home-link_black_yes'])[1]", driver).click()
            Log.WriteLine("Вход на яндекс")
            //
            if (!FindWindowYandex())
            {
                DriverFindElement("//*[@name='login']", driver).sendKeys("avtor2.avtorproject")
                Log.WriteLine("Ввод логина")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passport-Button-Text']", driver).click()
                Log.WriteLine("Войти")
            }

            if (FindWindowYandex())
            {
                DriverFindElement("//*[@name='login']", driver).sendKeys("avtor2.avtorproject")
                Log.WriteLine("Ввод Логина")
                //
                DriverFindElement("//*[@class='passp-login-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passp-password-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                //
            }
            //
            Thread.sleep(1500)
            //
            fun find() : Boolean
            {
                DriverFindElement("//*[contains(text(), 'Новые события в ваших проектах')]", driver)

                return true
            }
            //
            if (find())
            {
                try
                {
                    DriverFindElement("//*[contains(text(), 'Новые события в ваших проектах')]", driver).click()
                    DriverFindElement("(//*[contains(text(), '03:00')])[2]", driver).click()
                    Log.WriteLine("Выбор письма")
                    //
                    DriverFindElement("//*[contains(text(), 'Открыть подробную статистику')]", driver).click()
                }
                catch(e:Exception)
                {

                }
            }
            //
            Thread.sleep(1000)
            //
            Stop()
            Log.Close()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }



    @Test
    fun SettingsNoticeSendMailFooter(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Mailer")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Mailer\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("User-Footer-Подпишитесь на наши рассылки" + VersionBrowser(driver))






            Avtorization_Uz2name(driver, Log)
            //
            Thread.sleep(1500)
            //
            DriverFindElement("//*[@name='email']", driver).sendKeys("uz2name@yandex.ru")
            Log.WriteLine("Ввод адреса почты")
            //
            DriverFindElement("//*[@class='f-digest_btn btn']", driver).click()
            Log.WriteLine("Отправить письмо")
            //
            Thread.sleep(1000)
            driver.findElement(By.cssSelector(".toast-container .toast.toast-success")).getCssValue("#44b759")
            //
            DriverNavigate("https://yandex.ru", driver)
            //
            DriverFindElement("(//*[@class='home-link home-link_black_yes'])[1]", driver).click()
            Log.WriteLine("Вход на яндекс")
            //
            fun FindWindowYandex() : Boolean
            {
                return try {
                    Thread.sleep(1500)
                    driver.findElement(By.xpath("//*[@class='passp-page-overlay']"))
                    true
                } catch(e:Exception) {
                    false
                }
            }

            if (!FindWindowYandex())
            {
                DriverFindElement("//*[@name='login']", driver).sendKeys("uz2name")
                Log.WriteLine("Ввод логина")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passport-Button-Text']", driver).click()
                Log.WriteLine("Войти")
                //
                DriverFindElement("//*[@class='user-account__name user-account__name_hasAccentLetter']",driver).click()
                Log.WriteLine("Клик на меню яндекса")
                //
                DriverFindElement("//*[@class='menu__group']//li[2]",driver).click()
                Log.WriteLine("Почта")
                //
                DriverFindElement("(//*[@class='mail-MessageSnippet-FromText'])[1]", driver).click()
                Log.WriteLine("Выбор письма")
                //
                Thread.sleep(1000)
                //
                DriverFindElement("(//*[@rel='noopener noreferrer'])[2]", driver).click()
                Log.WriteLine("Перейти по Ссылке")
                //
            }

            if (FindWindowYandex())
            {
                DriverFindElement("//*[@name='login']", driver).sendKeys("uz2name")
                Log.WriteLine("Ввод Логина")
                //
                DriverFindElement("//*[@class='passp-login-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passp-password-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                //
                DriverFindElement("(//*[@class='mail-MessageSnippet-FromText'])[1]", driver).click()
                Log.WriteLine("Выбор письма")
                //
                Thread.sleep(1000)
                //
                DriverFindElement("(//*[@rel='noopener noreferrer'])[2]", driver).click()
                Log.WriteLine("Перейти по Ссылке")
                //
            }
            Thread.sleep(500)
            //
            val tabs = ArrayList(driver.windowHandles)
            driver.switchTo().window(tabs[1])
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }



}