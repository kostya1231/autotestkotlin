import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import java.nio.file.Files
import java.nio.file.Paths

class RazdelyUserOrders : SetupParallel() {

    @Test
    fun MyOrders(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("User-Мои Заказы" + VersionBrowser(driver))





            Avtorization_Uz4name(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[3]", driver).click()
            Log.WriteLine("Мои заказы")
            Thread.sleep(2500)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun MyOrdersCancelPurchases(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("User-Мои Заказы-Отменить покупку" + VersionBrowser(driver))





            Avtorization_Uz4name(driver, Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_name'])[3]", driver).click()
            Log.WriteLine("Мои заказы")
            //
        //    DriverFindElement("(//*[@class='profile-nav_link'])[1]", driver).click()
            Log.WriteLine("Выбрать раздел покупки")
            //
            try{
                DriverFindElement("(//*[@class='private-office-actions_name'])[1]", driver).click()
                Log.WriteLine("Отменить покупку")
            }catch (e:Exception){
                DriverFindElement("(//*[@class='private-office-actions_name'])[1]", driver).click()
                Log.WriteLine("Отменить покупку")
            }
            //
            DriverFindElement("//*[@class='btn btn-nd-primary']", driver).click()
            Log.WriteLine("Да")
            Thread.sleep(1000)
            driver.findElement(By.cssSelector(".toast-container .toast.toast-success")).getCssValue("#44b759")
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun RazdelySite(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Razdely")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Razdely\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("User-Авторизация + разделы" + VersionBrowser(driver));




            Avtorization_Uz4name(driver, Log)
            //
            DriverFindElement("(//*[@class='welcome-planeta-sections_list']//*[@class='welcome-planeta-sections_ico'])[1]", driver).click()
            Log.WriteLine("Школа краудфандинга")
            //
            driver.navigate().to(URL)
            //
            DriverFindElement("(//*[@class='welcome-planeta-sections_list']//*[@class='welcome-planeta-sections_ico'])[2]", driver).click()
            Log.WriteLine("Благотворительные проекты")
            //
            driver.navigate().to(URL)
            //
            DriverFindElement("(//*[@class='welcome-planeta-sections_list']//*[@class='welcome-planeta-sections_ico'])[3]", driver).click()
            Log.WriteLine("Машазин Planeta.ru")
            //
            driver.navigate().to(URL)
            //
            DriverFindElement("(//*[@class='welcome-planeta-sections_list']//*[@class='welcome-planeta-sections_ico'])[4]", driver).click()
            Log.WriteLine("Библиородина")
            //
            driver.navigate().to(URL)
            //
            DriverFindElement("(//*[@class='welcome-planeta-sections_list']//*[@class='welcome-planeta-sections_ico'])[5]", driver).click()
            Log.WriteLine("Онлайн кампус")
            //
            driver.navigate().to(URL)
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }

    }



}