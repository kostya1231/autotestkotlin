
import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import java.nio.file.Files
import java.nio.file.Paths


class Service : SetupParallel() {

    @Test
    fun AvtorizationMail() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Avtorization_Servise")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Avtorization_Servise\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("User-Авторизация-Mail" + VersionBrowser(driver))






            DriverNavigate(URL, driver)

            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")


            DriverFindElement("//*[@class='login-social_i login-social_mail']", driver).click()
            Log.WriteLine("Авторизация-Mail")

            DriverFindElement("//*[@name='Login']", driver).sendKeys("mailodmen@bk.ru")
            Log.WriteLine("Ввод Логина")

            DriverFindElement("//*[@name='Password']", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")

            DriverFindElement("//*[@class='ui-button-main']", driver).click()
            Log.WriteLine("Войти")

            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")

            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")

            Log.Close()
            Stop()
            driver.quit()

        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {

            Stop1()
        }
    }


    @Test
    fun AvtorizationVk() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Avtorization_Servise")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Avtorization_Servise\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("User-Авторизация-VK" + VersionBrowser(driver));




            DriverNavigate(URL, driver)

            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")


            DriverFindElement("//*[@class='login-social_i login-social_vk']", driver).click()
            Log.WriteLine("Авторизация-Mail")

            DriverFindElement("//*[@name='email']", driver).sendKeys("89778544307")
            Log.WriteLine("Ввод Логина")

            DriverFindElement("//*[@name='pass']", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")

            DriverFindElement("//*[@class='flat_button oauth_button button_wide']", driver).click()
            Log.WriteLine("Войти")

            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")

            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")

            Log.Close()
            Stop()
            driver.quit()

        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {

            Stop1()
        }
    }

    @Test
    fun AvtorizationFacebook() {
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Avtorization_Servise")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Avtorization_Servise\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("User-Авторизация-Facebok" + VersionBrowser(driver))




            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login-social_i login-social_fb']", driver).click()
            Log.WriteLine("Авторизация-FB")
            //
            DriverFindElement("//*[@name='email']", driver).sendKeys("89778665883")
            Log.WriteLine("Ввод Логина")
            //
            DriverFindElement("//*[@name='pass']", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@id='loginbutton']", driver).click()
            Log.WriteLine("Войти")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {

            Stop1()
        }
    }

    @Test
    fun RegisterAcc(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Register")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Register\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Регистрация" + VersionBrowser(driver))



            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("pain-net+" + GetRandomString() + "@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='login_btn-link']", driver).click()
            Log.WriteLine("Зарегестрироваться")
            //
            DriverFindElement("(//*[@class='form-control login-control_input ng-untouched ng-pristine ng-valid'])[3]", driver).sendKeys("nekroman911")
            Log.WriteLine("Подтвердите пароль")
            //
            DriverFindElement("//*[@class='form-ui-txt']", driver).click()
            Log.WriteLine("Я даю своё согласие")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Зарегестрироваться")
            //
            val text: WebElement = DriverFindElement("//*[@class='h-user_name']", driver) // Вытащить текст из элемента
            var Text1:String = text.text
            Text1 = Text1.replace("\\s".toRegex(),"")
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Личный кабинет")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).clear()
            //
            println(Text1)
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("$Text1@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            //DriverFindElement("(//*[@name='password'])", driver).SendKeys("nekroman911")
            //Log.WriteLine("Ввод пароля");
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            Thread.sleep(1500)
            //
            driver.navigate().to("https://yandex.ru")
            //
            DriverFindElement("(//*[@class='home-link home-link_black_yes'])[1]", driver).click()
            Log.WriteLine("Вход на яндекс")
            //
            fun FindWindowYandex() : Boolean
            {
                return try {

                    Thread.sleep(1500)
                    driver.findElement(By.xpath("//*[@class='passp-page-overlay']"))
                    true

                } catch(e:Exception) {

                    false
                }
            }

            if (!FindWindowYandex())
            {
                DriverFindElement("//*[@name='login']", driver).sendKeys("pain-net")
                Log.WriteLine("Ввод Логина")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman924")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passport-Button-Text']", driver).click()
                Log.WriteLine("Войти")
//                //
//                DriverFindElement("//*[@class='user-account__name user-account__name_hasAccentLetter']",driver).click()
//                Log.WriteLine("Клик на меню яндекса")
//                //
//                DriverFindElement("//*[@class='menu__group']//li[2]",driver).click()
//                Log.WriteLine("Почта")
                //
                DriverFindElement("(//*[@class='mail-MessageSnippet-FromText'])[1]", driver).click()
                Log.WriteLine("Выбор письма")
                //
                Thread.sleep(500)
                DriverFindElement("(//*[contains(text(), 'Подтвердить регистрацию')])[2]", driver).click()
                Log.WriteLine("Подтвердить регистрацию")
            }

            if (FindWindowYandex()){

                DriverFindElement("//*[@name='login']", driver).sendKeys("pain-net")
                Log.WriteLine("Ввод Логина")
                //
                DriverFindElement("//*[@class='passp-login-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman924")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passp-password-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                //
//                DriverFindElement("//*[@class='user-account__name user-account__name_hasAccentLetter']",driver).click()
//                Log.WriteLine("Клик на меню яндекса")
//                //
//                DriverFindElement("//*[@class='menu__group']//li[2]",driver).click()
//                Log.WriteLine("Почта")
                //
                DriverFindElement("(//*[@class='mail-MessageSnippet-FromText'])[1]", driver).click()
                Log.WriteLine("Выбор письма")
                //
                Thread.sleep(500)
                DriverFindElement("(//*[contains(text(), 'Подтвердить регистрацию')])[2]", driver).click()
                Log.WriteLine("Подтвердить регистрацию")
            }
            Thread.sleep(3500)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }


    @Test
    fun AvtorizationOdnoklassniky() {

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Avtorization_Servise")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Avtorization_Servise\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()





            Log.WriteLine("User-Авторизация-Odnoklasniky" + VersionBrowser(driver))





            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login-social_i login-social_ok']", driver).click()
            Log.WriteLine("Авторизация-Odnoklasniky")
            //
            DriverFindElement("//*[@name='fr.email']", driver).sendKeys("89778544307")
            Log.WriteLine("Ввод Логина")
            //
            DriverFindElement("//*[@name='fr.password']", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='button-pro __wide form-actions_yes']", driver).click()
            Log.WriteLine("Войти")
            //
            //DriverFindElement("//*[@class='h-user_img']", driver).Click();
            //Log.WriteLine("Открыть меню");
            ////
            //DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).Click();
            //Log.WriteLine("Выход");
            //
            Thread.sleep(500)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {

            Stop1()
        }
    }


    @Test
    fun AvtorizationYandex() {

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Avtorization_Servise")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Avtorization_Servise\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("User-Авторизация-Yandex" + VersionBrowser(driver))





            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход");
            //
            DriverFindElement("//*[@class='login-social_i login-social_ya']", driver).click()
            Log.WriteLine("Авторизация-Yandex")
            //
            fun FindWindowYandex() : Boolean
            {
                return try {

                    Thread.sleep(1500)
                    driver.findElement(By.xpath("//*[@class='passp-page-overlay']"))
                    true

                } catch(e:Exception) {

                    false
                }
            }

            if (!FindWindowYandex())
            {
                DriverFindElement("//*[@name='login']", driver).sendKeys("uz4name")
                Log.WriteLine("Ввод Логина")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passport-Button-Text']", driver).click()
                Log.WriteLine("Войти")
                //
                DriverFindElement("//*[@class='h-user_img']", driver).click()
                Log.WriteLine("Открыть меню")
                //
                DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
                Log.WriteLine("Выход")
                //
            }

            if (FindWindowYandex()){

                DriverFindElement("//*[@name='login']", driver).sendKeys("uz4name")
                Log.WriteLine("Ввод Логина")
                //
                DriverFindElement("//*[@class='passp-login-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passp-password-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                //
                DriverFindElement("//*[@class='h-user_img']", driver).click()
                Log.WriteLine("Открыть меню")
                //
                DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
                Log.WriteLine("Выход")
                //
            }
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }


    @Test
    fun AvtorizationForgotYourPassword() {

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Avtorization_Servise")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Avtorization_Servise\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("User-Авторизация-Забыли пароль" + VersionBrowser(driver))





            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("(//*[@class='login_btn-link'])[2]", driver).click()
            Log.WriteLine("Забыли пароль")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz2name@yandex.ru")
            Log.WriteLine("Введите ваш email")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Восстановить")
            //
            Thread.sleep(3000)
            //
            DriverNavigate("https://yandex.ru", driver)
            Log.WriteLine("Вход на яндекс")
            //
            DriverFindElement("(//*[@class='home-link home-link_black_yes'])[1]", driver).click()
            Log.WriteLine("Вход на яндекс")
            //


            fun FindWindowYandex() : Boolean
            {
                return try {

                    Thread.sleep(1500)
                    driver.findElement(By.xpath("//*[@class='passp-page-overlay']"))
                    true

                } catch(e:Exception) {

                    false
                }
            }

            if (!FindWindowYandex())
            {
                DriverFindElement("//*[@name='login']", driver).sendKeys("uz2name")
                Log.WriteLine("Ввод логина")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passport-Button-Text']", driver).click()
                Log.WriteLine("Войти")
                //
//                DriverFindElement("//*[@class='user-account__name user-account__name_hasAccentLetter']",driver).click()
//                Log.WriteLine("Клик на меню яндекса")
//                //
//                DriverFindElement("//*[@class='menu__group']//li[2]",driver).click()
//                Log.WriteLine("Почта")
                //
                DriverFindElement("(//*[@class='mail-MessageSnippet-FromText'])[1]", driver).click()
                Log.WriteLine("Выбор письма")
                //
                DriverFindElement("(//*[@rel='noopener noreferrer'])[2]", driver).click()
                Log.WriteLine("Перейти по Ссылке")
                //
            }

            if (FindWindowYandex())
            {
                DriverFindElement("//*[@name='login']", driver).sendKeys("uz2name")
                Log.WriteLine("Ввод Логина")
                //
                DriverFindElement("//*[@class='passp-login-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passp-password-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                //
//                DriverFindElement("//*[@class='user-account__name user-account__name_hasAccentLetter']",driver).click()
//                Log.WriteLine("Клик на меню яндекса")
//                //
//                DriverFindElement("//*[@class='menu__group']//li[2]",driver).click()
//                Log.WriteLine("Почта")
                //
                DriverFindElement("(//*[@class='mail-MessageSnippet-FromText'])[1]", driver).click()
                Log.WriteLine("Выбор письма")
                //
                DriverFindElement("(//*[@rel='noopener noreferrer'])[2]", driver).click()
                Log.WriteLine("Перейти по Ссылке")
                //
            }



            //
            driver.switchTo().window("").windowHandle[1]
            //Проверка валидации полей
            DriverFindElement("//*[@class='login_form ng-untouched ng-pristine ng-valid']//button[1]", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("//*[@class='ng-star-inserted']", driver)
            //
            DriverFindElement("//*[@name='password']", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод  пароля")
            //
            DriverFindElement("//*[@name='confirmation']", driver).sendKeys("nekroman911111")
            Log.WriteLine("Ввод нового неправильного пароля ещё раз")
            //
            DriverFindElement("//*[@class='login_form ng-untouched ng-pristine ng-valid']//button[1]", driver).click()
            Log.WriteLine("Сохранить");
            //
            DriverFindElement("//*[contains(text(), 'Пароли не совпадают')]", driver);
            //
            DriverFindElement("//*[@name='password']", driver).clear()
            //
            DriverFindElement("//*[@name='confirmation']", driver).clear()
            //
            DriverFindElement("//*[@name='password']", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод нового пароля")
            //
            DriverFindElement("//*[@name='confirmation']", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод нового пароля ещё раз")
            //
            DriverFindElement("//*[@class='login_form ng-untouched ng-pristine ng-valid']//button[1]", driver).click()
            Log.WriteLine("Сохранить")
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz2name@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            Thread.sleep(500)
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[7]", driver).click()
            Log.WriteLine("Выход")
            //
            Log.Close()
            Stop()
            driver.quit()

        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {

            Stop1()
        }
    }

    @Test
    fun AvtorizationWindowValidation() {

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Avtorization_Servise")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Avtorization_Servise\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()




            Log.WriteLine("User-Авторизация-Проверка полей" + VersionBrowser(driver))




            Avtorization(driver, Log) // Шаблон авторизации
            //
            DriverFindElement("//*[contains(text(), 'Неверный логин или пароль')]", driver).click()
            Log.WriteLine("Неверный логин или пароль")
            //
            DriverFindElement("//*[@class='login_btn-link']", driver).click()
            Log.WriteLine("Зарегестрироваться")
            //
            DriverFindElement("//*[@class='form-ui-txt']", driver).click()
            Log.WriteLine("Я даю своё согласие")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Зарегестрироваться")
            //
            DriverFindElement("//*[contains(text(), 'Пароли не совпадают')]", driver).click()
            Log.WriteLine("Пароли не совпадают")
            //
            DriverFindElement("//*[@class='form-ui-txt']", driver).click()
            Log.WriteLine("Я даю своё согласие-отключить")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Зарегестрироваться")
            //
            DriverFindElement("//*[contains(text(), ' Поле является обязательным для завершения регистрации ')]", driver).click()
            Log.WriteLine("Поле является обязательным для завершения регистрации")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {

            Stop1()
        }
    }





}