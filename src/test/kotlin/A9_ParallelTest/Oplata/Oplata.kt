
import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.interactions.Actions
import java.nio.file.Files
import java.nio.file.Paths


class Oplata : SetupParallel() {

    @Test
    fun SupportedProjectsOplata1(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("Поддержанные проекты Оплата сбер" + VersionBrowser(driver))





            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(500)
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[1]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary action-card_btn js-navigate-to-purchase'])[1]", driver).click()
            Log.WriteLine("Оплатить")
            //
           try {
               DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
               Log.WriteLine("Даю свое согласие")
           }catch (e:Exception){
               DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
               Log.WriteLine("Даю свое согласие")
           }
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
            Log.WriteLine("Оплатить покупку")
            //
            //DriverFindElement("(//*[@id='email'])[1]", driver).SendKeys("pain-net@yandex.ru");
            //Log.WriteLine("Сбер ввод email");
            ////
            DriverFindElement("(//*[@id='cardnumber'])[1]", driver).sendKeys("4111 1111 1111 1111")
            Log.WriteLine("Ваш банк ввод номера карты")
            //
            DriverFindElement("(//*[@name='expdate'])[1]", driver).sendKeys("1219")
            Log.WriteLine("Ваш банк ввод месяц год")
            //
            DriverFindElement("(//*[@id='cvc'])[1]", driver).sendKeys("123")
            Log.WriteLine("Ваш банк ввод CVC2")
            //
            DriverFindElement("(//*[@class='sbersafe-pay-button'])", driver).click()
            Log.WriteLine("Оплатить")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("12345678")
            Log.WriteLine("password")
            //
            DriverFindElement("(//*[@value='Submit'])", driver).click()
            Log.WriteLine("Submit")
            //
            DriverFindElement("(//*[@class='form-ui-txt'])[10]", driver).click()
            Log.WriteLine("Оценка качества сервиса 10")
            //
            DriverFindElement("(//*[@class='form-ui-txt'])[11]", driver).click()
            Log.WriteLine("я хочу принять участие в детальном опросе")
            //
            DriverFindElement("(//*[@class='quality-polling_btn'])", driver).click()
            Log.WriteLine("Отправить")
            //
            for (i in 1..3) {

                DriverFindElement("(//*[@class='project-card-item'])[" + i.toString() + "]", driver)
            }
            Log.WriteLine("Проверка наличия 3-х проектов на странице успешной оплаты")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }


    @Test
    fun SupportedProjectsOplata2(){
        try
        {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            fun find() : Boolean
            {
                return try {

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver)
                    true

                } catch(e:Exception) {

                    false
                }
            }



            Log.WriteLine("Поддержанные проекты Оплата карты других банков-CloudPayments" + VersionBrowser(driver))





            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(500)
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[1]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary action-card_btn js-navigate-to-purchase'])[1]", driver).click()
            Log.WriteLine("Оплатить")
            //
            for ( i in 1..40)
            {
                if (find())
                {
                    try {
                        DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
                        Log.WriteLine("Даю свое согласие на оформление заказа")
                    }catch (e:Exception){
                        DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                        Log.WriteLine("Даю свое согласие на оформление заказа")
                    }
                    break
                }
            }
            //
            DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver).click()
            Log.WriteLine("Карты других банков")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary project-payment_btn'])[1]", driver).click()
            Log.WriteLine("Оплатить покупку")
            //
            DriverFindElement("//*[@id='buttonsContainer']//button[1]", driver).click()
            Log.WriteLine("Оплата картой")
            //
            driver.switchTo().frame(0)
            DriverFindElement("(//*[@class='row'])[1]//input",driver).click()
            Log.WriteLine("клик по строке")
            DriverFindElement("(//*[@class='row'])[1]//input",driver).sendKeys("4111 1111 1111 1111")
            //
            DriverFindElement("(//*[@id='inputMonth'])[1]",driver).sendKeys("12")
            //
            DriverFindElement("(//*[@id='inputYear'])[1]",driver).sendKeys("19")
            //
            DriverFindElement("(//*[@data-mask='000'])[1]",driver).sendKeys("123")
            //
            DriverFindElement("(//*[@id='cardHolder'])[1]",driver).sendKeys("QWERTY")
            //
            DriverFindElement("(//*[@class='button'])[1]",driver).click()
            DriverFindElement("(//*[@class='button'])[2]",driver).click()
            Log.WriteLine("OK")
            driver.switchTo().defaultContent()
            //
            Log.WriteLine("Финальная страница")
            Thread.sleep(3000)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }


    @Test
    fun SupportedProjectsOplata3(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            fun find() : Boolean
            {
                return try {

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver)
                    true

                } catch(e:Exception) {

                    false
                }
            }


            Log.WriteLine("Поддержанные проекты Другие способы оплаты,Альфа-банк" + VersionBrowser(driver))






            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(300)
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[1]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary action-card_btn js-navigate-to-purchase'])[1]", driver).click()
            Log.WriteLine("Оплатить")
            //
            for (i in 0..39) {
                if (find()) {
                    try {
                        DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
                        Log.WriteLine("Даю свое согласие на оформление заказа")
                    }catch (e:Exception){
                        DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                        Log.WriteLine("Даю свое согласие на оформление заказа")
                    }
                    break
                }
            }
            //
            DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[3]", driver).click()
            Log.WriteLine("Другие способы оплаты")
            //
            DriverFindElement("(//*[@class='project-payment-type_method-logo'])[2]", driver).click()
            Log.WriteLine("Альфа-Банк")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary project-payment_btn']", driver).click()
            Log.WriteLine("Оплатить")
            Thread.sleep(1500)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

   @Test
   fun SupportedProjectsOplata4(){

       try {
           TestName = GetCurrentMethod()
           val folder = Paths.get("C:\\Log Test\\User")
           Files.createDirectories(folder)
           val currentFile = "C:\\Log Test\\User\\$TestName"
           val Log = Loger(currentFile, ".txt")
           val driver: WebDriver = Driver()

           fun find() : Boolean
           {
               return try {

                   DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver)
                   true

               } catch(e:Exception) {

                   false
               }
           }



           Log.WriteLine("Поддержанные проекты Другие способы оплаты,Промсвязь-Банк" + VersionBrowser(driver))




           Avtorization_Uz4name(driver, Log)
           //
           Thread.sleep(300)
           DriverFindElement("//*[@class='header_search-btn']", driver).click()
           Log.WriteLine("Поиск")
           //
           DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
           Log.WriteLine("Название проекта")
           //
           DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[1]", driver).click()
           Log.WriteLine("Выбрать проект")
           //
           DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
           Log.WriteLine("Поддержать проект")
           //
           DriverFindElement("(//*[@class='btn btn-nd-primary action-card_btn js-navigate-to-purchase'])[1]", driver).click()
           Log.WriteLine("Оплатить")
           //

           for (i in 0..39) {
               if (find()) {
                   try {
                       DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
                       Log.WriteLine("Даю свое согласие на оформление заказа")
                   }catch (e:Exception){
                       DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                       Log.WriteLine("Даю свое согласие на оформление заказа")
                   }
                   break
               }
           }


           //
           DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[3]", driver).click()
           Log.WriteLine("Другие способы оплаты")
           //
           DriverFindElement("(//*[@class='project-payment-type_method-logo'])[3]", driver).click()
           Log.WriteLine("Промсвязь банк")
           //
           DriverFindElement("//*[@class='btn btn-nd-primary project-payment_btn']", driver).click()
           Log.WriteLine("Оплатить")
           Thread.sleep(1500)
           Log.Close()
           Stop()
           driver.quit()
       }
       catch (e:Exception){
           println(e.printStackTrace())
       } finally {

           Stop1()
       }

   }

    @Test
    fun SupportedProjectsOplata5(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            fun find() : Boolean
            {
                return try {

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver)
                    true

                } catch(e:Exception) {

                    false
                }
            }



            Log.WriteLine("Поддержанные проекты Другие способы оплаты,Тинькофф-банк" + VersionBrowser(driver))





            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(300)
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[1]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary action-card_btn js-navigate-to-purchase'])[1]", driver).click()
            Log.WriteLine("Оплатить")
            //

            for (i in 0..39) {
                if (find()) {
                    try {
                        DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
                        Log.WriteLine("Даю свое согласие на оформление заказа")
                    }catch (e:Exception){
                        DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                        Log.WriteLine("Даю свое согласие на оформление заказа")
                    }
                    break
                }
            }
            //
            DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[3]", driver).click()
            Log.WriteLine("Другие способы оплаты")
            //
            DriverFindElement("(//*[@class='project-payment-type_method-logo'])[4]", driver).click()
            Log.WriteLine("Тинькофф-банк")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary project-payment_btn']", driver).click()
            Log.WriteLine("Оплатить")
            Thread.sleep(3500)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }

    @Test
    fun  SupportedProjectsOplata6(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            fun find() : Boolean
            {
                return try {

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver)
                    true

                } catch(e:Exception) {

                    false
                }
            }

            Log.WriteLine("Поддержанные проекты Другие способы оплаты,Яндекс-деньги" + VersionBrowser(driver))




            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(300)
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[1]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary action-card_btn js-navigate-to-purchase'])[1]", driver).click()
            Log.WriteLine("Оплатить")
            //
            for (i in 0..39) {
                if (find()) {
                    try {
                        DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
                        Log.WriteLine("Даю свое согласие на оформление заказа")
                    }catch (e:Exception){
                        DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                        Log.WriteLine("Даю свое согласие на оформление заказа")
                    }
                    break
                }
            }

            //
            DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[3]", driver).click()
            Log.WriteLine("Другие способы оплаты")
            //
            DriverFindElement("(//*[@class='project-payment-type_method-logo'])[5]", driver).click()
            Log.WriteLine("Яндекс-деньги")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary project-payment_btn']", driver).click()
            Log.WriteLine("Оплатить")
            Thread.sleep(3500)
            Log.Close()
            Stop()
            driver.quit()
        } catch (e:Exception){
            println(e.printStackTrace())
        } finally
        {
            Stop1()
        }
    }


    @Test
    fun SupportedProjectsOplata7(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            fun find() : Boolean
            {
                return try {

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver)
                    true

                } catch(e:Exception) {

                    false
                }
            }


            Log.WriteLine("Поддержанные проекты Другие способы оплаты,Visa-Qiwi" + VersionBrowser(driver))




            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(500)
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[1]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary action-card_btn js-navigate-to-purchase'])[1]", driver).click()
            Log.WriteLine("Оплатить")
            //

            for (i in 0..39) {
                if (find()) {
                    try {
                        DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
                        Log.WriteLine("Даю свое согласие на оформление заказа")
                    }catch (e:Exception){
                        DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                        Log.WriteLine("Даю свое согласие на оформление заказа")
                    }
                    break
                }
            }
            //
            DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[3]", driver).click()
            Log.WriteLine("Другие способы оплаты")
            //
            DriverFindElement("(//*[@class='project-payment-type_method-logo'])[6]", driver).click()
            Log.WriteLine("Visa-Qiwi")
            //
            DriverFindElement("//*[@id='core.phone-number']", driver).click()
            Log.WriteLine("click по полю")
            //
            val actions = Actions(driver)
            val element = DriverFindElement("//*[@id='core.phone-number']",driver)
            for (i in 0..15){
                actions.keyDown(Keys.CONTROL).sendKeys(Keys.BACK_SPACE).keyUp(Keys.CONTROL).sendKeys(Keys.DELETE).perform()
                Thread.sleep(100)
            }
            //
            actions.sendKeys("89778665883").build().perform()
            //
            DriverFindElement("//*[@class='btn btn-nd-primary project-payment_btn']", driver).click()
            Log.WriteLine("Оплатить")
            Thread.sleep(5500)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }


    @Test
    fun SupportedProjectsOplata8(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            fun find() : Boolean
            {
                return try {

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver)
                    true

                } catch(e:Exception) {

                    false
                }
            }




            Log.WriteLine("Поддержанные проекты Другие способы оплаты,Web-Money" + VersionBrowser(driver))




            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(300)
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[1]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary action-card_btn js-navigate-to-purchase'])[1]", driver).click()
            Log.WriteLine("Оплатить")
            //

            for (i in 0..39) {
                if (find()) {

                    try {
                        DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
                        Log.WriteLine("Даю свое согласие на оформление заказа")
                    }catch (e:Exception){
                        DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                        Log.WriteLine("Даю свое согласие на оформление заказа")
                    }
                    break
                }
            }


            //
            DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[3]", driver).click()
            Log.WriteLine("Другие способы оплаты")
            //
            DriverFindElement("(//*[@class='project-payment-type_method-logo'])[7]", driver).click()
            Log.WriteLine("Web-money")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary project-payment_btn']", driver).click()
            Log.WriteLine("Оплатить")
            Thread.sleep(3500)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun SupportedProjectsOplata9(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()

            fun find() : Boolean
            {
                return try {

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver)
                    true

                } catch(e:Exception) {

                    false
                }
            }



            Log.WriteLine("Поддержанные проекты Другие способы оплаты,Мобильные-Платежи" + VersionBrowser(driver))






            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(300)
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[1]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary action-card_btn js-navigate-to-purchase'])[1]", driver).click()
            Log.WriteLine("Оплатить")
            //
            for (i in 0..39) {
                if (find()) {
                    try {
                        DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
                        Log.WriteLine("Даю свое согласие на оформление заказа")
                    }catch (e:Exception){
                        DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                        Log.WriteLine("Даю свое согласие на оформление заказа")
                    }
                    break
                }
            }
            //
            DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[3]", driver).click()
            Log.WriteLine("Другие способы оплаты")
            //
            DriverFindElement("(//*[@class='project-payment-type_method-logo'])[8]", driver).click()
            Log.WriteLine("Мобильные-Платежи")
            //
            DriverFindElement("//*[@id='core.phone-number']", driver).click()
            Log.WriteLine("click по полю")
            Thread.sleep(400)
            //
            val actions = Actions(driver)
            val element = DriverFindElement("//*[@id='core.phone-number']",driver)
            for (i in 0..15){
                actions.keyDown(Keys.CONTROL).sendKeys(Keys.BACK_SPACE).keyUp(Keys.CONTROL).sendKeys(Keys.DELETE).perform()
                Thread.sleep(100)
            }
            actions.sendKeys("89778665883").build().perform()
            Thread.sleep(100)
            //
            DriverFindElement("//*[@class='btn btn-nd-primary project-payment_btn']", driver).click()
            Log.WriteLine("Оплатить")
            Thread.sleep(3500)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
        //Тут анотация Тест не нужна!!

    }


    @Test
    fun SupportedProjectsOplata10(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            fun find() : Boolean
            {
                return try {

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver)
                    true

                } catch(e:Exception) {

                    false
                }
            }




            Log.WriteLine("Поддержанные проекты Другие способы оплаты,Отделения-банков" + VersionBrowser(driver))




            Avtorization_Uz4name(driver, Log)
            //
            Thread.sleep(300)
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[1]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary action-card_btn js-navigate-to-purchase'])[1]", driver).click()
            Log.WriteLine("Оплатить")
            //
            for (i in 0..39) {
                if (find()) {
                    try {
                        DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
                        Log.WriteLine("Даю свое согласие на оформление заказа")
                    }catch (e:Exception){
                        DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                        Log.WriteLine("Даю свое согласие на оформление заказа")
                    }
                    break
                }
            }

            //
            DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[3]", driver).click()
            Log.WriteLine("Другие способы оплаты")
            //
            DriverFindElement("(//*[@class='project-payment-type_method-logo'])[9]", driver).click()
            Log.WriteLine("Отделения-банков")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary project-payment_btn']", driver).click()
            Log.WriteLine("Оплатить")
            Thread.sleep(5500)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun SupportedProjectsOplata11(){

        try {
            x = 3
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            fun find() : Boolean
            {
                return try {

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver)
                    true

                } catch(e:Exception) {

                    false
                }
            }


            Log.WriteLine("Поддержанные проекты Другие способы оплаты,Терминалы оплаты" + VersionBrowser(driver))






            Avtorization_Uz4name(driver, Log)
            //
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[1]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Поддержать проект")
            // T
            DriverFindElement("(//*[@class='btn btn-nd-primary action-card_btn js-navigate-to-purchase'])[1]", driver).click()
            Log.WriteLine("Оплатить")
            //

            for (i in 0..39) {
                if (find()) {
                    try {
                        DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
                        Log.WriteLine("Даю свое согласие на оформление заказа")
                    }catch (e:Exception){
                        DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                        Log.WriteLine("Даю свое согласие на оформление заказа")
                    }
                    break
                }
            }


            //
            DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[3]", driver).click()
            Log.WriteLine("Другие способы оплаты")
            //
            DriverFindElement("(//*[@class='project-payment-type_method-logo'])[10]", driver).click()
            Log.WriteLine("Терминалы оплаты")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary project-payment_btn']", driver).click()
            Log.WriteLine("Оплатить")
            Thread.sleep(5500)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        } finally {
            Stop1()
        }
    }

    @Test
    fun SupportedProjectsOplata12(){

        try {
            x = 4
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\User")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\User\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            fun find() : Boolean
            {
                return try {

                    DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[2]", driver)
                    true

                } catch(e:Exception) {

                    false
                }
            }



            Log.WriteLine("Поддержанные проекты Другие способы оплаты,Денежные переводы" + VersionBrowser(driver))




            Avtorization_Uz4name(driver, Log)
            //
            DriverFindElement("//*[@class='header_search-btn']", driver).click()
            Log.WriteLine("Поиск")
            //
            DriverFindElement("//*[@class='form-control h-search_input']", driver).sendKeys("Project_User")
            Log.WriteLine("Название проекта")
            //
            DriverFindElement("(//*[@class='h-search-results_list']//*[@class='h-search-results_img'])[1]", driver).click()
            Log.WriteLine("Выбрать проект")
            //
            DriverFindElement("(//*[@class='button button__b button__def'])[1]", driver).click()
            Log.WriteLine("Поддержать проект")
            //
            DriverFindElement("(//*[@class='btn btn-nd-primary action-card_btn js-navigate-to-purchase'])[1]", driver).click()
            Log.WriteLine("Оплатить")
            //

            for (i in 0..39) {
                if (find()) {
                    try {
                        DriverFindElement("(//*[@class='form-ui-label'])[2]", driver).click()
                        Log.WriteLine("Даю свое согласие на оформление заказа")
                    }catch (e:Exception){
                        DriverFindElement("(//*[@class='form-ui-label'])[1]", driver).click()
                        Log.WriteLine("Даю свое согласие на оформление заказа")
                    }
                    break
                }
            }


            //
            DriverFindElement("(//*[@class='project-payment-choice_logo-img'])[3]", driver).click()
            Log.WriteLine("Другие способы оплаты")
            //
            DriverFindElement("(//*[@class='project-payment-type_method-logo'])[11]", driver).click()
            Log.WriteLine("Денежные переводы")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary project-payment_btn']", driver).click()
            Log.WriteLine("Оплатить")
            Thread.sleep(5500)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }

    }
}