import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.interactions.Actions
import java.nio.file.Files
import java.nio.file.Paths

class SchoolProjectKyrs : Setup() {

    @Test
    fun Kyrs1(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\School")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\School\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            fun video() : Boolean
            {
                return try {
                    driver.findElement(By.xpath("//*[contains(text(), 'Повторить видео')]"))
                    true
                } catch(e:Exception) {
                    false
                }

            }



            Log.WriteLine("School-курсы-Модерация проекта" + VersionBrowser(driver))






            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz4name@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            Thread.sleep(1000)
            DriverNavigate(SchoolPlaneta, driver)
            //
            DriverFindElement("//*[@class='btn school-interactive-btn']", driver).click()
            Log.WriteLine("Пройти курс")
            //
            for (i in 1..3)
            {
                Thread.sleep(1000)
                if (video())
                {
                    driver.findElement(By.xpath("//*[contains(text(), 'Повторить видео')]")).click()
                }
                if (!video())
                {
                    break
                }
            }
            //
            val Element =  DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action =  Actions(driver)
            action.dragAndDropBy(Element, 3000, 0).perform()
            //
            DriverFindElement("//*[@class='intr-btn intr-btn-default btn-block js-next-step']", driver).click()
            Log.WriteLine("нет я знаю что такое краудфандинг")
            //
            for (i in 1..3)
            {
                Thread.sleep(1000)
                if (video())
                {
                    driver.findElement(By.xpath("//*[contains(text(), 'Повторить видео')]")).click()
                }
                if (!video())
                {
                    break
                }
            }
            //
            val Element1 =   DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action11 =  Actions(driver)
            action11.dragAndDropBy(Element1, 3000, 0).perform()
            //
            DriverFindElement("//*[@class='intr-btn_in intr-btn-primary_in']", driver).click()
            Log.WriteLine("Перейти к следующему шагу")
            //
            for (i in 1..3)
            {
                Thread.sleep(1000)
                if (video())
                {
                    driver.findElement(By.xpath("//*[contains(text(), 'Повторить видео')]")).click()
                }
                if (!video())
                {
                    break
                }
            }

            val Element2 =  DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action1 =  Actions(driver)
            action1.dragAndDropBy(Element2, 3000, 0).perform()
            //
            DriverFindElement("//*[@class='intr-btn_in intr-btn-primary_in']", driver).click()
            Log.WriteLine("Перейти к следующему шагу")
            //
            for (i in 1..3)
            {
                Thread.sleep(1000)
                if (video())
                {
                    driver.findElement(By.xpath("//*[contains(text(), 'Повторить видео')]")).click()
                }
                if (!video())
                {
                    break
                }
            }
            //
            val Element3 =    DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action2 =  Actions(driver)
            action2.dragAndDropBy(Element3, 3000, 0).perform()
            //
            DriverFindElement("//*[@class='intr-btn_in intr-btn-primary_in']", driver).click()
            Log.WriteLine("Приступим")
            //
            for (i in 1..3)
            {
                Thread.sleep(1000)
                if (video())
                {
                    driver.findElement(By.xpath("//*[contains(text(), 'Повторить видео')]")).click()
                }
                if (!video())
                {
                    break
                }
            }
            //
            val Element4 =    DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action3 =  Actions(driver)
            action3.dragAndDropBy(Element4, 3000, 0).perform()
            //
            DriverFindElement("(//*[@class='form-control video-form_input'])[1]", driver).sendKeys("Project_Kyrs" + Date())
            Log.WriteLine("Название")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")
            //
            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Фото по ссылке")
            //
            DriverFindElement("//*[@name='img_url']", driver).sendKeys("http://i68.ltalk.ru/52/76/397652/32/11490432/0b07a29948f0b71f72df2bae8d4a0bb0.jpeg")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")
            //
            Thread.sleep(3500)
            //
            DriverFindElement("(//*[@class='form-control video-form_input'])[2]", driver).sendKeys("sadasdasd")
            Log.WriteLine("Краткая информация о вашем проекте")
            //
            for (i in 1..3)
            {
                Thread.sleep(1000)
                if (video())
                {
                    driver.findElement(By.xpath("//*[contains(text(), 'Повторить видео')]")).click()
                }
                if (!video())
                {
                    break
                }
            }
            //
            val Element5 =    DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action4 =  Actions(driver)
            action4.dragAndDropBy(Element5, 3000, 0).perform()
            //
            DriverFindElement("//*[@class='intr-btn_in intr-btn-primary_in']", driver).click()
            Log.WriteLine("Перейти к следующему шагу")
            //
            val Element6 =   DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action5 =  Actions(driver)
            action5.dragAndDropBy(Element6, 3000, 0).perform()
            //
            DriverFindElement("//*[@class='intr-btn_in intr-btn-primary_in']", driver).click()
            Log.WriteLine("Перейти к следующему шагу")
            //
            val Element7 =    DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action6 =  Actions(driver)
            action6.dragAndDropBy(Element7, 3000, 0).perform()
            //
            DriverFindElement("(//*[@id='interactive-step-price-desired-target-amount'])[1]", driver).click()
            Log.WriteLine("Клик по полю")
            //
            val elAc =  DriverFindElement("(//*[@id='interactive-step-price-desired-target-amount'])[1]", driver)
            Log.WriteLine("Желаемая сумма")
            //
            Thread.sleep(1000)
            val elActions = Actions(driver)
            elActions.moveToElement(elAc).sendKeys("5000000").build().perform()
            //
//            DriverFindElement("//*[@name='targetAmount']",driver).click()
//            Log.WriteLine("Клик по полю")
//            //
            DriverFindElement("//*[@class='intr-btn_in intr-btn-primary_in']", driver).click()
            Log.WriteLine("Перейти к следующему шагу")
            //
            val Element8 =     DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action7 =  Actions(driver)
            action7.dragAndDropBy(Element8, 4000, 0).perform()
            //
            Thread.sleep(1500)
            //
            DriverFindElement("(//*[@class='intr-btn_in intr-btn-primary_in'])[1]", driver).click()
            Log.WriteLine("Заполнить")
            //
            val el3 = DriverFindElement("//*[@type='file']", driver)
            el3.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            Thread.sleep(2500)
            //
            DriverFindElement("//*[@class='btn btn-primary']",driver).click()
            Log.WriteLine("Сохранить")
            //
            Thread.sleep(1500)
            DriverFindElement("(//*[@class='intr-btn_in intr-btn-primary_in'])[2]", driver).click()
            Log.WriteLine("Перейти к следующему шагу")
            //
            val Element9 =    DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action8 =  Actions(driver)
            action8.dragAndDropBy(Element9, 3000, 0).perform()
            //
            Thread.sleep(1500)
            //
            DriverFindElement("(//*[@class='intr-btn_in intr-btn-primary_in'])[1]", driver).click()
            Log.WriteLine("Заполнить")
            Thread.sleep(500)
            //
            DriverFindElement("(//*[@class='intr-btn_in intr-btn-primary_in'])[2]", driver)
            //
            driver.switchTo().frame(0)
            Thread.sleep(500)
            DriverFindElement("//*[@class='mceContentBody  mceCampaignEditorBody']", driver).sendKeys("hcvhfghfdfjghfc")
            Log.WriteLine("Ввод описания")
            //
            driver.switchTo().defaultContent()
            //
            Thread.sleep(1500)
            //
            DriverFindElement("(//*[@class='intr-btn_in intr-btn-primary_in'])[2]", driver).click()
            Log.WriteLine("Перейти к следующему шагу")
            //
            val Element10 =     DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action9 =  Actions(driver)
            action9.dragAndDropBy(Element10, 3000, 0).perform()
            //
            Thread.sleep(1200)
            DriverFindElement("(//*[@class='intr-btn_in intr-btn-primary_in'])[2]", driver).click()
            Log.WriteLine("Добавить ещё вознаграждение")
            //
            DriverFindElement("//*[@class='form-control prj-crt-input js-share-name-limit']", driver).sendKeys("SATANA" + Date())
            Log.WriteLine("Название вознаграждения")
            //
            driver.switchTo().frame(0)
            //
            DriverFindElement("//*[@class='mceContentBody  mceCampaignEditorBody']", driver).sendKeys("666")
            Log.WriteLine("Описание вознаграждения")
            //
            driver.switchTo().defaultContent()
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[1]", driver).sendKeys("Прямо в жерло преисподни")
            Log.WriteLine("Способы получения")
            //
            DriverFindElement("(//*[@name='price'])", driver).clear()
            //
            DriverFindElement("(//*[@name='price'])", driver).sendKeys("100000")
            Log.WriteLine("Цена вознаграждения")
            //
            DriverFindElement("(//*[@class='checkbox'])[1]", driver)
            //
            DriverFindElement("//*[@class='form-control prj-crt-input prj-crt-input__date hasDatepicker']", driver).click()
            Log.WriteLine("Примерная дата доставки")
            //
            DriverFindElement("//*[@class='form-control prj-crt-input prj-crt-input__date hasDatepicker']", driver).sendKeys("31.10.2019")
            //
            Thread.sleep(200)
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")
            //
            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Фото по ссылке")
            //
            DriverFindElement("//*[@name='img_url']", driver).sendKeys("https://99px.ru/sstorage/53/2011/08/mid_18916_3907.jpg")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")
            //
            DriverFindElement("(//*[@class='checkbox'])[1]", driver).click()
            Log.WriteLine("Запрашивать адрес доставки")
            //
            DriverFindElement("(//*[@class='checkbox'])[1]", driver).click()
            Log.WriteLine("Возможен самовывоз")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[4]", driver).sendKeys("Москва")
            Log.WriteLine("Город")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[5]", driver).sendKeys("Ул Малая Большая д 23 кв 666")
            Log.WriteLine("Адрес и условия самовывоза")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[6]", driver).sendKeys("74956661323")
            Log.WriteLine("Телефон")
            //
            DriverFindElement("(//*[@class='checkbox'])[1]", driver).click()
            Log.WriteLine("Задать вопрос покупателю")
            Thread.sleep(2500)
            driver.findElement(By.xpath("//*[@class='select-cont']")).click()
            //
            DriverFindElement("//*[@class='dropdown-menu']//li[1]", driver).click()
            Log.WriteLine("Выбор вопроса")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-lg']", driver).click()
            Log.WriteLine("Добавить изображение")
            //
            Thread.sleep(500)
            DriverFindElement("(//*[@class='intr-btn_in intr-btn-primary_in'])[3]", driver).click()
            Log.WriteLine("Перейти к следующему шагу")
            //
            val Element11 =    DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action10 =  Actions(driver)
            action10.dragAndDropBy(Element11, 3000, 0).perform()
            //
            Thread.sleep(1500)
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("(//*[@class='intr-btn_in intr-btn-primary_in'])[2]", driver).click()
            Log.WriteLine("Перейти к следующему шагу")
            //
            val Element12 =     DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action12 =  Actions(driver)
            action12.dragAndDropBy(Element12, 3000, 0).perform()
            //
            DriverFindElement("//*[@class='intr-btn_in intr-btn-primary_in']", driver).click()
            Log.WriteLine("Отправить на модерацию")
            //
            val Element13 =    DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action13 = Actions(driver)
            action13.dragAndDropBy(Element13,3000,0).perform()
            //
            Thread.sleep(1500)
            Log.Close()
            Stop()
            driver.quit()
            Kyrs2()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }
    }


    fun Kyrs2(){
        Thread.sleep(5000)
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\School")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\School\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()

            fun video() : Boolean
            {
                return try {
                    driver.findElement(By.xpath("//*[contains(text(), 'Повторить видео')]"))
                    true
                } catch(e:Exception) {
                    false
                }

            }




            Log.WriteLine("School-курсы-просмотреть проект" + VersionBrowser(driver))






            DriverNavigate(URL, driver)
            //
            DriverFindElement("//*[@class='header_user-link ng-star-inserted']", driver).click()
            Log.WriteLine("Вход")
            //
            DriverFindElement("//*[@class='login_title']", driver).click()
            Log.WriteLine("Сбросить евент")
            //
            DriverFindElement("//*[@class='form-control login-control_input ng-pristine ng-valid ng-touched']", driver).sendKeys("uz4name@yandex.ru")
            Log.WriteLine("Ввод логина")
            //
            DriverFindElement("(//*[@name='password'])", driver).sendKeys("nekroman911")
            Log.WriteLine("Ввод пароля")
            //
            DriverFindElement("//*[@class='btn btn-nd-primary btn-block login_btn']", driver).click()
            Log.WriteLine("Войти на сайт")
            //
            Thread.sleep(1000)
            DriverNavigate(SchoolPlaneta, driver)
            //
            DriverFindElement("//*[@class='btn school-interactive-btn']", driver).click()
            Log.WriteLine("Пройти курс")
            //
            for (i in 1..3)
            {
                Thread.sleep(1000)
                if (video())
                {
                    driver.findElement(By.xpath("//*[contains(text(), 'Повторить видео')]")).click()
                }
                if (!video())
                {
                    break
                }
            }
            //
            val Element =  DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action =  Actions(driver)
            action.dragAndDropBy(Element, 3000, 0).perform()
            //
            DriverFindElement("//*[@class='intr-btn intr-btn-default btn-block js-next-step']", driver).click()
            Log.WriteLine("нет я знаю что такое краудфандинг")
            //
            for (i in 1..3)
            {
                Thread.sleep(1000)
                if (video())
                {
                    driver.findElement(By.xpath("//*[contains(text(), 'Повторить видео')]")).click()
                }
                if (!video())
                {
                    break
                }
            }
            //
            val Element1 =   DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action11 =  Actions(driver)
            action11.dragAndDropBy(Element1, 3000, 0).perform()
            //
            DriverFindElement("//*[@class='intr-btn_in intr-btn-primary_in']", driver).click()
            Log.WriteLine("Перейти к следующему шагу")
            //
            for (i in 1..3)
            {
                Thread.sleep(1000)
                if (video())
                {
                    driver.findElement(By.xpath("//*[contains(text(), 'Повторить видео')]")).click()
                }
                if (!video())
                {
                    break
                }
            }

            val Element2 =  DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action1 =  Actions(driver)
            action1.dragAndDropBy(Element2, 3000, 0).perform()
            //
            DriverFindElement("//*[@class='intr-btn_in intr-btn-primary_in']", driver).click()
            Log.WriteLine("Перейти к следующему шагу")
            //
            for (i in 1..3)
            {
                Thread.sleep(1000)
                if (video())
                {
                    driver.findElement(By.xpath("//*[contains(text(), 'Повторить видео')]")).click()
                }
                if (!video())
                {
                    break
                }
            }
            //
            val Element3 =    DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action2 =  Actions(driver)
            action2.dragAndDropBy(Element3, 3000, 0).perform()
            //
            DriverFindElement("//*[@class='intr-btn_in intr-btn-primary_in']", driver).click()
            Log.WriteLine("Приступим")
            //
            for (i in 1..3)
            {
                Thread.sleep(1000)
                if (video())
                {
                    driver.findElement(By.xpath("//*[contains(text(), 'Повторить видео')]")).click()
                }
                if (!video())
                {
                    break
                }
            }
            //
            val Element4 =    DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action3 =  Actions(driver)
            action3.dragAndDropBy(Element4, 3000, 0).perform()
            //
            DriverFindElement("(//*[@class='form-control video-form_input'])[1]", driver).sendKeys("Project_Kyrs" + Date())
            Log.WriteLine("Название")
            //
            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")
            //
            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Фото по ссылке")
            //
            DriverFindElement("//*[@name='img_url']", driver).sendKeys("http://i68.ltalk.ru/52/76/397652/32/11490432/0b07a29948f0b71f72df2bae8d4a0bb0.jpeg")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")
            //
            Thread.sleep(3500)
            //
            DriverFindElement("(//*[@class='form-control video-form_input'])[2]", driver).sendKeys("sadasdasd")
            Log.WriteLine("Краткая информация о вашем проекте")
            //
            for (i in 1..3)
            {
                Thread.sleep(1000)
                if (video())
                {
                    driver.findElement(By.xpath("//*[contains(text(), 'Повторить видео')]")).click()
                }
                if (!video())
                {
                    break
                }
            }
            //
            val Element5 =    DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action4 =  Actions(driver)
            action4.dragAndDropBy(Element5, 3000, 0).perform()
            //
            DriverFindElement("//*[@class='intr-btn_in intr-btn-primary_in']", driver).click()
            Log.WriteLine("Перейти к следующему шагу")
            //
            val Element6 =   DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action5 =  Actions(driver)
            action5.dragAndDropBy(Element6, 3000, 0).perform()
            //
            DriverFindElement("//*[@class='intr-btn_in intr-btn-primary_in']", driver).click()
            Log.WriteLine("Перейти к следующему шагу")
            //
            val Element7 =    DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action6 =  Actions(driver)
            action6.dragAndDropBy(Element7, 3000, 0).perform()
            //
            DriverFindElement("(//*[@id='interactive-step-price-desired-target-amount'])[1]", driver).click()
            Log.WriteLine("Клик по полю")
            //
            val elAc =  DriverFindElement("(//*[@id='interactive-step-price-desired-target-amount'])[1]", driver)
            Log.WriteLine("Желаемая сумма")
            //
            Thread.sleep(1000)
            val elActions = Actions(driver)
            elActions.moveToElement(elAc).sendKeys("5000000").build().perform()
            //
//            DriverFindElement("//*[@name='targetAmount']",driver).click()
//            Log.WriteLine("Клик по полю")
//            //
            DriverFindElement("//*[@class='intr-btn_in intr-btn-primary_in']", driver).click()
            Log.WriteLine("Перейти к следующему шагу")
            //
            val Element8 =     DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action7 =  Actions(driver)
            action7.dragAndDropBy(Element8, 4000, 0).perform()
            //
            Thread.sleep(1500)
            //
            DriverFindElement("(//*[@class='intr-btn_in intr-btn-primary_in'])[1]", driver).click()
            Log.WriteLine("Заполнить")
            //
            val el3 = DriverFindElement("//*[@type='file']", driver)
            el3.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            Thread.sleep(2500)
            //
            DriverFindElement("//*[@class='btn btn-primary']",driver).click()
            Log.WriteLine("Сохранить")
            //
            Thread.sleep(1500)
            DriverFindElement("(//*[@class='intr-btn_in intr-btn-primary_in'])[2]", driver).click()
            Log.WriteLine("Перейти к следующему шагу")
            //
            val Element9 =    DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action8 =  Actions(driver)
            action8.dragAndDropBy(Element9, 3000, 0).perform()
            //
            Thread.sleep(1500)
            //
            DriverFindElement("(//*[@class='intr-btn_in intr-btn-primary_in'])[1]", driver).click()
            Log.WriteLine("Заполнить")
            Thread.sleep(500)
            //
            DriverFindElement("(//*[@class='intr-btn_in intr-btn-primary_in'])[2]", driver)
            //
            driver.switchTo().frame(0)
            Thread.sleep(500)
            DriverFindElement("//*[@class='mceContentBody  mceCampaignEditorBody']", driver).sendKeys("hcvhfghfdfjghfc")
            Log.WriteLine("Ввод описания")
            //
            driver.switchTo().defaultContent()
            //
            Thread.sleep(1500)
            //
            DriverFindElement("(//*[@class='intr-btn_in intr-btn-primary_in'])[2]", driver).click()
            Log.WriteLine("Перейти к следующему шагу")
            //
            val Element10 =     DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action9 =  Actions(driver)
            action9.dragAndDropBy(Element10, 3000, 0).perform()
            //
            Thread.sleep(1500)
            //
            DriverFindElement("(//*[@class='intr-btn_in intr-btn-primary_in'])[2]", driver).click()
            Log.WriteLine("Добавить ещё вознаграждение")
            //
            DriverFindElement("//*[@class='form-control prj-crt-input js-share-name-limit']", driver).sendKeys("SATANA" + Date())
            Log.WriteLine("Название вознаграждения")
            //
            driver.switchTo().frame(0)
            //
            DriverFindElement("//*[@class='mceContentBody  mceCampaignEditorBody']", driver).sendKeys("666")
            Log.WriteLine("Описание вознаграждения")
            //
            driver.switchTo().defaultContent()
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[1]", driver).sendKeys("Прямо в жерло преисподни")
            Log.WriteLine("Способы получения")
            //
            DriverFindElement("(//*[@name='price'])", driver).clear()
            //
            DriverFindElement("(//*[@name='price'])", driver).sendKeys("100000")
            Log.WriteLine("Цена вознаграждения")
            //
            DriverFindElement("(//*[@class='checkbox'])[1]", driver)
            //
            DriverFindElement("//*[@class='form-control prj-crt-input prj-crt-input__date hasDatepicker']", driver).click()
            Log.WriteLine("Примерная дата доставки")
            //
            DriverFindElement("//*[@class='form-control prj-crt-input prj-crt-input__date hasDatepicker']", driver).sendKeys("31.10.2019")
            //
            Thread.sleep(200)
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")
            //
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")
            //
            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Фото по ссылке")
            //
            DriverFindElement("//*[@name='img_url']", driver).sendKeys("https://99px.ru/sstorage/53/2011/08/mid_18916_3907.jpg")
            Log.WriteLine("Ввод ссылки")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")
            //
            DriverFindElement("(//*[@class='checkbox'])[1]", driver).click()
            Log.WriteLine("Запрашивать адрес доставки")
            //
            DriverFindElement("(//*[@class='checkbox'])[1]", driver).click()
            Log.WriteLine("Возможен самовывоз")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[4]", driver).sendKeys("Москва")
            Log.WriteLine("Город")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[5]", driver).sendKeys("Ул Малая Большая д 23 кв 666")
            Log.WriteLine("Адрес и условия самовывоза")
            //
            DriverFindElement("(//*[@class='form-control prj-crt-input'])[6]", driver).sendKeys("74956661323")
            Log.WriteLine("Телефон")
            //
            DriverFindElement("(//*[@class='checkbox'])[1]", driver).click()
            Log.WriteLine("Задать вопрос покупателю")
            Thread.sleep(2500)
            driver.findElement(By.xpath("//*[@class='select-cont']")).click()
            //
            DriverFindElement("//*[@class='dropdown-menu']//li[1]", driver).click()
            Log.WriteLine("Выбор вопроса")
            //
            DriverFindElement("//*[@class='btn btn-primary btn-lg']", driver).click()
            Log.WriteLine("Добавить изображение")
            //
            Thread.sleep(500)
            DriverFindElement("(//*[@class='intr-btn_in intr-btn-primary_in'])[3]", driver).click()
            Log.WriteLine("Перейти к следующему шагу")
            //
            val Element11 =    DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action10 =  Actions(driver)
            action10.dragAndDropBy(Element11, 3000, 0).perform()
            //
            Thread.sleep(1500)
            //
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")
            //
            DriverFindElement("(//*[@class='intr-btn_in intr-btn-primary_in'])[2]", driver).click()
            Log.WriteLine("Перейти к следующему шагу")
            //
            val Element12 =     DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action12 =  Actions(driver)
            action12.dragAndDropBy(Element12, 3000, 0).perform()
            //
            DriverFindElement("//*[@class='intr-btn_in intr-btn-default_in']", driver).click()
            Log.WriteLine("Посмотреть проект")
            //
            val Element13 =    DriverFindElement("//*[@class='vjs-progress-holder vjs-slider vjs-slider-horizontal']", driver)
            val action13 = Actions(driver)
            action13.dragAndDropBy(Element13,3000,0).perform()
            //
            Thread.sleep(1500)
            Log.Close()
            Stop()
            driver.quit()

        }catch (e:Exception){
            println(e.printStackTrace())
        }finally {
            Stop1()
        }


    }



}