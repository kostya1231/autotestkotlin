
import org.junit.Test
import org.junit.experimental.ParallelComputer
import org.junit.runner.JUnitCore

class B_StartСonsistentTest {

    @Test
    fun Start() {
        val cls = arrayOf(///Последовательные тесты
                CreateProjectCarusel::class.java,
                A_ProjectCompletion::class.java,
                B_CreateProjectUserCharity::class.java,
                C_CreateProjectCharityPage::class.java,
                D_KampusProjectCompletion::class.java,
                `E_ProjectSuccessful+ProjectNoSuccessful`::class.java,
                 SchoolProjectKyrs::class.java)
        JUnitCore.runClasses(ParallelComputer.methods(), *cls)



        val cls2 = arrayOf(//Паралельные
                 A_Charity::class.java,
                 A_VipClub::class.java,
                `A_BibliorodinaSertificat+KampusAvtorizationMenu`::class.java,
                 A_TestElement::class.java,
                 A_UserLK::class.java,
                `B_UserOplataShop+UserBalance+UserMyPage`::class.java,
                E_UserSettings::class.java,
                A_Refund::class.java)
        JUnitCore.runClasses(ParallelComputer.methods(), *cls2)
    }
}