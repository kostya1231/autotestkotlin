
import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.WebDriver
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

class ProjectCategory : Setup() {

    @Test
    fun ProjectCategory(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()



            Log.WriteLine("User-Админка проверка проекта на категорию" + VersionBrowser(driver))






            Avtorization_Adminka(driver,Log)

            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")

            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")

            DriverFindElement("(//*[@class='menu-item-text'])[2]", driver).click()
            Log.WriteLine("Модерация")

            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Пользователи")

            DriverFindElement("//*[@class='form-control']", driver).sendKeys("avtor2.avtorproject@yandex.ru")
            Log.WriteLine("Ввод в строку поиска имя юзера")

            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Поиск")

            DriverFindElement("//*[@class='btn btn-primary btn-outline']", driver).click()
            Log.WriteLine("Редактировать")

            DriverFindElement("//*[@class='btn btn-primary btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Страница пользователя")

            val tabs = ArrayList(driver.windowHandles)
            driver.switchTo().window(tabs[1])

            try{
                DriverFindElement("//*[@class='more-link']", driver).click()
                Log.WriteLine("Раскрыть список проектов")
            }catch (s:Exception){

            }
            DriverFindElement("(//*[@class='n-own-project_cover n-own-project_i__not_started'])[1]", driver).click()
            Log.WriteLine("Выбор проекта который черновик")

            DriverFindElement("//*[@class='btn btn-nd-success js-start-campaign']", driver).click()
            Log.WriteLine("Запустить")

            DriverFindElement("//*[contains(text(), 'Проект не относится ни к одной категории. Запуск невозможен')]", driver)
            Log.WriteLine("Проект не относится ни к одной категории. Запуск невозможен")

            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("ОК")

            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }


    }
}