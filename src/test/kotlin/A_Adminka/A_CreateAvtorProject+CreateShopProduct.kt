import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.interactions.Actions
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

class `A_CreateAvtorProject+CreateShopProduct` : Setup() {

    @Test
    fun CreateAvtorProject(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            var world = 10
            val r = Random()
            var number = StringBuilder()
            for (i in 0 until world) {
                val tmp = r.nextInt(10)
                number.append(tmp)
            }

            var world1 = 9
            val r1 = Random()
            var number1 = StringBuilder()
            for (i in 0 until world1) {
                val tmp = r1.nextInt(9)
                number1.append(tmp)
            }




            Log.WriteLine("Создание проекта от автора + модерация" + VersionBrowser(driver))






            AvtorProject(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[1]", driver).click()
            //
            fun find() : Boolean
            {
                return try {
                    DriverFindElement("(//*[@class='profile-nav_link'])[1]", driver)
                    true
                } catch(e:Exception) {
                    false
                }

            }
            if (find())
            {
                try
                {
                    DriverFindElement("(//*[@class='profile-nav_link'])[1]", driver).click()

                }
                catch(e:Exception)
                {

                }
            }

            while (true)
            {
                try
                {
                    Thread.sleep(500)
                    driver.findElement(By.xpath("//*[@class='profile-project-info_delete-link']")).click()
                    Log.WriteLine("Удалить черновик")
                    Thread.sleep(500)
                    driver.findElement(By.xpath("//*[@class='btn btn-primary']")).click()
                    Log.WriteLine("Да")
                    Thread.sleep(200)
                }
                catch (e:Exception)
                {
                    break
                }
            }


            try
            {
                driver.findElement(By.xpath("//*[contains(text(), 'создать')]")).click()
                Log.WriteLine("Создать")
            }
            catch (e:Exception)
            {
                driver.findElement(By.xpath("//*[@class='header_create-link']")).click()
                Log.WriteLine("Создать Проект")
            }

            DriverFindElement("//*[@class='checkbox']", driver).click()
            Log.WriteLine("Я Согласен на Условия")

            DriverFindElement("//*[@class='btn btn-primary frc-i-accept-btn create-campaign']", driver).click()
            Log.WriteLine("Создать проект")

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[1]", driver).sendKeys("Project_User" + Date())
            Log.WriteLine("Название проекта")

            DriverFindElement("(//*[@class='form-control control-xlg borderless'])", driver).sendKeys("web" + number.toString())
            Log.WriteLine("Красивый адрес проекта")

            val el = DriverFindElement("//*[@type='file']", driver)
            el.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")

            try{
                DriverFindElement("//*[@class='btn btn-primary']",driver).click()
                Log.WriteLine("Сохранить")
            }catch (p:Exception){
                Log.WriteLine("Сохранить не выскочило т к размер фото меньше")
            }

            Thread.sleep(2800)
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")

            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Фото по ссылке")

            DriverFindElement("//*[@name='img_url']", driver).sendKeys("http://www.animacity.ru/sites/default/files/blog/82069/14624.jpg")
            Log.WriteLine("Ввод ссылки")

            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")

            try{
                DriverFindElement("//*[@class='btn btn-primary']",driver).click()
                Log.WriteLine("Сохранить")
            }catch (p:Exception){
                Log.WriteLine("Сохранить не выскочило т к размер фото меньше")
            }

            Thread.sleep(1800)

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[2]", driver).sendKeys("cxcdsfdsfqq")
            Log.WriteLine("Коротко о проекте")

            DriverFindElement("//*[@class='icon-select']", driver).click()
            Log.WriteLine("Страна реализции")

            DriverFindElement("//*[@class='dropdown-menu']/li[2]", driver).click()
            Log.WriteLine("Выбор страны")

            DriverFindElement("//*[@class='form-control control-xlg prj-crt-input ac_input']", driver).sendKeys("Москва")
            Log.WriteLine("Регион реализации")

            DriverFindElement("//*[@class='ac_results']//li[1]", driver).click()
            Log.WriteLine("Москва")

            DriverFindElement("//*[@name='cityNameRus']", driver).sendKeys("Алабино")
            Log.WriteLine("Город реализации")

            DriverFindElement("//*[@class='ac_results']//*[@class='ac_even ac_over']", driver).click()

            DriverFindElement("//*[@data-parse='number']", driver).clear()

            DriverFindElement("//*[@data-parse='number']", driver).sendKeys("500000")
            Log.WriteLine("Финансовая цель")

            DriverFindElement("//*[@class='project-create_col-val_col']", driver).click()
            Log.WriteLine("Срок окончания проекта")

            DriverFindElement("(//*[@data-parse='number'])[2]", driver).click()

            DriverFindElement("//*[@class='ui-datepicker-next ui-corner-all']", driver).click()
            Log.WriteLine("Следующий месяц")

            DriverFindElement("//*[@class='ui-datepicker-calendar']//tr[4]//td[5]", driver).click()
            Log.WriteLine("Выбор даты")

            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")

            Thread.sleep(800)

            DriverFindElement("(//*[@class='project-create_tab_link'])[2]", driver).click()
            Log.WriteLine("Детали")

            val el3 = DriverFindElement("//*[@type='file']", driver)
            el3.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")

            try{
                DriverFindElement("//*[@class='btn btn-primary']",driver).click()
                Log.WriteLine("Сохранить")
            }catch (p:Exception){
                Log.WriteLine("Сохранить не выскочило т к размер фото меньше")
            }

            Thread.sleep(1800)
            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")

            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Видео с Ютуба")

            DriverFindElement("//*[@name='img_url']", driver).sendKeys("https://www.youtube.com/watch?v=VVtZVabutYM")
            Log.WriteLine("Ввод ссылки")

            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")

            DriverFindElement("(//*[@class='mceIcon mce_planetaphoto'])", driver).click()
            Log.WriteLine("Загрузить картинку в поле text-area")

            DriverFindElement("//*[@class='link']", driver).click()
            Log.WriteLine("Загрузить картинку по ссылке")

            DriverFindElement("//*[@name='img_url']", driver).sendKeys("http://www.animacity.ru/sites/default/files/blog/82069/14624.jpg")
            Log.WriteLine("Ввод ссылки")

            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")

            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")

            DriverFindElement("(//*[@class='project-create_tab_link'])[3]", driver).click()
            Log.WriteLine("Вознаграждение")

            DriverFindElement("//*[@class='form-control prj-crt-input js-share-name-limit']", driver).sendKeys("SATANA")
            Log.WriteLine("Название вознаграждения")

            driver.switchTo().frame(0)

            DriverFindElement("//*[@class='mceContentBody  mceCampaignEditorBody']", driver).sendKeys("666")
            Log.WriteLine("Описание вознаграждения")

            driver.switchTo().defaultContent()

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[1]", driver).sendKeys("Прямо в жерло преисподни")
            Log.WriteLine("Способы получения")

            DriverFindElement("(//*[@name='price'])", driver).clear()

            DriverFindElement("(//*[@name='price'])", driver).sendKeys("100000")
            Log.WriteLine("Цена вознаграждения")

            DriverFindElement("//*[@class='checkbox active']", driver).click()
            Log.WriteLine("Отключить ЧекБокс")

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[3]", driver).clear()

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[3]", driver).sendKeys("100")

            DriverFindElement("//*[@class='form-control prj-crt-input prj-crt-input__date hasDatepicker']", driver).click()
            Log.WriteLine("Примерная дата доставки")

            DriverFindElement("//*[@class='ui-datepicker-calendar']//tr[4]//td[5]", driver).click()
            Log.WriteLine("Выбор даты")

            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")

            DriverFindElement("(//*[@class='project-create_media_link-text'])[2]", driver).click()
            Log.WriteLine("Удалить фото")

            DriverFindElement("(//*[@class='project-create_upload_text'])[2]", driver).click()
            Log.WriteLine("Фото по ссылке")

            DriverFindElement("//*[@name='img_url']", driver).sendKeys("http://www.animacity.ru/sites/default/files/blog/82069/14624.jpg")
            Log.WriteLine("Ввод ссылки")

            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Готово")

            DriverFindElement("(//*[@class='checkbox'])[1]", driver).click()
            Log.WriteLine("Запрашивать адрес доставки")

            Thread.sleep(1500)
            //
            val elAction =  DriverFindElement("(//*[@class='checkbox-row flat-ui-control'])[2]", driver)
            val action = Actions(driver)
            action.moveToElement(elAction).click().build().perform()

            val elActions2 = DriverFindElement("(//*[@class='checkbox-row flat-ui-control'])[3]", driver)
            val action2 = Actions(driver)
            action2.moveToElement(elActions2).click().build().perform()
            Log.WriteLine("Возможен самовывоз")

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[4]", driver).sendKeys("Москва")
            Log.WriteLine("Город")

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[5]", driver).sendKeys("Ул Малая Большая д 23 кв 666")
            Log.WriteLine("Адрес и условия самовывоза")

            DriverFindElement("(//*[@class='form-control prj-crt-input'])[6]", driver).sendKeys("74956661323")
            Log.WriteLine("Телефон")

            val elActions3 = DriverFindElement("(//*[@class='checkbox'])[1]", driver)
            val action3 = Actions(driver)
            action3.moveToElement(elActions3).click().build().perform()
            Log.WriteLine("Задать вопрос покупателю")
            Thread.sleep(2500)
            driver.findElement(By.xpath("//*[@class='select-cont']")).click()

            DriverFindElement("//*[@class='dropdown-menu']//li[1]", driver).click()
            Log.WriteLine("Выбор вопроса")

            DriverFindElement("//*[@class='btn btn-primary btn-lg']", driver).click()
            Log.WriteLine("Добавить изображение")

            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")

            Thread.sleep(500)

            DriverFindElement("(//*[@class='project-create_tab_link'])[4]", driver).click()
            Log.WriteLine("Контр-Агент")

            Thread.sleep(1000)

            DriverFindElement("(//*[@class='select-cont'])[1]", driver).click()
            Log.WriteLine("Выбор нового контрАгента")

            DriverFindElement("(//*[@class='dropdown-menu'])[1]//li//*[contains(text(), '                      Новый контрагент                 ')]", driver).click()
            Log.WriteLine("Выбор нового контр агента")

            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[1]", driver).sendKeys("фывфывфывфывфывфывфывфы")
            Log.WriteLine("ФИО физического лица ИП или наименование организации для юридического лица")

            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[2]", driver).sendKeys("И.И ИВанов")
            Log.WriteLine("Инициалы и фамилия для подписи в договоре")

            DriverFindElement("//*[@class='form-control prj-crt-input prj-crt-input__date hasDatepicker']", driver).sendKeys("20.9.1990")
            Log.WriteLine("Дата рождения")

            val ac = Actions(driver)
            ac.sendKeys(Keys.ESCAPE).build().perform()


            DriverFindElement("//*[@class='form-control control-xlg prj-crt-input js-city-name-input ac_input']", driver).sendKeys("Москва")
            Log.WriteLine("Город")
            //
            val ac2 = Actions(driver)
            ac2.sendKeys(Keys.ENTER).build().perform()
            //
            Log.WriteLine("Москва")

            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[3]", driver).sendKeys("84955555535")
            Log.WriteLine("Телефон")

            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[4]", driver).sendKeys(number.toString())
            Log.WriteLine("ИНН")

            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[5]", driver).sendKeys(number.toString())
            Log.WriteLine("Номер паспорта")

            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[6]", driver).sendKeys("Отделением ОУФМС по бла бла бла")
            Log.WriteLine("Кем выдан")

            DriverFindElement("(//*[@class='form-control prj-crt-input prj-crt-input__date hasDatepicker'])[2]", driver).sendKeys("20.03.2007")
            Log.WriteLine("Когда выдан паспорт")

            val ac3 = Actions(driver)
            ac3.sendKeys(Keys.ENTER).build().perform()

            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[7]", driver).sendKeys("770-060")
            Log.WriteLine("Код подразделения")

            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[6]", driver).click()


            DriverFindElement("(//*[@class='btn upload-file mrg-t-5'])", driver).click()
            Log.WriteLine("Необходимые документы для оформления договора - Загрузить")


            val el4 = DriverFindElement("//*[@type='file']", driver)
            el4.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Загрузить Фото")

            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("OK")

            DriverFindElement("//*[@class='form-control prj-crt-input js-save-model']", driver).sendKeys("Ул большая нищебродская дом картонная коробка")
            Log.WriteLine("Адрес регистрации (как в паспорте)")

            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[8]", driver).sendKeys(number.toString() + number.toString())
            Log.WriteLine("Расчётный счет")

            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[9]", driver).sendKeys("Банк Анимэ и кавая")
            Log.WriteLine("Банк получателя")

            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[10]", driver).sendKeys(number.toString() + number.toString())
            Log.WriteLine("Корреспондентский счет")

            DriverFindElement("(//*[@class='form-control control-xlg prj-crt-input js-save-model'])[11]", driver).sendKeys(number1.toString())
            Log.WriteLine("БИК")

            DriverFindElement("(//*[@class='form-control prj-crt-input js-save-model'])[2]", driver).sendKeys("Ашкараагалдыыыыыыыыыыыыыыыыыыы")
            Log.WriteLine("Прочие данные")

            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")

            DriverFindElement("//*[@class='btn project-create_save']", driver).click()
            Log.WriteLine("Сохранить")
            Thread.sleep(1800)
            DriverFindElement("//*[@class='checkbox js-agree-personal-data-processing']", driver).click()
            Log.WriteLine("Я согласен на сбор, хранение и обработку моих персональных данных")

            DriverFindElement("//*[@class='btn btn-primary project-create_next']", driver).click()
            Log.WriteLine("Отправить на модерацию")

            Thread.sleep(1500)
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }

    @Test
    fun  ShopProduct(){
        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()






            Log.WriteLine("Админка-Товары магазина Создать товар - Физический" + VersionBrowser(driver))



            Avtorization_Adminka(driver,Log)
            //
            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")
            //
            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")
            //
            DriverFindElement("(//*[@class='fa arrow'])[3]", driver).click()
            Log.WriteLine("Магазин")
            //
            DriverFindElement("//*[@class='nav nav-second-level collapse in'][1]//li[2]", driver).click()
            Log.WriteLine("Товары магазина")
            //
            DriverFindElement("//*[@class='btn btn-success btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Создать товар")
            //
            DriverFindElement("//*[@id='name']", driver).sendKeys("Товар_Товарович")
            Log.WriteLine("Название товара")
            //
            DriverFindElement("//*[@alt='Загрузить фото']", driver).click()
            Log.WriteLine("Загрузить фото")
            //
            val el2 = DriverFindElement("//*[@type='file']", driver)
            el2.sendKeys("$userprofile\\IdeaProjects\\Planeta_ru\\src\\test\\Image\\" + GetRandomNumberString() + ".jpg")
            Log.WriteLine("Фото")
            //
            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("OK")
            //
            DriverFindElement("//*[@name='referrerWebAlias']", driver).sendKeys("726665")
            Log.WriteLine("Автор товара")
            //
            DriverFindElement("//*[@name='donateId']", driver).clear()
            //
            DriverFindElement("//*[@name='donateId']", driver).sendKeys("10")
            Log.WriteLine("Товар поддержки автора")
            //
            DriverFindElement("//*[@id='js-human-date']", driver).click()
            Log.WriteLine("Выбор даты")
            //
            DriverFindElement("(//*[@class='table-condensed'])[1]//tbody//tr[1]//td[4]", driver).click()
            //
            for (i in 1..40)
            {
                DriverFindElement("(//*[@class='select2-selection__rendered'])[1]", driver).click()
                //
                DriverFindElement("(//*[@class='select2-results__options'])[1]//li[" + i.toString() + "]", driver).click()
            }
            //
            driver.switchTo().frame(0)
            //
            DriverFindElement("//*[@id='tinymce']", driver).sendKeys("666")
            Log.WriteLine("Описание товара")
            //
            driver.switchTo().defaultContent()
            //
            DriverFindElement("//*[@name='textForMail']", driver).sendKeys("qwerty")
            Log.WriteLine("Доп описания товара")
            //
            DriverFindElement("//*[@id='price']", driver).clear()
            //
            DriverFindElement("//*[@id='price']", driver).sendKeys("1000")
            Log.WriteLine("Цена товара")
            //
            DriverFindElement("//*[@name='totalQuantity']", driver).clear()
            //
            DriverFindElement("//*[@name='totalQuantity']", driver).sendKeys("1000")
            Log.WriteLine("Количество товара")
            //
            DriverFindElement("//*[@id='cashAvailable']", driver).click()
            Log.WriteLine("Оплата наличкой")
            //
            DriverFindElement("//*[@class='btn submit-button btn-primary']", driver).click()
            Log.WriteLine("Готово")
            //
            Log.Close()
            Stop()
            driver.quit()
        }
        catch (e:Exception){
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }
    }



}