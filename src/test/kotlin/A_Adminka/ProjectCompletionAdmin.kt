import Z_Settings.Loger
import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.interactions.Actions
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

class ProjectCompletionAdmin : Setup() {

    @Test
    fun ProjectCompletionAdmin(){

        try {
            TestName = GetCurrentMethod()
            val folder = Paths.get("C:\\Log Test\\Adminka")
            Files.createDirectories(folder)
            val currentFile = "C:\\Log Test\\Adminka\\$TestName"
            val Log = Loger(currentFile, ".txt")
            val driver: WebDriver = Driver()


            Log.WriteLine("Подтверждение админом модерации - запуск проекта" + VersionBrowser(driver))






            Avtorization_Adminka(driver,Log)

            DriverFindElement("//*[@class='h-user_img']", driver).click()
            Log.WriteLine("Открыть меню")

            DriverFindElement("(//*[@class='h-user-menu_ico'])[6]", driver).click()
            Log.WriteLine("Администрирование")

            DriverFindElement("(//*[@class='menu-item-text'])[2]", driver).click()
            Log.WriteLine("Модерация")

            DriverFindElement("//*[@class='nav nav-second-level collapse in']/li[1]", driver).click()
            Log.WriteLine("Пользователи")

            DriverFindElement("//*[@class='form-control']", driver).sendKeys("avtor2.avtorproject@yandex.ru")
            Log.WriteLine("Ввод в строку поиска имя юзера")

            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("Поиск")

            DriverFindElement("//*[@class='btn btn-primary btn-outline']", driver).click()
            Log.WriteLine("Редактировать")

            DriverFindElement("//*[@class='btn btn-primary btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Страница пользователя")

            val tabs = ArrayList(driver.windowHandles)

            driver.switchTo().window(tabs[1])

            DriverFindElement("//*[contains(text(), 'Project_User')]", driver).click()
            Log.WriteLine("Выбор проекта который на модерации")

            DriverFindElement("(//*[@class='project-author-controls_name'])[2]", driver).click()
            Log.WriteLine("Редактировать")

            DriverFindElement("(//*[@class='select2-container select2-container-multi select-tags js-select2'])", driver).click()

            DriverFindElement("//*[@class='select2-results']//li[1]", driver).click()
            Log.WriteLine("Выбор категории")

            DriverFindElement("(//*[@class='btn project-create_save'])[1]", driver).click()
            Log.WriteLine("Сохранить")


            driver.switchTo().window(tabs[0])


            DriverFindElement("//*[@class='btn btn-primary btn-circle btn-outline btn-lg']", driver).click()
            Log.WriteLine("Страница пользователя")

            val tabs2 = ArrayList(driver.windowHandles)
            driver.switchTo().window(tabs2[2])


            DriverFindElement("//*[contains(text(), 'Project_User')]", driver).click()
            Log.WriteLine("Выбор проекта который на модерации")

            DriverFindElement("//*[@class='btn btn-nd-success js-start-campaign']", driver).click()
            Log.WriteLine("Запустить")

            DriverFindElement("//*[@class='btn btn-primary']", driver).click()
            Log.WriteLine("ОК")

          //  DriverFindElementCss(".btn-nd-danger", driver)
            //
            DriverNavigate("https://yandex.ru", driver)
            //
            DriverFindElement("(//*[@class='home-link home-link_black_yes'])[1]", driver).click()
            Log.WriteLine("Вход на яндекс")
            //
            fun FindWindowYandex() : Boolean
            {
                return try {
                    Thread.sleep(1500)
                    driver.findElement(By.xpath("//*[@class='passp-page-overlay']"))
                    true
                } catch(e:Exception) {
                    false
                }
            }

            if (!FindWindowYandex())
            {
                DriverFindElement("//*[@name='login']", driver).sendKeys("avtor2.avtorproject")
                Log.WriteLine("Ввод Логина")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passport-Button-Text']", driver).click()
                Log.WriteLine("Войти")
                //
                try{
                    val ac = Actions(driver)
                    ac.sendKeys(Keys.ESCAPE).build().perform()
                }
                catch(g:Exception){

                }
                //
                DriverFindElement("(//*[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_sender js-message-snippet-sender'])[1]", driver).click()
                Log.WriteLine("Выбор письма")
                //
                try
                {
                    DriverFindElement("(//*[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_sender js-message-snippet-sender'])[1]", driver).click()
                    Log.WriteLine("Выбор письма о запущенном проекте")
                    //
                    DriverFindElement("(//*[@rel='noopener noreferrer'])[2]", driver).click()
                    Log.WriteLine("перейти к проеткту")
                    //
                }
                catch(e:Exception)
                {

                }
            }

            if (FindWindowYandex())
            {

                DriverFindElement("//*[@name='login']", driver).sendKeys("avtor2.avtorproject")
                Log.WriteLine("Ввод Логина")
                //
                DriverFindElement("//*[@class='passp-login-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                //
                DriverFindElement("//*[@name='passwd']", driver).sendKeys("nekroman911")
                Log.WriteLine("Ввод пароля")
                //
                DriverFindElement("//*[@class='passp-password-form']//button[1]", driver).click()
                Log.WriteLine("Войти")
                try{
                    Thread.sleep(500)
                    driver.switchTo().alert().accept()
                }
                catch(g:Exception){

                }
                //
                try
                {
                    DriverFindElement("(//*[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_sender js-message-snippet-sender'])[1]", driver).click()
                    Log.WriteLine("Выбор письма о запущенном проекте")
                    //
                    DriverFindElement("(//*[@rel='noopener noreferrer'])[2]", driver).click()
                    Log.WriteLine("перейти к проеткту")
                    //
                }
                catch(e:Exception)
                {

                }
                //
            }
            Thread.sleep(1000)
            //
            Log.Close()
            Stop()
            driver.quit()
        }catch (e:Exception) {
            println(e.printStackTrace())
        }
        finally {
            Stop1()
        }



    }

}